package com.tp.tpigateway;

import javax.persistence.EntityManager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tp.tpigateway.common.config.PtoponlineDbConfig;
import com.tp.tpigateway.common.postgresql.model.OcsPersonDialog;
import com.tp.tpigateway.common.postgresql.repository.OcsPersonDialogRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PtoponlineDbConfig.class})
public class Demo {

	@Autowired
	OcsPersonDialogRepository repository;
	
	@Autowired
	EntityManager em;
	
	@Test
	@Transactional("ptoponlineTransactionManager")
	public void test() {
		System.err.println(repository.count());
		System.err.println(em.find(OcsPersonDialog.class, 1).getSeqno());
	}

}
