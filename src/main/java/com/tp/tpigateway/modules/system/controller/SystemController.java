package com.tp.tpigateway.modules.system.controller;

import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.modules.system.service.SystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("system")
public class SystemController {

    private static Logger log = LoggerFactory.getLogger(SystemController.class);

    @Autowired
    SystemService systemService;

    @RequestMapping(value = "/updateFallBackScore", method = {RequestMethod.POST, RequestMethod.GET})
    public TPIGatewayResult updateFallBackScore() {
        return systemService.updateFallBackScore();
    }

    @RequestMapping(value = "/clearSysConfigCache", method = {RequestMethod.POST, RequestMethod.GET})
    public TPIGatewayResult clearSysConfigCache() {
        return systemService.clearSysConfigCache();
    }

    @RequestMapping(value = "/updateAutoComplete", method = {RequestMethod.POST, RequestMethod.GET})
    public TPIGatewayResult updateAutoComplete() {
        return systemService.updateAutoComplete();
    }

    @RequestMapping(value = "/updateBotTpiReplayText", method = {RequestMethod.POST, RequestMethod.GET})
    public TPIGatewayResult updateBotTpiReplayText() {
        return systemService.updateBotTpiReplayText();
    }

}
