package com.tp.tpigateway.modules.system.model;

import com.tp.tpigateway.common.mongo.entity.Nodemessages;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class ChatLogData {

    private Long lastChatDateTime;

    private List<Nodemessages.Payload> chatLog;

    public Long getLastChatDateTime() {
        return lastChatDateTime;
    }

    public void setLastChatDateTime(Long lastChatDateTime) {
        this.lastChatDateTime = lastChatDateTime;
    }

    public List<Nodemessages.Payload> getChatLog() {
        return chatLog;
    }

    public void setChatLog(List<Nodemessages.Payload> chatLog) {
        this.chatLog = chatLog;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("lastChatDateTime", lastChatDateTime)
                .append("chatLog", chatLog)
                .toString();
    }
}