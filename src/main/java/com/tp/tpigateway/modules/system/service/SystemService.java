package com.tp.tpigateway.modules.system.service;

import com.tp.common.model.TPIGatewayResult;

public interface SystemService {

    TPIGatewayResult updateFallBackScore();

    TPIGatewayResult clearSysConfigCache();

    TPIGatewayResult updateAutoComplete();

    TPIGatewayResult updateBotTpiReplayText();
}
