package com.tp.tpigateway.modules.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.tp.tpigateway.common.mongo.entity.Messagesessions;
import com.tp.tpigateway.common.mongo.entity.Nodemessages;
import com.tp.tpigateway.common.mongo.repository.MessagesessionsRepository;
import com.tp.tpigateway.common.mongo.repository.NodemessagesRepository;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.system.model.ChatLogData;
import com.tp.tpigateway.modules.system.service.ChatLogService;
import org.apache.commons.collections4.CollectionUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ChatLogServiceImpl implements ChatLogService {

    final Logger log = LoggerFactory.getLogger(ChatLogServiceImpl.class);

    @Autowired
    private NodemessagesRepository nodemessagesRepository;

    @Autowired
    private MessagesessionsRepository messagesessionsRepository;

    @Override
    public ChatLogData findChatLogBySessionIdAndStartDateTime(String sessionId, Long startDateTime) {
        ChatLogData chatLogData = new ChatLogData();

        List<Nodemessages.Payload> chatLogs = new ArrayList<>();

        List<Nodemessages> nodeMessages = findNodemessagesBySessionIdAndStartDateTimeOrderByRegisteredDateTimeAsc(sessionId, startDateTime);

        if (CollectionUtils.isNotEmpty(nodeMessages)) {
            for (Nodemessages nodeMessage : nodeMessages) {
                Nodemessages.Info info = nodeMessage.getInfo();
                if (info != null) {
                    Nodemessages.Payload payload = null;
                    try {
                        //payload = JSONUtils.obj2pojo(info.getPayload(), Nodemessages.Payload.class);
                        payload = JSON.parseObject(JSON.toJSONString(info.getPayload()), Nodemessages.Payload.class);
                    } catch (Exception e) {
                        // 轉不了的就當成垃圾資料
                        log.error(e.getMessage(), e);
                    }
                    if (payload != null) {
                        chatLogs.add(payload);
                        Date registeredDateTime = nodeMessage.getRegisteredDateTime();
                        if (registeredDateTime != null)
                            chatLogData.setLastChatDateTime(registeredDateTime.getTime());
                    }
                }
            }
        }

        chatLogData.setChatLog(chatLogs);

        return chatLogData;
    }

    private List<Nodemessages> findNodemessagesBySessionIdAndStartDateTimeOrderByRegisteredDateTimeAsc(String sessionId,
                                                                                                       Long startDateTime) {
        String appId = PropUtils.getAppId();

        List<Messagesessions> messagesessions = messagesessionsRepository
                .findBySessionIdAndAppIdOrderByStartDateAsc(sessionId, appId);

        if (CollectionUtils.isEmpty(messagesessions)) {
            log.debug("messagesessions is empty.");
            return null;
        }
        log.debug("messagesessions size=" + messagesessions.size());

        List<Nodemessages> nodeMessagesResult = new ArrayList<>();

        for (Messagesessions messagesession : messagesessions) {
            ObjectId _id = messagesession.getId();
            log.debug("ObjectId=" + _id);
            List<Nodemessages> nodeMessages = null;

            if (startDateTime != null) {
                nodeMessages = nodemessagesRepository.findBySessionAndRegisteredDateTimeGreaterThanEqualOrderByRegisteredDateTimeAsc(_id, new Date(startDateTime));
            } else {
                nodeMessages = nodemessagesRepository.findBySessionOrderByRegisteredDateTimeAsc(_id);
            }
            log.debug("nodeMessages=" + (nodeMessages == null ? 0 : nodeMessages.size()));
            if (CollectionUtils.isNotEmpty(nodeMessages)) {
                nodeMessagesResult.addAll(nodeMessages);
            }
        }

        return nodeMessagesResult;
    }
}
