package com.tp.tpigateway.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.util.ReadExcelUtils;
import com.tp.tpigateway.common.constant.CacheConst;
import com.tp.tpigateway.common.mybatis.entity.AllAutocomplete;
import com.tp.tpigateway.common.mybatis.entity.BotTpiReplyText;
import com.tp.tpigateway.common.mybatis.entity.SysConfig;
import com.tp.tpigateway.common.mybatis.service.IAllAutocompleteService;
import com.tp.tpigateway.common.mybatis.service.IBotTpiReplyTextService;
import com.tp.tpigateway.common.mybatis.service.ISysConfigService;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.system.service.SystemService;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SystemServiceImpl implements SystemService {

    private static Logger log = LoggerFactory.getLogger(SystemServiceImpl.class);

    @Autowired
    ISysConfigService sysConfigService;

    @Autowired
    CacheManager cacheManager;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    IAllAutocompleteService iAllAutocompleteService;

    @Autowired
    private IBotTpiReplyTextService botTpiReplyTextService;

    @Autowired
    Environment env;

    private static final String[] KEYS = {
            //證券
            //總機
            "cathaysec.first.nlu.url@cathaysec.firstNLU.fallbackScore",
            //服務台
            "cathaysec.first.nlu.support.url@cathaysec.firstNLUSupport.fallbackScore",
            //yes/no
            "cathaysec.yes.no.nlu.url@cathaysec.yes.no.nlu.nluFallbackScore",
            //密碼
            "cathaysec.pwd.operation.nlu.url@cathaysec.pwd.operation.nluFallbackScore",
            //定期定額
            "cathaysec.sip.operation.nlu.url@cathaysec.sip.operation.nluFallbackScore",
            //股票抽籤
            "cathaysec.lottery.operation.nlu.url@cathaysec.lottery.operation.nluFallbackScore",
            "cathaysec.lottery.search.nlu.url@cathaysec.lottery.search.nluFallbackScore",
            //報價
            "cathaysec.price.operation.nlu.url@cathaysec.price.operation.nluFallbackScore",
            //個人投資
            "cathaysec.investment.operation.nlu.url@cathaysec.investment.operation.nluFallbackScore",
            "cathaysec.investment.search.nlu.url@cathaysec.investment.search.nluFallbackScore",

            //投信
            //總機
            "cathaysite.first.nlu.url@cathaysite.firstNLU.fallbackScore",
            //服務台
            "cathaysite.first.nlu.support.url@cathaysite.firstNLUSupport.fallbackScore",
            //yes/no
            "cathaysite.yes.no.nlu.url@cathaysite.yes.no.nlu.nluFallbackScore",
            //基金
            "cathaysite.fund.operation.nlu.url@cathaysite.fund.operation.nluFallbackScore",
            "cathaysite.fund.query.nlu.url@cathaysite.fund.query.nluFallbackScore",
            "cathaysite.fund.purchase.nlu.url@cathaysite.fund.purchase.nluFallbackScore",
            //帳戶
            "cathaysite.account.operation.nlu.url@cathaysite.account.operation.nluFallbackScore",
            //個人投資
            "cathaysite.investment.operation.nlu.url@cathaysite.investment.operation.nluFallbackScore",
            //帳單
            "cathaysite.bill.operation.nlu.url@cathaysite.bill.operation.nluFallbackScore",
            //密碼
            "cathaysite.pwd.operation.nlu.url@cathaysite.pwd.operation.nluFallbackScore",
            //開戶
            "cathaysite.newAccount.operation.nlu.url@cathaysite.newAccount.operation.nluFallbackScore",

            //產險
            //總機
            "cathayins.first.nlu.url@cathayins.firstNLU.fallbackScore",
            //服務台
            "cathayins.first.nlu.support.url@cathayins.firstNLUSupport.fallbackScore",
            //yes/no
            "cathayins.yes.no.nlu.url@cathayins.yes.no.nlu.nluFallbackScore",
            //保單
            "cathayins.policy.operation.nlu.url@cathayins.policy.operation.nluFallbackScore",
            "cathayins.query.item.nlu.url@cathayins.query.item.nluFallbackScore",
            "cathayins.update.item.nlu.url@cathayins.update.item.nluFallbackScore",
            //商品
            "cathayins.product.operation.nlu.url@cathayins.product.operation.nluFallbackScore",
            "cathayins.consult.item.nlu.url@cathayins.consult.item.nluFallbackScore",
            //會員
            "cathayins.member.operation.nlu.url@cathayins.member.operation.nluFallbackScore",
            //理賠
            "cathayins.claim.operation.nlu.url@cathayins.claim.operation.nluFallbackScore",
            //道路救援
            "cathayins.road.operation.nlu.url@cathayins.road.operation.nluFallbackScore",
    };

    private static final String TEST_FALLBACK_SCORE_Q1 = "1qaz2wsx3edc";
    //private static final String TEST_FALLBACK_SCORE_Q2 = "4rfv5tgb6yhn";

    private double getFallBackScore(String url) {
        double score = 0.0;

        Map<String, Object> nluResult = restTemplate.getForObject(url, HashMap.class);

        List<Map<String, Object>> intents = (List<Map<String, Object>>) nluResult.get("intents");

        if (CollectionUtils.isNotEmpty(intents)) {
            Map<String, Object> intent = intents.get(0);
            score = (Double) intent.get("score");
        }

        return score;
    }

    /*
    {
        "tokenized_sentence": "ccc",
        "sentence_id": 11840,
        "intents": [
            {
                "intent_id": 115,
                "intent": "定期定額",
                "score": 0.24690739178582627
            },
            {
                "intent_id": 116,
                "intent": "密碼",
                "score": 0.22132024410511053
            },
        ],
        "entities": [],
        "sentence": "ccc"
    }
     */
    @Override
    @Transactional
    public TPIGatewayResult updateFallBackScore() {
        Map<String, String> sysConfigData = sysConfigService.getAllSysConfig();

        for (String key : KEYS) {
            String[] keyArray = key.split("@");

            String urlKey = keyArray[0];
            String fallBackScoreKey = keyArray[1];

            String url = sysConfigData.get(urlKey);

//            if (StringUtils.isNotBlank(url)) {
//                double fallBackScore1 = getFallBackScore(url + TEST_FALLBACK_SCORE_Q1);
//                double fallBackScore2 = getFallBackScore(url + TEST_FALLBACK_SCORE_Q2);
//
//                //亂問的兩個分數相等才把他當原點值
//                if (fallBackScore1 == fallBackScore2 && fallBackScore1 > 0) {
//                    if (sysConfigData.get(fallBackScoreKey) != null) {
//                        SysConfig sysConfig = new SysConfig();
//                        sysConfig.setValue(String.valueOf(fallBackScore1));
//                        sysConfig.setUpdateTime(LocalDateTime.now());
//
//                        UpdateWrapper<SysConfig> updateWrapper = new UpdateWrapper<>();
//
//                        sysConfigService.update(sysConfig, updateWrapper.eq("key", fallBackScoreKey));
//                    } else {
//                        SysConfig sysConfig = new SysConfig();
//                        sysConfig.setKey(fallBackScoreKey);
//                        sysConfig.setValue(String.valueOf(fallBackScore1));
//                        sysConfig.setCreateTime(LocalDateTime.now());
//                        sysConfig.setUpdateTime(LocalDateTime.now());
//
//                        sysConfigService.save(sysConfig);
//                    }
//                }
//            }

            if (StringUtils.isNotBlank(url)) {
                double fallBackScore1 = getFallBackScore(url + TEST_FALLBACK_SCORE_Q1);

                if (fallBackScore1 > 0) {
                    if (sysConfigData.get(fallBackScoreKey) != null) {
                        SysConfig sysConfig = new SysConfig();
                        sysConfig.setValue1(String.valueOf(fallBackScore1));
                        sysConfig.setUpdateTime(LocalDateTime.now());

                        UpdateWrapper<SysConfig> updateWrapper = new UpdateWrapper<>();

                        sysConfigService.update(sysConfig, updateWrapper.eq("key", fallBackScoreKey));
                    } else {
                        SysConfig sysConfig = new SysConfig();
                        sysConfig.setKey1(fallBackScoreKey);
                        sysConfig.setValue1(String.valueOf(fallBackScore1));
                        sysConfig.setCreateTime(LocalDateTime.now());
                        sysConfig.setUpdateTime(LocalDateTime.now());

                        sysConfigService.save(sysConfig);
                    }
                }
            }
        }

        evictSysConfigCache();

        return TPIGatewayResult.ok();
    }

    private void evictSysConfigCache() {
        cacheManager.getCache(CacheConst.TPI_CACHE).evict(CacheConst.SYS_CONFIG_KEY);
    }

    @Override
    public TPIGatewayResult clearSysConfigCache() {
        evictSysConfigCache();
        return TPIGatewayResult.ok();
    }

    @Override
    @Transactional
    public TPIGatewayResult updateAutoComplete() {
        List<AllAutocomplete> allAutoCompleteList = null;
        try {
            // 讀取Excel
            String filepath = env.getProperty("allAutoComplete.excel.path");

            if (StringUtils.isBlank(filepath)) {
                return TPIGatewayResult.error("filePath is blank");
            }

            ReadExcelUtils excelReader = new ReadExcelUtils(filepath);

            Map<Integer, Map<Integer, Map<Integer, Object>>> results = new HashMap<>();

            // 開始讀取Excel資料
            int count = 1;

            for (int sheetId = 0; sheetId < count; sheetId++) {
                try {
                    Map<Integer, Map<Integer, Object>> result = excelReader.readExcelContent(sheetId);
                    results.put(sheetId, result);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }

            // 轉置為寫入資料庫的資料
            if (MapUtils.isNotEmpty(results)) {
                allAutoCompleteList = new ArrayList<>();

                LocalDateTime nowDateTime = LocalDateTime.now();

                for (Map.Entry<Integer, Map<Integer, Map<Integer, Object>>> roles : results.entrySet()) {
                    for (Map.Entry<Integer, Map<Integer, Object>> rows : roles.getValue().entrySet()) {
                        try {
                            if (this.checkAutoCompleteRowData(rows)) {
                                Integer string_Order = MapUtils.getInteger(rows.getValue(), 0);
                                String display_string = getRowValue(rows, 1);
                                String hidden_string = getRowValue(rows, 2);

                                AllAutocomplete allAutoComplete = new AllAutocomplete();
                                allAutoComplete.setDisplayString(display_string);

                                if (string_Order != null) {
                                    allAutoComplete.setStringOrder(string_Order);
                                } else {
                                    allAutoComplete.setStringOrder(999);
                                }
                                if (StringUtils.isNotEmpty(hidden_string)) {
                                    allAutoComplete.setHiddenString(hidden_string);
                                } else {
                                    allAutoComplete.setHiddenString(display_string);
                                }

                                allAutoComplete.setCreateTime(nowDateTime);
                                allAutoComplete.setUpdateTime(nowDateTime);

                                allAutoCompleteList.add(allAutoComplete);
                            }
                        } catch (Exception e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                }
            }

            // 清空並寫入資料庫
            if (CollectionUtils.isNotEmpty(allAutoCompleteList)) {
                iAllAutocompleteService.remove(Wrappers.emptyWrapper());
                iAllAutocompleteService.saveBatch(allAutoCompleteList);
            }
        } catch (Exception e) {
            log.error("updateAutoComplete error:" + e.getMessage(), e);
            return TPIGatewayResult.error("system error");
        }

        return TPIGatewayResult.ok();
    }

    @Override
    @Transactional
    public TPIGatewayResult updateBotTpiReplayText() {
        List<BotTpiReplyText> botTpiReplyTexts = null;

        try {
            // 讀取excel
            String filepath = PropUtils.getReplayExcelPath();

            if (StringUtils.isBlank(filepath)) {
                return TPIGatewayResult.error("filePath is blank");
            }

            ReadExcelUtils excelReader = new ReadExcelUtils(filepath);

            // 角色清單, 數量及順序 與 excel 的 sheet 相對應
            //String[] roleNames = StringUtils.split(PropUtils.REPLY_SHEET_ROLENAMES, ",");

            // <sheetId, <rowId, <columnId, wording>>>
            Map<Integer, Map<Integer, Map<Integer, Object>>> results = new HashMap<>();

            // 開始讀取sheet資料
            int count = 1;
            for (int sheetId = 0; sheetId < count; sheetId++) {
                try {
                    Map<Integer, Map<Integer, Object>> result = excelReader.readExcelContent(sheetId);
                    results.put(sheetId, result);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }

            // 轉置為寫入資料庫的資料
            if (MapUtils.isNotEmpty(results)) {
                botTpiReplyTexts = new ArrayList<>();
                for (Map.Entry<Integer,Map<Integer,Map<Integer,Object>>> roles : results.entrySet()) {
                    for (Map.Entry<Integer,Map<Integer,Object>> rows : roles.getValue().entrySet()) {
                        if (this.checkBotTextRowData(rows)) {
                            BotTpiReplyText replyText = new BotTpiReplyText();
                            //replyText.setId(DataUtils.getUUID());
                            replyText.setRoleId((String) rows.getValue().get(0));
                            replyText.setTextId((String) rows.getValue().get(1));
                            replyText.setText((String) rows.getValue().get(2));
                            botTpiReplyTexts.add(replyText);
                        }
                    }
                }
            }

            // 清空並寫入資料庫
            if (org.apache.commons.collections.CollectionUtils.isNotEmpty(botTpiReplyTexts)) {
                botTpiReplyTextService.remove(Wrappers.emptyWrapper());
                botTpiReplyTextService.saveBatch(botTpiReplyTexts);
            }
        } catch (Exception e) {
            log.error("updateBotTpiReplayText error:" + e.getMessage(), e);
        }

        return TPIGatewayResult.ok();
    }

    private boolean checkAutoCompleteRowData(Map.Entry<Integer, Map<Integer, Object>> rows) {
        boolean result = true;
        try {
            // Just verify PK
            if (StringUtils.isBlank((String) rows.getValue().get(0))) {
                result = false;
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    private String getRowValue(Map.Entry<Integer, Map<Integer, Object>> rows, int index) {
        String str = null;
        try {
            str = (String) rows.getValue().get(index);
        } catch (Exception e) {
        } finally {
            if (str == null) {
                str = "";
            }
        }
        return str;
    }

    private boolean checkBotTextRowData(Map.Entry<Integer, Map<Integer, Object>> rows) {
        boolean result = true;
        rows.getKey();
        if (StringUtils.isBlank((String) rows.getValue().get(1))) {
            log.trace("rows.getKey()= " + rows.getKey() +":checkBotTextRowData error: roleId is Empty" );
            result = false;
        }
        if (StringUtils.isBlank((String) rows.getValue().get(2))) {
            log.trace("rows.getKey()= " + rows.getKey() + ":checkBotTextRowData error: textId is Empty" );
            result = false;
        }

        return result;
    }

}
