package com.tp.tpigateway.modules.system.controller;

import com.alibaba.fastjson.JSON;
import com.tp.common.util.JsonResult;
import com.tp.tpigateway.common.mongo.entity.Nodemessages;
import com.tp.tpigateway.modules.system.model.ChatLogData;
import com.tp.tpigateway.modules.system.service.ChatLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 使用者呼叫相關控制程式
 */
@Controller
public class ChatLogController {

    private static Logger log = LoggerFactory.getLogger(ChatLogController.class);

    @Autowired
    private ChatLogService chatLogService;

    @RequestMapping(value = "/chatLogData"/* , method = RequestMethod.POST */)
    @ResponseBody
    public JsonResult chatLogData(String chatID) {
        List<Nodemessages.Payload> chatLogData = null;
        ChatLogData result = chatLogService.findChatLogBySessionIdAndStartDateTime(chatID, null);
        if (result.getChatLog() != null) {
            chatLogData = result.getChatLog();
        }

        return JsonResult.success().add("chatLogData", JSON.toJSONString(chatLogData));
    }

    /*
     * 轉真人後對話紀錄的呈現頁面
       將拿到的sessionId那去查mongo取出對話紀錄往前端丟
     */
    @RequestMapping(value = "/chatLog"/* , method = RequestMethod.POST */)
    public String chatLog(Model model, String chatLog, String chatID) {
        try {
            ChatLogData result = chatLogService.findChatLogBySessionIdAndStartDateTime(chatID, null);
            if (result.getChatLog() != null) {
                //chatLog = JSONUtils.obj2json(result.getChatLog());
                chatLog = JSON.toJSONString(result.getChatLog());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        model.addAttribute("chatLog", chatLog);
        model.addAttribute("sessionId", chatID);
        return "chatLog";
    }

    /*
     * 轉真人後對話紀錄的呈現頁面(傳進來的對話紀錄可能沒照時間排序，故取出其sessionId後再去查一次)
     */
    @RequestMapping(value = "/chatLog_by_session_id"/* , method = RequestMethod.POST */)
    public String chatLog_by_session_id(String messages, Model model) throws Exception {
        String chatlogData = "";

        String sessionId = null;
        try {
            //List<Map> dataList = JSONUtils.json2list(messages, Map.class);
            List<Map> dataList = JSON.parseArray(messages, Map.class);
            log.debug("<chatLog_by_session_id> dataList=" + dataList.size());
            for (Map data : dataList) {
                sessionId = (String) data.get("session_id");

                if (sessionId != null) {
                    log.debug("<chatLog_by_session_id> sessionId=" + sessionId);
                    ChatLogData result = chatLogService.findChatLogBySessionIdAndStartDateTime(sessionId, null);
                    if (result.getChatLog() != null) {
                        //chatlogData = JSONUtils.obj2json(result.getChatLog());
                        chatlogData = JSON.toJSONString(result.getChatLog());
                    }
                    break;
                }
            }

        } catch (Exception e) {
            log.error("<chatLog_by_session_id: " + sessionId + "> error : " + e.getMessage(), e);
        }

        log.debug("<chatLog_by_session_id: " + sessionId + "> chatlogData=" + chatlogData);
        model.addAttribute("chatLog", chatlogData);
        model.addAttribute("sessionId", sessionId);
        return "chatLog";
    }

    /*
       1.先取出sessionId給前端顯示用(也可在前段自己取出)
       2.將拿到的sessionId那去查mongo取出對話紀錄往前端丟
    */
    @RequestMapping(value = "/chatLog_by_session_id2"/* , method = RequestMethod.POST */)
    public String chatLog_by_session_id2(String chatLog, String messages, Model model) throws Exception {
        String chatlogData = "";

        String sessionId = null;
        try {
            if (messages == null) messages = chatLog;
            //List<Map> dataList = JSONUtils.json2list(messages, Map.class);
            List<Map> dataList = JSON.parseArray(messages, Map.class);
            log.debug("<chatLog_by_session_id2> dataList="+dataList.size());
            for (Map data : dataList) {
                sessionId = (String) data.get("session_id");

                if (sessionId != null) {
                    log.debug("<chatLog_by_session_id2: "+sessionId+"> sessionId="+sessionId);
                    ChatLogData result = chatLogService.findChatLogBySessionIdAndStartDateTime(sessionId, null);
                    if (result.getChatLog() != null) {
                        //chatlogData = JSONUtils.obj2json(result.getChatLog());
                        chatlogData = JSON.toJSONString(result.getChatLog());
                    }
                    break;
                }
            }

        } catch (Exception e) {
            log.error("<chatLog_by_session_id2: "+sessionId+"> error : " + e.getMessage(), e);
        }

        model.addAttribute("chatLog", chatlogData);
        model.addAttribute("sessionId", sessionId);
        return "chatLog";

    }

    /**
     * 對話紀錄，改接ES版
     * 1.先取出sessionId給前端顯示用(也可在前段自己取出)
     * 2.將拿到的對話紀錄直接往前端丟去解析
     * 
     * @param chatLog
     * @param messages
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/chatLog_by_chatLog"/* , method = RequestMethod.POST */)
    public String chatLog_by_chatLog(String chatLog, String messages, Model model) throws Exception {
        String sessionId = null;
        try {
            if (messages == null) messages = chatLog;
            //List<Map> dataList = JSONUtils.json2list(messages, Map.class);
            List<Map> dataList = JSON.parseArray(messages, Map.class);

            for (Map data : dataList) {
                sessionId = (String) data.get("session_id");
                if (sessionId != null) {
                    break;
                }
            }

        } catch (Exception e) {
            log.error("<chatLog_by_chatLog: "+sessionId+"> error : " + e.getMessage(), e);
        }

        model.addAttribute("chatLog", messages);
        model.addAttribute("sessionId", sessionId);
        return "chatLog";
    }

}
