package com.tp.tpigateway.modules.system.service;

import com.tp.tpigateway.modules.system.model.ChatLogData;

public interface ChatLogService {

    ChatLogData findChatLogBySessionIdAndStartDateTime(String sessionId, Long startDateTime);
}
