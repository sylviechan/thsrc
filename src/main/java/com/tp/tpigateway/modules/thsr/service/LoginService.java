package com.tp.tpigateway.modules.thsr.service;

import java.util.Map;

import com.tp.common.model.LoginRequest;
import com.tp.common.model.TPIGatewayResult;

//wayne
//20191230
public interface LoginService {

	void sendSMS(Map<String, String> params);
	TPIGatewayResult loginActioninfo(LoginRequest loginRequest);

}
