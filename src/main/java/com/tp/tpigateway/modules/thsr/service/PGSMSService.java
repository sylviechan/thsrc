package com.tp.tpigateway.modules.thsr.service;

import com.tp.common.model.PGSMSRequest;
import com.tp.common.model.TPIGatewayResult;

// wayne
//20191226
public interface PGSMSService {
	TPIGatewayResult create(PGSMSRequest pgsmsRequest);

	TPIGatewayResult search(PGSMSRequest botRequest);
}
