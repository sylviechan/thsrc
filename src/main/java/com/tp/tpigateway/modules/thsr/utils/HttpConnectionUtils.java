package com.tp.tpigateway.modules.thsr.utils;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

// wayne
// 20191230
// 模擬高鐵Api串接
public class HttpConnectionUtils {

	public static final String THSR_LFS_API_URL = "http://localhost:8180/TPIGateway/fake/createLostForm";
	public static final String THSR_AUTH_API_URL = "http://localhost:8180/TPIGateway/fake/sendSMS";
	
	// 20191228
		// 使用okHttp
		public static Response sendPost(String url, String param, MediaType mediaType) {
			OkHttpClient client = new OkHttpClient();
			Request request = new Request.Builder()
					.url(url)
					.post(RequestBody.create(mediaType, param))
					.build();
			try {
				return client.newCall(request).execute();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	
}
