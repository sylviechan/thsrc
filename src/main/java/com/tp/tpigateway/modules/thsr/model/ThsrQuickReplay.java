package com.tp.tpigateway.modules.thsr.model;

import com.tp.common.util.DataUtils;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.InsUrlGather;
import com.tp.tpigateway.modules.cathayins.model.InsQuickReplay;

import org.apache.commons.collections.MapUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ThsrQuickReplay extends QuickReplay {

    //制式問法
//    public static final QuickReplay 保單_查詢 = new wayneQuickReplay(1, "查詢保單內容", "查詢保單內容");
//    public static final QuickReplay 保單_變更 = new wayneQuickReplay(1, "變更保單內容", "變更保單內容");
//    public static final QuickReplay 保單_申請 = new wayneQuickReplay(1, "下載車險電子強制證", "下載車險電子強制證");
//
//    public static final QuickReplay 商品_諮詢 = new wayneQuickReplay(1, "商品內容諮詢", "商品內容諮詢");
//    public static final QuickReplay 會員_申請 = new wayneQuickReplay(1, "會員升級", "會員升級");
//
//    public static final QuickReplay 理賠_查詢 = new wayneQuickReplay(1, "理賠進度查詢", "理賠進度查詢");
//    public static final QuickReplay 理賠_申請 = new wayneQuickReplay(1, "理賠申請", "理賠申請");
//
//    public static final QuickReplay 道路救援_查詢 = new wayneQuickReplay(1, "道路救援查詢", "道路救援查詢");
//    public static final QuickReplay 道路救援_申請 = new wayneQuickReplay(1, "申請道路救援", "申請道路救援");
	
	// waynewayne
	public static final QuickReplay LOSS_STEP_1_1 = new ThsrQuickReplay(1, "已報失過", "已經報失過了", "", "", "");
	public static final QuickReplay LOSS_STEP_1_2 = new ThsrQuickReplay(1, "尚未報失", "我還沒報失", "", "", "");
	
	public static final QuickReplay LOSS_STEP_2_1 = new ThsrQuickReplay(1, "沒有手機號碼", "不方便提供手機", "", "", "");
	public static final QuickReplay LOSS_STEP_2_2 = new ThsrQuickReplay(1, "取消本次操作", "我要取消", "", "", "");

    // hello
    public static final QuickReplay HELLO_A05_1 = new ThsrQuickReplay(1, "車險", "車險保單查詢", "", "", "");
    public static final QuickReplay HELLO_A05_2 = new ThsrQuickReplay(1, "旅遊綜合保險", "旅遊綜合保險保單查詢", "", "", "");
    public static final QuickReplay HELLO_A05_3 = new ThsrQuickReplay(1, "健康與傷害險", "健康與傷害險保單查詢", "", "", "");
    public static final QuickReplay HELLO_A05_4 = new ThsrQuickReplay(1, "下載電子強制保險證", "下載電子強制保險證", "", "", "");
    public static final QuickReplay HELLO_C01_1 = new ThsrQuickReplay(1, "資格查詢", "我要查道路救援資格", "", "", "");
    public static final QuickReplay HELLO_C01_2 = new ThsrQuickReplay(1, "呼叫道路救援", "呼叫道路救援", "", "", "");
    
    //熱門問題
    public static final QuickReplay TOP_QUESTION_1 = new ThsrQuickReplay(5, "投保熱門問題", "App.quertTopQuestion('ChatWeb','alpha',1)", "", "", "");
    public static final QuickReplay TOP_QUESTION_2 = new ThsrQuickReplay(5, "理賠熱門問題", "App.quertTopQuestion('ChatWeb','alpha',2)", "", "", "");
    public static final QuickReplay TOP_QUESTION_3 = new ThsrQuickReplay(5, "道路救援熱門問題", "App.quertTopQuestion('ChatWeb','alpha',3)", "", "", "");
    
    //保單批改
    public static final QuickReplay T01_1_1 = new ThsrQuickReplay(1, "改旅遊綜合保險保險期間", "改旅遊綜合保險保險期間", "", "", "");
    public static final QuickReplay T01_1_2 = new ThsrQuickReplay(1, "改車主資料", "改車主資料", "", "", "");
    public static final QuickReplay T01_1_3 = new ThsrQuickReplay(1, "改車險通訊資料", "改車險通訊資料", "", "", "");
    public static final QuickReplay T01_1_4 = new ThsrQuickReplay(1, "改車籍資料", "改車籍資料", "", "", "");
    public static final QuickReplay T01_1_5 = new ThsrQuickReplay(1, "車險批改進度查詢", "車險批改進度查詢", "", "", "");
    
    //common
    public static final QuickReplay INIT = new ThsrQuickReplay(1, "看阿發還能幫什麼忙", "看阿發還能幫什麼忙", "", "", "");
    public static final QuickReplay END_CHAT = new ThsrQuickReplay(1, "不用了, 結束對話", "不用了, 結束對話", "", "", "");

    // 道路救援
    public static final QuickReplay C01_1_1 = new ThsrQuickReplay(1, "只是查詢資格", "只是查詢資格", "C01_1_1", "", "");
    public static final QuickReplay C01_1_2 = new ThsrQuickReplay(1, "申請道路救援服務", "申請道路救援服務", "C01_1_2", "", "");

    // waynewayne
    // 20191227 新增CARD_LOSS_1到3
    public static final QuickReplay WAYNE_1 = new ThsrQuickReplay(1, "好我滾", "滾", "WAYNE_1", "", "");
    public static final QuickReplay WAYNE_2 = new ThsrQuickReplay(1, "再見臭雞雞", "再見", "WAYNE_2", "", "");
    public static final QuickReplay CARD_203_1 = new ThsrQuickReplay(15, "正確", "正確", "", "", "");
    public static final QuickReplay CARD_203_2 = new ThsrQuickReplay(15, "修改", "修改", "", "", "");
    public static final QuickReplay CARD_203_3 = new ThsrQuickReplay(15, "取消本次操作", "取消本次操作", "", "", "");
    
	public static QuickReplay C01_2_1(String ssoId) {
		return new ThsrQuickReplay(4, "道路救援服務說明", "道路救援服務說明", "", InsUrlGather.insurance_Services + DataUtils.getRectifySSOId(ssoId) , "");
	}
    public static final QuickReplay C01_2_2 = new ThsrQuickReplay(1, "申告道路救援", "申請道路救援服務", "C01_2_2", "", "");
    public static final QuickReplay C01_2_4 = new ThsrQuickReplay(1, "問別的事情", "問別的事情", "C01_2_4", "", "");
    public static final QuickReplay C01_2_5 = new ThsrQuickReplay(2, "道路救援資格查詢", "道路救援資格查詢", "C01_2_5", "", "");

    public static QuickReplay C02_2_1(String tel) {
        return new ThsrQuickReplay(12, "立即撥號", tel, "", "", "");
    }
    
    //產險-電子強制險證下載
    public static final QuickReplay C05_2_1 = new ThsrQuickReplay(1, "下載其他強制證", "下載其他強制證", "C05_2_1", "", "");
    public static final QuickReplay C05_2_2 = new ThsrQuickReplay(1, "問其它問題", "問其它問題", "C05_2_2", "", "");
    public static final QuickReplay C05_2_3 = new ThsrQuickReplay(1, "重新輸入", "重新輸入", "保單", "重新輸入電子強制險證", MapUtils.EMPTY_MAP);
    //public static final QuickReplay C05_2_1 = new InsQuickReplay(4, "下載", "下載", "", "https://www.google.com/", "");

    //認證會員
    public static final QuickReplay A04_1_1 = new ThsrQuickReplay(1, "認證", "認證", "A04_1_1", "", "");
    public static final QuickReplay A04_1_2 = new ThsrQuickReplay(1, "不認證", "不認證", "A04_1_2", "", "");
    public static final QuickReplay A04_2_1 = new ThsrQuickReplay(1, "登入", "登入", "A04_2_1", "", "");

    //查詢保單
    public static final QuickReplay A05_RESET = new ThsrQuickReplay(18, "不用了，結束並清空對話", "不用了，結束並清空對話", "", "", "");
    public static final QuickReplay A05_2_3 = new ThsrQuickReplay(1, "轉真人客服", "轉真人客服", "", "", "");
    public static final QuickReplay AGNT_STOP = new ThsrQuickReplay(1, "離開真人客服", "離開真人客服", "", "", "");
    
    public static final QuickReplay A05_OTHERHELP = new ThsrQuickReplay(2, "看阿發還能幫什麼忙", "看阿發還能幫什麼忙", "", "", "");
    public static final QuickReplay A05_TRAVEL_1 = new ThsrQuickReplay(15, "查國泰產險保單", "我要查旅遊綜合保險保單", "買的", "保單", "clickTouristSourceType", getParameter("touristSourceType", "PC01_C"));
    public static final QuickReplay A05_TRAVEL_2 = new ThsrQuickReplay(15, "查信用卡旅遊綜合保險", "我要查信用卡旅遊綜合保險", "刷卡送的", "保單", "clickTouristSourceType", getParameter("touristSourceType", "PC01_D"));
    public static final QuickReplay A05_TRAVEL_3 = new ThsrQuickReplay(15, "查國泰人壽滿1100送的旅遊不便險保障", "我要查國泰人壽滿1100送的旅遊不便險保障", "旅平險送的", "保單", "clickTouristSourceType", getParameter("touristSourceType", "PC01_B"));
    public static final QuickReplay A05_TRAVEL_2_1 = new ThsrQuickReplay(2, "國泰世華", "國泰世華", "保單", "", MapUtils.EMPTY_MAP);
    public static final QuickReplay A05_TRAVEL_2_2 = new ThsrQuickReplay(2, "花旗", "花旗", "保單", "", MapUtils.EMPTY_MAP);
    public static final QuickReplay A05_TRAVEL_2_3 = new ThsrQuickReplay(2, "滙豐", "滙豐", "保單", "", MapUtils.EMPTY_MAP);
    public static final QuickReplay A05_TRAVEL_2_X_1 = new ThsrQuickReplay(2, "旅遊平安險", "旅遊平安險", "保單", "", MapUtils.EMPTY_MAP);
    public static final QuickReplay A05_TRAVEL_2_X_2 = new ThsrQuickReplay(2, "不便險", "不便險", "保單", "", MapUtils.EMPTY_MAP);

    public static final QuickReplay TO_AGENT = new ThsrQuickReplay(2, "我要轉真人文字客服", "我要轉真人文字客服", "", "", MapUtils.EMPTY_MAP);
    public static final QuickReplay NOT_TO_AGENT = new ThsrQuickReplay(2, "現在先不用", "現在先不用", "", "", MapUtils.EMPTY_MAP);
    //查詢保單明細
    //public static final QuickReplay A06_2_1 = new InsQuickReplay(1, "申請保單變更", "申請保單變更", "A06_2_1", "", "");
    //public static final QuickReplay A06_2_2 = new InsQuickReplay(1, "車主及通訊資料", "車主及通訊資料", "A06_2_2", "", "");

    /** 看車輛資訊 */
    public static QuickReplay A06_2_1(Map<String, Object> parameter) {
        return new ThsrQuickReplay(1, "看車輛資訊", "看車輛資訊", "保單", "clickInsInfoComp_Car", parameter);
    }

    /** 看其他保單 */
    public static final QuickReplay A06_2_2 = new ThsrQuickReplay(1, "看其他保單", "看其他保單", "保單", "clickInsOtherPolicy", Collections.emptyMap());
    public static final QuickReplay A06_2_3 = new ThsrQuickReplay(1, "問別的事情", "問別的事情", "A06_2_3", "", "");
    public static final QuickReplay A06_2_4 = new ThsrQuickReplay(1, "會員升級", "會員升級", "A04", "", "");

    /** 看同車其他保單 */
    public static QuickReplay A06_2_5(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "看同車保單", "保單明細0", "保單", "clickInsOtherSameComp_Car", parameter);
    }
    /** 看保單其他資訊Health */
    public static QuickReplay A06_2_6(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "看保單其他資訊", "保單明細1", "保單", "clickInsOtherInfoComp_Health", parameter);
    }
    /** 看保單其他資訊Travel */
    public static QuickReplay A06_2_7(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "看保單其他資訊", "保單明細2", "保單", "clickInsOtherInfoComp_Travel", parameter);
    }
    /** 看保障內容Health */
    public static QuickReplay A06_2_8(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "看保障內容", "看保障內容HEALTH", "保單", "clickInsContentComp_Health", parameter);
    }
    /** 看關係人資訊Health */
    public static QuickReplay A06_2_9(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "看關係人資訊", "看關係人資訊", "保單", "clickInsManInfoComp_Health", parameter);
    }
    /** 看保障內容Travel */
    public static QuickReplay A06_2_10(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "看保障內容", "看保障內容TRAVEL", "保單", "clickInsContentComp_Travel", parameter);
    }
    /** 保期變更Travel */
    public static QuickReplay A06_2_11(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "保期變更", "變更旅平險保單日期", "保單", "sendTravelPolicy", parameter);
    }
    /** 關係人資訊Travel */
    public static QuickReplay A06_2_12(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "關係人資訊", "關係人資訊", "保單", "clickInsManInfoComp_Travel", parameter);
    }
    /** 快速理賠資訊Travel */
    public static QuickReplay A06_2_13(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "快速理賠資訊", "快速理賠資訊", "保單", "clickInsClaimInfoComp_Travel", parameter);
    }
    /** 看保障項目說明Travel */
    public static QuickReplay A06_ContentDesc(String key, Map<String, Object> parameter) {
    	return new ThsrQuickReplay(2, "看保障項目說明", "看保障項目說明" + key, "保單", "clickInsContentDescComp_Travel", parameter);
    }
    /** 看更多項目Travel */
    public static QuickReplay A06_MoreContent(Map<String, Object> parameter) {
    	return new ThsrQuickReplay(2, "看更多項目", "看更多項目", "保單", "clickInsContentMoreComp_Travel", parameter);
    }
    
    //車輛保險
	public static final QuickReplay ChangeCar = new ThsrQuickReplay(1, "重新選擇車輛", "重新選擇車輛", "", "", "");

	public static QuickReplay GO_URL_MEMBER(String ssoId) {
		return new ThsrQuickReplay(4, "前往數位服務平台", "前往數位服務平台", "", InsUrlGather.OCAB_0110 + DataUtils.getRectifySSOId2(ssoId) , "");
	}

	public static QuickReplay GO_URL_COMMUNICATION(String ssoId) {
		return new ThsrQuickReplay(4, "前往數位服務平台", "前往數位服務平台", "", InsUrlGather.OCAB_0120 + DataUtils.getRectifySSOId2(ssoId) , "");
	}

	public static QuickReplay GO_URL_REGISTRATION(String ssoId) {
		return new ThsrQuickReplay(4, "前往數位服務平台", "前往數位服務平台", "", InsUrlGather.OCAB_0130 + DataUtils.getRectifySSOId2(ssoId) , "");
	}
    public static final QuickReplay Change_CS = new ThsrQuickReplay(1, "轉真人客服", "轉真人客服", "", "", "");
    public static final QuickReplay CameraOrUPD = new ThsrQuickReplay(1, "拍照或上傳", "拍照或上傳", "", "", "");
    public static final QuickReplay Change = new ThsrQuickReplay(1, "修改", "修改", "", "", "");
    public static final QuickReplay No = new ThsrQuickReplay(1, "不，保持原狀", "不，保持原狀", "", "", "");
    public static final QuickReplay ChangeProgressQuery = new ThsrQuickReplay(1, "變更進度查詢", "變更進度查詢", "", "", "");
    public static final QuickReplay ChangeCarInsurance = new ThsrQuickReplay(1, "改其他通訊資料", "改其他通訊資料", "", "", "");
    
    public static QuickReplay GOContact(String ssoId) {
		return new ThsrQuickReplay(4, "前往留言", "前往留言", "", InsUrlGather.insurance_Contact + DataUtils.getRectifySSOId(ssoId), "");
	}

    public static QuickReplay GODigitalService(String ssoId) {
		return new ThsrQuickReplay(4, "前往數位服務平台", "前往數位服務平台", "", InsUrlGather.OCAB_1000 + DataUtils.getRectifySSOId2(ssoId), "");
	}

    //商品
	public static QuickReplay HELLO_M01_1(String ssoId) {
		return new ThsrQuickReplay(4, "車險商品查詢", "車險商品查詢", "", InsUrlGather.insurance_Products_Car + DataUtils.getRectifySSOId(ssoId) , "");
	}
    
    /** 看旅遊綜合保險商品 */
	public static QuickReplay HELLO_M01_2(Map<String, Object> parameter) {
		return new ThsrQuickReplay(1, "旅遊綜合保險商品查詢", "旅遊綜合保險商品查詢", "商品", "", parameter);
	}

	public static QuickReplay HELLO_M01_3(String ssoId) {
		return new ThsrQuickReplay(4, "健康與傷害險商品查詢", "健康與傷害險商品查詢", "", InsUrlGather.insurance_Products + DataUtils.getRectifySSOId(ssoId) , "");
	}

	public static QuickReplay HELLO_M01_4(String ssoId) {
		return new ThsrQuickReplay(4, "其他險種商品查詢", "其他險種商品查詢", "", InsUrlGather.insurance_Products + DataUtils.getRectifySSOId(ssoId) , "");
	}
    public static final QuickReplay M02_TOURISM_1 = new ThsrQuickReplay(1, "海外", "海外", "", "", "");
    public static final QuickReplay M02_TOURISM_2 = new ThsrQuickReplay(1, "國內", "國內", "", "", "");
    public static final QuickReplay M02_TOURISM_3 = new ThsrQuickReplay(1, "看別的項目", "看別的項目", "商品", "clickOtherItem", Collections.emptyMap());
    public static final QuickReplay M02_TOURISM_4 = new ThsrQuickReplay(1, "查看我的保單", "查看我的保單", "保單", "clickInsOtherPolicy", Collections.emptyMap());
    public static final QuickReplay M02_TOURISM_5 = new ThsrQuickReplay(1, "辦理理賠", "辦理理賠", "", "", ""); 
    public static final QuickReplay M02_TRAVEL_1 = new ThsrQuickReplay(2, "旅遊平安險", "旅遊平安險", "", "", "");
    public static final QuickReplay M02_TRAVEL_2 = new ThsrQuickReplay(2, "不便險", "不便險", "", "", "");
    public static final QuickReplay M02_PROBLEM_1 = new ThsrQuickReplay(1, "承保範圍", "承保範圍", "", "", ""); 
	public static final QuickReplay M02_PROBLEM_2 = new ThsrQuickReplay(1, "理賠項目/金額", "理賠項目/金額",
			"PC01_B01001_2_CIA", "", "");
	public static final QuickReplay M02_PROBLEM_3 = new ThsrQuickReplay(1, "不保物品", "不保物品", "", "", ""); 
    public static final QuickReplay M02_PROBLEM_4 = new ThsrQuickReplay(1, "不保事項", "不保事項", "", "", ""); 
    public static final QuickReplay M02_PROBLEM_5 = new ThsrQuickReplay(1, "給付項目", "給付項目", "", "", ""); 
    public static final QuickReplay M02_PROBLEM_6 = new ThsrQuickReplay(1, "除外責任", "除外責任", "", "", ""); 
    public static final QuickReplay M02_PROBLEM_7 = new ThsrQuickReplay(1, "理賠應備文件", "理賠應備文件", "", "", ""); 
    
    
    //保單變更init
    public static final QuickReplay OTHERHELP = new ThsrQuickReplay(2, "看阿發還能幫什麼忙", "看阿發還能幫什麼忙");
    public static final QuickReplay RESET = new ThsrQuickReplay(2, "不用了，結束並清空對話", "不用了，結束並清空對話");
    public static final QuickReplay T02_CHANGE = new ThsrQuickReplay(2, "好，幫我改", "好，幫我改", "保單", "travel_yes", MapUtils.EMPTY_MAP);
    public static final QuickReplay T02_1_1 = new ThsrQuickReplay(1, "改旅遊綜合保險保險期間", "我要改旅遊綜合保險保險期間");
    public static final QuickReplay T02_1_2 = new ThsrQuickReplay(2, "改車主資料", "改車主資料");
    public static final QuickReplay T02_1_3 = new ThsrQuickReplay(2, "改車險通訊資料", "改車險通訊資料");
    public static final QuickReplay T02_1_4 = new ThsrQuickReplay(2, "改車籍資料", "改車籍資料");
    public static final QuickReplay T02_1_5 = new ThsrQuickReplay(2, "車險批改進度查詢", "車險批改進度查詢");

    public static QuickReplay T04_1(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "查保單內容", "查保單內容","保單","clickInsListComp_Travel", parameter);
    }
    
    /** 變更旅平險保單-篩選保單 */
    public static QuickReplay T02_3_1(Map<String, Object> parameter) {
        return new ThsrQuickReplay(2, "變更旅平險保單", "變更旅平險保單", "", "", "", "保單", "getTravelPolicy", parameter);
    }

    public static QuickReplay T03_1_1(Map<String, Object> uplAppllteNo) {
    	return new ThsrQuickReplay(2, "確認並繳費", "確認並繳費", "保單","getCreditCardView", uplAppllteNo);
    }
    public static QuickReplay T03_1_2(Map<String, Object> uplAppllteNo) {
    	return new ThsrQuickReplay(2, "確認變更", "確認變更", "保單","getCreditCardView", uplAppllteNo);
    }
    public static QuickReplay T03_2(Map<String, Object> policyMap) {
        return new ThsrQuickReplay(2, "再修改期間", "再修改期間","保單","sendTravelPolicy", policyMap);
    }
    public static QuickReplay T03_3(String url) {
        return new ThsrQuickReplay(4, "修改會員資料", "修改會員資料","",url,"");
    }

	// 理賠辦理
	/** 前往數位服務平台 */
	public static QuickReplay F01_1_1(String ssoId) {
		return new ThsrQuickReplay(4, "前往數位服務平台", "前往數位服務平台", "F01_1_1", InsUrlGather.OCAA_0100 + DataUtils.getRectifySSOId2(ssoId), "");
	}
	
	public static QuickReplay F02_1_1(String ssoId) {
		return new ThsrQuickReplay(4, "前往數位服務平台", "前往數位服務平台", "F02_1_1", InsUrlGather.OCAB_1000_1 + DataUtils.getRectifySSOId2(ssoId), "");
	}

    private static Map getParameter(String key, String value) {
        Map<String, Object> parameter = new HashMap();
        parameter.put(key, value);
        return parameter;
    }

    public ThsrQuickReplay(int action, String text, String alt, String reply, String url, String customerAction) {
        super(action, text, alt, reply, url, customerAction);
    }

    public ThsrQuickReplay(int action, String text, String alt, String reply, String url, String customerAction, String intent, String secondBorIntent, Map<String, Object> parameter) {
        super(action, text, alt, reply, url, customerAction, intent, secondBorIntent, parameter);
    }

    public ThsrQuickReplay(int action, String text, String alt, String intent, String secondBotIntent, Map<String, Object> parameter) {
        super(action, text, alt, intent, secondBotIntent, parameter);
    }

    public ThsrQuickReplay(int action, String text, String alt, String replay, String intent, String secondBotIntent, Map<String, Object> parameter) {
        super(action, text, alt, replay, intent, secondBotIntent, parameter);
    }

    public ThsrQuickReplay(int action, String text, String alt) {
        super(action, text, alt);
    }
}