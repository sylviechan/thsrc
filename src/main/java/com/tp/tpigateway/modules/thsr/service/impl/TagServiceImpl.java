package com.tp.tpigateway.modules.thsr.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.TagRequest;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.thsr.service.TagService;

/**
 * @author west
 * @version 建立時間:Dec 25, 2019 9:34:37 AM 類說明
 */
@Service("tagService")
public class TagServiceImpl implements TagService {

	@Autowired
	BotResponseUtils botResponseUtils;

//	@Autowired
//	Environment env;

	@Override
	public TPIGatewayResult input(TagRequest tagRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessageForThsrCard(tagRequest.getChannel());

		List<Map<String, Object>> cardList;
		cardList = getInputList(botMessageVO, tagRequest);

		botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, cardList);

		return tpiGatewayResult.setData(botMessageVO);

	}

	private List<Map<String, Object>> getInputList(BotMessageVO botMessageVO, TagRequest tagRequest) {

		List<Map<String, Object>> cardList = new ArrayList<>();
		Map<String, Object> cardsMap = new HashMap<>();

		List<Map<String, Object>> cLinkList = new ArrayList<>();
		List<String> inputList = tagRequest.getInput();
		for (int i = 0; i < inputList.size(); i++) {
			Map<String, Object> inputMap = new HashMap<>();
			inputMap.put("clAction", 2);
			inputMap.put("clText", inputList.get(i));
			inputMap.put("clAlt", inputList.get(i));

			cLinkList.add(inputMap);

		}
		cardsMap.put("cWidth", BotMessageVO.CWIDTH_200);
		cardsMap.put("cLinkList", cLinkList);

		cardList.add(cardsMap);

		return cardList;

	}

	// 取得選項的數量
	public String getInputSize(List<String> inputList) {

		int inputSize = inputList.size();
		String size = "";
		switch (inputSize) {
		case 2:
			size = "select-tag01";
			break;
		case 3:
			size = "select-tag02";
			break;
		case 4:
			size = "select-tag03";
			break;
		case 5:
			size = "select-tag04";
			break;
		default:
			size = "select-tag04 search_all";
			break;

		}

		return size;

	}

}
