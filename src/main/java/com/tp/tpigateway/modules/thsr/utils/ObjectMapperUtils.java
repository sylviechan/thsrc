package com.tp.tpigateway.modules.thsr.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import okhttp3.Response;

// wayne
// 20191228
public class ObjectMapperUtils {
	private static ObjectMapper mapper = new ObjectMapper();
	
	public static String mapToJsonString(Map<String, String> map) {
		try {
			return mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return String.format("\"error\":\"%s\"", e.getMessage());
		}
	}
	
	public static Map<String, Object> jsonStringToMap(String json) {
		try {
			return mapper.readValue(json, new TypeReference<Map<String,Object>>(){});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return new HashMap<String, Object>() {{
				put("error", e.getMessage());
			}};
		}
	}
	
	public static Map<String, Object> getMapFromResponse(Response response) {
		try {
			return jsonStringToMap(response.body().string());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return new HashMap<String, Object>() {{
				put("error", e.getMessage());
			}};
		}
	}
	
	// wayne
	// 20191231
	public static JsonNode getItemFromKey(Object obj, String key) {
		JsonNode node = mapper.valueToTree(obj);
		return node.findValue(key);
	}
	
	// wayne
	// 20191231
	public static List<Map<String, Object>> ListMapFromKey(Object obj, String key) {
		JsonNode json = getItemFromKey(obj, key);
		System.err.println("222222222222222   "+ json);
		List<Map<String, Object>> result = new ArrayList<>();
		json.forEach(j -> {
			result.add(
					mapper.convertValue(j, new TypeReference<Map<String, Object>>(){}));
		});
		
		return result;
	}
}
