package com.tp.tpigateway.modules.thsr.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.PGSMSRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.PGSMSService;

// wayne
// 20191226
@RestController
@RequestMapping("notifyflow")
public class NotifyFlowController extends BaseController {

    @Autowired
    PGSMSService pgsmsService;

    @RequestMapping(value = "/docreateneedinfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult create(@RequestBody PGSMSRequest botRequest) {
        return pgsmsService.create(botRequest);
    }
    
 // 20191227
    @RequestMapping(value = "/dosearchneedinfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult search(@RequestBody PGSMSRequest botRequest) {
        return pgsmsService.search(botRequest);
    }
}
