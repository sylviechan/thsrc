package com.tp.tpigateway.modules.thsr.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.CardRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.thsr.feign.ThsrApiFeignService;
import com.tp.tpigateway.modules.thsr.service.LFSService;
import com.tp.tpigateway.modules.thsr.utils.ObjectMapperUtils;

// wayne
// 20191227
@Service("lfsService")
public class LFSServiceImpl implements LFSService {

	@Autowired
    BotResponseUtils botResponseUtils;
	
	@Autowired
	ThsrApiFeignService thsrApiFeignService;
    
	//20191230
	@Override
	public TPIGatewayResult createOrder(CardRequest cardRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
//		Response response = HttpConnectionUtils.sendPost(
//				HttpConnectionUtils.THSR_LFS_API_URL,
//				toFakeLFSRequest(cardRequest),
//				MediaType.parse("APPLICATION_JSON"));
		
		// use spring cloud
		return tpiGatewayResult.setData(
				thsrApiFeignService.createLostForm(toFakeLFSRequest(cardRequest)));
	}
	
	private String toFakeLFSRequest(CardRequest cardRequest) {
		HashMap<String, String> result = new HashMap<>();
		result.put("psgName", cardRequest.getPsgName());
		result.put("psgTel", cardRequest.getPsgTel());
		result.put("pickupStationId", cardRequest.getPickupStationId());
		result.put("psgAddr", cardRequest.getPsgAddr());
		result.put("psgSex", cardRequest.getPsgSex());
		result.put("lostSite", cardRequest.getLostSite());
		result.put("lostStationId", cardRequest.getLostStationId());
		result.put("lostTrainNo", cardRequest.getLostTrainNo());
		result.put("lostSeatNo", cardRequest.getLostSeatNo());
		result.put("lostLocation", cardRequest.getLostLocation());
		result.put("lostDate", cardRequest.getLostDate());
		result.put("lostTime", cardRequest.getLostTime());
		result.put("lostObjectName", cardRequest.getLostObjectName());
		return ObjectMapperUtils.mapToJsonString(result);
	}

//	private HttpResponse sendPost(String url, String param,
//            ContentType contentType, HttpHost proxy) throws Exception {
//		CloseableHttpClient httpClient = null;
//        if (proxy == null) {
//            httpClient = HttpClientBuilder.create().build();
//        } else {
//            DefaultProxyRoutePlanner route = new DefaultProxyRoutePlanner(proxy);
//            httpClient = HttpClientBuilder.create().setRoutePlanner(route).build();
//        }
//
//        /* ==== POST with param ==== */
//        HttpPost httpPost = new HttpPost(url);
//        httpPost.setHeader("Content-type", contentType.getMimeType());
//        if (StringUtils.isNotEmpty(param)) {
//            httpPost.setEntity(new ByteArrayEntity(param.getBytes("UTF-8")));
//        }
//        /* ==== POST with param ==== */
////        List<NameValuePair> params = new ArrayList<NameValuePair>();
////        params.add(new BasicNameValuePair("username", ${username}));
////        params.add(new BasicNameValuePair("password", ${password));
////        httpPost.setEntity(new UrlEncodedFormEntity(params));
//
//        /* ==== POST with authorization ==== */
////        UsernamePasswordCredentials creds = new UsernamePasswordCredentials(${name}, ${password});
////        httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost, null));
//
//        /* ==== POST Multipart ==== */
////        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
////        builder.addTextBody("username", ${username});
////        builder.addTextBody("password", ${password);
////        builder.addBinaryBody("file", new File("test.txt"), ContentType.APPLICATION_OCTET_STREAM, "file.ext");
//
//        /* ==== Upload a File using HttpClient ==== */
////        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
////        builder.addBinaryBody("file", new File("test.txt"), ContentType.APPLICATION_OCTET_STREAM, "file.ext");
////        HttpEntity multipart = builder.build();
////        httpPost.setEntity(multipart);
//
//        CloseableHttpResponse response = httpClient.execute(httpPost);
////        int statusCode = response.getStatusLine().getStatusCode();
////        String message = response.getStatusLine().getReasonPhrase();
////        String responseString = "";
//
////        HttpEntity responseHttpEntity = response.getEntity();
////        InputStream content = responseHttpEntity.getContent();
////        BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
////        String line;
////        while ((line = buffer.readLine()) != null) {
////            responseString += line;
////        }
////        buffer.close();
////        httpClient.close();
//        return response;
//	}
	
}
