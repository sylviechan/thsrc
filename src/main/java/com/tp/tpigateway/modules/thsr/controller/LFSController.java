package com.tp.tpigateway.modules.thsr.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.CardRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.LFSService;

// wayne
// 20191227
@RestController
@RequestMapping("thsr/LFS")
public class LFSController extends BaseController {

    @Autowired
    LFSService lfsService;

    @RequestMapping(value = "/createOrder", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult createOrder(@RequestBody CardRequest cardRequest) {
        return lfsService.createOrder(cardRequest);
    }
}
