package com.tp.tpigateway.modules.thsr.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.FloatRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.thsr.service.FloatService;
import com.tp.tpigateway.modules.thsr.utils.ObjectMapperUtils;

/**
 * @author West
 * @version 建立時間:Dec 30, 2019 23:34:37 AM 類說明
 */
@Service("flaotService")
public class FloatServiceImpl implements FloatService {

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	Environment env;

	@Override
	public TPIGatewayResult actioninfo(FloatRequest floatRequest) {

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage();

		int action = floatRequest.getAction();

		switch (action) {
		case 302:
			String Date = floatRequest.getStartdate();
			String openDate = Date.substring(0, 8);

			Object requestBody = RequestContent(openDate);
			requestBody.toString();
			String url = "http://localhost:8180/TPIGateway/test/hpi";
			HttpResponse backdata = null;
			try {
				backdata = postRequest(url, requestBody);

				HttpEntity entity = backdata.getEntity();
				String responseString = EntityUtils.toString(entity, "UTF-8");
				Map<String, Object> responseMap = ObjectMapperUtils.jsonStringToMap(responseString);
				Map<String, Object> resultMap = (Map<String, Object>) responseMap.get("result");
				Map<String, Object> timeTableMap = (Map<String, Object>) resultMap.get("timeTable");
				String opDate = (String) timeTableMap.get("opDate");
				Map<String, Object> trainMap = (Map<String, Object>) timeTableMap.get("train");
				String direction = (String) trainMap.get("direction");
				String number = (String) trainMap.get("number");
				Integer nrCars = (Integer) trainMap.get("nrCars");
				List<Map<String, Object>> stationList = (List<Map<String, Object>>) trainMap.get("station");
				List<Map<String, Object>> stationDataList = new ArrayList<>();
				for (Map<String, Object> stationMap : stationList) {
					Map<String, Object> stationMapData = new HashMap<>();

					stationMapData.put("arrTime", (String) stationMap.get("arrTime"));
					stationMapData.put("code", (String) stationMap.get("code"));
					stationDataList.add(stationMapData);

				}
				Map<String, Object> basicFare = (Map<String, Object>) resultMap.get("basicFare");
				List<Map<String, Object>> recordsList = (List<Map<String, Object>>) basicFare.get("records");
				List<Map<String, Object>> basicFareDataList = new ArrayList<>();
				for (Map<String, Object> basicMap : recordsList) {
					Map<String, Object> basicFareMapData = new HashMap<>();
					basicFareMapData.put("fareName", (String) basicMap.get("fareName"));
					basicFareMapData.put("price", (String) basicMap.get("price"));
					stationDataList.add(basicFareMapData);

				}
				String widget = "TimeTable";
				botResponseUtils.setCardsResult(botMessageVO, action, widget, opDate, number, direction, nrCars,
						stationDataList, stationDataList);

				tpiGatewayResult.setData(botMessageVO);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		default:
			throw new RuntimeException("Must has Action attributes.");
		}

		return tpiGatewayResult;
	}

	public HttpResponse sendPost(String url, String param, ContentType contentType, HttpHost proxy) throws Exception {
		CloseableHttpClient httpClient = null;
		if (proxy == null) {
			httpClient = HttpClientBuilder.create().build();
		} else {
			DefaultProxyRoutePlanner route = new DefaultProxyRoutePlanner(proxy);
			httpClient = HttpClientBuilder.create().setRoutePlanner(route).build();
		}

		/* ==== POST with param ==== */
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Content-type", contentType.getMimeType());
		if (StringUtils.isNotEmpty(param)) {
			httpPost.setEntity(new ByteArrayEntity(param.getBytes("UTF-8")));
		}
		/* ==== POST with param ==== */
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("username", ${username}));
//        params.add(new BasicNameValuePair("password", ${password));
//        httpPost.setEntity(new UrlEncodedFormEntity(params));

		/* ==== POST with authorization ==== */
//        UsernamePasswordCredentials creds = new UsernamePasswordCredentials(${name}, ${password});
//        httpPost.addHeader(new BasicScheme().authenticate(creds, httpPost, null));

		/* ==== POST Multipart ==== */
//        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//        builder.addTextBody("username", ${username});
//        builder.addTextBody("password", ${password);
//        builder.addBinaryBody("file", new File("test.txt"), ContentType.APPLICATION_OCTET_STREAM, "file.ext");

		/* ==== Upload a File using HttpClient ==== */
//        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//        builder.addBinaryBody("file", new File("test.txt"), ContentType.APPLICATION_OCTET_STREAM, "file.ext");
//        HttpEntity multipart = builder.build();
//        httpPost.setEntity(multipart);

		CloseableHttpResponse response = httpClient.execute(httpPost);
		int statusCode = response.getStatusLine().getStatusCode();
		String message = response.getStatusLine().getReasonPhrase();
		String responseString = "";

//        HttpEntity responseHttpEntity = response.getEntity();
//        InputStream content = responseHttpEntity.getContent();
//        BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
//        String line;
//        while ((line = buffer.readLine()) != null) {
//            responseString += line;
//        }
//        buffer.close();
//        httpClient.close();
		return response;
	}

	public HttpResponse postRequest(String urlString, Object body) throws UnsupportedEncodingException {
		HttpPost httpPost = new HttpPost(urlString);

		CloseableHttpClient httpClient = HttpClients.custom().build();
//		   header.
//		httpPost.addHeader("serviceToken", "2885f056-875f-42a3-ac55-66d037c1b469");
		StringEntity stringEntity = new StringEntity(body.toString());
		httpPost.setEntity(stringEntity);
//		ResponseHandler<String> responseHandler = response -> {
//			HttpEntity entity = response.getEntity();
//			return entity != null ? EntityUtils.toString(stringEntity) : null;
//		};
		HttpResponse result = null;
		try {
			result = httpClient.execute(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

//組成發送到高鐵request
	private Object RequestContent(String opDate) {

		Map<String, Object> aicsMap = new HashMap<>();
		Map<String, Object> header = new HashMap<>();
		Map<String, Object> parameter = new HashMap<>();

		header.put("clientIp", "123.123.123.123");
		header.put("timestamp", "20190731112526543");
		header.put("requestToken", "28288065-891f-4cdc-8bb0-3c27cba7c9ad");

		parameter.put("opDate", opDate);
		aicsMap.put("header", header);

		aicsMap.put("parameter", parameter);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = "";
		try {
			json = objectMapper.writeValueAsString(aicsMap);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return json;

	}

}
