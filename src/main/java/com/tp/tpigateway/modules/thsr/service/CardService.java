package com.tp.tpigateway.modules.thsr.service;

import com.tp.common.model.CardRequest;
import com.tp.common.model.TPIGatewayResult;

//wayne 
//20191225
public interface CardService {

	TPIGatewayResult card(CardRequest cardRequest);
    
}
