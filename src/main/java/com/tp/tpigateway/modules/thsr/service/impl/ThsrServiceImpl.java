package com.tp.tpigateway.modules.thsr.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathayins.model.InsQuickReplay;
import com.tp.tpigateway.modules.thsr.model.ThsrQuickReplay;
import com.tp.tpigateway.modules.thsr.service.ThsrService;

@Service("thsrService")
public class ThsrServiceImpl implements ThsrService {

    @Autowired
    BotResponseUtils botResponseUtils;

    @Autowired
    Environment env;

    private static final String LOSS_STEP_1_Q = "請問您先前有近線客服中心或是透過文字客服/車站/列車人員報失過嗎?";
    private static final String LOSS_STEP_1_1 = "承辦人員若有尋獲符合您敘述的遺失物，會致電您進行確認。感謝您的耐心等候!";
    private static final String LOSS_STEP_1_2 = "好的，我立刻為您處理，請問貴姓大名";
    private static final String LOSS_STEP_2 = "%s您好，未進行身分驗證，請輸入可收簡訊的手機號碼，若不方便提供，登陸資訊後可選擇由真人為您完成報失程序";
    private static final String LOSS_STEP_CANCEL = "好的，請問還需要其他服務嗎?";
    private static final Map<String, QuickReplay> INTENT_QR_MAP;

    static {
        INTENT_QR_MAP = new HashMap<>();
        INTENT_QR_MAP.put("已報失", ThsrQuickReplay.LOSS_STEP_1_1);
        INTENT_QR_MAP.put("未報失", ThsrQuickReplay.LOSS_STEP_1_2);
    }
    
    @Override
    public TPIGatewayResult hello(BotRequest botRequest) {
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());
        List<String> msgList = new ArrayList<String>();
        List<QuickReplay> optionList = new ArrayList<QuickReplay>();
        
        botResponseUtils.setTextResult(botMessageVO, "你是在哈囉?");
      
        // 牌卡
        botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getInitCardList(botMessageVO, msgList, optionList, botRequest.getSsoId()));

        tpiGatewayResult.setMsgList(msgList);
        tpiGatewayResult.setOptionList(optionList);

        return tpiGatewayResult.setData(botMessageVO);
    }

    @Override
    public TPIGatewayResult fallback(FallBackRequest fallBackRequest) {
        BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());

        TPIGatewayResult defaultTpiResult = TPIGatewayResult.ok();
        TPIGatewayResult tpiGatewayResult = null;


        String firstNluCanAnswer = fallBackRequest.getFirstNluCanAnswer();

        if (StringUtils.equals(firstNluCanAnswer, "Y")) { //總機聽的懂, secondBot聽不懂

            String fallbackNlu = StringUtils.defaultString(fallBackRequest.getFallbackNlu(), "");

            defaultTpiResult.put(TPIConst.SECOND_BOT_ASK, "Y");

            switch (fallbackNlu) {
                case "保單_操作":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo);
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.保單_查詢, InsQuickReplay.保單_變更, InsQuickReplay.保單_申請);
                    break;
                case "保單_查詢_項目":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo);
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.保單_查詢, InsQuickReplay.保單_變更, InsQuickReplay.保單_申請);
                    break;
                case "保單_變更_項目":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo);
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.保單_查詢, InsQuickReplay.保單_變更, InsQuickReplay.保單_申請);
                    break;
                case "商品_操作":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.商品_諮詢);
                    break;
                case "商品_諮詢_項目":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.商品_諮詢);
                    break;
                case "會員_操作":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.會員_申請);
                    break;
                case "理賠_操作":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.理賠_查詢, InsQuickReplay.理賠_申請);
                    break;
                case "道路救援_操作":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.道路救援_查詢, InsQuickReplay.道路救援_申請);
                    break;
                default:
                    tpiGatewayResult = handleDefaultFallback(fallBackRequest);
                    break;
            }
        } else {
            tpiGatewayResult = handleIntentAndFaqCard(fallBackRequest);
        }

        if (tpiGatewayResult != null) {
            return tpiGatewayResult;
        }

        return defaultTpiResult.setData(vo);
    }

    /*
        組裝intentCard+FAQ牌卡
        "intents": [
            {
                "intent_id": 1313,
                "intent": "保單",
                "score": 0.8903623599411904
            },
        ]

        "results": [
            {
                "similar_questions": [
                    "定期定額契約能變動嗎",
                    "改定期(不)定額日期",
                    "改定期(不)定額金額",
                    "如何復扣",
                    "我想異動定時定額",
                    "可以更動定時定額嗎",
                    "變更定時定額的申請書哪裡下載",
                    "我要如何取得定期(不)定額授權變更申請書",
                    "我能在e家人系統改定期定額嗎",
                    "定期定額契約能動嗎"
                ],
                "id_db": 3688,
                "answer": "可以來電索取或至國泰投信網站下載『定期(不)定額授權變更申請書』，填妥變更的項目，加蓋原留印鑑，於收件截止日期前，將正本傳真或寄達國泰投信辦理。\n有辦理電子交易的客戶可至e家人系統內變更",
                "score": 0.4303687555132756,
                "keywords": [
                    "定時定額",
                    "書面"
                ],
                "orig_question": "變更定期(不)定額契約",
                "question": "我想異動定時定額",
                "id": 22,
                "categories": [
                    "暫停",
                    "扣款",
                    "基金",
                    "不定額",
                    "停扣",
                    "定期",
                    "恢復",
                    "變更",
                    "終止",
                    "定額"
                ]
            },
        ],
     */
    private TPIGatewayResult handleIntentAndFaqCard(FallBackRequest fallBackRequest) {
        BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String msg = fallBackRequest.getMsg();
        List<Map<String, Object>> firstNluIntents = fallBackRequest.getFirstNluIntents();
        List<Map<String, Object>> faqResult = fallBackRequest.getFaqResult();

        List<Map<String, Object>> cardList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(firstNluIntents)) {
            List<QuickReplay> intentCardQRs = new ArrayList<>();

            for (Map map : firstNluIntents) {

                String score = MapUtils.getString(map, "score");

                if (NumberUtils.toDouble(score) <= NumberUtils.toDouble(PropUtils.getCathayInsScoreNluMin())) {
                    continue;
                }

                String intent = MapUtils.getString(map, "intent");
                if (StringUtils.equals(intent, "Hello") || StringUtils.equals(intent, "End") || StringUtils.equals(intent, "轉真人")) {
                    continue;
                }

                intentCardQRs = new ArrayList<>();
                intentCardQRs.add(INTENT_QR_MAP.get(intent));

                if (intentCardQRs.size() >= 3) {
                    break;
                }
            }

            if (intentCardQRs.size() > 0) {
                CardItem cards = buildBaseCard(vo, "card04.png", intentCardQRs.toArray(new QuickReplay[intentCardQRs.size()]));
                cardList.add(cards.getCards());
            }
        }

        if (CollectionUtils.isNotEmpty(faqResult)) {
            //card06.png
            List<QuickReplay> faqCardQRs = new ArrayList<>();

            for (Map map : faqResult) {

                String score = MapUtils.getString(map, "score");

                if (NumberUtils.toDouble(score) < NumberUtils.toDouble(PropUtils.getCathayInsScoreFaqMin())) {
                    continue;
                }

                String text = MapUtils.getString(map, "question");
                String alt = MapUtils.getString(map, "answer");
                //看需求是否需要過濾不顯示的categorie //TODO 要過濾後面是@2以上的
                //List<String> categories = (List<String>) MapUtils.getObject(map, "categories");

                QuickReplay q = new InsQuickReplay(10, text, alt);
                faqCardQRs.add(q);

                if (faqCardQRs.size() >= 3) {
                    break;
                }
            }

            if (faqCardQRs.size() > 0) {
                CardItem cards = buildBaseCard(vo, "card06.png", faqCardQRs.toArray(new QuickReplay[faqCardQRs.size()]));
                cardList.add(cards.getCards());
            }
        }

        if (cardList.size() > 0) {
            botResponseUtils.setTextResult(vo, "阿發猜想你要的答案是");
            // 牌卡
            botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);

            tpiGatewayResult.put(TPIConst.ASK_CARD, "Y");

            return tpiGatewayResult.setData(vo);
        } else {
            return handleDefaultFallback(fallBackRequest);
        }
    }

    private CardItem buildBaseCard(BotMessageVO vo, String image, QuickReplay... quickReplays) {
        CardItem cards = vo.new CardItem();
        cards.setCName("");
        cards.setCWidth(BotMessageVO.CWIDTH_200);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CC);
        cImageData.setCImageUrl(image);
        cards.setCImageData(cImageData.getCImageDatas());

        cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplays));

        return cards;
    }

    private TPIGatewayResult handleDefaultFallback(FallBackRequest fallBackRequest) {
        BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        StringBuilder sb = new StringBuilder();
        sb.append("你要問「")
                .append(fallBackRequest.getMsg())
//                .append("(")
//                .append(fallBackRequest.getSecondBot())
//                .append(")")
                .append("」什麼相關內容呢？你可以描述得更完整一些，我會比較容易理解喔！");

        botResponseUtils.setTextResult(vo, sb.toString());

        tpiGatewayResult.put(TPIConst.CAN_NOT_ANS, "Y");

        return tpiGatewayResult.setData(vo);
    }

    @Override
    public TPIGatewayResult handleFaqReplay(Map<String, Object> request) {
        BotMessageVO vo = botResponseUtils.initBotMessage(MapUtils.getString(request, "channel"), MapUtils.getString(request, "role"));

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String faqAns = MapUtils.getString(request, "faqAns");

        String splitMark = env.getProperty("faq.ans.split", "");

        if (StringUtils.isBlank(splitMark)) {
            botResponseUtils.setTextResult(vo, faqAns);
        }
        else {
            String[] faqAnsList = faqAns.split(splitMark);

            for (String s : faqAnsList) {
                botResponseUtils.setTextResult(vo, s);
            }
        }

        return tpiGatewayResult.setData(vo);
    }


    public TPIGatewayResult getTopquestion(String topQuestionType) {
        //投保、理賠、道路救援

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
        BotMessageVO vo = botResponseUtils.initBotMessage("chatWeb", "alpha");

        botResponseUtils.doTopQuestion(vo);

        if (StringUtils.equals("投保", topQuestionType)) {
            botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.TOP_QUESTION_1);
        } else if (StringUtils.equals("理賠", topQuestionType)) {
            botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.TOP_QUESTION_2);

        } else if (StringUtils.equals("道路救援", topQuestionType)) {
            botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.TOP_QUESTION_3);
        }

        return tpiGatewayResult.setData(vo);
    }

    @Override
    public TPIGatewayResult supportNLU(Map<String, Object> request) {
        BotMessageVO vo = botResponseUtils.initBotMessage(MapUtils.getString(request, "channel"), MapUtils.getString(request, "role"));

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String operationIntent = MapUtils.getString(request, "operationIntent", "");

        switch (operationIntent) {
            //健傷險 旅綜險 查詢 檢查衝突 申請 諮詢 變更 車險
            case "健傷險":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.商品_諮詢, InsQuickReplay.保單_查詢);
                break;
            case "旅綜險":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.商品_諮詢, InsQuickReplay.保單_查詢);
                break;
            case "車險":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.商品_諮詢, InsQuickReplay.保單_查詢);
                break;
            case "檢查衝突":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.商品_諮詢, InsQuickReplay.保單_查詢);
                break;
            case "變更":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.保單_變更);
                break;
            case "諮詢":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.商品_諮詢);
                break;
            case "查詢":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.理賠_查詢, InsQuickReplay.保單_查詢, InsQuickReplay.道路救援_查詢);
                break;
            case "申請":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.理賠_申請, InsQuickReplay.道路救援_查詢, InsQuickReplay.保單_申請);
                break;
            default:
                String msg = MapUtils.getString(request, "msg");
                StringBuilder sb = new StringBuilder();
                sb.append("你要問「")
                        .append(msg)
                        .append("」什麼相關內容呢？你可以描述得更完整一些，我會比較容易理解喔！");
                botResponseUtils.setTextResult(vo, sb.toString());

                //botResponseUtils.setTextResult(vo, "TODO 服務台反問 : " + operationIntent);
                break;
        }

        return tpiGatewayResult.setData(vo);
    }

    private List<Map<String, Object>> getInitCardList(BotMessageVO vo, List<String> msgList, List<QuickReplay> optionList, String ssoId) {
        List<Map<String, Object>> cardList = new ArrayList<>();

//        // card-熱門問題
//        CardItem cards1 = vo.new CardItem();
//        cards1.setCName("");
//        cards1.setCWidth(BotMessageVO.CWIDTH_200);
//
//        CImageDataItem cImageData1 = vo.new CImageDataItem();
//        cImageData1.setCImage(BotMessageVO.IMG_CC);
//        cImageData1.setCImageUrl("card-Ins-hot@2x.png");
//        cards1.setCImageData(cImageData1.getCImageDatas());
//
//        cards1.setCTextType("5");
//        cards1.setCTitle("熱門問題");
//        List<Map<String, Object>> cText1List = new ArrayList<Map<String, Object>>();
//        CTextItem cText1 = vo.new CTextItem();
//        cText1.setCText("最常被問到的產險相關問題，是不是也有你關心的呢？");
//        cText1List.add(cText1.getCTexts());
//        cards1.setCTexts(cText1List);
//
//        cards1.setCLinkList(botResponseUtils.getCLinkList(vo, InsQuickReplay.TOP_QUESTION_1, InsQuickReplay.TOP_QUESTION_2, InsQuickReplay.TOP_QUESTION_3));
//
//        msgList.add(InsQuickReplay.TOP_QUESTION_1.getText());
//        msgList.add(InsQuickReplay.TOP_QUESTION_2.getText());
//        msgList.add(InsQuickReplay.TOP_QUESTION_3.getText());
//        optionList.add(InsQuickReplay.TOP_QUESTION_1);
//        optionList.add(InsQuickReplay.TOP_QUESTION_2);
//        optionList.add(InsQuickReplay.TOP_QUESTION_3);
//
//        // card-商品介紹
//        CardItem cards2 = vo.new CardItem();
//        cards2.setCName("");
//        cards2.setCWidth(BotMessageVO.CWIDTH_200);
//
//        CImageDataItem cImageData2 = vo.new CImageDataItem();
//        cImageData2.setCImage(BotMessageVO.IMG_CC);
//        cImageData2.setCImageUrl("card-b39.jpg");
//        cards2.setCImageData(cImageData2.getCImageDatas());
//
//        cards2.setCTextType("5");
//        cards2.setCTitle("商品介紹");
//        List<Map<String, Object>> cText2List = new ArrayList<Map<String, Object>>();
//        CTextItem cText2 = vo.new CTextItem();
//        cText2.setCText("各種產險商品都在這喔！");
//        cText2List.add(cText2.getCTexts());
//        cards2.setCTexts(cText2List);
//
//        Map<String, Object> parameter = new HashMap<String, Object>();
//        parameter.put("commodityType", "PC01_A");
//        cards2.setCLinkList(botResponseUtils.getCLinkList(vo, InsQuickReplay.HELLO_M01_1(ssoId),
//                InsQuickReplay.HELLO_M01_2(parameter), InsQuickReplay.HELLO_M01_3(ssoId), InsQuickReplay.HELLO_M01_4(ssoId)));
//
//        msgList.add(InsQuickReplay.HELLO_M01_2(parameter).getText());
//        optionList.add(InsQuickReplay.HELLO_M01_2(parameter));
//
//        // card-保單查詢
//        CardItem cards3 = vo.new CardItem();
//        cards3.setCName("");
//        cards3.setCWidth(BotMessageVO.CWIDTH_200);
//
//        CImageDataItem cImageData3 = vo.new CImageDataItem();
//        cImageData3.setCImage(BotMessageVO.IMG_CC);
//        cImageData3.setCImageUrl("policy_02-01.jpg");
//        cards3.setCImageData(cImageData3.getCImageDatas());
//
//        cards3.setCTextType("5");
//        cards3.setCTitle("保單查詢");
//        List<Map<String, Object>> cText3List = new ArrayList<Map<String, Object>>();
//        CTextItem cText3 = vo.new CTextItem();
//        cText3.setCText("已經投保的保單內容都在這裡~");
//        cText3List.add(cText3.getCTexts());
//        cards3.setCTexts(cText3List);
//
//        cards3.setCLinkType(1);
//        cards3.setCLinkList(botResponseUtils.getCLinkList(vo, InsQuickReplay.HELLO_A05_1, InsQuickReplay.HELLO_A05_2, InsQuickReplay.HELLO_A05_3, InsQuickReplay.HELLO_A05_4));
//
//        msgList.add(InsQuickReplay.HELLO_A05_1.getText());
//        msgList.add(InsQuickReplay.HELLO_A05_2.getText());
//        msgList.add(InsQuickReplay.HELLO_A05_3.getText());
//        msgList.add(InsQuickReplay.HELLO_A05_4.getText());
//        optionList.add(InsQuickReplay.HELLO_A05_1);
//        optionList.add(InsQuickReplay.HELLO_A05_2);
//        optionList.add(InsQuickReplay.HELLO_A05_3);
//        optionList.add(InsQuickReplay.HELLO_A05_4);
//
//        // card-道路救援
//        CardItem cards4 = vo.new CardItem();
//        cards4.setCName("");
//        cards4.setCWidth(BotMessageVO.CWIDTH_200);
//
//        CImageDataItem cImageData4 = vo.new CImageDataItem();
//        cImageData4.setCImage(BotMessageVO.IMG_CC);
//        cImageData4.setCImageUrl("parking02-01.jpg");
//        cards4.setCImageData(cImageData4.getCImageDatas());
//
//        cards4.setCTitle("道路救援");
//        List<Map<String, Object>> cText4List = new ArrayList<Map<String, Object>>();
//        CTextItem cText4 = vo.new CTextItem();
//        cText4.setCText("阿發可以幫你查剩餘次數，以及呼叫道路救援哦~");
//        cText4List.add(cText4.getCTexts());
//        cards4.setCTexts(cText4List);
//
//        cards4.setCLinkList(botResponseUtils.getCLinkList(vo, InsQuickReplay.HELLO_C01_1, InsQuickReplay.HELLO_C01_2));
//
//        msgList.add(InsQuickReplay.HELLO_C01_1.getText());
//        msgList.add(InsQuickReplay.HELLO_C01_2.getText());
//        optionList.add(InsQuickReplay.HELLO_C01_1);
//        optionList.add(InsQuickReplay.HELLO_C01_2);
//
//        // card-保單批改
//        CardItem cards5 = vo.new CardItem();
//        cards5.setCName("");
//        cards5.setCWidth(BotMessageVO.CWIDTH_200);
//
//        CImageDataItem cImageData5 = vo.new CImageDataItem();
//        cImageData5.setCImage(BotMessageVO.IMG_CC);
//        cImageData5.setCImageUrl("policy_04.jpg");
//        cards5.setCImageData(cImageData5.getCImageDatas());
//
//        cards5.setCTitle("保單批改");
//        List<Map<String, Object>> cText5List = new ArrayList<Map<String, Object>>();
//        CTextItem cText5 = vo.new CTextItem();
//        cText5.setCText("要改資料嗎？阿發可以幫忙喔~");
//        cText5List.add(cText5.getCTexts());
//        cards5.setCTexts(cText5List);
//
//        cards5.setCLinkList(botResponseUtils.getCLinkList(vo, InsQuickReplay.T01_1_1, InsQuickReplay.T01_1_2, InsQuickReplay.T01_1_3, InsQuickReplay.T01_1_4, InsQuickReplay.T01_1_5));
//
//        msgList.add(InsQuickReplay.T01_1_1.getText());
//        msgList.add(InsQuickReplay.T01_1_2.getText());
//        msgList.add(InsQuickReplay.T01_1_3.getText());
//        msgList.add(InsQuickReplay.T01_1_4.getText());
//        msgList.add(InsQuickReplay.T01_1_5.getText());
//        optionList.add(InsQuickReplay.T01_1_1);
//        optionList.add(InsQuickReplay.T01_1_2);
//        optionList.add(InsQuickReplay.T01_1_3);
//        optionList.add(InsQuickReplay.T01_1_4);
//        optionList.add(InsQuickReplay.T01_1_5);
//
//        // card-理賠辦理
//        CardItem cards6 = vo.new CardItem();
//        cards6.setCName("");
//        cards6.setCWidth(BotMessageVO.CWIDTH_200);
//
//        CImageDataItem cImageData6 = vo.new CImageDataItem();
//        cImageData6.setCImage(BotMessageVO.IMG_CC);
//        cImageData6.setCImageUrl("policy_05.jpg");
//        cards6.setCImageData(cImageData6.getCImageDatas());
//
//        cards6.setCTitle("理賠辦理");
//        List<Map<String, Object>> cText6List = new ArrayList<Map<String, Object>>();
//        CTextItem cText6 = vo.new CTextItem();
//        cText6.setCText("理賠辦理、理賠進度與記錄查詢的方式都在這裡~");
//        cText6List.add(cText6.getCTexts());
//        cards6.setCTexts(cText6List);
//
//        InsQuickReplay claim_q1 = new InsQuickReplay(1, "理賠辦理", "我要申請理賠", "", "", "");
//        InsQuickReplay claim_q2 = new InsQuickReplay(1, "進度與記錄查詢", "我要查理賠進度", "", "", "");
//
//        cards6.setCLinkList(botResponseUtils.getCLinkList(vo, claim_q1, claim_q2));
//
//        msgList.add(claim_q1.getText());
//        msgList.add(claim_q2.getText());
//        optionList.add(claim_q1);
//        optionList.add(claim_q2);

        //waynewayne
        //card7
        CardItem cards7 = vo.new CardItem();
        cards7.setCName("");
        cards7.setCWidth(BotMessageVO.CWIDTH_200);
        
        CImageDataItem cImageData7 = vo.new CImageDataItem();
        cImageData7.setCImage(BotMessageVO.IMG_CB03_ROUND);
        cImageData7.setCImageUrl("oct.jpg");
        cards7.setCImageData(cImageData7.getCImageDatas());
        
        cards7.setCTitle("哈囉哈囉");
        List<Map<String, Object>> cText7List = new ArrayList<Map<String, Object>>();
        CTextItem cText7 = vo.new CTextItem();
        cText7.setCText("今天很嗆是吧?");
        cText7List.add(cText7.getCTexts());
        cards7.setCTexts(cText7List);
        
        cards7.setCLinkList(botResponseUtils.getCLinkList(vo, ThsrQuickReplay.WAYNE_1, ThsrQuickReplay.WAYNE_2));
        
//        msgList.add(InsQuickReplay.T02_1_4.getText());
//        msgList.add(InsQuickReplay.T01_1_3.getText());
//        optionList.add(InsQuickReplay.T02_1_2);
//        optionList.add(InsQuickReplay.T01_1_2);
//        optionList.add(InsQuickReplay.T02_1_3);
//        optionList.add(InsQuickReplay.T02_1_4);
//        optionList.add(InsQuickReplay.T02_1_5);
        
//        cardList.add(cards1.getCards());
//        cardList.add(cards2.getCards());
//        cardList.add(cards3.getCards());
//        cardList.add(cards4.getCards());
//        cardList.add(cards5.getCards());
//        cardList.add(cards6.getCards());
        cardList.add(cards7.getCards());

        return cardList;
    }

    @Override
    public BotMessageVO showSurvey(BotRequest botRequest) {
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());

        botResponseUtils.setSurvey(botMessageVO);

        return botMessageVO;
    }

	@Override
	public TPIGatewayResult lossStep1(BotRequest botRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());
        // 訊息
        botResponseUtils.setTextResult(botMessageVO, LOSS_STEP_1_Q);
        
        // 按鈕清單
        botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, ThsrQuickReplay.LOSS_STEP_1_1, ThsrQuickReplay.LOSS_STEP_1_2);
        
		return tpiGatewayResult.setData(botMessageVO);
	}

	@Override
	public TPIGatewayResult lossStep1_1(BotRequest botRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		return tpiGatewayResult.setData(onlyText(botRequest, LOSS_STEP_1_1));
	}

	@Override
	public TPIGatewayResult lossStep1_2(BotRequest botRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		return tpiGatewayResult.setData(onlyText(botRequest, LOSS_STEP_1_2));
	}

	@Override
	public TPIGatewayResult lossStep2(BotRequest botRequest) {
		String name = botRequest.getMsg().substring(5);
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = onlyText(botRequest, String.format(LOSS_STEP_2, name));
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, ThsrQuickReplay.LOSS_STEP_2_1, ThsrQuickReplay.LOSS_STEP_2_2);
		
		return tpiGatewayResult.setData(botMessageVO);
	}
	
	private BotMessageVO onlyText(BotRequest botRequest, String text) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());
        // 訊息
        botResponseUtils.setTextResult(botMessageVO,text);
		return botMessageVO;
	}

	@Override
	public TPIGatewayResult lossStepCancel(BotRequest botRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = onlyText(botRequest, LOSS_STEP_CANCEL);
		
		// 牌卡
        botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getInitCardList(botMessageVO, null,null, botRequest.getSsoId()));
		
		return tpiGatewayResult.setData(botMessageVO);
	}
}
