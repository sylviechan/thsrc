package com.tp.tpigateway.modules.thsr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.PGSMSRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.thsr.fakeData.PGSMS;
import com.tp.tpigateway.modules.thsr.service.PGSMSService;

// wayne
// 20191226
@Service("pgsmsSService")
public class PGSMSServiceImpl implements PGSMSService {

	@Autowired
	BotResponseUtils botResponseUtils;
	
	@Override
	public TPIGatewayResult create(PGSMSRequest pgsmsRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		
		return tpiGatewayResult.setData(PGSMS.create());
	}
	
	@Override
	public TPIGatewayResult search(PGSMSRequest botRequest) {
		// TODO Auto-generated method stub
		return null;
	}

}
