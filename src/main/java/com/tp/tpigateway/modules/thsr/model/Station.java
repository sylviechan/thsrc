package com.tp.tpigateway.modules.thsr.model;
/**
* @author West
* @version 建立時間:Dec 26, 2019 3:37:36 PM
* 類說明
*/
public class Station {

	private String arrTime;
	private String depTime;
	private String seq;
	private String code;
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public String getDepTime() {
		return depTime;
	}
	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
