package com.tp.tpigateway.modules.thsr.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathayins.model.InsQuickReplay;
import com.tp.tpigateway.modules.thsr.model.ThsrQuickReplay;
import com.tp.tpigateway.modules.thsr.service.PtoponlineService;
import com.tp.tpigateway.modules.thsr.service.TagService;
import com.tp.tpigateway.modules.thsr.service.ThsrService;

@Service("ptoponlineService")
public class PtoponlineServiceImpl implements PtoponlineService {

    @Autowired
    BotResponseUtils botResponseUtils;

    @Autowired
    Environment env;

    private static final String LOSS_STEP_1_Q = "請問您先前有近線客服中心或是透過文字客服/車站/列車人員報失過嗎?";
    private static final String LOSS_STEP_1_1 = "承辦人員若有尋獲符合您敘述的遺失物，會致電您進行確認。感謝您的耐心等候!";
    private static final String LOSS_STEP_1_2 = "好的，我立刻為您處理，請問貴姓大名";
    private static final String LOSS_STEP_2 = "%s您好，未進行身分驗證，請輸入可收簡訊的手機號碼，若不方便提供，登陸資訊後可選擇由真人為您完成報失程序";
    private static final String LOSS_STEP_CANCEL = "好的，請問還需要其他服務嗎?";
    private static final Map<String, QuickReplay> INTENT_QR_MAP;

    static {
        INTENT_QR_MAP = new HashMap<>();
        INTENT_QR_MAP.put("已報失", ThsrQuickReplay.LOSS_STEP_1_1);
        INTENT_QR_MAP.put("未報失", ThsrQuickReplay.LOSS_STEP_1_2);
    }
    
    @Override
    public TPIGatewayResult hello(BotRequest botRequest) {
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());
        List<String> msgList = new ArrayList<String>();
        List<QuickReplay> optionList = new ArrayList<QuickReplay>();
        
        botResponseUtils.setTextResult(botMessageVO, "你是在哈囉?");
      
        // 牌卡
//        botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getInitCardList(botMessageVO, msgList, optionList, botRequest.getSsoId()));

        tpiGatewayResult.setMsgList(msgList);
        tpiGatewayResult.setOptionList(optionList);

        return tpiGatewayResult.setData(botMessageVO);
    }

}
