package com.tp.tpigateway.modules.thsr.controller;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.LoginRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.LoginService;

// wayne
// 20191230
// aviod duplicate in cathaysec, maybe comment cathaysec later
@RestController("LoginController") 
@RequestMapping("thsr/login")
public class LoginController extends BaseController {

    @Autowired
    LoginService loginService;

    @RequestMapping(value = "/sendSMS", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void sendSMS(@RequestBody Map<String, String> params) {
        loginService.sendSMS(params);
    }
    
    @RequestMapping(value = "/actioninfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult loginActioninfo(@RequestBody LoginRequest loginRequest) {
       return  loginService.loginActioninfo(loginRequest);
    }
  
}
