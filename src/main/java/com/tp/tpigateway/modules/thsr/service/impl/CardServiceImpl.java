package com.tp.tpigateway.modules.thsr.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.CardRequest;
import com.tp.common.model.Item;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.thsr.model.ThsrQuickReplay;
import com.tp.tpigateway.modules.thsr.service.CardService;

//wayne 
// 20191225
@Service("thsrCardService")
public class CardServiceImpl implements CardService {

	@Autowired
    BotResponseUtils botResponseUtils;
	
	@Override
	public TPIGatewayResult card(CardRequest cardRequest) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessageForThsrCard(cardRequest.getChannel());
		List<Map<String, Object>> cardList;
		int action = cardRequest.getAction();
		switch(action) {
			case 200:
				cardList = getCardListFromCardAction200Request(botMessageVO, cardRequest);
				break;
			case 203:
				cardList = getCardListFromCardAction203Request(botMessageVO, cardRequest);
				break;
			default:
				throw new RuntimeException("Must has Action attributes.");
		}
		botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, cardList);
		
		return tpiGatewayResult.setData(botMessageVO);
	}
	
	// 20191228
	// 因應規格改變增加屬性
	private List<Map<String, Object>> getCardListFromCardAction200Request(BotMessageVO vo, CardRequest cardRequest) {
		
		List<Map<String, Object>> cardList = new ArrayList<>();
		
		for(Item item:cardRequest.getItemtype()) {
			CardItem card = vo.new CardItem();
	        card.setCWidth(BotMessageVO.CWIDTH_200);
	        card.setCTextType("2");
	        card.setCTitle(item.getTitle());
	        card.setCMessage("");
	        // 要加上ctexts 才會顯示文字
	        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
	        card.setCTexts(cTextList);
	        card.setCLinkType(1);
	        List<QuickReplay> quickReplay = item.getItemsname().stream()
	        		.map(name -> new ThsrQuickReplay(15, name, name))
	        		.collect(Collectors.toList());
		    card.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplay));
		    
		    // 20191228
		    //  新增圖像資訊
		    CImageDataItem cImageData = vo.new CImageDataItem();
	        cImageData.setCImageType(1);
	        cImageData.setCImageUrl("avatar02_2x.png");
	        card.setCImageData(cImageData.getCImageDatas());
	        
		    cardList.add(card.getCards());
		}
        return cardList;
	}
	
	private List<Map<String, Object>> getCardListFromCardAction203Request(BotMessageVO vo, CardRequest cardRequest) {
		
		List<Map<String, Object>> cardList = new ArrayList<>();
		
		CardItem card = vo.new CardItem();
	    card.setCWidth(BotMessageVO.CWIDTH_200);
	    card.setCTextType("99");    
	    card.setCTitle("遺失物報失資料");
	    card.setCMessage("");
	    
	    List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText2 = vo.new CTextItem();
        cText2.setCLabel("旅客完整姓名:");
        cText2.setCText(cardRequest.getPsgName());
        CTextItem cText3 = vo.new CTextItem();
        cText3.setCLabel("旅客市話號碼:");
        cText3.setCText(cardRequest.getPsgTel());
        CTextItem cText4 = vo.new CTextItem();
        cText4.setCLabel("方便領取的車站:");
        cText4.setCText(cardRequest.getPickupStationId());
        CTextItem cText5 = vo.new CTextItem();
        cText5.setCLabel("地址:");
        cText5.setCText(cardRequest.getPsgAddr());
        CTextItem cText6 = vo.new CTextItem();
        cText6.setCLabel("性別:");
        cText6.setCText(cardRequest.getPsgSex());
        CTextItem cText7 = vo.new CTextItem();
        cText7.setCLabel("遺失地點:");
        cText7.setCText(cardRequest.getLostSite());
        CTextItem cText8 = vo.new CTextItem();
        cText8.setCLabel("遺失發生的車站:");
        cText8.setCText(cardRequest.getLostStationId());
        CTextItem cText9 = vo.new CTextItem();
        cText9.setCLabel("遺失列車車次:");
        cText9.setCText(cardRequest.getLostTrainNo());
        CTextItem cText10 = vo.new CTextItem();
        cText10.setCLabel("遺失座位號碼:");
        cText10.setCText(cardRequest.getLostSeatNo());
        CTextItem cText11 = vo.new CTextItem();
        cText11.setCLabel("遺失位置:");
        cText11.setCText(cardRequest.getLostLocation());
        CTextItem cText12 = vo.new CTextItem();
        cText12.setCLabel("遺失日期:");
        cText12.setCText(cardRequest.getLostDate());
        CTextItem cText13 = vo.new CTextItem();
        cText13.setCLabel("遺失時間:");
        cText13.setCText(cardRequest.getLostTime());
        CTextItem cText14 = vo.new CTextItem();
        cText14.setCLabel("遺失物內容:");
        cText14.setCText(cardRequest.getLostObjectName());
        
	    cTextList.add(cText2.getCTexts());
	    cTextList.add(cText3.getCTexts());
	    cTextList.add(cText4.getCTexts());
	    cTextList.add(cText5.getCTexts());
	    cTextList.add(cText6.getCTexts());
	    cTextList.add(cText7.getCTexts());
	    cTextList.add(cText8.getCTexts());
	    cTextList.add(cText9.getCTexts());
	    cTextList.add(cText10.getCTexts());
	    cTextList.add(cText11.getCTexts());
	    cTextList.add(cText12.getCTexts());
	    cTextList.add(cText13.getCTexts());
	    cTextList.add(cText14.getCTexts());
	    card.setCTexts(cTextList);

	    // 20191227
	    // 增加連結列表

	    card.setCLinkType(1);
	    card.setCLinkList(botResponseUtils.getCLinkList(vo, 
	    		ThsrQuickReplay.CARD_203_1, 
	    		ThsrQuickReplay.CARD_203_2, 
	    		ThsrQuickReplay.CARD_203_3));

	    // 20191227
	    // 增加圖像資訊
	    CImageDataItem cImageData = vo.new CImageDataItem();
	    cImageData.setCImageType(1);
	    cImageData.setCImageUrl("oct.jpg");
        card.setCImageData(cImageData.getCImageDatas());
	    
		cardList.add(card.getCards());
        return cardList;
	}
}
