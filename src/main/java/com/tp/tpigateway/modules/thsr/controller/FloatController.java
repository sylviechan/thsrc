package com.tp.tpigateway.modules.thsr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.FloatRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.FloatService;

/**
 * @author west
 * @version 建立時間:Dec 26, 2019 9:34:37 AM 類說明
 */

@RestController
@RequestMapping("thsr/float")
public class FloatController extends BaseController {

	@Autowired
	FloatService floatService;

	@RequestMapping(value = "/actioninfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult actionInfo(@RequestBody FloatRequest floatRequest) {
		return floatService.actioninfo(floatRequest);
	}

}
