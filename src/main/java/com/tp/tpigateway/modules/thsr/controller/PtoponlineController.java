package com.tp.tpigateway.modules.thsr.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.ThsrService;

@RestController
@RequestMapping("ptoponline")
public class PtoponlineController extends BaseController {

    @Autowired
    ThsrService wayneService;

    @RequestMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult hello(@RequestBody BotRequest botRequest) {
        return wayneService.hello(botRequest);
    }
    
   
    

}
