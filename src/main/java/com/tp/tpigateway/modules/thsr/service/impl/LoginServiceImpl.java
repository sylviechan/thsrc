package com.tp.tpigateway.modules.thsr.service.impl;

import java.sql.Timestamp;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.LoginRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.postgresql.model.OcsPersonDialog;
import com.tp.tpigateway.common.postgresql.repository.OcsPersonDialogDetailRepository;
import com.tp.tpigateway.common.postgresql.repository.OcsPersonDialogRepository;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.thsr.feign.ThsrApiFeignService;
import com.tp.tpigateway.modules.thsr.service.LoginService;
import com.tp.tpigateway.modules.thsr.utils.ObjectMapperUtils;

// wayne
// 20191230
// use spring cloud
@Service("loginService")
public class LoginServiceImpl implements LoginService {

	@Autowired
	ThsrApiFeignService thsrApiFeignService;
	
	@Autowired
	OcsPersonDialogRepository ocsPersonDialogRepository;
	
	@Autowired
	OcsPersonDialogDetailRepository ocsPersonDialogDetailRepository;

	@Autowired
	BotResponseUtils botResponseUtils;
	
	// wayne
	// 20200101
	@Transactional("ptoponlineTransactionManager")
	@Override
	public void sendSMS(Map<String, String> params) {
		OcsPersonDialog ocsPersonDialog = new OcsPersonDialog();
		ocsPersonDialog.setSessionId(params.get("sessionID"));
		ocsPersonDialog.setIncomingType("test");
		String timestampString = convertToTimestampFormat(params.get("start_time"));
		ocsPersonDialog.setStartTime(Timestamp.valueOf(timestampString));
		ocsPersonDialogRepository.save(ocsPersonDialog);
		
		thsrApiFeignService.sendSMS(ObjectMapperUtils.mapToJsonString(params));
	}
	

	// yyyy-mm-dd hh:mm:ss[.fffffffff]
	private String convertToTimestampFormat(String s) {
		StringBuilder builder = new StringBuilder();
		builder.append(s.substring(0, 4));
		builder.append("-");
		builder.append(s.substring(4, 6));
		builder.append("-");
		builder.append(s.substring(6, 8));
		builder.append(" ");
		builder.append(s.substring(8, 10));
		builder.append(":");
		builder.append(s.substring(10, 12));
		builder.append(":");
		builder.append(s.substring(12, 14));
		return builder.toString();
	}


	@Override
	public TPIGatewayResult loginActioninfo(LoginRequest loginRequest) {

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage();

		int action = loginRequest.getAction();

		switch (action) {
		case 304:
			String widget = "doLogin";
			botResponseUtils.setCardsResultWithPhone(botMessageVO, action, widget, loginRequest.getPhone());

			tpiGatewayResult.setData(botMessageVO);
			break;

		default:
			throw new RuntimeException("Must has Action attributes.");
		}

		return tpiGatewayResult;
	}

}
