package com.tp.tpigateway.modules.thsr.model;

import java.util.List;

/**
* @author West
* @version 建立時間:Dec 26, 2019 3:34:21 PM
* 類說明
*/
public class Train {
	private String direction;
	private String number;
	private int nrCars;
	private List<Station>station;
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getNrCars() {
		return nrCars;
	}
	public void setNrCars(int nrCars) {
		this.nrCars = nrCars;
	}
	public List<Station> getStation() {
		return station;
	}
	public void setStation(List<Station> station) {
		this.station = station;
	}
	
	
}
