package com.tp.tpigateway.modules.thsr.service;

import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.TagRequest;

/**
 * @author west
 * @version 建立時間:Dec 25, 2019 9:34:37 AM 類說明
 */
public interface TagService {

	TPIGatewayResult input(TagRequest tagRequest);

}
