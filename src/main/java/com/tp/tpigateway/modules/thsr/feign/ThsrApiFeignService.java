package com.tp.tpigateway.modules.thsr.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

// wayne 
// 20191230
@FeignClient(name = "thsrapi", url = "${thsr.path}")
public interface ThsrApiFeignService {
	
	@PostMapping(value = "/lfs/v1/POST/createLostForm")
	Map<String, Object> createLostForm(String json);
	
	@PostMapping(value = "/ens/v1/POST/sendSMS")
	void sendSMS(String json);
}
