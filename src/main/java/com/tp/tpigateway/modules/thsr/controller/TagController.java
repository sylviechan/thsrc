package com.tp.tpigateway.modules.thsr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.TagRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.TagService;

/**
 * @author west
 * @version 建立時間:Dec 25, 2019 9:34:37 AM 類說明
 */
@RestController
@RequestMapping("thsr/tag")
public class TagController extends BaseController {

	@Autowired
	TagService tagService;

	@RequestMapping(value = "/input", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult input(@RequestBody TagRequest tagRequest) {
		return tagService.input(tagRequest);
	}

}
