package com.tp.tpigateway.modules.thsr.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.ThsrService;

@RestController
@RequestMapping("wayne")
public class ThsrController extends BaseController {

    @Autowired
    ThsrService wayneService;

    @RequestMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult hello(@RequestBody BotRequest botRequest) {
        return wayneService.hello(botRequest);
    }
    
    @RequestMapping(value = "/loss/step1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult lossStep1(@RequestBody BotRequest botRequest) {
        return wayneService.lossStep1(botRequest);
    }
    
    @RequestMapping(value = "/loss/step1-1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult lossStep1_1(@RequestBody BotRequest botRequest) {
        return wayneService.lossStep1_1(botRequest);
    }
    
    @RequestMapping(value = "/loss/step1-2", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult lossStep1_2(@RequestBody BotRequest botRequest) {
        return wayneService.lossStep1_2(botRequest);
    }
    
    @RequestMapping(value = "/loss/step2", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult lossStep2(@RequestBody BotRequest botRequest) {
        return wayneService.lossStep2(botRequest);
    }
    
    @RequestMapping(value = "/loss/cancel", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult lossStepCancel(@RequestBody BotRequest botRequest) {
        return wayneService.lossStepCancel(botRequest);
    }

    @RequestMapping(value = "/fallback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult fallback(@RequestBody FallBackRequest fallBackRequest) {
        return wayneService.fallback(fallBackRequest);
    }
    

    /*
       組FAQ回答樣式
    */
    @RequestMapping(value = "/handleFaqReplay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult handleFaqReplay(@RequestBody Map<String, Object> request) {
        return wayneService.handleFaqReplay(request);
    }

    /*
        服務台反問
     */
    @RequestMapping(value = "/supportNLU", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult supportNLU(@RequestBody Map<String, Object> request) {
        return wayneService.supportNLU(request);
    }

    @RequestMapping(value = "/showSurvey", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult showSurvey(@RequestBody BotRequest botRequest) {
        BotMessageVO botMessageVO = wayneService.showSurvey(botRequest);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }
    

}
