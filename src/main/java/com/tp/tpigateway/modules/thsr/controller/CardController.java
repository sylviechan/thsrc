package com.tp.tpigateway.modules.thsr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.CardRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.CardService;

//wayne 
//20191225
@RestController
@RequestMapping("thsr/card")
public class CardController extends BaseController {

    @Autowired
    CardService cardService;

    @RequestMapping(value = "/actioninfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult card(@RequestBody CardRequest cardRequest) {
        return cardService.card(cardRequest);
    }
}
