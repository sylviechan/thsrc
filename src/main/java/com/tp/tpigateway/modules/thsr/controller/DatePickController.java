package com.tp.tpigateway.modules.thsr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.FloatRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.thsr.service.DatepickService;

/**
 * @author west
 * @version 建立時間:Dec 30, 2019 23:34:37 AM 類說明
 */

@RestController
@RequestMapping("thsr/datepick")
public class DatePickController extends BaseController {

	@Autowired
	DatepickService datepickService;

	@RequestMapping(value = "/actioninfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult actionInfo(@RequestBody FloatRequest floatRequest) {
		return datepickService.datepick(floatRequest);
	}

}
