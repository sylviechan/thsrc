package com.tp.tpigateway.modules.thsr.model;

import java.util.List;

/**
* @author West
* @version 建立時間:Dec 26, 2019 3:40:56 PM
* 類說明
*/
public class BasicFare {
	
	private String recCount;
	private List<Records> records;
	public String getRecCount() {
		return recCount;
	}
	public void setRecCount(String recCount) {
		this.recCount = recCount;
	}
	public List<Records> getRecords() {
		return records;
	}
	public void setRecords(List<Records> records) {
		this.records = records;
	}
	
	

}
