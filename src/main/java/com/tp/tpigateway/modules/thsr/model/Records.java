package com.tp.tpigateway.modules.thsr.model;
/**
* @author West
* @version 建立時間:Dec 26, 2019 3:42:39 PM
* 類說明
*/
public class Records {

	private String versionNo;
	private String effectedDate;
	private String fareId;
	private String fareName;
	private String departStation;
	private String arriveStation;
	private String price;
	public String getVersionNo() {
		return versionNo;
	}
	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}
	public String getEffectedDate() {
		return effectedDate;
	}
	public void setEffectedDate(String effectedDate) {
		this.effectedDate = effectedDate;
	}
	public String getFareId() {
		return fareId;
	}
	public void setFareId(String fareId) {
		this.fareId = fareId;
	}
	public String getFareName() {
		return fareName;
	}
	public void setFareName(String fareName) {
		this.fareName = fareName;
	}
	public String getDepartStation() {
		return departStation;
	}
	public void setDepartStation(String departStation) {
		this.departStation = departStation;
	}
	public String getArriveStation() {
		return arriveStation;
	}
	public void setArriveStation(String arriveStation) {
		this.arriveStation = arriveStation;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	
	
}
