package com.tp.tpigateway.modules.thsr.model;
/**
* @author West
* @version 建立時間:Dec 26, 2019 2:42:00 PM
* 時刻表
*/
public class TimeTable {
	
	private String opDate;
	private Train train;
	public String getOpDate() {
		return opDate;
	}
	public void setOpDate(String opDate) {
		this.opDate = opDate;
	}
	public Train getTrain() {
		return train;
	}
	public void setTrain(Train train) {
		this.train = train;
	}

	
}
