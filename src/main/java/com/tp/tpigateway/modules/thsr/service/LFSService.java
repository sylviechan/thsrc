package com.tp.tpigateway.modules.thsr.service;

import com.tp.common.model.CardRequest;
import com.tp.common.model.TPIGatewayResult;


public interface LFSService {

    TPIGatewayResult createOrder(CardRequest cardRequest);
}
