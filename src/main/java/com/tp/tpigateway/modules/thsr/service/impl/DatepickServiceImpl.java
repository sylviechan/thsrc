package com.tp.tpigateway.modules.thsr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.FloatRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.thsr.service.DatepickService;

/**
 * @author West
 * @version 建立時間:Dec 26, 2019 9:34:37 AM 類說明
 */
@Service("datepickService")
public class DatepickServiceImpl implements DatepickService {

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	Environment env;

	@Override
	public TPIGatewayResult datepick(FloatRequest floatRequest) {

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage();

		int action = floatRequest.getAction();

		switch (action) {
		case 301:
			String widget = "popdatepick";
			botResponseUtils.setCardsResult(botMessageVO, action, widget, floatRequest.getIntent());

			tpiGatewayResult.setData(botMessageVO);
			break;

		default:
			throw new RuntimeException("Must has Action attributes.");
		}

		return tpiGatewayResult;
	}

}
