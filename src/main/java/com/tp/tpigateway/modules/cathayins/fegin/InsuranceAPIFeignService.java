package com.tp.tpigateway.modules.cathayins.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URI;
import java.util.Map;

@FeignClient(name = "insuranceapi", url = "${api.path}")
@TpSysLog(remark = "InsuranceAPIFeignService")
public interface InsuranceAPIFeignService {

	@TpSysLog(remark = "InsuranceAPIFeignService", showParams = false, showResult = false)
	@RequestMapping(value = "doLogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getA01(Map<String, Object> map);

	@TpSysLog(remark = "InsuranceAPIFeignService", showParams = false, showResult = false)
	@RequestMapping(value = "sendLoginOTP2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getA02(Map<String, Object> map);

	@TpSysLog(remark = "InsuranceAPIFeignService", showParams = false, showResult = false)
	@RequestMapping(value = "checkLoginOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getA03(Map<String, Object> map);

//	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	String getA04(URI baseUrl, String json);
	
	//產險測試環境使用
	@TpSysLog(remark = "InsuranceAPIFeignService", showParams = false, showResult = false)
	@RequestMapping(value = "upgradeMemberLevel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getA04(Map<String, Object> map);

	@RequestMapping(value = "queryPolicyData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getA05(Map<String, Object> map);

	@RequestMapping(value = "queryPolicyDetail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getA06(Map<String, Object> map);
	
	@RequestMapping(value = "checkWorkDay", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getA07(Map<String, Object> map);

	@RequestMapping(value = "queryCarRescue", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getC01(Map<String, Object> map);

	@RequestMapping(value = "downloadForce", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getC05(Map<String, Object> map);

	@RequestMapping(value = "queryEndrsmntStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getC06(Map<String, Object> map);

	@RequestMapping(value = "queryTravelList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getT02(String json);

	@RequestMapping(value = "saveTemp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> saveTemp(Map<String, Object> map);

	@RequestMapping(value = "queryCarList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getQueryCarList(Map<String, Object> map);

	@RequestMapping(value = "checkTravelChange", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getT03(String json);

	@RequestMapping(value = "doTravelConfirm", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getT04(String json);
}
