package com.tp.tpigateway.modules.cathayins.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathayins.service.InsCameraAlbumService;

@RestController
@RequestMapping("/CameraAlbum")
public class CameraAlbumController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(CameraAlbumController.class);

	@Autowired
	InsCameraAlbumService insCameraAlbumService;

	@RequestMapping(value = "/showView")
	public TPIGatewayResult cameraAlbum(@RequestBody BotRequest reqVO) {
		BotMessageVO botMessageVO = null;
		botMessageVO = insCameraAlbumService.cameraAlbum(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

}
