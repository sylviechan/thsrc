package com.tp.tpigateway.modules.cathayins.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.CarInsuranceRequest;
import com.tp.common.model.cathayins.InsProductTellRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathayins.service.InsProductTellService;

@RestController
@RequestMapping("/ProductTell")
public class InsProductTellController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(InsProductTellController.class);

	@Autowired
	InsProductTellService productTellService;

	@RequestMapping(value = "/related")
	public TPIGatewayResult changeOwnerInformation(@RequestBody InsProductTellRequest reqVO) {
		String cmd = reqVO.getCmd();
		BotMessageVO botMessageVO = null;

		// 商品-初始畫面
		// 商品大類
		if (cmd.equals("commodityBigSort")) {
			return productTellService.commodityBigSort(reqVO);
		}
		// 商品中類
		else if (cmd.equals("commodityMediumSort")) {
			return productTellService.commodityMediumSort(reqVO);
		}
		// 商品小類
		else if (cmd.equals("commoditySmallSort")) {
			return productTellService.commoditySmallSort(reqVO);
		}
		// 承辦項目
		else if (cmd.equals("undertakeProject")) {
			return productTellService.undertakeProject(reqVO);
		}
		// 項目內容
		else if (cmd.equals("projectContent")) {
			return productTellService.projectContent(reqVO);
		}
		/*
		 * 項目內容
		 * PPT Page 37 From A06看保障項目內容說明
		 * Input : cmd, policyNo, productPolicyCode, productSubId, categoryCode
		 * Output : 文字訊息、牌卡、快速回覆按鈕
		 */
		else if (cmd.equals("projectContentA06")) {
			return productTellService.projectContentA06(reqVO);
		}
		// 項目內容QA
		else if (cmd.equals("projectContentQA")) {
			botMessageVO = productTellService.projectContentQA(reqVO);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

}
