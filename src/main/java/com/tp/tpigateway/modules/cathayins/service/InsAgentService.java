package com.tp.tpigateway.modules.cathayins.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsAgentRequest;

import java.util.Map;

public interface InsAgentService {

	/**
	 * For 產險-非API回覆
	 */
	BotMessageVO chooseCmd(InsAgentRequest reqVO);
	
	/**
	 * For 轉接專人API
	 */
	TPIGatewayResult getAgent4_1(InsAgentRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 客戶訊息API
	 */
	TPIGatewayResult getAgent4_2(InsAgentRequest reqVO, Map<String, Object> param);	
	
	/**
	 * For 離開排隊佇列API
	 */
	TPIGatewayResult getAgent4_7(InsAgentRequest reqVO, Map<String, Object> param);	
		
}
