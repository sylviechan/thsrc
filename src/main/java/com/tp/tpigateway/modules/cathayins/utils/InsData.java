package com.tp.tpigateway.modules.cathayins.utils;

import java.util.HashMap;
import java.util.Map;

public class InsData {

    public static Map<String, String> conflictTypeMap = new HashMap<>();

    static {
        //key: 衝突項目  vaule: 要反問的險種
        //1:車險 2:旅綜險 3:健傷險 4:其他
        conflictTypeMap.put("海外突發疾病", "23");
        conflictTypeMap.put("海外急難救助", "23");
        conflictTypeMap.put("意外險", "23");
        conflictTypeMap.put("意外身故", "23");
        conflictTypeMap.put("意外失能", "23");
        conflictTypeMap.put("傷害險", "23");
        conflictTypeMap.put("竊盜", "12");
        conflictTypeMap.put("第三人責任", "123");
    }
}
