package com.tp.tpigateway.modules.cathayins.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathayins.service.InsCameraAlbumService;

/*
 *  投資相關
 *  
 */

@Service("insCameraAlbumService")
public class InsCameraAlbumServiceImpl implements InsCameraAlbumService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BotResponseUtils botResponseUtils;

	@Override
	public BotMessageVO cameraAlbum(BotRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setInsCameraAlbumComp(vo,"1");
		return vo;
	}

}
