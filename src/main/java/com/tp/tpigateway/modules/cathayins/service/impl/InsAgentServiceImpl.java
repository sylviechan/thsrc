package com.tp.tpigateway.modules.cathayins.service.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsAgentRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathayins.InsTestApiData;
import com.tp.tpigateway.modules.cathayins.fegin.InsAgentAPIFeignService;
import com.tp.tpigateway.modules.cathayins.model.InsQuickReplay;
import com.tp.tpigateway.modules.cathayins.service.InsAgentService;

@Service("insAgentService")
public class InsAgentServiceImpl implements InsAgentService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	InsAgentAPIFeignService insAgentAPIFeignService;

	@Autowired
	PropUtils propUtils;

	@Override
	public BotMessageVO chooseCmd(InsAgentRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String cmd = reqVO.getCmd().toUpperCase();

		switch (cmd) {
			case "TOAGENT":
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.AGNT_STOP,
						InsQuickReplay.INIT);
				String text = "{\"content\":" + Object2Json(vo.getContent()).toString() + "}";
				vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
				botResponseUtils.setAgent(vo, text);
				break;
			case "A05_TRAVEL_T01":
				botResponseUtils.setTextResult(vo, "TODO CALL API T01");
				break;
			case "A05_ASK_OTHER":
				botResponseUtils.setTextResult(vo, "功能還不能使用喔~~");
				break;
			case "ASK_TO_AGENT":
				botResponseUtils.setTextResult(vo, "確定要現在幫你轉接真人文字客服嗎");
				botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.TO_AGENT, InsQuickReplay.NOT_TO_AGENT);
				break;
			case "FALLBACK_ASK_TO_AGENT":
				botResponseUtils.setTextResult(vo, "阿發聽不懂");
				botResponseUtils.setTextResult(vo, "確定要現在幫你轉接真人文字客服嗎");
				botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.TO_AGENT, InsQuickReplay.NOT_TO_AGENT);
				break;
			default:
				break;
		}

		return vo;
	}

	@Override
	public TPIGatewayResult getAgent4_1(InsAgentRequest reqVO, Map<String, Object> param) {
//		Map<String, Object> result = insAgentAPIFeignService.get4_1(JSON.toJSONString(param));        
//		return TPIGatewayResult.ok().setData(result);

		// TODO 目前沒通無法測試
		Map<String, Object> returnMap = new HashMap<String, Object>();
		returnMap.put("request", param);
		returnMap.put("response", JSON.parseObject(InsTestApiData.getAgent4_1ResponseString()));

		return TPIGatewayResult.ok().setData(returnMap);
	}

	@Override
	public TPIGatewayResult getAgent4_2(InsAgentRequest reqVO, Map<String, Object> param) {
//		Map<String, Object> result = insAgentAPIFeignService.get4_2(JSON.toJSONString(param));        
//		return TPIGatewayResult.ok().setData(result);

		// TODO 目前沒通無法測試
		Map<String, Object> returnMap = new HashMap<String, Object>();
		returnMap.put("request", param);
		returnMap.put("response", JSON.parseObject(InsTestApiData.getAgent4_2ResponseString()));

		return TPIGatewayResult.ok().setData(returnMap);
	}

	@Override
	public TPIGatewayResult getAgent4_7(InsAgentRequest reqVO, Map<String, Object> param) {
//		Map<String, Object> result = insAgentAPIFeignService.get4_7(JSON.toJSONString(param));        
//		return TPIGatewayResult.ok().setData(result);

		// TODO 目前沒通無法測試
		Map<String, Object> returnMap = new HashMap<String, Object>();
		returnMap.put("request", param);
		returnMap.put("response", JSON.parseObject(InsTestApiData.getAgent4_7ResponseString()));

		return TPIGatewayResult.ok().setData(returnMap);
	}

	public String getString(Object str) {
		String returnStr = "";
		if (str != null) {
			returnStr = str.toString();
		}

		return returnStr;
	}

	public Integer getInteger(Object data) {
		Integer returnInt = 0;
		if (data != null) {
			try {
				returnInt = Integer.parseInt(data.toString());
			} catch (NumberFormatException e) {
				return returnInt;
			}
		}

		return returnInt;
	}

	private URI getURI(String url) {
		URI baseUrl;
		try {
			baseUrl = new URI(url);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage());
		}
		return baseUrl;
	}

	public static Map<String, Object> returnToMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> rumap = new HashMap<String, Object>();
		try {
			rumap = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
			});
			return rumap;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rumap;
	}

	/**
	 * 物件轉JSON功能
	 *
	 * @param obj
	 * @return
	 */
	public static String Object2Json(Object obj) {

		ObjectMapper mapper = new ObjectMapper();

		String jsonInString = "";

		try {
			jsonInString = mapper.writeValueAsString(obj);

		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonInString;

	}

	public List<Map<String, Object>> drawCard(BotMessageVO vo, List<Map<String, Object>> arrayList,
			String[] detailArray, String titleKey, String title) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		for (int i = 0; i < arrayList.size(); i++) {
			String idKey = getString(arrayList.get(i).get(titleKey));
			String idName = getString(arrayList.get(i).get("StockName"));

			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			cards.setCTextType("5");
			cards.setCLinkType(1);

			// 牌卡圖片
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl("policy_01.jpg");
			cards.setCImageData(cImageData.getCImageDatas());

			if (!getString(title).equals("")) {
				cards.setCTitle(title);
			} else if (!getString(titleKey).equals("")) {
				cards.setCTitle(idKey);
			}

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
			for (int j = 0; j < detailArray.length; j++) {
				CTextItem cText = vo.new CTextItem();
				String detailMsg = detailArray[j].split(":")[1] + " : "
						+ getString(arrayList.get(i).get(detailArray[j].split(":")[0]));
				cText.setCText(detailMsg);
				cTextList.add(cText.getCTexts());
			}
			cards.setCTexts(cTextList);

			QuickReplay[] link1s = {
					new InsQuickReplay(15, "查看個股資訊", "查看(" + idKey + ")資訊", "RB02_select:" + idKey, "", ""),
					new InsQuickReplay(15, "申購", "申購(" + idKey + ")", "RB02_buy:" + idKey + ":" + idName, "", "") };

			cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));

			cardList.add(cards.getCards());
		}

		return cardList;
	}

	private String testA05ResponseString() {
		return "" + "{" + "    \"returnCode\": \"0\"," + "    \"returnMsg\": \"API success.\"," + "    \"detail\": {"
				+ "        \"RETURN_MSG\": \"\"," + "        \"TRAVEL\": [" + "            {"
				+ "                \"POLICY_NO\": \"150107TDK097550\"," + "                \"PRODUCT_CODE\": \"P\","
				+ "                \"PRODUCT_SUB_CODE\": \"PC01\","
				+ "                \"PRODUCT_POLICY_CODE\": \"PC01002\","
				+ "                \"PRODUCT_POLICY_NAME\": \"旅綜險(國外)\"," + "                \"CUSTOMER_COUNT\": 1,"
				+ "                \"DISCOUNT_PREMIUM\": \"439.00\","
				+ "                \"ISSUE_DATE\": \"2018-12-10\","
				+ "                \"START_DATETIME\": \"Mar 31, 2019 5:30:00 AM\","
				+ "                \"END_DATETIME\": \"Apr 9, 2019 5:30:00 AM\","
				+ "                \"INSRNCE_DAY\": 9,"
				+ "                \"APPLICANT_OF_CUSTOMER_ID\": \"S122531322\","
				+ "                \"APPLICANT_OF_CUSTOMER_NAME\": \"簡○紋\","
				+ "                \"APPLICANT_OF_BIRTHDAY\": \"1983-07-28\","
				+ "                \"APPLICANT_OF_PHONE_CODE\": \"0923-27551299\","
				+ "                \"APPLICANT_OF_CELLULAR_PHONE\": \"0923123456\","
				+ "                \"APPLICANT_OF_EMAIL\": \"XXXXX@cathlife.com.tw\","
				+ "                \"APPLICANT_OF_ADDRESS\": \"臺中市西屯區國安\","
				+ "                \"CUSTOMER_ID\": \"S122531322\"," + "                \"CUSTOMER_NAME\": \"簡○紋\","
				+ "                \"APPLLTE_TYPE\": \"1\"," + "                \"TRAVEL_SITE\": \"4\","
				+ "                \"TRAVEL_SITE_REMARK\": \"韓國\"," + "                \"AGREE_SMS_NOTIFY\": \"1\","
				+ "                \"TRAVEL_CONVNTE\": \"1\"," + "                \"E_POLICY\": \"0\","
				+ "                \"CONTRACT_NO\": \"1812CA0277771386\"," + "                \"IS_NEED\": \"1\","
				+ "                \"MEMBER_TYPE\": \"1\"," + "                \"TRAVEL_SITE_NAME\": \"其他\","
				+ "                \"POLICY_NO_1\": \"150107TDK097550\","
				+ "                \"PA_PRODUCT_POLICY_NAME\": \"旅遊綜合保險（國外）\","
				+ "                \"START_DATE\": \"108/03/31\"," + "                \"END_DATE\": \"108/04/09\""
				+ "            }," + "            {" + "                \"POLICY_NO\": \"150107TDK097550\","
				+ "                \"PRODUCT_CODE\": \"P\"," + "                \"PRODUCT_SUB_CODE\": \"PC01\","
				+ "                \"PRODUCT_POLICY_CODE\": \"PC01002\","
				+ "                \"PRODUCT_POLICY_NAME\": \"旅綜險(國外)\"," + "                \"CUSTOMER_COUNT\": 1,"
				+ "                \"DISCOUNT_PREMIUM\": \"439.00\","
				+ "                \"ISSUE_DATE\": \"2018-12-10\","
				+ "                \"START_DATETIME\": \"Mar 31, 2019 5:30:00 AM\","
				+ "                \"END_DATETIME\": \"Apr 9, 2019 5:30:00 AM\","
				+ "                \"INSRNCE_DAY\": 9,"
				+ "                \"APPLICANT_OF_CUSTOMER_ID\": \"S122531322\","
				+ "                \"APPLICANT_OF_CUSTOMER_NAME\": \"王○名\","
				+ "                \"APPLICANT_OF_BIRTHDAY\": \"1983-07-28\","
				+ "                \"APPLICANT_OF_PHONE_CODE\": \"0923-27551299\","
				+ "                \"APPLICANT_OF_CELLULAR_PHONE\": \"0923123456\","
				+ "                \"APPLICANT_OF_EMAIL\": \"XXXXX@cathlife.com.tw\","
				+ "                \"APPLICANT_OF_ADDRESS\": \"臺中市西屯區國安\","
				+ "                \"CUSTOMER_ID\": \"S122531322\"," + "                \"CUSTOMER_NAME\": \"王○名\","
				+ "                \"APPLLTE_TYPE\": \"1\"," + "                \"TRAVEL_SITE\": \"4\","
				+ "                \"TRAVEL_SITE_REMARK\": \"泰國\"," + "                \"AGREE_SMS_NOTIFY\": \"1\","
				+ "                \"TRAVEL_CONVNTE\": \"1\"," + "                \"E_POLICY\": \"0\","
				+ "                \"CONTRACT_NO\": \"1812CA0277771386\"," + "                \"IS_NEED\": \"1\","
				+ "                \"MEMBER_TYPE\": \"1\"," + "                \"TRAVEL_SITE_NAME\": \"其他\","
				+ "                \"POLICY_NO_1\": \"150107TDK097550\","
				+ "                \"PA_PRODUCT_POLICY_NAME\": \"旅遊綜合保險（國外）\","
				+ "                \"START_DATE\": \"108/08/13\"," + "                \"END_DATE\": \"108/09/08\""
				+ "            }" + "        ]," + "        \"RETURN_CODE\": \"0000\"," + "        \"CAR\": ["
				+ "            {" + "                \"VEHICLE_KIND_NO\": \"01\","
				+ "                \"CAR_NO\": \"552-EDQ\"," + "                \"LIST_CAR_MAIN\": ["
				+ "                    {" + "                        \"DISCOUNT_PREMIUM\": \"488.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"強制險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"109/06/06\","
				+ "                        \"POLICY_NO\": \"150119W000028\","
				+ "                        \"START_DATE\": \"108/06/06\"" + "                    },"
				+ "                    {" + "                        \"DISCOUNT_PREMIUM\": \"1368.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"任意險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"109/06/06\","
				+ "                        \"POLICY_NO\": \"1501UW000928\","
				+ "                        \"START_DATE\": \"108/06/06\"" + "                    },"
				+ "                    {" + "                        \"DISCOUNT_PREMIUM\": \"520.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"駕駛人傷害險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"109/06/06\","
				+ "                        \"POLICY_NO\": \"1501UE900026\","
				+ "                        \"START_DATE\": \"108/06/06\"" + "                    }"
				+ "                ]," + "                \"ENGINE_EXHAUST_UNIT\": \"1\","
				+ "                \"END_DATE\": \"109/06/06\"," + "                \"START_DATE\": \"108/06/06\","
				+ "                \"POLICY_NO\": \"150119W000028,1501UW000928,1501UE900026\","
				+ "                \"VEHICLE_KIND_NAME\": \"普通重機\","
				+ "                \"ENGINE_EXHAUST_UNIT_NAME\": \"CC\","
				+ "                \"ENGINE_EXHAUST\": \"111.00\","
				+ "                \"PRODUCT_POLICY_NAME\": \"強制險/任意險/駕駛人傷害險\","
				+ "                \"CUSTOMER_NAME\": \"余承遠\"," + "                \"QUERY_TYPE\": \"1\","
				+ "                \"MODEL_BRAND_NAME\": \"YAMAHA山葉\","
				+ "                \"APPLICANT_OF_CUSTOMER_NAME\": \"余承遠\"" + "            }," + "            {"
				+ "                \"VEHICLE_KIND_NO\": \"22\"," + "                \"CAR_NO\": \"ATK-7859\","
				+ "                \"LIST_CAR_MAIN\": [" + "                    {"
				+ "                        \"DISCOUNT_PREMIUM\": \"988.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"強制險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"108/08/28\","
				+ "                        \"POLICY_NO\": \"150118B905686\","
				+ "                        \"START_DATE\": \"107/08/28\"" + "                    },"
				+ "                    {" + "                        \"DISCOUNT_PREMIUM\": \"9712.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"任意險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"108/08/28\","
				+ "                        \"POLICY_NO\": \"1501TW005665\","
				+ "                        \"START_DATE\": \"107/08/28\"" + "                    }"
				+ "                ]," + "                \"ENGINE_EXHAUST_UNIT\": \"1\","
				+ "                \"END_DATE\": \"108/08/28\"," + "                \"START_DATE\": \"107/08/28\","
				+ "                \"POLICY_NO\": \"150118B905686,1501TW005665\","
				+ "                \"VEHICLE_KIND_NAME\": \"客貨兩用\","
				+ "                \"ENGINE_EXHAUST_UNIT_NAME\": \"CC\","
				+ "                \"ENGINE_EXHAUST\": \"1798.00\","
				+ "                \"PRODUCT_POLICY_NAME\": \"強制險/任意險\","
				+ "                \"CUSTOMER_NAME\": \"余○遠\"," + "                \"QUERY_TYPE\": \"1\","
				+ "                \"MODEL_BRAND_NAME\": \"國瑞/豐田TOYOTA\","
				+ "                \"APPLICANT_OF_CUSTOMER_NAME\": \"余○遠\"" + "            }" + "        ],"
				+ "        \"HEALTH\": [" + "            {" + "                \"POLICY_NO\": \"150106PAS00002\","
				+ "                \"PRODUCT_POLICY_CODE\": \"PP01055\","
				+ "                \"PRODUCT_POLICY_NAME\": \"個人傷害險－平安行網路投保專案\","
				+ "                \"CUSTOMER_COUNT\": 1," + "                \"DISCOUNT_PREMIUM\": \"1233.00\","
				+ "                \"START_DATETIME\": \"2019-05-10 24:00:00\","
				+ "                \"END_DATETIME\": \"2020-05-10 24:00:00\","
				+ "                \"APPLICANT_OF_CUSTOMER_ID\": \"S122531322\","
				+ "                \"APPLICANT_OF_CUSTOMER_NAME\": \"余○遠\","
				+ "                \"APPLICANT_OF_BIRTHDAY\": \"1981-08-17\","
				+ "                \"APPLICANT_OF_CELLULAR_PHONE\": \"0935123456\","
				+ "                \"APPLICANT_OF_EMAIL\": \"XXXXX@cathlife.com.tw\","
				+ "                \"APPLICANT_OF_ADDRESS\": \"新北市三重區仁義\","
				+ "                \"CUSTOMER_ID\": \"S122531322\"," + "                \"CUSTOMER_NAME\": \"余○遠\","
				+ "                \"IS_NEXTYEAR_AUTOCONT\": \"1\"," + "                \"PERSONAL_OR_TEAM\": \"P\","
				+ "                \"E_POLICY\": \"1\"," + "                \"CONTRACT_NO\": \"1706CA0074916261\","
				+ "                \"IS_DEFAULT_STRTIME\": \"1\"," + "                \"IS_DEFAULT_ENDTIME\": \"1\","
				+ "                \"IS_NAME_LIST\": \"1\","
				+ "                \"CONTINUE_POLICY_NO\": \"150106PAS00002\"," + "                \"IS_NOW\": \"1\","
				+ "                \"CARD_PAYMENT_YEAR\": \"108\"," + "                \"CARD_PAYMENT_MONTH\": \"05\","
				+ "                \"CARD_PAYMENT_DATE\": \"10\","
				+ "                \"IS_NEXTYEAR_AUTOCONT_NM\": \"是\"," + "                \"MEMBER_TYPE\": \"1\","
				+ "                \"START_DATE\": \"108/05/10\"," + "                \"END_DATE\": \"109/05/10\","
				+ "                \"PRODUCT_POLICY_NAME1\": \"個人傷害險\","
				+ "                \"PRODUCT_POLICY_NAME2\": \"平安行網路投保專案\"" + "            }" + "        ]" + "    }"
				+ "}";
	}

	private String testC01ResponseString() {
		return "" + "{" + "    \"returnCode\": \"0\"," + "    \"returnMsg\": \"API success.\"," + "    \"detail\": {"
				+ "        \"RETURN_MSG\": \"\"," + "        \"RETURN_CODE\": \"0000\"," + "        \"DATAS\": ["
				+ "            {" + "                \"VEHICLE_KIND_NO\": \"22\","
				+ "                \"CAR_NO\": \"ATK-7859\"," + "                \"RESCUE_CARD_DATA\": ["
				+ "                    \"免費贈送：100公里，保單年度不限次但一天限一次\"," + "                    \"道路救援險：每次限額2萬元，保單年度限3次\""
				+ "                ]," + "                \"LIST_CAR_MAIN\": [" + "                    {"
				+ "                        \"DISCOUNT_PREMIUM\": \"988.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"強制險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"108/08/28\","
				+ "                        \"POLICY_NO\": \"150118B905686\","
				+ "                        \"START_DATE\": \"107/08/28\"" + "                    },"
				+ "                    {" + "                        \"DISCOUNT_PREMIUM\": \"9712.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"任意險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"108/08/28\","
				+ "                        \"POLICY_NO\": \"1501TW005665\","
				+ "                        \"START_DATE\": \"107/08/28\"" + "                    }"
				+ "                ]," + "                \"ENGINE_EXHAUST_UNIT\": \"1\","
				+ "                \"END_DATE\": \"108/08/28\"," + "                \"START_DATE\": \"107/08/28\","
				+ "                \"POLICY_NO\": \"150118B905686,1501TW005665\","
				+ "                \"VEHICLE_KIND_NAME\": \"客貨兩用\","
				+ "                \"RESCUE_CARD_MEMO\": \"服務內容依照公司公告為準\","
				+ "                \"ENGINE_EXHAUST_UNIT_NAME\": \"CC\","
				+ "                \"ENGINE_EXHAUST\": \"1798.00\","
				+ "                \"PRODUCT_POLICY_NAME\": \"強制險/任意險\","
				+ "                \"CUSTOMER_NAME\": \"余○遠\"," + "                \"QUERY_TYPE\": \"1\","
				+ "                \"MODEL_BRAND_NAME\": \"國瑞/豐田TOYOTA\","
				+ "                \"APPLICANT_OF_CUSTOMER_NAME\": \"余○遠\"" + "            }," + "            {"
				+ "                \"VEHICLE_KIND_NO\": \"03\"," + "                \"CAR_NO\": \"6666-AS\","
				+ "                \"LIST_CAR_MAIN\": [" + "                    {"
				+ "                        \"DISCOUNT_PREMIUM\": \"849.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"強制險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"109/06/06\","
				+ "                        \"POLICY_NO\": \"150119B901109\","
				+ "                        \"START_DATE\": \"108/06/06\"" + "                    },"
				+ "                    {" + "                        \"DISCOUNT_PREMIUM\": \"3456.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"任意險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"109/06/06\","
				+ "                        \"POLICY_NO\": \"1501UW000927\","
				+ "                        \"START_DATE\": \"108/06/06\"" + "                    },"
				+ "                    {" + "                        \"DISCOUNT_PREMIUM\": \"519.00\","
				+ "                        \"PRODUCT_POLICY_NAME\": \"駕駛人傷害險\","
				+ "                        \"QUERY_TYPE\": \"1\","
				+ "                        \"END_DATE\": \"109/06/06\","
				+ "                        \"POLICY_NO\": \"1501UE900025\","
				+ "                        \"START_DATE\": \"108/06/06\"" + "                    }"
				+ "                ]," + "                \"ENGINE_EXHAUST_UNIT\": \"1\","
				+ "                \"END_DATE\": \"109/06/06\"," + "                \"START_DATE\": \"108/06/06\","
				+ "                \"POLICY_NO\": \"150119B901109,1501UW000927,1501UE900025\","
				+ "                \"VEHICLE_KIND_NAME\": \"自小客車\","
				+ "                \"ENGINE_EXHAUST_UNIT_NAME\": \"CC\","
				+ "                \"ENGINE_EXHAUST\": \"1500.00\","
				+ "                \"PRODUCT_POLICY_NAME\": \"強制險/任意險/駕駛人傷害險\","
				+ "                \"CUSTOMER_NAME\": \"余承遠\"," + "                \"QUERY_TYPE\": \"1\","
				+ "                \"MODEL_BRAND_NAME\": \"國瑞/豐田TOYOTA\","
				+ "                \"APPLICANT_OF_CUSTOMER_NAME\": \"余承遠\"" + "            }" + "        ]" + "    }"
				+ "}";
	}

}
