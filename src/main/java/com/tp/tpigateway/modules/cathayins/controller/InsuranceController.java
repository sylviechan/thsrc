package com.tp.tpigateway.modules.cathayins.controller;

import com.alibaba.fastjson.JSON;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsuranceRequest;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.service.CommonService;
import com.tp.tpigateway.modules.cathayins.service.InsuranceService;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/Insurance")
public class InsuranceController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(InsuranceController.class);
    private static final String SOURCE = "insurance";

    @Autowired
    InsuranceService insuranceService;

    @Autowired
    CommonService commonService;

    @PostMapping("/loginView")
    public TPIGatewayResult loginView(@RequestBody BotRequest botRequest) {
        BotMessageVO botMessageVO = insuranceService.getLogin(botRequest);
        logger.info("JSON.toJSONString(botMessageVO) : " + JSON.toJSONString(botMessageVO));
        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/cmd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult cmd(@RequestBody InsuranceRequest reqVO) {
        return insuranceService.chooseCmd(reqVO);
    }

    @RequestMapping(value = "/A01", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult A01(@RequestBody InsuranceRequest reqVO) {
        // 設定必要參數
        Map<String, String> requireParams = new HashMap<>();
        requireParams.put("MEMBER_ID", reqVO.getMemberId());
        requireParams.put("PASSWORD", reqVO.getPassword());
        requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
        requireParams.put("SESSION_ID", reqVO.getSessionId());

        // 必要參數檢核
        if (checkParams(requireParams)) {
            requireParams.put("LOGIN_IP", reqVO.getLoginIp());
            requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

            return insuranceService.getA01(reqVO, requireParams);
        } else {
            return TPIGatewayResult.error("參數錯誤");
        }
    }

    @RequestMapping(value = "/A02", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult A02(@RequestBody InsuranceRequest reqVO) {
        // 設定必要參數
        Map<String, String> requireParams = new HashMap<>();
        requireParams.put("MEMBER_ID", reqVO.getMemberId());
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMdd");
        DateTimeFormatter reqDf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthday = LocalDate.parse(reqVO.getBirthday(), df);
        String reqBirthday = birthday.format(reqDf);
        requireParams.put("BIRTHDAY", reqBirthday);
        requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
        requireParams.put("SESSION_ID", reqVO.getSessionId());

        // 必要參數檢核
        if (checkParams(requireParams)) {
            requireParams.put("LOGIN_IP", reqVO.getLoginIp());
            requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

            return insuranceService.getA02(reqVO, requireParams);
        } else {
            return TPIGatewayResult.error("參數錯誤");
        }
    }

    @RequestMapping(value = "/A03", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult A03(@RequestBody InsuranceRequest reqVO) {
        // 設定必要參數
        Map<String, String> requireParams = new HashMap<String, String>();
        requireParams.put("MEMBER_ID", reqVO.getMemberId());
        requireParams.put("otpPre", reqVO.getOtpPre());
        requireParams.put("otpCode", reqVO.getOtpCode());
        requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
        requireParams.put("SESSION_ID", reqVO.getSessionId());

        // 必要參數檢核
        if (checkParams(requireParams)) {
            requireParams.put("LOGIN_IP", reqVO.getLoginIp());
            requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

            return insuranceService.getA03(reqVO, requireParams);
        } else {
            return TPIGatewayResult.error("參數錯誤");
        }
    }

    @RequestMapping(value = "/A04", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult A04(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;

        try {
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("MEMBER_ID", reqVO.getMemberId());
            requireParams.put("MEMBER_TOKEN", reqVO.getMemberToken());
            requireParams.put("VALID_KEY_NO", reqVO.getValidKeyNo());
            requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
            requireParams.put("SESSION_ID", reqVO.getSessionId());

            // 必要參數檢核
            if (checkParams(requireParams)) {
                requireParams.put("LOGIN_IP", reqVO.getLoginIp());
                requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

                result = insuranceService.getA04(reqVO, requireParams);
                botMessageVO = (BotMessageVO) result.get("botMessageVO");
            }
        } finally {
            logger.error("[" + SOURCE + "_A04] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO).put("waitVALID_KEY_NO", MapUtils.getBooleanValue(result, "waitVALID_KEY_NO", false)).put("MEMBER_LEVEL", MapUtils.getInteger(result, "MEMBER_LEVEL", 0));
    }

    @RequestMapping(value = "/A05", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult A05(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;

        try {
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("MEMBER_ID", reqVO.getMemberId());
            requireParams.put("MEMBER_TOKEN", reqVO.getMemberToken());
            requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
            requireParams.put("SESSION_ID", reqVO.getSessionId());

            // 必要參數檢核
            if (checkParams(requireParams)) {
                requireParams.put("LOGIN_IP", reqVO.getLoginIp());
                requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

                botMessageVO = insuranceService.getA05(reqVO, requireParams);
            }
        } finally {
            logger.error("[" + SOURCE + "_A05] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/A06", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult A06(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;

        try {
            // TODO 文件沒定義清楚
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("MEMBER_ID", reqVO.getMemberId());
            requireParams.put("MEMBER_TOKEN", reqVO.getMemberToken());
            requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
            requireParams.put("SESSION_ID", reqVO.getSessionId());
            requireParams.put("PRODUCT_TYPE", reqVO.getProductType());

            // 必要參數檢核
            if (checkParams(requireParams)) {
                requireParams.put("LOGIN_IP", reqVO.getLoginIp());
                requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

                if (StringUtils.isNotBlank(reqVO.getListCarMain())) {
                    requireParams.put("LIST_CAR_MAIN", reqVO.getListCarMain());
                }
                if (StringUtils.isNotBlank(reqVO.getCarNo())) {
                    requireParams.put("CAR_NO", reqVO.getCarNo());
                }
                if (StringUtils.isNotBlank(reqVO.getMemberType())) {
                    requireParams.put("MEMBER_TYPE", reqVO.getMemberType());
                }
                if (StringUtils.isNotBlank(reqVO.getPersonalOrTeam())) {
                    requireParams.put("PERSONAL_OR_TEAM", reqVO.getPersonalOrTeam());
                }
                if (StringUtils.isNotBlank(reqVO.getPolicyNo())) {
                    requireParams.put("POLICY_NO", reqVO.getPolicyNo());
                }
                if (StringUtils.isNotBlank(reqVO.getAppliteType())) {
                    requireParams.put("APPLLTE_TYPE", reqVO.getAppliteType());
                }

                // 2019/10/28 將A06依畫面分拆成數個Function，根據Cmd決定呼叫哪一個Function
                switch (reqVO.getCmd()) {
                    case "CAR":
                        if (reqVO.getMemberLevel() != null && reqVO.getMemberLevel() == 1) {
                            result = insuranceService.getCarDetail(reqVO, requireParams);
                        } else {
                            requireParams.remove("LIST_CAR_MAIN");
                            requireParams.remove("CAR_NO");
                            requireParams.remove("PRODUCT_TYPE");
                            result = insuranceService.getCarDetailNormal(reqVO, requireParams);
                        }
                        break;
                    case "CARCONTENT":
                        if (reqVO.getMemberLevel() != null && reqVO.getMemberLevel() == 1) {
                            result = insuranceService.getCarContent(reqVO, requireParams);
                        } else {
                            result = insuranceService.getContentNormalReply(reqVO, requireParams);
                        }
                        break;
                    case "CARINFO":
                        result = insuranceService.getCarInfo(reqVO, requireParams);
                        break;
                    case "HEALTH":
                        if (reqVO.getMemberLevel() != null && reqVO.getMemberLevel() == 1) {
                            result = insuranceService.getHealthDetail(reqVO, requireParams);
                        } else {
                            requireParams.remove("PERSONAL_OR_TEAM");
                            requireParams.remove("MEMBER_TYPE");
                            requireParams.remove("POLICY_NO");
                            requireParams.remove("PRODUCT_TYPE");
                            result = insuranceService.getHealthDetailNormal(reqVO, requireParams);
                        }

                        break;
                    case "HEALTHCONTENT":
                        if (reqVO.getMemberLevel() != null && reqVO.getMemberLevel() == 1) {
                            result = insuranceService.getHealthContent(reqVO, requireParams);
                        } else {
                            result = insuranceService.getContentNormalReply(reqVO, requireParams);
                        }
                        break;
                    case "HEALTHMANINFO":
                        result = insuranceService.getHealthManInfo(reqVO, requireParams);
                        break;
                    case "TRAVEL":
                        if (reqVO.getMemberLevel() != null && reqVO.getMemberLevel() == 1) {
                            result = insuranceService.getTravelDetail(reqVO, requireParams);
                        } else {
                            requireParams.remove("APPLLTE_TYPE");
                            requireParams.remove("MEMBER_TYPE");
                            requireParams.remove("POLICY_NO");
                            requireParams.remove("PRODUCT_TYPE");
                            result = insuranceService.getTravelDetailNormal(reqVO, requireParams);
                        }

                        break;
                    case "TRAVELCONTENT":
                        if (reqVO.getMemberLevel() != null && reqVO.getMemberLevel() == 1) {
                            result = insuranceService.getTravelContent(reqVO, requireParams);
                        } else {
                            result = insuranceService.getContentNormalReply(reqVO, requireParams);
                        }
                        break;
                    case "TRAVELCONTENTMORE":
                        result = insuranceService.getTravelContent(reqVO, requireParams);
                        break;
                    case "TRAVELCONTENTDESC":
                        /*
                         * PPT Page 36 看保障內容說明
                         * Input : memberId, memberToken, receiveType, sessionId, loginIp, viewDevice, productType, memberType, appliteType, policyNo
                         * Output : 浮出視窗 content.type = 16
                         *
                         * 如 PPT Page 36 ，按下選項後，會送出如下資料
                         * {
                         * 	intent : "商品",
                         * 	secondBotIntent : "projectContentA06",
                         * 	parameter : {policyNo, productPolicyCode, categoryCode, productSubId}
                         * }
                         */
                        result = insuranceService.getTravelContentDesc(reqVO, requireParams);
                        break;
                    case "TRAVELMANINFO":
                        result = insuranceService.getTravelManInfo(reqVO, requireParams);
                        break;
                    case "TRAVELCLAIMINFO":
                        result = insuranceService.getTravelClaimInfo(reqVO, requireParams);
                        break;
                }
                botMessageVO = (BotMessageVO) result.get("botMessageVO");
            }
        } finally {
            logger.error("[" + SOURCE + "_A06] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO).put("msgList", result.get("msgList")).put("optionList", result.get("optionList"));
    }

    @RequestMapping(value = "/A07", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult A07(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
        try {
            // TODO 文件沒定義清楚
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("CHECK_DATE", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

            // 必要參數檢核
            if (checkParams(requireParams)) {
 
                tpiGatewayResult = insuranceService.getA07(reqVO, requireParams);
                logger.info("JSON.toJSONString(botMessageVO) : " + JSON.toJSONString(botMessageVO));
            }
        } finally {
            logger.error("[" + SOURCE + "_A07] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return tpiGatewayResult;
    }

    @RequestMapping(value = "/C01", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult C01(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;

        try {
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("MEMBER_ID", reqVO.getMemberId());
            requireParams.put("MEMBER_TOKEN", reqVO.getMemberToken());
            requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
            requireParams.put("SESSION_ID", reqVO.getSessionId());

            // 必要參數檢核
            if (checkParams(requireParams)) {
                requireParams.put("LOGIN_IP", reqVO.getLoginIp());
                requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

                botMessageVO = insuranceService.getC01(reqVO, requireParams);
            }

        } finally {
            logger.error("[" + SOURCE + "_C01] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/C05", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult C05(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;

        try {
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("MEMBER_ID", reqVO.getMemberId());
            requireParams.put("CAR_NO", reqVO.getCarNo());
            // requireParams.put("MEMBER_ID", reqVO.getMemberId());
            // requireParams.put("MEMBER_TOKEN", reqVO.getMemberToken());
            requireParams.put("RECEIVE_TYPE", reqVO.getReceiveType());
            requireParams.put("SESSION_ID", reqVO.getSessionId());

            // 必要參數檢核
            if (checkParams(requireParams)) {
            	//車號幫忙轉大寫
            	requireParams.put("CAR_NO", requireParams.get("CAR_NO").toUpperCase());
            	requireParams.put("MEMBER_ID", requireParams.get("MEMBER_ID").toUpperCase());
                requireParams.put("LOGIN_IP", reqVO.getLoginIp());
                requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());

                result = insuranceService.getC05(reqVO, requireParams);
                botMessageVO = (BotMessageVO) result.get("botMessageVO");
            }

        } finally {
            logger.error("[" + SOURCE + "_C05] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO).put("msgList", result.get("msgList")).put("optionList", result.get("optionList"));
    }

    @RequestMapping(value = "/F01", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult F01(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();

        try {
            botMessageVO = insuranceService.getF01(reqVO);
        } finally {
            logger.info(
                    "[" + SOURCE + "_F01] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/F02", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult F02(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();

        try {
            botMessageVO = insuranceService.getF02(reqVO);
        } finally {
            logger.info(
                    "[" + SOURCE + "_F02] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/T02", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult T02(@RequestBody InsuranceRequest reqVO) {
        return insuranceService.getT02(reqVO);
    }

    @RequestMapping(value = "/getDateView", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult getDateView(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = insuranceService.getDateView(reqVO);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/T03", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult T03(@RequestBody InsuranceRequest reqVO) {
        // 設定必要參數
        Map<String, String> requireParams = new HashMap<String, String>();
        requireParams.put("MEMBER_ID", reqVO.getMemberId());
        requireParams.put("MEMBER_TOKEN", reqVO.getMemberToken());
        requireParams.put("RECEIVE_TYPE", "65");
        requireParams.put("SESSION_ID", reqVO.getSessionId());
        requireParams.put("LOGIN_IP", reqVO.getLoginIp());

        // 必要參數檢核
        if (checkParams(requireParams)) {
            return insuranceService.getT03(reqVO, requireParams);
        } else {
            return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
        }
    }

    @PostMapping("/getCreditCardView")
    public TPIGatewayResult getCreditCardView(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = insuranceService.getCreditCardView(reqVO);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/T04", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult T04(@RequestBody InsuranceRequest reqVO) {
        return insuranceService.getT04(reqVO);
    }

    @RequestMapping(value = "/topQuestions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult topQuestions(@RequestBody InsuranceRequest reqVO) {
        BotMessageVO botMessageVO = insuranceService.getTopQuestions(reqVO);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }

}
