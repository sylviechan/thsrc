package com.tp.tpigateway.modules.cathayins.service;

import java.util.Map;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.CarInsuranceRequest;

public interface InsCarInsuranceService {

	// flow call
	BotMessageVO memberInformationInitial(CarInsuranceRequest reqVO);

	BotMessageVO memberInformationCarSelected(CarInsuranceRequest reqVO);

	BotMessageVO memberInformationChangeNameSet1(CarInsuranceRequest reqVO);

	BotMessageVO memberInformationChangeNameSet2(CarInsuranceRequest reqVO);

	BotMessageVO memberInformationOpenCameraAlbum(CarInsuranceRequest reqVO);

	BotMessageVO cameraAlbumUPD(CarInsuranceRequest reqVO, Boolean isVehicleCertificate);

	BotMessageVO memberInformationChangeMarriageSet1(CarInsuranceRequest reqVO);

	BotMessageVO memberInformationChangeMarriage(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationInitial(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationCarSelected(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangeCellularPhoneSet1(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangePhoneSet1(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangeEmailSet1(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangeAddressSet1(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangeCellularPhoneSet2(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangePhoneSet2(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangeEmailSet2(CarInsuranceRequest reqVO);

	BotMessageVO carInsuranceCommunicationChangeAddressSet2(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationInitial(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationCarSelected(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationCarNoSet1(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationCarEngineSet1(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationCarDocumentSet1(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationCarNoSet2(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationCarEngineSet2(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationCarDocumentSet2(CarInsuranceRequest reqVO);

	BotMessageVO carRegistrationOpenCameraAlbum(CarInsuranceRequest reqVO);

	TPIGatewayResult carRegistrationSummaryPage(CarInsuranceRequest reqVO);
	
	BotMessageVO searchCarChangeList(CarInsuranceRequest reqVO);

	// Ajax
	TPIGatewayResult changeCarMemberCheck(CarInsuranceRequest reqVO);

	TPIGatewayResult changeCarInsuranceCheck(CarInsuranceRequest reqVO);

	TPIGatewayResult changeCarRegistrationCheck(CarInsuranceRequest reqVO);

	BotMessageVO changeCarRegistration(CarInsuranceRequest reqVO);

}
