package com.tp.tpigateway.modules.cathayins.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsProductTellRequest;

public interface InsProductTellService {

	/**
	 * 商品-初始畫面
	 * For 產險-商品大類
	 */
	TPIGatewayResult commodityBigSort(InsProductTellRequest reqVO);

	/**
	 * For 產險-商品中類
	 */
	TPIGatewayResult commodityMediumSort(InsProductTellRequest reqVO);

	/**
	 * For 產險-商品小類
	 */
	TPIGatewayResult commoditySmallSort(InsProductTellRequest reqVO);

	/**
	 * For 產險-承辦項目
	 */
	TPIGatewayResult undertakeProject(InsProductTellRequest reqVO);

	/**
	 * For 產險-項目內容
	 */
	TPIGatewayResult projectContent(InsProductTellRequest reqVO);

	/**
	 * For 產險-項目內容From A06
	 */
	TPIGatewayResult projectContentA06(InsProductTellRequest reqVO);
	
	/**
	 * For 產險-項目內容QA
	 */
	BotMessageVO projectContentQA(InsProductTellRequest reqVO);
}
