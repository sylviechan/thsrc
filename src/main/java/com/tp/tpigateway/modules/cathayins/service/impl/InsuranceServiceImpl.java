package com.tp.tpigateway.modules.cathayins.service.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotMessageVO.ContentItem;
import com.tp.common.model.BotMessageVO.DateView;
import com.tp.common.model.BotMessageVO.LinkListItem;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsuranceRequest;
import com.tp.common.util.DataUtils;
import com.tp.common.util.DateUtils;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.InsUrlGather;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathayins.fegin.InsuranceAPIFeignService;
import com.tp.tpigateway.modules.cathayins.model.InsQuickReplay;
import com.tp.tpigateway.modules.cathayins.mybatis.entity.TopQuestion;
import com.tp.tpigateway.modules.cathayins.mybatis.mapper.TopQuestionMapper;
import com.tp.tpigateway.modules.cathayins.service.InsuranceService;
import com.tp.tpigateway.modules.cathayins.utils.InsData;

@SuppressWarnings("unchecked")
@Service("insuranceServiceService")
public class InsuranceServiceImpl implements InsuranceService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	InsuranceAPIFeignService insuranceAPIFeignService;

	@Autowired
	PropUtils propUtils;

	@Autowired
	TopQuestionMapper topQuestionMapper;

	@Override
	public BotMessageVO getLogin(BotRequest botRequest) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());
		botResponseUtils.setLogin(botMessageVO);

		BotMessageVO messageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());
		String typeName = "";
		if (botRequest.getMsg().indexOf("車險") > -1) {
			typeName = "車險";
		} else if (botRequest.getMsg().indexOf("健康傷害險") > -1) {
			typeName = "健康傷害險";
		} else if (botRequest.getMsg().indexOf("旅遊綜合險") > -1) {
			typeName = "旅遊綜合險";
		} else {
			typeName = "其他";
		}
		botResponseUtils.setTextResult(messageVO, "咦，還沒註冊會員嗎？快來<a href=\"javascript:void(0);\" onclick=\"window.open('"
				+ InsUrlGather.OCH1_0200 + "');\">加入我們</a>吧!");
		botMessageVO.getContent().get(0).put("closeObject", messageVO.getContent());
		botMessageVO.getContent().get(0).put("typeName", typeName);
		botMessageVO.getContent().get(0).put("forgetPassword", InsUrlGather.OCH1_0300);
		botMessageVO.getContent().get(0).put("registered", InsUrlGather.OCH1_0200 + DataUtils.getRegFrom());
		botMessageVO.getContent().get(0).put("callagent", InsUrlGather.insurance_Contact);

		return botMessageVO;
	}

	@Override
	public TPIGatewayResult chooseCmd(InsuranceRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult defaultTpiResult = TPIGatewayResult.ok();
		TPIGatewayResult tpiGatewayResult = null;

		String cmd = reqVO.getCmd().toUpperCase();

		switch (cmd) {
		case "C01_1":
			botResponseUtils.setTextResult(vo, "請問你現在是否需要道路救援呢?還是只是要查資格呢?");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.C01_1_1,
					InsQuickReplay.C01_1_2);
			break;
		case "C01_2":
			botResponseUtils.setTextResult(vo, "道路救援免付費電話：0800-020-345，讓專業的客服為你服務~");
			botResponseUtils.setTextResult(vo, "更多服務內容說明、收費標準等資訊，可以到我們<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.insurance_Services + DataUtils.getRectifySSOId(reqVO.getSsoId()) + "');\">網站</a>了解詳情哦~");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.C02_2_1("0800020345"),
					InsQuickReplay.C01_2_5, InsQuickReplay.A05_OTHERHELP);
			break;
		case "C01_CANCEL":
			botResponseUtils.setTextResult(vo, "還有什麼需要阿發為你服務的呢?");
			break;
		case "道路救援服務詳細說明":
			botResponseUtils.setInsRescue(vo, "1", textRescue());
			break;
		case "C01_TODO":
			botResponseUtils.setTextResult(vo, "功能還不能使用喔~~");
			break;
		case "C05_1":
			botResponseUtils.setTextResult(vo, "請給我車主的身分證號/統編/居留證號喔");
			botResponseUtils.setTextResult(vo, "提醒你，電子強制保險證可於辦理車輛驗車、過戶或警察臨檢時使用喔!");
			break;
		case "C05_2":
			botResponseUtils.setTextResult(vo, "請給我你的車牌號碼，格式要像ABC-1234這樣喔");
			break;
		case "C05_OTHER":
			botResponseUtils.setTextResult(vo, "還有什麼需要阿發為你服務的呢?");
			break;
		case "A04_1":
			botResponseUtils.setTextResult(vo,
					"通過驗證升級，即可立即使用服務！<BR/>請輸入車險、健康傷害險或旅綜險之有效保單號碼，或前述險種2年內的報案/賠案號碼。<BR/>輸入時請輸入半形數字（避免全形，空白或其他符號）");
			// botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01,
			// InsQuickReplay.A04_1_1, InsQuickReplay.A04_1_2);
			break;
		case "A04_YES":
			botResponseUtils.setTextResult(vo, "請輸入車險、健康傷害險或旅綜險之有效保單號碼，或前述險種2年內的報案/賠案號碼。<BR/>輸入時請輸入半形數字（避免全形，空白或其他符號）");
			break;
		case "A04_CANCEL":
			botResponseUtils.setTextResult(vo,
					"咦？還沒通過保單驗證嗎？快來<a href=\"javascript:void(0);\" onclick=\"App.showUserMsg('進行保單驗證');App.sendMsgToChatWeb('會員升級');\">進行保單驗證</a>吧！");
			break;
		case "A05_CHECK":
			a05_check(vo, reqVO);
			break;
		case "A05_TRAVEL_CHECK":
			String[] titleArr = { "國泰產險商品", "銀行信用卡贈送", "刷國泰世華卡投保國泰人壽海外旅平險贈送" };
			String[] labelArr = { "1. 與國泰產險購買的旅遊綜合保險保單<br />2. 與國泰人壽購買旅遊平安保險加購的國泰產險不便險保單",
					"刷國泰世華、花旗、滙豐信用卡購買機票或支付團費贈送的旅遊綜合保險", "單人單件保費達1100元以上，贈送之旅遊不便險保障" };

			QuickReplay[] quickReplayArr = { InsQuickReplay.A05_TRAVEL_1, InsQuickReplay.A05_TRAVEL_2,
					InsQuickReplay.A05_TRAVEL_3 };

			a05_travel(vo, titleArr, labelArr, quickReplayArr);

			defaultTpiResult.setMsgList(Arrays.asList(InsQuickReplay.A05_TRAVEL_1.getText(),
					InsQuickReplay.A05_TRAVEL_2.getText(), InsQuickReplay.A05_TRAVEL_3.getText()));
			defaultTpiResult.setOptionList(Arrays.asList(InsQuickReplay.A05_TRAVEL_1, InsQuickReplay.A05_TRAVEL_2,
					InsQuickReplay.A05_TRAVEL_3));

			break;
		case "A05_TRAVEL_2":
			botResponseUtils.setTextResult(vo, "國泰產險與國泰世華、花旗以及滙豐銀行有合作。你想知道哪一家銀行的信用卡所送的旅遊綜合保險呢？");

			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A05_TRAVEL_2_1,
					InsQuickReplay.A05_TRAVEL_2_2, InsQuickReplay.A05_TRAVEL_2_3, InsQuickReplay.A05_OTHERHELP);

			defaultTpiResult.setMsgList(Arrays.asList(InsQuickReplay.A05_TRAVEL_2_1.getText(),
					InsQuickReplay.A05_TRAVEL_2_2.getText(), InsQuickReplay.A05_TRAVEL_2_3.getText()));

			break;
		case "A05_TRAVEL_2_1":
			tpiGatewayResult = a05_travel2(vo, cmd);
			break;
		case "A05_TRAVEL_2_2":
			tpiGatewayResult = a05_travel2(vo, cmd);
			break;
		case "A05_TRAVEL_2_3":
			tpiGatewayResult = a05_travel2(vo, cmd);
			break;
		case "A05_TRAVEL_3":
			a05_travel3(vo);
			break;
		case "A05_TRAVEL_T01":
			botResponseUtils.setTextResult(vo, "TODO CALL API T01");
			break;
		case "A05_ASK_OTHER":
			botResponseUtils.setTextResult(vo, "咦，不查了嗎？若需要查詢保單內容，可以隨時找我喔~");
			QuickReplay quickReplay = new QuickReplay(2, "看車險保單", "車險", "保單", "", "") {
			};
			botResponseUtils.setQuickReplayResult(vo, quickReplay, InsQuickReplay.A05_OTHERHELP);
			break;
		case "TOAGENT_FALLBACK":
			botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");
			botResponseUtils.setQuickReplayResult(vo, InsQuickReplay.A05_OTHERHELP);
			break;
		default:
			break;
		}

		if (tpiGatewayResult != null) {
			return tpiGatewayResult;
		}

		return defaultTpiResult.setData(vo);
	}

	@Override
	public TPIGatewayResult getA01(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);
		Map<String, Object> a01Result = insuranceAPIFeignService.getA01(map);
		return TPIGatewayResult.ok().setData(a01Result);

	}

	@Override
	public TPIGatewayResult getA02(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);
		Map<String, Object> a02Result = insuranceAPIFeignService.getA02(map);
		return TPIGatewayResult.ok().setData(a02Result);

	}

	@Override
	public TPIGatewayResult getA03(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);
		Map<String, Object> a03Result = insuranceAPIFeignService.getA03(map);
		return TPIGatewayResult.ok().setData(a03Result);

	}

	@Override
	public Map<String, Object> getA04(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		Map<String, Object> returnResult = new HashMap<>();
		boolean waitVALID_KEY_NO = false;
		Integer rspMEMBER_LEVEL = null;

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();

		// 產險測試環境使用這一支
		String rpjson = insuranceAPIFeignService.getA04(map);

		logger.info("rpjson : " + rpjson);
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);

			Map<String, Object> detail = (Map<String, Object>) result.get("detail");

			String resultCode = getString(detail.get("RETURN_CODE"));
			Map datas = MapUtils.getMap(detail, "DATAS");
			rspMEMBER_LEVEL = MapUtils.getInteger(datas, "MEMBER_LEVEL", 0);

			if ("0000".equals(resultCode)) {
				// String replyText = "A04 api success";
				// String replyText = "認證成功！現在開始你享有完整的會員權益與功能了~";
				botResponseUtils.setTextResult(vo, "認證成功！現在開始你享有完整的會員權益與功能了~");
				botResponseUtils.setTextResult(vo, "你可以向上捲動到認證前的問題，重新進行，也可以直接點選下方按鈕，再次選擇想要問阿發的問題，或者你也可以直接發問哦~");
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A05_OTHERHELP);
				/*
				 * }else if("1002".equals(resultCode)) { String replyText =
				 * "你所輸入的認證號碼[{VALID_KEY_NO}]有誤或不存在，請再次確認！"; botResponseUtils.setTextResult(vo,
				 * replyText); }else if("1003".equals(resultCode) || "9999".equals(resultCode))
				 * { String replyText = "系統忙碌中，請稍後"; botResponseUtils.setTextResult(vo,
				 * replyText); }else if("1004".equals(resultCode)) { String replyText =
				 * "認證失敗，有可能是你未登入或憑證已失效，請重新登入"; botResponseUtils.setQuickReplayResult(vo,
				 * BotMessageVO.LTYPE_01, InsQuickReplay.A04_2_1);
				 * botResponseUtils.setTextResult(vo, replyText);
				 */
			} else {
				// TODO 目前使用測試資料會回傳code9999，"您已經是認證會員了!"的訊息，該錯誤訊息代碼與其他的相同無法獨立判斷，先寫死在這邊
				String resultMsg = getString(detail.get("RETURN_MSG"));
				if (StringUtils.contains(resultMsg, "您已經是認證會員了")) {
					botResponseUtils.setTextResult(vo, resultMsg);
				} else {
					int waitCount = NumberUtils.toInt(reqVO.getWaitCount(), 0);
					if (waitCount < 2) {
						waitVALID_KEY_NO = true;
						botResponseUtils.setTextResult(vo, "咦，阿發找不到資料耶，能再告訴阿發一次嗎？");
					} else { // 失敗時，需分第二次時不同訊息
						botResponseUtils.setTextResult(vo, "不好意思，阿發還是找不到這筆資料耶，請專業的客服來協助你好嗎?");
					}
				}

				// String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A05_2_3,
						InsQuickReplay.A05_OTHERHELP);
			}

		} else {
			String replyText = "A04 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("waitVALID_KEY_NO", waitVALID_KEY_NO);
		returnResult.put("MEMBER_LEVEL", rspMEMBER_LEVEL);

		return returnResult;
	}

	@Override
	public BotMessageVO getA05(InsuranceRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(vo, "登入成功~阿發馬上來調你的保單資料，稍等一下喔~");
		Map<String, Object> result = new HashMap<String, Object>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA05(map);

		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);

			Map<String, Object> detail = (Map<String, Object>) MapUtils.getMap(result, "detail");
			// (Map<String, Object>) result.get("detail");

			String resultCode = MapUtils.getString(detail, "RETURN_CODE");
			// getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
				int type = 0;
				String intent = "保單";
				if ("CAR".equals(reqVO.getCmd().toUpperCase())) {
					type = 0;
					dataList = (List<Map<String, Object>>) detail.get("CAR");
					if (dataList != null && dataList.size() > 0) {
						for (int i = 0; i < dataList.size(); i++) {
							dataList.get(i).put("dlAction", 2);
							dataList.get(i).put("dlAlt", "保單明細0");
							dataList.get(i).put("dlText", "查詢保單明細");

							String secondBotIntent = "clickInsListComp_Car";
							dataList.get(i).put("dlIntent", intent);
							dataList.get(i).put("dlSecondBotIntent", secondBotIntent);
						}
						botResponseUtils.setInsList(vo, dataList, type);
					} else {
						botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的有效保單喔！");
						botResponseUtils.setTextResult(vo,
								"你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.ECIE_1000
										+ DataUtils.getRectifySSOId(reqVO.getSsoId()) + "');\">bobe線上投保網站</a>，直接投保哦~");

						// botResponseUtils.setTextResult(vo, replyText);
						botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A05_RESET,
								InsQuickReplay.A05_OTHERHELP);
					}
				} else if ("HEALTH".equals(reqVO.getCmd().toUpperCase())) {
					type = 1;
					dataList = (List<Map<String, Object>>) detail.get("HEALTH");
					if (dataList != null && dataList.size() > 0) {
						for (int i = 0; i < dataList.size(); i++) {
							// 2019/12/03，透過CUSTOMER_COUNT判斷被保人數
							Integer customerCount = MapUtils.getInteger(dataList.get(i), "CUSTOMER_COUNT", 1);
							String customerName = MapUtils.getString(dataList.get(i), "CUSTOMER_NAME");
							if (customerCount != null && customerCount > 1) {
								dataList.get(i).put("CUSTOMER_NAME", customerName + " 等 " + customerCount + " 人");
							}
							dataList.get(i).put("dlAction", 2);
							dataList.get(i).put("dlAlt", "保單明細1");
							dataList.get(i).put("dlText", "查詢保單明細");

							String secondBotIntent = "clickInsListComp_Health";
							dataList.get(i).put("dlIntent", intent);
							dataList.get(i).put("dlSecondBotIntent", secondBotIntent);
						}
						botResponseUtils.setInsList(vo, dataList, type);
					} else {
						botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的有效保單喔！");
						botResponseUtils.setTextResult(vo,
								"你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.ECIE_1000
										+ DataUtils.getRectifySSOId(reqVO.getSsoId()) + "');\">bobe線上投保網站</a>，直接投保哦~");

						// botResponseUtils.setTextResult(vo, replyText);
						botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A05_RESET,
								InsQuickReplay.A05_OTHERHELP);
					}
				} else if ("TRAVEL".equals(reqVO.getCmd().toUpperCase())) {
					type = 2;
					dataList = (List<Map<String, Object>>) detail.get("TRAVEL");
					if (dataList != null && dataList.size() > 0) {
						for (int i = 0; i < dataList.size(); i++) {
							// 2019/12/03，透過CUSTOMER_COUNT判斷被保人數
							Integer customerCount = MapUtils.getInteger(dataList.get(i), "CUSTOMER_COUNT", 1);
							String customerName = MapUtils.getString(dataList.get(i), "CUSTOMER_NAME");
							if (customerCount != null && customerCount > 1) {
								dataList.get(i).put("CUSTOMER_NAME", customerName + " 等 " + customerCount + " 人");
							}
							dataList.get(i).put("dlAction", 2);
							dataList.get(i).put("dlAlt", "保單明細2");
							dataList.get(i).put("dlText", "查詢保單明細");

							String secondBotIntent = "clickInsListComp_Travel";
							dataList.get(i).put("dlIntent", intent);
							dataList.get(i).put("dlSecondBotIntent", secondBotIntent);
						}
						botResponseUtils.setInsList(vo, dataList, type);
					} else {
						botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的有效保單或過期半年內的保單喔！");
						botResponseUtils.setTextResult(vo,
								"你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.ECIE_1000
										+ DataUtils.getRectifySSOId(reqVO.getSsoId()) + "');\">bobe線上投保網站</a>，直接投保哦~");

						// botResponseUtils.setTextResult(vo, replyText);
						botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A05_RESET,
								InsQuickReplay.A05_OTHERHELP);
					}
				}
			} else {
				botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的有效保單喔！");
				botResponseUtils.setTextResult(vo,
						"你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.ECIE_1000
								+ DataUtils.getRectifySSOId(reqVO.getSsoId()) + "');\">bobe線上投保網站</a>，直接投保哦~");

				// botResponseUtils.setTextResult(vo, replyText);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A05_RESET,
						InsQuickReplay.A05_OTHERHELP);
			}
		} else {
			String replyText = "A05 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		return vo;
	}

	public void a05_travel(BotMessageVO vo, String[] titleArr, String[] labelArr, QuickReplay[] quickReplayArr) {
		botResponseUtils.setTextResult(vo, "阿發可以幫你查這些");

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		for (int i = 0; i < titleArr.length; i++) {
			CardItem card = vo.new CardItem();
			card.setCName("");
			card.setCWidth(BotMessageVO.CWIDTH_250);
			card.setCMessage("");
			card.setCTextType("11");
			card.setCTitle(titleArr[i]);

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			CTextItem cText = vo.new CTextItem();
			cText.setCLabel(labelArr[i]);
			cText.setCText("");
			cTextList.add(cText.getCTexts());
			card.setCTexts(cTextList);

			card.setCLinkType(1);

			card.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplayArr[i]));
			cardList.add(card.getCards());
		}

		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A05_OTHERHELP);
	}

	public TPIGatewayResult a05_travel2(BotMessageVO vo, String cmd) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		String bankName = "";
		String fileName = "";
		switch (cmd) {
		case "A05_TRAVEL_2_1":
			bankName = "國泰世華銀行";
			fileName = "cathay.pdf";
			break;
		case "A05_TRAVEL_2_2":
			bankName = "花旗銀行";
			fileName = "citibank.pdf";
			break;
		case "A05_TRAVEL_2_3":
			bankName = "滙豐銀行";
			fileName = "hsbc.pdf";
			break;
		}
		String downloadUrl = "resources/cathayins/file/" + fileName;
		botResponseUtils.setTextResult(vo, "以下是<span style='color:red'>" + bankName + "</span>信用卡所贈送的保障內容：");

		// 旅遊綜合險卡種比較表 圖片待補 先用其他張代替
		// botResponseUtils.setImageResult31(vo, imgUrl);
		List<Map<String, Object>> pdfCardList = new ArrayList<Map<String, Object>>();

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImageUrl("card13-1.png");
		// cImageData.setCImage("img-responsive");
		cImageData.setCImage("img-credbank06");
		cImageData.setCImageTextType(11);

		List<Map<String, Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageText("信用卡權益說明");
		cImageTextList.add(cImageText.getCImageTexts());

		cImageData.setCImageTexts(cImageTextList);

		CardItem pdfCard = vo.new CardItem();

		pdfCard.setCImageData(cImageData.getCImageDatas());
		pdfCard.setCName("");
		pdfCard.setCWidth(BotMessageVO.CWIDTH_250);
		pdfCard.setCTextType("");
		pdfCard.setCLinkType(1);

		// QuickReplay[] link1s = { new InsQuickReplay(5, "下載", "App.download_file('" +
		// downloadUrl + "', '" + fileName + "');", "", "", "") };
		QuickReplay[] link1s = { new InsQuickReplay(4, "下載", "下載", "", downloadUrl, "") };

		pdfCard.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));

		pdfCardList.add(pdfCard.getCards());
		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, pdfCardList);

		botResponseUtils.setTextResult(vo,
				"只要你用<span style='color:red'>" + bankName + "</span>信用卡，為自己或家人(限配偶及未滿25足歲之未婚子女)");

		botResponseUtils.setTextResult(vo, "支付公共運輸工具全部票款，或80%以上之團費，即可享有各項保障喔");

		botResponseUtils.setTextResult(vo, "你想了解哪個項目呢？");

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		String[] labelArr = { "旅遊平安險", "不便險" };
		String[] textArr = { "與「人」相關的保障，如意外受傷、疾病、醫療等等", "與「物」相關的保障，如班機延誤、行李遺失、竊盜、租車等" };

		QuickReplay q1 = InsQuickReplay.A05_TRAVEL_2_X_1;
		QuickReplay q2 = InsQuickReplay.A05_TRAVEL_2_X_2;

		QuickReplay[] quickReplayArr = { q1, q2 };

		for (int j = 0; j < labelArr.length; j++) {
			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			cards.setCTextType("10");

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			CTextItem cText = vo.new CTextItem();
			cText.setCLabel(labelArr[j]);
			cText.setCText(textArr[j]);
			cTextList.add(cText.getCTexts());
			cards.setCTexts(cTextList);

			// LifeLink[] link1s = {new LifeLink(2, labelArr[j], "A05_TRAVEL_T01_" + (j +
			// 1), "", "", "")};
			// cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));
			cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplayArr[j]));

			cards.setCLinkType(1);
			cardList.add(cards.getCards());
		}
		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));

		return tpiGatewayResult.setData(vo);
	}

	public void a05_travel3(BotMessageVO vo) {
		botResponseUtils.setTextResult(vo, "只要以國泰世華信用卡為自己或家人刷卡投保國泰人壽海外旅平險，單人單件達NT$1,100以上，即可享有海外旅遊不便險保障");
		botResponseUtils.setTextResult(vo, "你想了解哪個項目呢？");

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		String[] labelArr = { "不便險" };
		String[] textArr = { "與「物」相關的保障，如班機延誤、行李遺失、竊盜、租車等" };

		for (int j = 0; j < labelArr.length; j++) {
			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			cards.setCTextType("10");

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			CTextItem cText = vo.new CTextItem();
			cText.setCLabel(labelArr[j]);
			cText.setCText(textArr[j]);
			cTextList.add(cText.getCTexts());
			cards.setCTexts(cTextList);

			QuickReplay[] quickReplayArr = { InsQuickReplay.A05_TRAVEL_2_X_2 };
			cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplayArr));
			cards.setCLinkType(1);
			cardList.add(cards.getCards());
		}
		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
	}

	public void a05_check(BotMessageVO vo, InsuranceRequest reqVO) {
		String conflictType = reqVO.getConflictType();
		// String insType = reqVO.getInsType();

		List<String> imageList = new ArrayList<>();
		List<String> strList = new ArrayList<>();

		if (StringUtils.isNotEmpty(conflictType)) {
			// 1:車險 2:旅綜險 3:健傷險 4:其他
			if (StringUtils.contains(InsData.conflictTypeMap.get(conflictType), "1")) {
				imageList.add("card-Ins-car@2x.png");
				strList.add("車險");
			}
			if (StringUtils.contains(InsData.conflictTypeMap.get(conflictType), "3")) {
				imageList.add("card-Ins-hospital@2x.png");
				strList.add("健康與傷害險");
			}
			if (StringUtils.contains(InsData.conflictTypeMap.get(conflictType), "2")) {
				imageList.add("policy_06.png");
				strList.add("旅遊綜合保險");
			}
		} else {
			imageList.add("card-Ins-car@2x.png");
			strList.add("車險");

			imageList.add("card-Ins-hospital@2x.png");
			strList.add("健康與傷害險");

			imageList.add("policy_06.png");
			strList.add("旅遊綜合保險");
		}

		botResponseUtils.setTextResult(vo, "阿發可以查車險、健康與傷害險和旅遊綜合保險，你要查哪種呢？其他險種的保單內容查詢，請洽詢你的業務人員，或由專人替你服務哦~");

		List<Map<String, Object>> cardList = new ArrayList<>();

		for (int i = 0; i < imageList.size(); i++) {
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImageUrl(imageList.get(i));
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageTextType(1);
			CardItem cards = vo.new CardItem();
			cards.setCImageData(cImageData.getCImageDatas());
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			cards.setCTextType("");
			cards.setCLinkType(1);

			QuickReplay q = new InsQuickReplay(2, strList.get(i), "我要查" + strList.get(i) + "保單");
			cards.setCLinkList(botResponseUtils.getCLinkList(vo, q));
			cardList.add(cards.getCards());
		}
		botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A05_2_3,
				InsQuickReplay.A05_OTHERHELP);
	}

	@Override
	public Map<String, Object> getCarDetail(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");
				Map<String, Object> carDataList = (Map<String, Object>) dataList.get("CAR_DATA_LIST");
				Map<String, Object> carRescueMap = (Map<String, Object>) dataList.get("MAP_CAR_RESCUE");
				List<Map<String, Object>> listCarMain = (List<Map<String, Object>>) dataList.get("LIST_CAR_MAIN");
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				if (listCarMain.size() > 0) {
					botResponseUtils.setTextResult(vo,
							"阿發幫你查到" + getString(carDataList.get("ORI_VEHICLE_KIND_NAME"))
									+ getString(carDataList.get("CAR_NO")) + "，要保人為" + getString(reqVO.getBirthday())
									+ "，被保人為" + getString(carDataList.get("CUSTOMER_NAME")) + "，所保的保單如下:");
					String[] titleLabel = { "保單名稱", "保單號碼" };
					String[] label = { "保費", "保單起始日", "保單到期日" };
					String[] titleText = { "PRODUCT_POLICY_NAME", "POLICY_NO" };
					String[] text = { "DISCOUNT_PREMIUM", "START_DATE", "END_DATE" };

					for (int i = 0; i < listCarMain.size(); i++) {
						CImageDataItem cImageData = vo.new CImageDataItem();
						cImageData.setCImage(BotMessageVO.IMG_CB00);
						cImageData.setCImageUrl("");
						cImageData.setCImageTextType(9);
						List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();
						List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
						List<Map<String, Object>> cTextList2 = new ArrayList<Map<String, Object>>();

						for (int k = 0; k < titleLabel.length; k++) {
							CImageTextItem cImageText = vo.new CImageTextItem();
							cImageText.setCImageTextTitle(titleLabel[k]);
							cImageText.setCImageText(getString(listCarMain.get(i).get(titleText[k])));
							cImageTextList.add(cImageText.getCImageTexts());
						}
						cImageData.setCImageTexts(cImageTextList);
						CardItem cards = vo.new CardItem();
						cards.setCImageData(cImageData.getCImageDatas());
						cards.setCName("");
						cards.setCWidth(BotMessageVO.CWIDTH_250);
						cards.setCMessage("");
						cards.setCTextType("111");

						for (int j = 0; j < text.length; j++) {
							CTextItem cText = vo.new CTextItem();

							cText.setCLabel(label[j]);
							if (j == 0) {
								DecimalFormat df = new DecimalFormat("###,##0");
								Double amount = Double.parseDouble(getString(listCarMain.get(i).get(text[j])));
								cText.setCText("$ " + df.format(amount));
							} else {
								cText.setCText(getString(listCarMain.get(i).get(text[j])));
							}

							cTextList.add(cText.getCTexts());
						}
						cards.setCLinkType(1);

						Map<String, Object> parameter = new HashMap<>();
						parameter.put("LIST_CAR_MAIN", getString(reqVO.getListCarMain()));
						parameter.put("CAR_NO", getString(reqVO.getCarNo()));
						// 補傳 policyNo
						parameter.put("POLICY_NO", getString(listCarMain.get(i).get("POLICY_NO")));

						InsQuickReplay[] link1s = { new InsQuickReplay(2, "看保障內容", "看保障內容CAR" + i, "保單",
								"clickInsContentComp_Car", parameter) };

						cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));

						cards.setCTexts(cTextList);
						cards.setCTexts2(cTextList2);
						cardList.add(cards.getCards());
					}

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

					if (carRescueMap != null && carRescueMap.containsKey("RESCUE_CARD_DATA")) {
						botResponseUtils.setTextResult(vo, "另外，這台車的道路救援資格是這樣:");
						List<String> carRescue = (List<String>) carRescueMap.get("RESCUE_CARD_DATA");
						for (int i = 0; i < carRescue.size(); i++) {
							String carRescueStr = "";
							carRescueStr = carRescue.get(i);
							botResponseUtils.setTextResult(vo, carRescueStr);
						}
					} else {
						botResponseUtils.setTextResult(vo, "另外，這台車沒有道路救援資格");
					}
					botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

					Map<String, Object> parameter = new HashMap<>();
					parameter.put("LIST_CAR_MAIN", getString(reqVO.getListCarMain()));
					parameter.put("CAR_NO", getString(reqVO.getCarNo()));
					QuickReplay A06_2_1 = InsQuickReplay.A06_2_1(parameter);

					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_1, InsQuickReplay.A06_2_2,
							InsQuickReplay.A05_OTHERHELP);

					msgList.addAll(Arrays.asList(new String[] { A06_2_1.getText(), InsQuickReplay.A06_2_2.getText() }));
					optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_1, InsQuickReplay.A06_2_2 }));

				} else {
					botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的車險保單喔");
				}
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}
		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	public Map<String, Object> getCarDetailNormal(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA05(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				List<Map<String, Object>> dataList = (List<Map<String, Object>>) detail.get("CAR");

				for (Map<String, Object> data : dataList) {
					// 用車牌號碼和LIST_CAR_MAIN判斷是不是使用者選擇的保單
					List<Map<String, Object>> listCarMain = (List<Map<String, Object>>) data.get("LIST_CAR_MAIN");

					String listCarJson = "";
					ObjectMapper objectMapper = new ObjectMapper();
					try {
						listCarJson = objectMapper.writeValueAsString(listCarMain);
					} catch (JsonProcessingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (StringUtils.equals(getString(data.get("CAR_NO")), reqVO.getCarNo())
							&& StringUtils.equals(getString(listCarJson), reqVO.getListCarMain())) {
						List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
						if (listCarMain.size() > 0) {
							botResponseUtils.setTextResult(vo,
									"阿發幫你查到" + getString(data.get("VEHICLE_KIND_NAME")) + getString(data.get("CAR_NO"))
											+ "，要保人為" + getString(data.get("APPLICANT_OF_CUSTOMER_NAME")) + "，被保人為"
											+ getString(data.get("CUSTOMER_NAME")) + "，所保的保單如下:");
							String[] titleLabel = { "保單名稱", "保單號碼" };
							String[] label = { "保費", "保單起始日", "保單到期日" };
							String[] titleText = { "PRODUCT_POLICY_NAME", "POLICY_NO" };
							String[] text = { "DISCOUNT_PREMIUM", "START_DATE", "END_DATE" };

							Map<String, Object> parameter = new HashMap<>();
							parameter.put("LIST_CAR_MAIN", getString(reqVO.getListCarMain()));
							parameter.put("CAR_NO", getString(reqVO.getCarNo()));

							for (int i = 0; i < listCarMain.size(); i++) {
								CImageDataItem cImageData = vo.new CImageDataItem();
								cImageData.setCImage(BotMessageVO.IMG_CB00);
								cImageData.setCImageUrl("");
								cImageData.setCImageTextType(9);
								List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();
								List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
								List<Map<String, Object>> cTextList2 = new ArrayList<Map<String, Object>>();

								for (int k = 0; k < titleLabel.length; k++) {
									CImageTextItem cImageText = vo.new CImageTextItem();
									cImageText.setCImageTextTitle(titleLabel[k]);
									cImageText.setCImageText(getString(listCarMain.get(i).get(titleText[k])));
									cImageTextList.add(cImageText.getCImageTexts());
								}
								cImageData.setCImageTexts(cImageTextList);
								CardItem cards = vo.new CardItem();
								cards.setCImageData(cImageData.getCImageDatas());
								cards.setCName("");
								cards.setCWidth(BotMessageVO.CWIDTH_250);
								cards.setCMessage("");
								cards.setCTextType("111");

								for (int j = 0; j < text.length; j++) {
									CTextItem cText = vo.new CTextItem();

									cText.setCLabel(label[j]);
									if (j == 0) {
										DecimalFormat df = new DecimalFormat("###,##0");
										Double amount = Double.parseDouble(getString(listCarMain.get(i).get(text[j])));
										cText.setCText("$ " + df.format(amount));
									} else {
										cText.setCText(getString(listCarMain.get(i).get(text[j])));
									}

									cTextList.add(cText.getCTexts());
								}
								cards.setCLinkType(1);

								InsQuickReplay[] link1s = { new InsQuickReplay(2, "看保障內容", "看保障內容CAR" + i, "保單",
										"clickInsContentComp_Car", parameter) };

								cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));

								cards.setCTexts(cTextList);
								cards.setCTexts2(cTextList2);
								cardList.add(cards.getCards());
							}

							botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

							botResponseUtils.setTextResult(vo, "升級以使用更多服務!<br />現在升級認證會員，將可使用更多專屬服務，等你來體驗");
							botResponseUtils.setImageResult31(vo, "upgradeMemberLevel01.png");
							String annoTitle = "注意事項";
							String imgUrl = "resources/common/images/upgradeMemberLevel02.png";
							String annoText = "<img src='" + imgUrl
									+ "' alt='' class='img-responsive' onclick=\"PicComponent2.send('" + imgUrl
									+ "');\">";
							botResponseUtils.setNoteResult(vo, annoTitle, annoText);
							botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A06_2_4,
									InsQuickReplay.A06_2_2, InsQuickReplay.A05_OTHERHELP);

							msgList.addAll(Arrays.asList(new String[] { InsQuickReplay.A06_2_2.getText() }));
							optionList.addAll(Arrays.asList(new QuickReplay[] { InsQuickReplay.A06_2_2 }));

						} else {
							botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的車險保單喔");
						}
						break;
					}
				}

			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}
		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getCarContent(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");
				List<Map<String, Object>> listCarMain = (List<Map<String, Object>>) dataList.get("LIST_CAR_MAIN");
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				String policyNo = getString(reqVO.getPolicyNo());

				for (Map<String, Object> carMain : listCarMain) {
					if (StringUtils.equals(policyNo, getString(carMain.get("POLICY_NO")))) {
						List<List<String>> categoryDataList = (List<List<String>>) carMain.get("CATEGORY_DATA_LIST");
						// 到牌卡範例那裏找注意事項的範本來用
						// List<List<String>> categoryMemoList = (List<List<String>>)
						// carMain.get("CATEGORY_DATA_MEMO");
						int totalNum = categoryDataList.size(); // 總項目數
						int pageNum = 5; // 每張牌卡最多5個項目

						int cardAmount = totalNum % pageNum == 0 ? totalNum / pageNum
								: Math.floorDiv(totalNum, pageNum) + 1;
						;
						for (int l = 0; l < cardAmount; l++) {
							int start = l * pageNum;
							int end = (l + 1) * pageNum > totalNum ? totalNum : (l + 1) * pageNum;

							List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

							List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();

							CImageDataItem cImageData = vo.new CImageDataItem();
							cImageData.setCImage(BotMessageVO.IMG_CC);

							if (StringUtils.equals(getString(carMain.get("PRODUCT_POLICY_NAME")), "任意險")) {
								cImageData.setCImageUrl("parking07-01.jpg");
							} else if (StringUtils.equals(getString(carMain.get("PRODUCT_POLICY_NAME")), "強制險")) {
								cImageData.setCImageUrl("parking06-01.jpg");
							} else if (StringUtils.equals(getString(carMain.get("PRODUCT_POLICY_NAME")), "駕駛人傷害險")) {
								cImageData.setCImageUrl("parking04-01.jpg");
							} else {
								cImageData.setCImageUrl("card_todo_1.png");
							}

							cImageData.setCImageTextType(1);

							/*
							 * CImageTextItem cImageText = vo.new CImageTextItem();
							 * cImageText.setCImageText(getString(carMain.get("PRODUCT_POLICY_NAME")));
							 * cImageTextList.add(cImageText.getCImageTexts());
							 * 
							 * 
							 * cImageData.setCImageTexts(cImageTextList);
							 */
							CardItem cards = vo.new CardItem();
							cards.setCImageData(cImageData.getCImageDatas());
							cards.setCName("");
							cards.setCWidth(BotMessageVO.CWIDTH_200);
							cards.setCMessage("");
							cards.setCTextType("3");

							for (int j = start; j < end; j++) {
								CTextItem cText = vo.new CTextItem();
								String categoryName = getString(categoryDataList.get(j).get(1));
								String categoryDescription = getString(categoryDataList.get(j).get(2));
								String amountData = getString(categoryDataList.get(j).get(3));
								String ddcbAmountData = getString(categoryDataList.get(j).get(4)); // 自負額
								// String discountPremium = getString(categoryDataList.get(j).get(7)); //保費
								cText.setCLabel(categoryName);

								String categoryContent = "";
								if (StringUtils.isNotBlank(categoryDescription)) {
									categoryContent += categoryDescription;
								}

								if (StringUtils.isNotBlank(amountData)) {
									if (StringUtils.isNotBlank(categoryContent)) {
										categoryContent += "<br />";
									}
									categoryContent += amountData;
								}

								if (StringUtils.isNotBlank(ddcbAmountData)) {
									if (StringUtils.isNotBlank(categoryContent)) {
										categoryContent += "<br />";
									}
									categoryContent += ddcbAmountData;
								}

								cText.setCText(categoryContent);
								cTextList.add(cText.getCTexts());

							}

							cards.setCTexts(cTextList);

							cardList.add(cards.getCards());
						}

						botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
						botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");
						Map<String, Object> parameter = new HashMap<>();
						parameter.put("LIST_CAR_MAIN", getString(reqVO.getListCarMain()));
						parameter.put("CAR_NO", getString(reqVO.getCarNo()));

						QuickReplay A06_2_5 = InsQuickReplay.A06_2_5(parameter);

						botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_5,
								InsQuickReplay.A06_2_2, InsQuickReplay.A05_OTHERHELP);

						msgList.addAll(
								Arrays.asList(new String[] { A06_2_5.getText(), InsQuickReplay.A06_2_2.getText() }));

						break;
					}
				}
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}
		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getCarInfo(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				Map<String, Object> categoryDataList = (Map<String, Object>) dataList.get("CAR_DATA_LIST");

				String telext = getString(categoryDataList.get("INSTELEXT"));
				String phone = getString(categoryDataList.get("PHONE_CODE"));
				phone = phone.replaceAll("/", "");
				phone = phone.replaceAll(" ", "");
				if (StringUtils.isNotBlank(telext)) {
					phone = phone + "#" + telext + " /";
				}
				String[] titleArr = { "車主資料", "通訊資料", "車籍資料" };
				String[][] label2dArr = { { "姓名", "婚姻" }, { "連絡電話", "地址", "Email" }, { "車牌號碼", "引擎/車身號碼", "發照年月" } };
				String[][] text2dArr = {
						{ getString(categoryDataList.get("CUSTOMER_NAME")),
								getString(categoryDataList.get("MARRIAGE")) },
						{ phone + "<br />" + getString(categoryDataList.get("CELLULAR_PHONE"))
						// , getString(categoryDataList.get("ADDRESS")).substring(0,11) + "<br />" +
						// getString(categoryDataList.get("ADDRESS")).substring(11)
								, getString(categoryDataList.get("ADDRESS")),
								getString(categoryDataList.get("EMAIL")) },
						{ getString(categoryDataList.get("CAR_NO")), getString(categoryDataList.get("ENGINE_NO")),
								getString(categoryDataList.get("ORIGIN_ISSUE_DATE")) } };
				String[] linkTextArr = { "修改車主資料", "修改通訊資料", "修改車籍資料" };
				for (int i = 0; i < 3; i++) {
					CardItem card = vo.new CardItem();
					card.setCName("");
					card.setCWidth(BotMessageVO.CWIDTH_250);
					card.setCMessage("");
					card.setCTextType("11");
					card.setCTitle(titleArr[i]);
					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

					for (int j = 0; j < label2dArr[i].length; j++) {
						CTextItem cText = vo.new CTextItem();
						cText.setCLabel(label2dArr[i][j]);
						cText.setCText(text2dArr[i][j]);
						cTextList.add(cText.getCTexts());
					}
					card.setCTexts(cTextList);

					card.setCLinkType(1);
					InsQuickReplay[] link1s = {
							new InsQuickReplay(2, linkTextArr[i], "C02" + linkTextArr[i], "保單", "", "") };

					card.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));
					cardList.add(card.getCards());
				}

				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

				botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");
				Map<String, Object> parameter = new HashMap<>();
				parameter.put("LIST_CAR_MAIN", getString(reqVO.getListCarMain()));
				parameter.put("CAR_NO", getString(reqVO.getCarNo()));

				QuickReplay A06_2_5 = InsQuickReplay.A06_2_5(parameter);

				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_5, InsQuickReplay.A06_2_2,
						InsQuickReplay.A05_OTHERHELP);

				msgList.addAll(Arrays.asList(new String[] { A06_2_5.getText(), InsQuickReplay.A06_2_2.getText() }));
				optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_5, InsQuickReplay.A06_2_2 }));
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}
		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getHealthDetail(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				botResponseUtils.setTextResult(vo, "阿發幫你查到保單詳細資訊如下：");
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("PERSONAL_OR_TEAM", dataList.get("PERSONAL_OR_TEAM"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));

				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

				CImageDataItem cImageData = vo.new CImageDataItem();
				cImageData.setCImage(BotMessageVO.IMG_CB00);
				cImageData.setCImageUrl("");
				cImageData.setCImageTextType(9);
				List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();
				String[] titleLabel = { "保單名稱", "保單號碼" };
				String[] label = { "保費", "要保人", "被保人", "保險期間", "來年是否自動續保", "" };
				String[] titleText = { "PRODUCT_POLICY_NAME", "POLICY_NO" };
				String[] text = { "DISCOUNT_PREMIUM", "APPLICANT_OF_CUSTOMER_NAME", "CUSTOMER_NAME",
						"START_DATETIME END_DATETIME", "IS_NEXTYEAR_AUTOCONT_NM", "(本保險為非保證續保商品)" };
				// 2019/12/03，透過CUSTOMER_COUNT判斷被保人數
				Integer customerCount = MapUtils.getInteger(dataList, "CUSTOMER_COUNT", 1);
				String customerName = MapUtils.getString(dataList, "CUSTOMER_NAME");
				if (customerCount != null && customerCount > 1) {
					dataList.put("CUSTOMER_NAME", customerName + " 等 " + customerCount + " 人");
				}
				if (dataList != null && dataList.size() > 0) {
					CardItem cards = vo.new CardItem();
					cards.setCName("");
					cards.setCWidth(BotMessageVO.CWIDTH_250);
					cards.setCMessage("");
					cards.setCTitle(" ");
					cards.setCTextType("11");

					for (int k = 0; k < titleLabel.length; k++) {
						CImageTextItem cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle(titleLabel[k]);
						cImageText.setCImageText(getString(dataList.get(titleText[k])));
						cImageTextList.add(cImageText.getCImageTexts());
					}
					cImageData.setCImageTexts(cImageTextList);

					cards.setCImageData(cImageData.getCImageDatas());

					for (int j = 0; j < label.length; j++) {
						CTextItem cText = vo.new CTextItem();

						cText.setCLabel(label[j]);
						if (j == 0) {
							DecimalFormat df = new DecimalFormat("###,##0");
							Double amount = Double.parseDouble(getString(dataList.get(text[j])));
							cText.setCText("$ " + df.format(amount));
						} else if (j == 3) {
							String[] temp = text[j].split(" ");
							if (temp.length > 1) {
								int startYear = Integer.parseInt(getString(dataList.get(temp[0])).substring(0, 4));
								int endYear = Integer.parseInt(getString(dataList.get(temp[1])).substring(0, 4));
								startYear = startYear > 1911 ? startYear - 1911 : startYear;
								endYear = endYear > 1911 ? endYear - 1911 : endYear;
								String policyDate = startYear
										+ getString(dataList.get(temp[0])).replace("-", "/").substring(4, 10)
										+ "<br />~" + endYear
										+ getString(dataList.get(temp[1])).replace("-", "/").substring(4, 10);
								cText.setCText(policyDate);
							} else {
								cText.setCText(getString(dataList.get(text[j])));
							}
						} else if (j == 5) {
							cText.setCText(text[j]);
						} else {
							cText.setCText(getString(dataList.get(text[j])));
						}

						cTextList.add(cText.getCTexts());
					}
					cards.setCLinkType(1);

					QuickReplay A06_2_8 = InsQuickReplay.A06_2_8(parameter); // 看保障內容Health
					QuickReplay A06_2_9 = InsQuickReplay.A06_2_9(parameter); // 看關係人資訊Health
					QuickReplay[] link1s = { A06_2_8, A06_2_9 };

					cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));

					cards.setCTexts(cTextList);
					// cards.setCTexts2(cTextList2);
					cardList.add(cards.getCards());
					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
					botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A06_2_2,
							InsQuickReplay.A05_OTHERHELP);

					msgList.addAll(Arrays.asList(
							new String[] { A06_2_8.getText(), A06_2_9.getText(), InsQuickReplay.A06_2_2.getText() }));
					optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_8, A06_2_9, InsQuickReplay.A06_2_2 }));
				} else {
					botResponseUtils.setTextResult(vo, "查不到你的健康傷害險保單");
				}
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getHealthDetailNormal(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA05(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				botResponseUtils.setTextResult(vo, "阿發幫你查到保單詳細資訊如下：");
				List<Map<String, Object>> dataList = (List<Map<String, Object>>) detail.get("HEALTH");
				if (dataList != null && dataList.size() > 0) {
					for (Map<String, Object> data : dataList) {
						if (StringUtils.equals(getString(data.get("POLICY_NO")), reqVO.getPolicyNo())) {
							Map<String, Object> parameter = new HashMap<>();
							parameter.put("MEMBER_TYPE", data.get("MEMBER_TYPE"));
							parameter.put("PERSONAL_OR_TEAM", data.get("PERSONAL_OR_TEAM"));
							parameter.put("POLICY_NO", data.get("POLICY_NO"));

							List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

							List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

							CImageDataItem cImageData = vo.new CImageDataItem();
							cImageData.setCImage(BotMessageVO.IMG_CB00);
							cImageData.setCImageUrl("");
							cImageData.setCImageTextType(9);
							List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();
							String[] titleLabel = { "保單名稱", "保單號碼" };
							String[] label = { "保費", "要保人", "被保人", "保險期間", "來年是否自動續保", "" };
							String[] titleText = { "PRODUCT_POLICY_NAME", "POLICY_NO" };
							String[] text = { "DISCOUNT_PREMIUM", "APPLICANT_OF_CUSTOMER_NAME", "CUSTOMER_NAME",
									"START_DATETIME END_DATETIME", "IS_NEXTYEAR_AUTOCONT_NM", "(本保險為非保證續保商品)" };
							// 2019/12/03，透過CUSTOMER_COUNT判斷被保人數
							Integer customerCount = MapUtils.getInteger(data, "CUSTOMER_COUNT", 1);
							String customerName = MapUtils.getString(data, "CUSTOMER_NAME");
							if (customerCount != null && customerCount > 1) {
								data.put("CUSTOMER_NAME", customerName + " 等 " + customerCount + " 人");
							}
							CardItem cards = vo.new CardItem();
							cards.setCName("");
							cards.setCWidth(BotMessageVO.CWIDTH_250);
							cards.setCMessage("");
							cards.setCTitle(" ");
							cards.setCTextType("11");

							for (int k = 0; k < titleLabel.length; k++) {
								CImageTextItem cImageText = vo.new CImageTextItem();
								cImageText.setCImageTextTitle(titleLabel[k]);
								cImageText.setCImageText(getString(data.get(titleText[k])));
								cImageTextList.add(cImageText.getCImageTexts());
							}
							cImageData.setCImageTexts(cImageTextList);

							cards.setCImageData(cImageData.getCImageDatas());

							for (int j = 0; j < label.length; j++) {
								CTextItem cText = vo.new CTextItem();

								cText.setCLabel(label[j]);
								if (j == 0) {
									DecimalFormat df = new DecimalFormat("###,##0");
									Double amount = Double.parseDouble(getString(data.get(text[j])));
									cText.setCText("$ " + df.format(amount));
								} else if (j == 3) {
									String[] temp = text[j].split(" ");
									if (temp.length > 1) {
										int startYear = Integer.parseInt(getString(data.get(temp[0])).substring(0, 4));
										int endYear = Integer.parseInt(getString(data.get(temp[1])).substring(0, 4));
										startYear = startYear > 1911 ? startYear - 1911 : startYear;
										endYear = endYear > 1911 ? endYear - 1911 : endYear;
										String policyDate = startYear
												+ getString(data.get(temp[0])).replace("-", "/").substring(4, 10)
												+ "<br />~" + endYear
												+ getString(data.get(temp[1])).replace("-", "/").substring(4, 10);
										cText.setCText(policyDate);
									} else {
										cText.setCText(getString(data.get(text[j])));
									}
								} else if (j == 5) {
									cText.setCText(text[j]);
								} else {
									cText.setCText(getString(data.get(text[j])));
								}

								cTextList.add(cText.getCTexts());
							}
							cards.setCLinkType(1);

							QuickReplay A06_2_8 = InsQuickReplay.A06_2_8(parameter); // 看保障內容Health
							QuickReplay[] link1s = { A06_2_8 };

							cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));
							cards.setCTexts(cTextList);

							cardList.add(cards.getCards());
							botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

							botResponseUtils.setTextResult(vo, "升級以使用更多服務!<br />現在升級認證會員，將可使用更多專屬服務，等你來體驗");
							botResponseUtils.setImageResult31(vo, "upgradeMemberLevel01.png");
							String annoTitle = "注意事項";
							String imgUrl = "resources/common/images/upgradeMemberLevel02.png";
							String annoText = "<img src='" + imgUrl
									+ "' alt='' class='img-responsive' onclick=\"PicComponent2.send('" + imgUrl
									+ "');\">";
							botResponseUtils.setNoteResult(vo, annoTitle, annoText);
							botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A06_2_4,
									InsQuickReplay.A06_2_2, InsQuickReplay.A05_OTHERHELP);

							msgList.addAll(Arrays.asList(new String[] { InsQuickReplay.A06_2_2.getText() }));
							optionList.addAll(Arrays.asList(new QuickReplay[] { InsQuickReplay.A06_2_2 }));
							break;
						}
					}
				} else {
					botResponseUtils.setTextResult(vo, "查不到你的健康傷害險保單");
				}

			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getHealthContent(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);
		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("PERSONAL_OR_TEAM", dataList.get("PERSONAL_OR_TEAM"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));

				Map<String, Object> benefitMap = (Map<String, Object>) dataList.get("BENEFIT_MAP");

				List<Map<String, Object>> policyContentList = (List<Map<String, Object>>) benefitMap.get("DTAGZ213");

				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();

				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

				if (policyContentList.size() > 0) {
					/*
					 * String title = "保障內容"; CImageTextItem cImageText = vo.new CImageTextItem();
					 * cImageText.setCImageText(title);
					 * 
					 * cImageTextList.add(cImageText.getCImageTexts());
					 */
					for (Map<String, Object> policyItem : policyContentList) {
						CTextItem cText = vo.new CTextItem();

						cText.setCLabel(getString(policyItem.get("PRODUCT_FULL_NAME")));
						cText.setCText(getString(policyItem.get("AMOUNT_SHOW")));

						cTextList.add(cText.getCTexts());

					}
				}

				int totalNum = cTextList.size(); // 總項目數
				int pageNum = 5; // 每張牌卡最多5個項目

				CardItem cards = vo.new CardItem();
				cards.setCName("");
				cards.setCWidth(BotMessageVO.CWIDTH_200);
				cards.setCMessage("");
				cards.setCTextType("3");

				CImageDataItem cImageData = vo.new CImageDataItem();
				cImageData.setCImage(BotMessageVO.IMG_CC);
				cImageData.setCImageUrl("card33_01.jpg");
				cImageData.setCImageTextType(1);

				cImageData.setCImageTexts(cImageTextList);

				cards.setCImageData(cImageData.getCImageDatas());

				cards.setCLinkType(1);
				int cardAmount = totalNum % pageNum == 0 ? totalNum / pageNum : Math.floorDiv(totalNum, pageNum) + 1;
				for (int l = 0; l < cardAmount; l++) {
					logger.info("l : " + l);
					int start = l * pageNum;
					int end = (l + 1) * pageNum > totalNum ? totalNum : (l + 1) * pageNum;

					// 開始組牌卡
					CardItem splitCards = vo.new CardItem();
					splitCards.setCName("");
					splitCards.setCWidth(BotMessageVO.CWIDTH_200);
					splitCards.setCMessage("");
					splitCards.setCTextType("8");

					splitCards.setCImageData(cImageData.getCImageDatas());

					splitCards.setCLinkType(1);
					splitCards.setCTexts(cTextList.subList(start, end));
					cardList.add(splitCards.getCards());
					// 結束組牌卡
				}

				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
				botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

				QuickReplay A06_2_6 = InsQuickReplay.A06_2_6(parameter);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_6, InsQuickReplay.A06_2_2,
						InsQuickReplay.A05_OTHERHELP);

				msgList.addAll(Arrays.asList(new String[] { A06_2_6.getText(), InsQuickReplay.A06_2_2.getText() }));
				optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_6, InsQuickReplay.A06_2_2 }));
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}
		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getHealthManInfo(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("PERSONAL_OR_TEAM", dataList.get("PERSONAL_OR_TEAM"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));

				Map<String, Object> benefitMap = (Map<String, Object>) dataList.get("BENEFIT_MAP");
				String allAssuredAprtnNm = getString(benefitMap.get("ASSURED_APPRTN_NM"));

				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				String phoneCode = getString(dataList.get("APPLICANT_OF_PHONE_CODE"));
				String cellularPhone = getString(dataList.get("APPLICANT_OF_CELLULAR_PHONE"));
				String phoneAndCellPhone = "";
				if (StringUtils.isNotBlank(phoneCode) && StringUtils.isNotBlank(cellularPhone)) {
					phoneAndCellPhone = phoneCode + "<br />" + cellularPhone;
				} else {
					phoneAndCellPhone = phoneCode + cellularPhone;
				}

				String[] titleArr = { "要保人資料", "被保人資料", "身故受益人" };
				String[][] label2dArr = { { "姓名", "出生日期", "聯絡電話", "地址", "電子郵件" }, { "姓名" }, { "" } };
				String[][] text2dArr = {
						{ getString(dataList.get("APPLICANT_OF_CUSTOMER_NAME")),
								getString(dataList.get("APPLICANT_OF_BIRTHDAY")).replace("-", "/"), phoneAndCellPhone,
								getString(dataList.get("APPLICANT_OF_ADDRESS")),
								getString(dataList.get("APPLICANT_OF_EMAIL")) },
						{ getString(benefitMap.get("CUSTOMER_NAME")) },
						{ allAssuredAprtnNm } };
				for (int i = 0; i < 3; i++) {
					CardItem card = vo.new CardItem();
					card.setCName("");
					card.setCWidth(BotMessageVO.CWIDTH_250);
					card.setCMessage("");
					card.setCTextType("11");
					card.setCTitle(titleArr[i]);
					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

					for (int j = 0; j < label2dArr[i].length; j++) {
						//2019/12/10 要保人資料(姓名、出生日期等)，如果是空的，不顯示
						if(i == 0 && StringUtils.isBlank(text2dArr[i][j])) {
							continue;
						}
						CTextItem cText = vo.new CTextItem();
						cText.setCLabel(label2dArr[i][j]);
						cText.setCText(text2dArr[i][j]);

						cTextList.add(cText.getCTexts());
					}
					card.setCTexts(cTextList);

					card.setCLinkType(1);

					cardList.add(card.getCards());
				}

				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
				
				botResponseUtils.setTextResult(vo, "被保人詳細資料，可至"
						+ "<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.OCH1_0000 + DataUtils.getRectifySSOId(reqVO.getSsoId()) + "')\">數位服務平台</a>查詢喔");

				botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

				QuickReplay A06_2_6 = InsQuickReplay.A06_2_6(parameter);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_6, InsQuickReplay.A06_2_2,
						InsQuickReplay.A05_OTHERHELP);

				msgList.addAll(Arrays.asList(new String[] { A06_2_6.getText(), InsQuickReplay.A06_2_2.getText() }));
				optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_6, InsQuickReplay.A06_2_2 }));
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getTravelDetail(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));
				parameter.put("APPLLTE_TYPE", dataList.get("APPLLTE_TYPE"));

				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				CImageDataItem cImageData = vo.new CImageDataItem();
				cImageData.setCImage(BotMessageVO.IMG_CB00);
				cImageData.setCImageUrl("");
				cImageData.setCImageTextType(9);
				List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();

				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

				botResponseUtils.setTextResult(vo, "阿發幫你查到保單詳細資料如下：");
				String[] titleLabel = { "保單名稱", "保單號碼" };
				String[] label = { "保費", "要保人", "被保人", "保險期間", "旅遊地點" };
				String[] titleText = { "PRODUCT_POLICY_NAME", "POLICY_NO" };
				String[] text = { "DISCOUNT_PREMIUM", "APPLICANT_OF_CUSTOMER_NAME", "CUSTOMER_NAME",
						"START_DATETIME END_DATETIME INSRNCE_DAY", "TRAVEL_SITE_NAME" };

				// 2019/12/03，透過CUSTOMER_COUNT判斷被保人數
				Integer customerCount = MapUtils.getInteger(dataList, "CUSTOMER_COUNT", 1);
				String customerName = MapUtils.getString(dataList, "CUSTOMER_NAME");
				if (customerCount != null && customerCount > 1) {
					dataList.put("CUSTOMER_NAME", customerName + " 等 " + customerCount + " 人");
				}
				if (dataList != null && dataList.size() > 0) {
					CardItem cards = vo.new CardItem();
					cards.setCName("");
					cards.setCWidth(BotMessageVO.CWIDTH_250);
					cards.setCMessage("");
					cards.setCTitle(" ");
					cards.setCTextType("11");

					for (int k = 0; k < titleText.length; k++) {
						CImageTextItem cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle(titleLabel[k]);
						cImageText.setCImageText(getString(dataList.get(titleText[k])));
						cImageTextList.add(cImageText.getCImageTexts());
					}
					cImageData.setCImageTexts(cImageTextList);

					cards.setCImageData(cImageData.getCImageDatas());

					for (int j = 0; j < label.length; j++) {
						CTextItem cText = vo.new CTextItem();

						cText.setCLabel(label[j]);
						if (j == 0) {
							DecimalFormat df = new DecimalFormat("###,##0");
							Double amount = Double.parseDouble(getString(dataList.get(text[j])));
							cText.setCText("$ " + df.format(amount));
						} else if (j == 3) {
							String[] temp = text[j].split(" ");
							if (temp.length > 2) {
								cText.setCText(getString(dataList.get(temp[0]) + "~<br />" + dataList.get(temp[1]))
										+ "<br />共" + getString(dataList.get(temp[2]) + "天"));
							} else {
								cText.setCText(getString(dataList.get(text[j])));
							}
						} else {
							cText.setCText(getString(dataList.get(text[j])));
						}

						cTextList.add(cText.getCTexts());
					}
					cards.setCLinkType(1);

					QuickReplay A06_2_10 = InsQuickReplay.A06_2_10(parameter); // 看保障內容Travel
					QuickReplay A06_2_11 = InsQuickReplay.A06_2_11(parameter); // 保期變更Travel
					QuickReplay A06_2_12 = InsQuickReplay.A06_2_12(parameter); // 關係人資訊Travel
					QuickReplay A06_2_13 = InsQuickReplay.A06_2_13(parameter); // 快速理賠資訊Travel

					QuickReplay[] link1s;
					if (StringUtils.equals("Y", String.valueOf(dataList.get("IS_GO_T03")))) {
						link1s = new QuickReplay[] { A06_2_10, A06_2_11, A06_2_12, A06_2_13 };
						msgList.addAll(Arrays.asList(new String[] { A06_2_10.getText(), A06_2_11.getText(),
								A06_2_12.getText(), A06_2_13.getText(), InsQuickReplay.A06_2_2.getText() }));
						optionList.addAll(Arrays.asList(
								new QuickReplay[] { A06_2_10, A06_2_11, A06_2_12, A06_2_13, InsQuickReplay.A06_2_2 }));
					} else {
						link1s = new QuickReplay[] { A06_2_10, A06_2_12, A06_2_13 };
						msgList.addAll(Arrays.asList(new String[] { A06_2_10.getText(), A06_2_12.getText(),
								A06_2_13.getText(), InsQuickReplay.A06_2_2.getText() }));
						optionList.addAll(Arrays
								.asList(new QuickReplay[] { A06_2_10, A06_2_12, A06_2_13, InsQuickReplay.A06_2_2 }));
					}

					cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));
					cards.setCTexts(cTextList);

					cardList.add(cards.getCards());
					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

					botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A06_2_2,
							InsQuickReplay.A05_OTHERHELP);

				}
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getTravelDetailNormal(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA05(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				List<Map<String, Object>> dataList = (List<Map<String, Object>>) detail.get("TRAVEL");
				if (dataList != null && dataList.size() > 0) {
					for (Map<String, Object> data : dataList) {
						if (StringUtils.equals(getString(data.get("POLICY_NO")), reqVO.getPolicyNo())) {
							Map<String, Object> parameter = new HashMap<>();
							parameter.put("MEMBER_TYPE", data.get("MEMBER_TYPE"));
							parameter.put("POLICY_NO", data.get("POLICY_NO"));
							parameter.put("APPLLTE_TYPE", data.get("APPLLTE_TYPE"));

							List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

							CImageDataItem cImageData = vo.new CImageDataItem();
							cImageData.setCImage(BotMessageVO.IMG_CB00);
							cImageData.setCImageUrl("");
							cImageData.setCImageTextType(9);
							List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();

							List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

							botResponseUtils.setTextResult(vo, "阿發幫你查到保單詳細資料如下：");
							String[] titleLabel = { "保單名稱", "保單號碼" };
							String[] label = { "保費", "要保人", "被保人", "保險期間", "旅遊地點" };
							String[] titleText = { "PRODUCT_POLICY_NAME", "POLICY_NO" };
							String[] text = { "DISCOUNT_PREMIUM", "APPLICANT_OF_CUSTOMER_NAME", "CUSTOMER_NAME",
									"START_DATETIME END_DATETIME INSRNCE_DAY", "TRAVEL_SITE_NAME" };
							// 2019/12/03，透過CUSTOMER_COUNT判斷被保人數
							Integer customerCount = MapUtils.getInteger(data, "CUSTOMER_COUNT", 1);
							String customerName = MapUtils.getString(data, "CUSTOMER_NAME");
							if (customerCount != null && customerCount > 1) {
								data.put("CUSTOMER_NAME", customerName + " 等 " + customerCount + " 人");
							}
							CardItem cards = vo.new CardItem();
							cards.setCName("");
							cards.setCWidth(BotMessageVO.CWIDTH_250);
							cards.setCMessage("");
							cards.setCTitle(" ");
							cards.setCTextType("11");

							for (int k = 0; k < titleText.length; k++) {
								CImageTextItem cImageText = vo.new CImageTextItem();
								cImageText.setCImageTextTitle(titleLabel[k]);
								cImageText.setCImageText(getString(data.get(titleText[k])));
								cImageTextList.add(cImageText.getCImageTexts());
							}
							cImageData.setCImageTexts(cImageTextList);

							cards.setCImageData(cImageData.getCImageDatas());

							for (int j = 0; j < label.length; j++) {
								CTextItem cText = vo.new CTextItem();

								cText.setCLabel(label[j]);
								if (j == 0) {
									DecimalFormat df = new DecimalFormat("###,##0");
									Double amount = Double.parseDouble(getString(data.get(text[j])));
									cText.setCText("$ " + df.format(amount));
								} else if (j == 3) {
									String[] temp = text[j].split(" ");
									if (temp.length > 2) {
										cText.setCText(getString(data.get(temp[0]) + "~<br />" + data.get(temp[1]))
												+ "<br />共" + getString(data.get(temp[2]) + "天"));
									} else {
										cText.setCText(getString(data.get(text[j])));
									}
								} else {
									cText.setCText(getString(data.get(text[j])));
								}

								cTextList.add(cText.getCTexts());
							}
							cards.setCLinkType(1);

							QuickReplay A06_2_10 = InsQuickReplay.A06_2_10(parameter); // 看保障內容Travel

							QuickReplay[] link1s = { A06_2_10 };

							cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));
							cards.setCTexts(cTextList);
							cardList.add(cards.getCards());
							botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

							botResponseUtils.setTextResult(vo, "升級以使用更多服務!<br />現在升級認證會員，將可使用更多專屬服務，等你來體驗");
							botResponseUtils.setImageResult31(vo, "upgradeMemberLevel01.png");
							String annoTitle = "注意事項";
							String imgUrl = "resources/common/images/upgradeMemberLevel02.png";
							String annoText = "<img src='" + imgUrl
									+ "' alt='' class='img-responsive' onclick=\"PicComponent2.send('" + imgUrl
									+ "');\">";
							botResponseUtils.setNoteResult(vo, annoTitle, annoText);
							botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A06_2_4,
									InsQuickReplay.A06_2_2, InsQuickReplay.A05_OTHERHELP);

							msgList.addAll(Arrays.asList(new String[] { InsQuickReplay.A06_2_2.getText() }));
							optionList.addAll(Arrays.asList(new QuickReplay[] { InsQuickReplay.A06_2_2 }));
							break;
						}
					}
				} else {
					botResponseUtils.setTextResult(vo, "查不到你的旅遊綜合險保單");
				}

			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getTravelContent(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));
				parameter.put("APPLLTE_TYPE", dataList.get("APPLLTE_TYPE"));

				List<Map<String, Object>> paPolicyList = (List<Map<String, Object>>) dataList.get("PA_POLICY_LIST");
				Map<String, Object> categoryTitleMap = (Map<String, Object>) dataList.get("PA_CATEGORY_CLASS_TITLE");
				String productPolicyCode = (String) dataList.get("PRODUCT_POLICY_CODE");

				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				Map<String, List<Map<String, Object>>> allCTextMap = new LinkedHashMap<>();
				Map<String, List<Map<String, Object>>> allCImageTextMap = new LinkedHashMap<>();

				if (paPolicyList != null && paPolicyList.size() > 0) {
					Map<String, Object> paPolicy = paPolicyList.get(0);
					List<Map<String, Object>> policyItemList = (List<Map<String, Object>>) paPolicy.get("DTAGZ213");
					if (policyItemList != null && policyItemList.size() > 0) {
						for (Map<String, Object> policyItem : policyItemList) {
							String relKey = getString(policyItem.get("REL_KEY"));
							List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();

							List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
							if (reqVO.getCmd().toUpperCase().equals("TRAVELCONTENTMORE") && !relKey.equals("10")) {
								continue;
							}
							if (!allCImageTextMap.containsKey(relKey)) {
								CImageTextItem cImageText = vo.new CImageTextItem();

								if (categoryTitleMap != null) {
									cImageText.setCImageText(getString(categoryTitleMap.get(relKey)));
								}

								cImageText.setCImageTextTitle(relKey);
								cImageTextList.add(cImageText.getCImageTexts());

								allCImageTextMap.put(relKey, cImageTextList);

								CTextItem cText = vo.new CTextItem();
								// 醫療及身故 格式不同要另外處理
								if (relKey.equals("2060")) {
									cText.setCLabel(getString(policyItem.get("PRODUCT_SUBID_NAME")));
									cText.setCText(getString(policyItem.get("AMOUNT_SHOW")));
								} else {
									cText.setCLabel(getString(policyItem.get("ITEM2")));
									cText.setCText(getString(policyItem.get("ITEM3")));
								}
								cText.setCUnit(getString(policyItem.get("CATEGORY_CODE")) + ":"
										+ getString(policyItem.get("PRODUCT_SUB_ID")));

								cTextList.add(cText.getCTexts());
								allCTextMap.put(relKey, cTextList);
							} else {
								CTextItem cText = vo.new CTextItem();
								// 醫療及身故 格式不同要另外處理
								if (relKey.equals("2060")) {
									cText.setCLabel(getString(policyItem.get("PRODUCT_SUBID_NAME")));
									cText.setCText(getString(policyItem.get("AMOUNT_SHOW")));
								} else {
									cText.setCLabel(getString(policyItem.get("ITEM2")));
									cText.setCText(getString(policyItem.get("ITEM3")));
								}
								cText.setCUnit(getString(policyItem.get("CATEGORY_CODE")) + ":"
										+ getString(policyItem.get("PRODUCT_SUB_ID")));

								allCTextMap.get(relKey).add(cText.getCTexts());
							}
						}
					} else {
						botResponseUtils.setTextResult(vo, "查不到該保單的保障內容。");
					}
				} else {
					botResponseUtils.setTextResult(vo, "查不到該保單的保障內容。");
				}

				for (String key : allCImageTextMap.keySet()) {
					CardItem cards = vo.new CardItem();
					cards.setCName("");
					cards.setCWidth(BotMessageVO.CWIDTH_200);
					cards.setCMessage("");
					cards.setCTextType("8");

					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_CC);
					switch (key) {
					case "10":
						cImageData.setCImageUrl("card-Ins-travel02@2x.png");
						break;
					case "30":
						cImageData.setCImageUrl("card-Ins-travel04@2x.png");
						break;
					case "2060":
						cImageData.setCImageUrl("card-Ins-travel01@2x.png");
						break;
					case "40":
						cImageData.setCImageUrl("card-Ins-travel03@2x.png");
						break;
					default:
						cImageData.setCImageUrl("card-Ins-travel02@2x.png");
						break;
					}

					// cImageData.setCImageTextType(5);
					cImageData.setCImageTextType(1);

					// cImageData.setCImageTexts(allCImageTextMap.get(key));

					cards.setCImageData(cImageData.getCImageDatas());

					cards.setCLinkType(1);

					int totalNum = allCTextMap.get(key).size(); // 總項目數
					int pageNum = 5; // 每張牌卡最多5個項目
					logger.info("totalNum : " + totalNum);

					if (totalNum <= pageNum) {
						cards.setCTexts(allCTextMap.get(key));
						if (StringUtils.equals(productPolicyCode, "PC01001")
								|| StringUtils.equals(productPolicyCode, "PC01002")
								|| StringUtils.equals(productPolicyCode, "PC01009")) {
							cards.setCLinkList(
									botResponseUtils.getCLinkList(vo, InsQuickReplay.A06_ContentDesc(key, parameter)));
						}

						cardList.add(cards.getCards());
					} else {
						cards.setCTexts(allCTextMap.get(key).subList(0, 5));
						if (!reqVO.getCmd().toUpperCase().equals("TRAVELCONTENTMORE")) {
							if (StringUtils.equals(productPolicyCode, "PC01001")
									|| StringUtils.equals(productPolicyCode, "PC01002")
									|| StringUtils.equals(productPolicyCode, "PC01009")) {
								cards.setCLinkList(
										botResponseUtils.getCLinkList(vo, InsQuickReplay.A06_MoreContent(parameter),
												InsQuickReplay.A06_ContentDesc(key, parameter)));
							} else {
								cards.setCLinkList(
										botResponseUtils.getCLinkList(vo, InsQuickReplay.A06_MoreContent(parameter)));
							}

							cardList.add(cards.getCards());
						} else {
							if (StringUtils.equals(productPolicyCode, "PC01001")
									|| StringUtils.equals(productPolicyCode, "PC01002")
									|| StringUtils.equals(productPolicyCode, "PC01009")) {
								cards.setCLinkList(botResponseUtils.getCLinkList(vo,
										InsQuickReplay.A06_ContentDesc(key, parameter)));
							}
							cardList.add(cards.getCards());

							int cardAmount = totalNum % pageNum == 0 ? totalNum / pageNum
									: Math.floorDiv(totalNum, pageNum) + 1;
							logger.info("cardAmount : " + cardAmount);
							for (int l = 1; l < cardAmount; l++) {
								logger.info("l : " + l);
								int start = l * pageNum;
								int end = (l + 1) * pageNum > totalNum ? totalNum : (l + 1) * pageNum;

								// 開始組牌卡
								CardItem splitCards = vo.new CardItem();
								splitCards.setCName("");
								splitCards.setCWidth(BotMessageVO.CWIDTH_200);
								splitCards.setCMessage("");
								splitCards.setCTextType("8");

								splitCards.setCImageData(cImageData.getCImageDatas());

								splitCards.setCLinkType(1);
								splitCards.setCTexts(allCTextMap.get(key).subList(start, end));

								if (StringUtils.equals(productPolicyCode, "PC01001")
										|| StringUtils.equals(productPolicyCode, "PC01002")
										|| StringUtils.equals(productPolicyCode, "PC01009")) {
									splitCards.setCLinkList(botResponseUtils.getCLinkList(vo,
											InsQuickReplay.A06_ContentDesc(key, parameter)));
								}

								cardList.add(splitCards.getCards());
								// 結束組牌卡
							}
						}
					}
				}
				if (cardList.size() > 0) {
					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
				}
				botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

				QuickReplay A06_2_7 = InsQuickReplay.A06_2_7(parameter);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_7, InsQuickReplay.A06_2_2,
						InsQuickReplay.A05_OTHERHELP);

				msgList.addAll(Arrays.asList(new String[] { A06_2_7.getText(), InsQuickReplay.A06_2_2.getText() }));
				optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_7, InsQuickReplay.A06_2_2 }));
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getTravelContentDesc(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));
				parameter.put("APPLLTE_TYPE", dataList.get("APPLLTE_TYPE"));

				List<Map<String, Object>> paPolicyList = (List<Map<String, Object>>) dataList.get("PA_POLICY_LIST");
				Map<String, Object> categoryTitleMap = (Map<String, Object>) dataList.get("PA_CATEGORY_CLASS_TITLE");

				Map<String, List<Map<String, Object>>> allCTextMap = new LinkedHashMap<>();
				Map<String, List<Map<String, Object>>> allCImageTextMap = new LinkedHashMap<>();

				if (paPolicyList != null && paPolicyList.size() > 0) {
					Map<String, Object> paPolicy = paPolicyList.get(0);
					List<Map<String, Object>> policyItemList = (List<Map<String, Object>>) paPolicy.get("DTAGZ213");
					if (policyItemList != null && policyItemList.size() > 0) {
						for (Map<String, Object> policyItem : policyItemList) {
							String relKey = getString(policyItem.get("REL_KEY"));
							List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();

							List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
							if (reqVO.getCmd().toUpperCase().equals("TRAVELCONTENTMORE") && !relKey.equals("10")) {
								continue;
							}
							if (!allCImageTextMap.containsKey(relKey)) {
								CImageTextItem cImageText = vo.new CImageTextItem();

								if (categoryTitleMap != null) {
									cImageText.setCImageText(getString(categoryTitleMap.get(relKey)));
								}

								cImageText.setCImageTextTitle(relKey);
								cImageTextList.add(cImageText.getCImageTexts());

								allCImageTextMap.put(relKey, cImageTextList);

								CTextItem cText = vo.new CTextItem();
								// 醫療及身故 格式不同要另外處理
								if (relKey.equals("2060")) {
									cText.setCLabel(getString(policyItem.get("PRODUCT_SUBID_NAME")));
									cText.setCText(getString(policyItem.get("AMOUNT_SHOW")));
								} else {
									cText.setCLabel(getString(policyItem.get("ITEM2")));
									cText.setCText(getString(policyItem.get("ITEM3")));
								}
								Map<String, Object> textMap = cText.getCTexts();
								textMap.put("CATEGORY_CODE", getString(policyItem.get("CATEGORY_CODE")));
								textMap.put("PRODUCT_SUB_ID", getString(policyItem.get("PRODUCT_SUB_ID")));

								cTextList.add(textMap);
								allCTextMap.put(relKey, cTextList);
							} else {
								CTextItem cText = vo.new CTextItem();
								// 醫療及身故 格式不同要另外處理
								if (relKey.equals("2060")) {
									cText.setCLabel(getString(policyItem.get("PRODUCT_SUBID_NAME")));
									cText.setCText(getString(policyItem.get("AMOUNT_SHOW")));
								} else {
									cText.setCLabel(getString(policyItem.get("ITEM2")));
									cText.setCText(getString(policyItem.get("ITEM3")));
								}
								Map<String, Object> textMap = cText.getCTexts();
								textMap.put("CATEGORY_CODE", getString(policyItem.get("CATEGORY_CODE")));
								textMap.put("PRODUCT_SUB_ID", getString(policyItem.get("PRODUCT_SUB_ID")));
								allCTextMap.get(relKey).add(textMap);
							}
						}
					}
				}

				if (reqVO.getCmd().toUpperCase().equals("TRAVELCONTENTDESC")
						&& StringUtils.isNotBlank(reqVO.getRelNo())) {
					String relNo = getString(reqVO.getRelNo());
					List<Map<String, Object>> tmpTxtList = allCTextMap.get(relNo);
					List<Map<String, Object>> replyList = new ArrayList<>();

					if (tmpTxtList != null) {
						for (Map<String, Object> txt : tmpTxtList) {
							String cLabel = getString(txt.get("cLabel"));

							String categoryCode = getString(txt.get("CATEGORY_CODE"));
							String productSubId = getString(txt.get("PRODUCT_SUB_ID"));

							Map<String, Object> t01Param = new HashMap<>();
							t01Param.put("policyNo", dataList.get("POLICY_NO"));
							t01Param.put("productPolicyCode", dataList.get("PRODUCT_POLICY_CODE"));
							t01Param.put("categoryCode", categoryCode);
							t01Param.put("productSubId", productSubId);
							LinkListItem link = vo.new LinkListItem();
							link.setLAction(2);
							link.setLText(cLabel);
							link.setLAlt("旅遊綜合保險商品查詢");
							link.setLIntent("商品");
							link.setLSecondBotIntent("projectContentA06");
							link.setLParameter(t01Param);
							replyList.add(link.getLinkList());
						}
						botResponseUtils.setInsTopQuestion(vo, replyList, "");
						returnResult.put("botMessageVO", vo);
						return returnResult;
					} else {
						vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
						botResponseUtils.setTextResult(vo, "查不到對應的保障內容說明");
						returnResult.put("botMessageVO", vo);
						return returnResult;
					}
				}
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}
		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getTravelManInfo(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));
				parameter.put("APPLLTE_TYPE", dataList.get("APPLLTE_TYPE"));

				List<Map<String, Object>> paPolicyList = (List<Map<String, Object>>) dataList.get("PA_POLICY_LIST");
				String allAssuredAprtnNm = "";
				for (Map<String, Object> paPolicyItem : paPolicyList) {
					if (StringUtils.isNotBlank(allAssuredAprtnNm)
							&& StringUtils.isNotBlank(getString(paPolicyItem.get("ASSURED_APPRTN_NM")))) {
						allAssuredAprtnNm += "<br />";
						allAssuredAprtnNm += getString(paPolicyItem.get("ASSURED_APPRTN_NM"));
					} else {
						allAssuredAprtnNm += getString(paPolicyItem.get("ASSURED_APPRTN_NM"));
					}
				}
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				String phoneCode = getString(dataList.get("APPLICANT_OF_PHONE_CODE"));
				String cellularPhone = getString(dataList.get("APPLICANT_OF_CELLULAR_PHONE"));
				String phoneAndCellPhone = "";
				if (StringUtils.isNotBlank(phoneCode) && StringUtils.isNotBlank(cellularPhone)) {
					phoneAndCellPhone = phoneCode + "<br />" + cellularPhone;
				} else {
					phoneAndCellPhone = phoneCode + cellularPhone;
				}

				String[] titleArr = { "要保人資料", "被保人資料", "身故受益人" };
				String[][] label2dArr = { { "姓名", "聯絡電話", "地址", "Email" }, { "姓名" }, { "" } };
				String[][] text2dArr = {
						{ getString(dataList.get("APPLICANT_OF_CUSTOMER_NAME")), phoneAndCellPhone,
								getString(dataList.get("APPLICANT_OF_ADDRESS")),
								getString(dataList.get("APPLICANT_OF_EMAIL")) },
						{ getString(paPolicyList.get(0).get("CUSTOMER_NAME"))},
						{ allAssuredAprtnNm } };
				for (int i = 0; i < 3; i++) {
					CardItem card = vo.new CardItem();
					card.setCName("");
					card.setCWidth(BotMessageVO.CWIDTH_250);
					card.setCMessage("");
					card.setCTextType("11");
					card.setCTitle(titleArr[i]);
					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

					for (int j = 0; j < label2dArr[i].length; j++) {
						//2019/12/10 要保人資料(姓名、出生日期等)，如果是空的，不顯示
						if(i == 0 && StringUtils.isBlank(text2dArr[i][j])) {
							continue;
						}
						CTextItem cText = vo.new CTextItem();
						cText.setCLabel(label2dArr[i][j]);
						cText.setCText(text2dArr[i][j]);
						cTextList.add(cText.getCTexts());
					}
					card.setCTexts(cTextList);

					card.setCLinkType(1);

					cardList.add(card.getCards());
				}

				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
				
				botResponseUtils.setTextResult(vo, "被保人詳細資料，可至"
						+ "<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.OCH1_0000 + DataUtils.getRectifySSOId(reqVO.getSsoId()) + "')\">數位服務平台</a>查詢喔");

				botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

				QuickReplay A06_2_7 = InsQuickReplay.A06_2_7(parameter);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_7, InsQuickReplay.A06_2_2,
						InsQuickReplay.A05_OTHERHELP);

				msgList.addAll(Arrays.asList(new String[] { A06_2_7.getText(), InsQuickReplay.A06_2_2.getText() }));
				optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_7, InsQuickReplay.A06_2_2 }));
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	public Map<String, Object> getTravelClaimInfo(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getA06(map);

		rpjson = rpjson.replace("\r\n", "");
		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				Map<String, Object> dataList = (Map<String, Object>) detail.get("DATAS");

				Map<String, Object> parameter = new HashMap<>();
				parameter.put("MEMBER_TYPE", dataList.get("MEMBER_TYPE"));
				parameter.put("POLICY_NO", dataList.get("POLICY_NO"));
				parameter.put("APPLLTE_TYPE", dataList.get("APPLLTE_TYPE"));

				List<Map<String, Object>> paPolicyList = (List<Map<String, Object>>) dataList.get("PA_POLICY_LIST");
				if (paPolicyList != null && paPolicyList.size() > 0) {
					List<Map<String, Object>> flightList = (List<Map<String, Object>>) paPolicyList.get(0)
							.get("DTABP712");
					if (flightList != null && flightList.size() > 0) {
						String goFlightText = "";
						String backFlightText = "";
						String bankShow = getString(paPolicyList.get(0).get("BANK_ACCOUNT_SHOW"));
						for (Map<String, Object> flight : flightList) {
							String flightType = getString(flight.get("FLIGHT_TYPE"));
							if (flightType.equals("1")) {
								goFlightText = getString(flight.get("FLIGHT_DATE")).replace("-", "/").trim() + " "
										+ getString(flight.get("FLIGHT_NO"));
							} else if (flightType.equals("2")) {
								backFlightText = getString(flight.get("FLIGHT_DATE")).replace("-", "/").trim() + " "
										+ getString(flight.get("FLIGHT_NO"));
							}
						}

						List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

						String[] labelArr = { "去程航班", "回程航班", "帳戶號碼" };
						// 目前測試資料還沒有以下欄位，所以應該會出錯
						String[] textArr = { goFlightText, backFlightText, bankShow };

						CardItem card = vo.new CardItem();
						card.setCName("");
						card.setCWidth(BotMessageVO.CWIDTH_250);
						card.setCMessage("");
						card.setCTextType("11");
						card.setCTitle("");
						List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
						for (int i = 0; i < 3; i++) {
							CTextItem cText = vo.new CTextItem();
							cText.setCLabel(labelArr[i]);
							cText.setCText(textArr[i]);
							cTextList.add(cText.getCTexts());
						}
						card.setCTexts(cTextList);

						card.setCLinkType(1);

						cardList.add(card.getCards());

						botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

						botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

						QuickReplay A06_2_7 = InsQuickReplay.A06_2_7(parameter);
						botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_7,
								InsQuickReplay.A06_2_2, InsQuickReplay.A05_OTHERHELP);

						msgList.addAll(
								Arrays.asList(new String[] { A06_2_7.getText(), InsQuickReplay.A06_2_2.getText() }));
						optionList.addAll(Arrays.asList(new QuickReplay[] { A06_2_7, InsQuickReplay.A06_2_2 }));
					} else {
						String replyText = "你所查詢的保單沒有快速理賠資訊";
						botResponseUtils.setTextResult(vo, replyText);
						botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎？");

						QuickReplay A06_2_7 = InsQuickReplay.A06_2_7(parameter);
						botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, A06_2_7,
								InsQuickReplay.A06_2_2, InsQuickReplay.A05_OTHERHELP);
					}

				} else {
					String replyText = "查不到快速理賠的航班資料";
					botResponseUtils.setTextResult(vo, replyText);
				}

			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}

		} else {
			String replyText = "A06 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public Map<String, Object> getContentNormalReply(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();

		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(vo, "現在馬上升級認證會員就可以看到完整保障項目喔！");

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A06_2_4, InsQuickReplay.A06_2_2,
				InsQuickReplay.A05_OTHERHELP);

		msgList.addAll(Arrays.asList(new String[] { InsQuickReplay.A06_2_2.getText() }));
		optionList.addAll(Arrays.asList(new QuickReplay[] { InsQuickReplay.A06_2_2 }));

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public TPIGatewayResult getA07(InsuranceRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("CHECK_DATE", DateUtils.format(new Date(), DateUtils.DATE_PATTERN));

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", map2);
		Map<String, Object> result = insuranceAPIFeignService.getA07(map);

		if (StringUtils.equals(getString(result.get("returnCode")), "0")) {
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			Map<String, Object> datas = (Map<String, Object>) detail.get("DATAS");
			if (StringUtils.equals(getString(detail.get("RETURN_CODE")), "0000") && datas != null
					&& StringUtils.equals(getString(datas.get("IS_WORKDAY")), "1")) {
				if (StringUtils.equals(reqVO.getFallback(), "Y")) {
					botResponseUtils.setTextResult(vo, "阿發聽不懂你現在在說什麼");
				}
				botResponseUtils.setTextResult(vo, "確定要現在幫你轉接真人文字客服嗎?");
				QuickReplay quickReplay = InsQuickReplay.TO_AGENT;
				QuickReplay quickReplay1 = InsQuickReplay.NOT_TO_AGENT;
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, quickReplay, quickReplay1);
				tpiGatewayResult.put("askToAgent", "Y");
				logger.info("JSON.toJSONString(vo)" + JSON.toJSONString(vo));
			} else {
				botResponseUtils.setTextResult(vo, "不好意思目前非真人文字客服服務時間【服務時間：週一至週五08:30-17:30(國定假日除外)】");
				botResponseUtils.setTextResult(vo, "你可以先透過以下方式聯絡真人客服處理你的問題。");
				ContentItem contentItem = vo.new ContentItem();// level 2
				contentItem.setLinkList(new ArrayList<Map<String, Object>>());
				contentItem.setCards(new ArrayList<Map<String, Object>>());
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				String[] titleArr = {"國泰產險客服信箱" , "國泰產險客服專線" };
				String[] labelArr = {"將在兩個工作日內，由專人回電為你服務" , "電話客服為你服務"};
				// 立即撥號 前往留言
				QuickReplay[] quickReplayArr = { InsQuickReplay.GOContact(getString(reqVO.getSsoId())) ,
						 InsQuickReplay.C02_2_1("0800212880") };

				for (int i = 0; i < labelArr.length; i++) {
					CardItem card = vo.new CardItem();
					card.setCName("");
					card.setCWidth(BotMessageVO.CWIDTH_200);
					card.setCMessage("");
					card.setCTextType("11");
					card.setCTitle(titleArr[i]);
					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
					CTextItem cText = vo.new CTextItem();
					cText.setCLabel(labelArr[i]);
					cText.setCText("");
					cTextList.add(cText.getCTexts());
					card.setCTexts(cTextList);
					card.setCLinkType(1);
					card.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplayArr[i]));

					cardList.add(card.getCards());
				}

				botResponseUtils.setCardsResult19(vo, "card", cardList);

				botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎?");
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.A05_OTHERHELP);
			}
		}
		logger.info("JSON.toJSONString(vo) : " + JSON.toJSONString(vo));
		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO getC01(InsuranceRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getC01(map);

		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if ("0000".equals(resultCode)) {
				List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) detail.get("DATAS");
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

				if (dataList.size() > 0) {
					botResponseUtils.setTextResult(vo, "下面是阿發查到以你為要保人或被保人的車子喔");

					for (int i = 0; i < dataList.size(); i++) {
						CardItem cards = vo.new CardItem();
						cards.setCName("");
						cards.setCWidth(BotMessageVO.CWIDTH_200);
						cards.setCTextType("10");
						cards.setCLinkType(1);

						// 牌卡圖片
						CImageDataItem cImageData = vo.new CImageDataItem();
						cImageData.setCImage(BotMessageVO.IMG_CC);
						cImageData.setCImageUrl("card35-NoTypeface.png");
						cImageData.setCImageTextType(1111);

						List<Map<String, Object>> cImageTextList = new ArrayList<>();
						CImageTextItem cImageText = vo.new CImageTextItem();
						cImageText.setCImageText(getString(dataList.get(i).get("CAR_NO")));
						cImageTextList.add(cImageText.getCImageTexts());

						cImageData.setCImageTexts(cImageTextList);
						cards.setCImageData(cImageData.getCImageDatas());

						List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
						CTextItem cText = vo.new CTextItem();
						cText.setCLabel("廠牌名稱");
						cText.setCText(getString(dataList.get(i).get("MODEL_BRAND_NAME")));
						cTextList.add(cText.getCTexts());

						cText = vo.new CTextItem();
						cText.setCLabel("車輛種類/排氣量");
						int engineExhaust = Double.valueOf(getString(dataList.get(i).get("ENGINE_EXHAUST"))).intValue();
						cText.setCText(getString(dataList.get(i).get("VEHICLE_KIND_NAME")) + "/"
								+ String.valueOf(engineExhaust) + " "
								+ getString(dataList.get(i).get("ENGINE_EXHAUST_UNIT_NAME")));
						cTextList.add(cText.getCTexts());
						cards.setCTexts(cTextList);

						cTextList = new ArrayList<Map<String, Object>>();
						List<Map<String, Object>> rescueData = (ArrayList<Map<String, Object>>) dataList.get(i)
								.get("RESCUE_CARD_DATA");

						if (rescueData != null) {
							for (int k = 0; k < rescueData.size(); k++) {
								cText = vo.new CTextItem();
								if (k == 0) {
									cText.setCLabel("道路救援資格及條件");
								} else {
									cText.setCLabel("");
								}
								cText.setCText(getString(rescueData.get(k)));
								cTextList.add(cText.getCTexts());
							}
						} else {
							cText = vo.new CTextItem();
							cText.setCLabel("道路救援資格及條件");
							cText.setCText("無");
							cTextList.add(cText.getCTexts());
						}

						cards.setCTexts2(cTextList);
						cardList.add(cards.getCards());
					}

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
					botResponseUtils.setTextResult(vo, "阿發提醒你，道路救援可用次數，以申告當時的剩餘次數為準喔！");
					botResponseUtils.setTextResult(vo, "阿發還可以告訴你更多道路救援的相關資訊喔～");
					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04,
							InsQuickReplay.C01_2_1(reqVO.getSsoId()), InsQuickReplay.C01_2_2,
							InsQuickReplay.TOP_QUESTION_3, InsQuickReplay.A05_OTHERHELP);
				} else {
					botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的汽車保單喔!<br/>點我看<a href='javascript:void(0)' onclick=\"App.sendMsg('免費道路救援贈送標準');\">免費道路救援贈送標準<a/>");
				}
			} else {
				String replyText = getString(detail.get("RETURN_MSG"));
				botResponseUtils.setTextResult(vo, replyText);
			}
		} else {
			String replyText = "C01 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		return vo;
	}

	@Override
	public Map<String, Object> getC05(InsuranceRequest reqVO, Map<String, String> param) {
		Map<String, Object> returnResult = new HashMap<>();
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);

		String rpjson = insuranceAPIFeignService.getC05(map);

		if (StringUtils.isNotBlank(rpjson)) {
			result = returnToMap(rpjson);
			Map<String, Object> detail = (Map<String, Object>) result.get("detail");
			String resultCode = getString(detail.get("RETURN_CODE"));

			if (resultCode.equals("0000")) {
				String replyText = "阿發幫你找到電子強制證囉，提醒你，檔案開啟密碼為車主「身分證號/統編/居留證號」，第一個字母須大寫喔!";
				botResponseUtils.setTextResult(vo, replyText);
				List<Map<String, Object>> dataList = (List<Map<String, Object>>) detail.get("DATAS");
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				for (int i = 0; i < dataList.size(); i++) {
					String url = getString(dataList.get(i).get("URL"));
					String startDate = getString(dataList.get(i).get("START_DATE"));
					String endDate = getString(dataList.get(i).get("END_DATE"));
					String errorMsg = getString(dataList.get(i).get("ERROR_MSG"));
					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImageUrl("card13-1.png");
					// cImageData.setCImage("img-responsive");
					cImageData.setCImage("img-credbank06");
					cImageData.setCImageTextType(11);

					if (StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)) {
						List<Map<String, Object>> cImageTextList = new ArrayList<>();
						CImageTextItem cImageText = vo.new CImageTextItem();
						cImageText.setCImageText("保期 " + startDate + "<br />~" + endDate);
						cImageTextList.add(cImageText.getCImageTexts());

						cImageData.setCImageTexts(cImageTextList);
					}

					CardItem cards = vo.new CardItem();

					cards.setCImageData(cImageData.getCImageDatas());
					cards.setCName("");
					cards.setCWidth(BotMessageVO.CWIDTH_250);
					cards.setCTextType("");
					cards.setCLinkType(1);

					if (StringUtils.isNotBlank(url)) {
						QuickReplay[] link1s = { new InsQuickReplay(4, "下載", "下載", "", url, "") };

						cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));
					} else {
						QuickReplay[] link1s = { new InsQuickReplay(5, "下載",
								"App.showMsg(App.getTxtData('" + getString(errorMsg) + "'));", "", "", "") };

						cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));
					}

					cardList.add(cards.getCards());
				}
				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

				replyText = "阿發還可以幫你什麼嗎？";
				botResponseUtils.setTextResult(vo, replyText);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.C05_2_1,
						InsQuickReplay.A05_OTHERHELP);
			} else {
				String replyText = "阿發查不到你輸入的身分證號/統編/居留證號或車號喔！是不是打錯了呢？";
				botResponseUtils.setTextResult(vo, replyText);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.C05_2_3,
						InsQuickReplay.A05_OTHERHELP);
				msgList.addAll(Arrays.asList(new String[] { InsQuickReplay.C05_2_3.getText() }));
				optionList.addAll(Arrays.asList(new QuickReplay[] { InsQuickReplay.C05_2_3 }));
			}
		} else {
			String replyText = "C05 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		returnResult.put("botMessageVO", vo);
		returnResult.put("msgList", msgList);
		returnResult.put("optionList", optionList);

		return returnResult;
	}

	@Override
	public BotMessageVO getF01(InsuranceRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(vo, "各種保險有不同的理賠方法喔！");

		List<Map<String, Object>> cardList = new ArrayList<>();
		{
			CardItem cardItem = vo.new CardItem();
			cardItem.setCName("");
			cardItem.setCWidth(BotMessageVO.CWIDTH_200);

			cardItem.setCTextType("5");
			cardItem.setCTitle("<span style='color:#FF9A00'>國泰產險商品</span>");

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			CTextItem cTextItem = vo.new CTextItem();
			cTextItem.setCText("以下這些保單都可以在國泰產險官網的數位服務平台裡辦理理賠哦~");
			cTextList.add(cTextItem.getCTexts());
			cTextItem = vo.new CTextItem();
			cTextItem.setCText("1. 車險保單");
			cTextList.add(cTextItem.getCTexts());
			cTextItem = vo.new CTextItem();
			cTextItem.setCText("2. 健康與傷害險保單");
			cTextList.add(cTextItem.getCTexts());
			cTextItem = vo.new CTextItem();
			cTextItem.setCText("3.與國泰產險購買的旅遊綜合保險保單");
			cTextList.add(cTextItem.getCTexts());
			cTextItem = vo.new CTextItem();
			cTextItem.setCText("4.與國泰人壽購買旅遊平安保險加購的國泰產險不便險");
			cTextList.add(cTextItem.getCTexts());

			cardItem.setCTexts(cTextList);

			cardItem.setCLinkType(1);

			cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, InsQuickReplay.F01_1_1(reqVO.getSsoId())));
			cardList.add(cardItem.getCards());
		}

		{
			CardItem cardItem = vo.new CardItem();
			cardItem.setCName("");
			cardItem.setCWidth(BotMessageVO.CWIDTH_200);

			cardItem.setCTextType("5");
			cardItem.setCTitle("<span style='color:#FF9A00'>銀行信用卡贈送</span>");

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			CTextItem cTextItem = vo.new CTextItem();
			cTextItem.setCText("刷國泰世華、花旗、滙豐信用卡購買機票或支付團費贈送的旅遊綜合保險，請下載理賠申請書，並依照申請書說明備齊應備文件後將資料寄回就可以囉~");
			cTextList.add(cTextItem.getCTexts());

			cardItem.setCTexts(cTextList);

			cardItem.setCLinkType(1);
			InsQuickReplay q1 = new InsQuickReplay(4, "國泰世華理賠申請書", "國泰世華理賠申請書", "credit", InsUrlGather.pdf1, "");
			InsQuickReplay q2 = new InsQuickReplay(4, "花旗銀行理賠申請書", "花旗銀行理賠申請書", "flower", InsUrlGather.pdf2, "");
			InsQuickReplay q3 = new InsQuickReplay(4, "滙豐銀行理賠申請書", "滙豐銀行理賠申請書", "hsbc", InsUrlGather.pdf1, "");

			cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2, q3));
			cardList.add(cardItem.getCards());
		}

		{
			CardItem cardItem = vo.new CardItem();
			cardItem.setCName("");
			cardItem.setCWidth(BotMessageVO.CWIDTH_200);

			cardItem.setCTextType("5");
			cardItem.setCTitle("<span style='color:#FF9A00'>刷國泰世華卡投保國泰人壽海外旅平險所贈送</span>");

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

			CTextItem cTextItem = vo.new CTextItem();
			cTextItem.setCText("單人單件保費達1100元以上，贈送之旅遊不便險保障，請下載理賠申請書，並依照申請書說明備齊應備文件後將資料寄回就可以囉~");
			cTextList.add(cTextItem.getCTexts());

			cardItem.setCTexts(cTextList);

			cardItem.setCLinkType(1);
			InsQuickReplay q1 = new InsQuickReplay(4, "理賠申請書", "理賠申請書", "C708FF876E813217AE0F6AB882119C894CE53AD7",
					InsUrlGather.pdf3, "");
			InsQuickReplay q2 = new InsQuickReplay(4, "理賠應備文件", "理賠應備文件", "824C27557A6E65082D510B53D2691B98497CDDFC",
					InsUrlGather.pdf4, "");

			cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2));
			cardList.add(cardItem.getCards());
		}

		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.Change_CS, InsQuickReplay.INIT);

		return vo;
	}

	@Override
	public BotMessageVO getF02(InsuranceRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(vo, "車險、健康傷害險與旅遊綜合保險的理賠申請進度與記錄，可以在國泰產險官網的數位服務平台裡查詢喔~");
		botResponseUtils.setTextResult(vo, "其他險種的理賠申請進度與記錄，阿發可以請真人客服來幫你哦~");

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.F02_1_1(reqVO.getSsoId()),
				InsQuickReplay.Change_CS, InsQuickReplay.INIT);
		return vo;
	}

	public Map<String, Object> getT02APIDatas(InsuranceRequest reqVO) {
		HashMap<String, String> requestData = new HashMap<String, String>();
		requestData.put("MEMBER_ID", reqVO.getMemberId());
		requestData.put("BIRTHDAY", reqVO.getBirthday());
		requestData.put("MEMBER_TOKEN", reqVO.getMemberToken());
		requestData.put("RECEIVE_TYPE", reqVO.getReceiveType());
		requestData.put("SESSION_ID", reqVO.getSessionId());
		requestData.put("LOGIN_IP", reqVO.getLoginIp());
		requestData.put("VIEW_DEVICE", reqVO.getViewDevice());

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("map", requestData);
		String rpjson = insuranceAPIFeignService.getT02(JSON.toJSONString(map));
		Map<String, Object> responseMap = returnToMap(rpjson);
		Map<String, Object> detail = (Map<String, Object>) responseMap.get("detail");

		return detail;
	}

	@Override
	public TPIGatewayResult getT02(InsuranceRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		// productType 1:初始牌卡 2:阿發提醒 3:修改
		if (StringUtils.equals(reqVO.getCmd(), "1")) {
			// 變更保單init牌卡
			ContentItem contentItem = vo.new ContentItem();
			contentItem.setType(19);

			CImageDataItem cImageDataItem = vo.new CImageDataItem();
			cImageDataItem.setCImage("img-creditcard");
			cImageDataItem.setCImageTextType(1);
			cImageDataItem.setCImageUrl("card18.png");

			CardItem cardItem = vo.new CardItem();
			cardItem.setCImageData(cImageDataItem.getCImageDatas());
			cardItem.setCTexts(new ArrayList<Map<String, Object>>());
			cardItem.setCWidth("segment secard");
			cardItem.setCTextType("3");
			cardItem.setCLinkType(1);

			CTextItem cTextItem = vo.new CTextItem();
			cTextItem.setCLabel("保單批改");
			cTextItem.setCText("要改資料嗎?阿發可以幫忙歐~");
			cardItem.getCTexts().add(cTextItem.getCTexts());

			List<Map<String, Object>> cLinkList = botResponseUtils.getCLinkList(vo, InsQuickReplay.T02_1_1,
					InsQuickReplay.T02_1_2, InsQuickReplay.T02_1_3, InsQuickReplay.T02_1_4, InsQuickReplay.T02_1_5);
			cardItem.setCLinkList(cLinkList);
			contentItem.setCards(new ArrayList<Map<String, Object>>());
			contentItem.getCards().add(cardItem.getCards());
			botResponseUtils.setInsCard(vo, vo.CTYPE_CARD, contentItem.getCards());

			botResponseUtils.setTextResult(vo, "其他的保單變更服務，阿發可以請真人客服來幫你哦~");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, InsQuickReplay.A05_2_3,
					InsQuickReplay.A05_OTHERHELP);
		} else if (StringUtils.equals(reqVO.getCmd(), "2")) {
			botResponseUtils.setTextResult(vo, "阿發提醒你：<br />" + "1.每張保單一天僅能更改一次<br />"
					+ "2.只能延長保期及異動保險期間，不能縮短保期喔~<br />" + "3.繳費方式限線上刷卡~<br />" + "4.要被保人不相同，或團體件，需要真人客服幫忙喔~");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.T02_CHANGE, InsQuickReplay.Change_CS, InsQuickReplay.OTHERHELP);

			tpiGatewayResult.setMsgList(Arrays.asList(InsQuickReplay.T02_CHANGE.getText()));
			tpiGatewayResult.setOptionList(Arrays.asList(InsQuickReplay.T02_CHANGE));

		} else if (StringUtils.equals(reqVO.getCmd(), "3")) {

			if (reqVO.getMemberLevel() == 1) {
				Map<String, Object> detail = getT02APIDatas(reqVO);
				List<Map<String, Object>> datas = (List<Map<String, Object>>) detail.get("DATAS");

				if (StringUtils.equals(String.valueOf(detail.get("RETURN_CODE")), "0000")) {
					List<Map<String, Object>> resultDatas = new ArrayList<Map<String, Object>>();
					for (Map<String, Object> dataMap : datas) {
						Map<String, Object> resultMap = new HashMap<String, Object>();

						resultMap.put("POLICY_NO", String.valueOf(dataMap.get("POLICY_NO")));
						resultMap.put("TRAVEL_SITE_NAME", String.valueOf(dataMap.get("TRAVEL_SITE_NAME")));
						resultMap.put("DISPLAY_START_DATE", String.valueOf(dataMap.get("DISPLAY_START_DATE")));
						resultMap.put("DISPLAY_END_DATE", String.valueOf(dataMap.get("DISPLAY_END_DATE")));
						resultMap.put("CUSTOMER_NAME", String.valueOf(dataMap.get("CUSTOMER_NAME")));
						resultMap.put("INSURED_NAME", String.valueOf(dataMap.get("INSURED_NAME")));

						resultDatas.add(resultMap);
					}
					botResponseUtils.setInsList(vo, resultDatas, 3);
				} else {
					botResponseUtils.setTextResult(vo, "阿發查不到以你為要保人或被保人的有效保單喔！");
					botResponseUtils.setTextResult(vo,
							"你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('" + InsUrlGather.ECIE_1000
									+ DataUtils.getRectifySSOId(reqVO.getSsoId()) + "')\">bobe線上投保網站</a>，直接投保哦~");
				}
			} else {

				botResponseUtils.setTextResult(vo, "不好意思你現在是一般會員，這個任務要認證會員才可以線上完成喔！");
				botResponseUtils.setTextResult(vo, "升級以使用更多服務！<br />現在升級認證會員，將可使用更多專屬服務，等你來體驗");
				String annoTitle = "注意事項";
				String imgUrl = "resources/common/images/upgradeMemberLevel01.png";
				String annoText = "<img src='" + imgUrl	+ "' alt='' class='img-responsive' onclick=\"PicComponent2.send('" + imgUrl + "');\">";
				botResponseUtils.setNoteResult(vo, annoTitle, annoText);
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_03, InsQuickReplay.會員_申請, InsQuickReplay.OTHERHELP);
				
			}

		}
		logger.info("T02 resultData : " + JSON.toJSONString(vo));

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO getDateView(InsuranceRequest reqVO) {
		Map<String, Object> detail = getT02APIDatas(reqVO);
		List<Map<String, Object>> datas = (List<Map<String, Object>>) detail.get("DATAS");
		Map<String, Object> data = new HashMap<String, Object>();
		logger.info("reqVO.getPolicyNo() : " + reqVO.getPolicyNo());
		for (Map<String, Object> map : datas) {
			if (StringUtils.equals(reqVO.getPolicyNo(), String.valueOf(map.get("POLICY_NO")))) {
				data.putAll(map);
			}
		}
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		String checkMsg = String.valueOf(data.get("CHECK_MSG"));
		logger.info(JSON.toJSONString("data : " + data));
		if (StringUtils.isNotBlank(checkMsg)) {
			botResponseUtils.setTextResult(vo, checkMsg);
			botResponseUtils.setTextResult(vo, "阿發還可以幫你什麼嗎?");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.RESET, InsQuickReplay.OTHERHELP,
					InsQuickReplay.Change_CS);
		} else {
			String[] startDateArr = String.valueOf(data.get("START_DATETIME")).split("-");
			String startDate = startDateArr[0] + '/' + startDateArr[1] + '/' + startDateArr[2];
			String[] endDateArr = ((String) data.get("END_DATETIME")).split("-");
			String endDate = endDateArr[0] + '/' + endDateArr[1] + '/' + endDateArr[2];
			String dateTime = startDateArr[3].substring(0, 2) + ':' + startDateArr[3].substring(3, 5);

			DateView dateView = vo.new DateView();
			dateView.setDVTitle("保單日期修改");
			dateView.setDVDateStart(startDate);
			dateView.setDVDateEnd(endDate);
			dateView.setDVDateTime(dateTime);
			dateView.setDVPlace(String.valueOf(data.get("TRAVEL_SITE_NAME")));
			botResponseUtils.setDateView(vo, dateView.getDVData());
		}
		return vo;
	}

	@Override
	public TPIGatewayResult getT03(InsuranceRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		Map<String, Object> detail = getT02APIDatas(reqVO);
		List<Map<String, Object>> datas = (List<Map<String, Object>>) detail.get("DATAS");
		Map<String, Object> data = new HashMap<String, Object>();
		for (Map<String, Object> map : datas) {
			if (StringUtils.equals(reqVO.getPolicyNo(), String.valueOf(map.get("POLICY_NO")))) {
				data.putAll(map);
			}
		}
		// -----API-start-T03
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss.S");
		DateTimeFormatter dfRequest = DateTimeFormatter.ofPattern("yyyy/MM/ddHH:mm");
		DateTimeFormatter dfResult = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String newStartDate = df.format(LocalDateTime.parse(reqVO.getDateStart() + reqVO.getDateTime(), dfRequest));
		String newEndDate = df.format(LocalDateTime.parse(reqVO.getDateEnd() + reqVO.getDateTime(), dfRequest));
		String resultStartDate = dfResult
				.format(LocalDateTime.parse(reqVO.getDateStart() + reqVO.getDateTime(), dfRequest));
		String resultEndDate = dfResult
				.format(LocalDateTime.parse(reqVO.getDateEnd() + reqVO.getDateTime(), dfRequest));
		reqVO.setNewStartDateTime(newStartDate);
		reqVO.setNewEndDateTime(newEndDate);
		param.put("newSTART_DATETIME", newStartDate);
		param.put("newEND_DATETIME", newEndDate);
		param.put("POLICY_NO", String.valueOf(data.get("POLICY_NO")));
		param.put("CUSTOMER_ID", String.valueOf(data.get("CUSTOMER_ID")));
		param.put("CONTRACT_NO", String.valueOf(data.get("CONTRACT_NO")));
		param.put("START_DATETIME", String.valueOf(data.get("START_DATETIME")));
		param.put("END_DATETIME", String.valueOf(data.get("END_DATETIME")));
		param.put("INSRNCE_DAY", String.valueOf(data.get("INSRNCE_DAY")));

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("map", param);
		logger.info("T03 : map : " + map);

		String rpjson = insuranceAPIFeignService.getT03(JSON.toJSONString(map));

		Map<String, Object> responseMap = returnToMap(rpjson);
		Map<String, Object> resultDetail = (Map<String, Object>) responseMap.get("detail");
		if (StringUtils.equals(String.valueOf(resultDetail.get("RETURN_CODE")), "0000")) {
			Map<String, Object> datasMap = (Map<String, Object>) resultDetail.get("DATAS");
			botResponseUtils.setTextResult(vo, "好的，你想要改成這樣對嗎?");
			CardItem cardItem = vo.new CardItem();
			cardItem.setCTexts(new ArrayList<Map<String, Object>>());

			CTextItem cTextItem = vo.new CTextItem();
			cTextItem.setCLabel("保單名稱");
			cTextItem.setCText(String.valueOf(data.get("PRODUCT_POLICY_NAME")));
			cardItem.getCTexts().add(cTextItem.getCTexts());

			cTextItem = vo.new CTextItem();
			cTextItem.setCLabel("旅遊地點");
			cTextItem.setCText(String.valueOf(data.get("TRAVEL_SITE_NAME")));
			cardItem.getCTexts().add(cTextItem.getCTexts());

			cTextItem = vo.new CTextItem();

			cTextItem.setCLabel("變更後保險期間");
			String interval = resultStartDate + "起<br/>" + resultEndDate + "止";
			cTextItem.setCText(interval);
			cardItem.getCTexts().add(cTextItem.getCTexts());
			
			String premiumChange= getString(datasMap.get("DISCOUNT_PREMIUM_CHANGE"));
			cTextItem = vo.new CTextItem();
			cTextItem.setCLabel("新增保費");
			cTextItem.setCText("NTD " + premiumChange);
			cardItem.getCTexts().add(cTextItem.getCTexts());

			cTextItem = vo.new CTextItem();
			cTextItem.setCLabel("電子批單寄送資訊");
			cTextItem.setCText(reqVO.getEmail() + "<br/>" + reqVO.getPhone());
			cardItem.getCTexts().add(cTextItem.getCTexts());

			cardItem.setCWidth("segment secard");
			cardItem.setCTextType("10");
			ContentItem contentItem = vo.new ContentItem();
			contentItem.setCards(new ArrayList<Map<String, Object>>());
			contentItem.getCards().add(cardItem.getCards());
			botResponseUtils.setCardsResult19(vo, "card", contentItem.getCards());

			Map<String, Object> policyMap = new HashMap<String, Object>();
			policyMap.put("POLICY_NO", reqVO.getPolicyNo());
			Map<String, Object> uplAppllteNo = new HashMap<String, Object>();
			uplAppllteNo.put("UPL_APPLLTE_NO", String.valueOf(datasMap.get("UPL_APPLLTE_NO")));

			QuickReplay quicklyReplay ;
			QuickReplay quicklyPolicyMap ;
			if(Integer.valueOf(premiumChange)==0) {
				quicklyReplay = InsQuickReplay.T03_1_2(uplAppllteNo);
				quicklyPolicyMap = InsQuickReplay.T03_1_2(policyMap);
			}else {
				quicklyReplay = InsQuickReplay.T03_1_1(uplAppllteNo);
				quicklyPolicyMap = InsQuickReplay.T03_1_1(policyMap);
			}
			cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, quicklyReplay,InsQuickReplay.T03_2(policyMap)));
			botResponseUtils.setTextResult(vo, "提醒你，要確認新的保險期間有涵蓋到你抵達家門的時間喔~");
			botResponseUtils.setTextResult(vo, "線上批改僅寄送電子批單，如需變更寄送資訊請至<a href='javascript:void(0);' onclick=\"window.open('"
							+ InsUrlGather.OCH1_0400 + DataUtils.getRectifySSOId2(reqVO.getSsoId()) + "')\" >數位服務平台</a>修改會員資料，並重新進行變更作業。");
			
			
			tpiGatewayResult.setMsgList(Arrays.asList(quicklyPolicyMap.getText(),InsQuickReplay.T03_2(uplAppllteNo).getText()));
			tpiGatewayResult.setOptionList(Arrays.asList(quicklyPolicyMap, InsQuickReplay.T03_2(uplAppllteNo)));
			Map<String, Object> T03Data = new HashMap<String, Object>();
			T03Data.put("POLICY_NO", reqVO.getPolicyNo());
			T03Data.put("UPL_APPLLTE_NO", String.valueOf(datasMap.get("UPL_APPLLTE_NO")));
			T03Data.put("DISCOUNT_PREMIUM", String.valueOf(datasMap.get("DISCOUNT_PREMIUM")));
			T03Data.put("DISCOUNT_PREMIUM_CHANGE", String.valueOf(datasMap.get("DISCOUNT_PREMIUM_CHANGE")));
			tpiGatewayResult.put("T03Data", T03Data);

		} else {
			botResponseUtils.setTextResult(vo, String.valueOf(resultDetail.get("RETURN_MSG")));
		}
		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO getCreditCardView(InsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setCreditCardView(botMessageVO, reqVO.getUplAppllteNo(), null);
		return botMessageVO;
	}

	@Override
	public TPIGatewayResult getT04(InsuranceRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO vo = new BotMessageVO();
		// -----API-start-T04
		HashMap<String, String> requestData = new HashMap<String, String>();

		// T03的資料
		requestData.put("MEMBER_ID", reqVO.getMemberId());
		requestData.put("RECEIVE_TYPE", "65");
		requestData.put("SESSION_ID", reqVO.getSessionId());
		requestData.put("LOGIN_IP", reqVO.getLoginIp());
		requestData.put("VIEW_DEVICE", reqVO.getViewDevice());
		requestData.put("MEMBER_TOKEN", reqVO.getMemberToken());

		requestData.put("POLICY_NO", reqVO.getPolicyNo());
		requestData.put("UPL_APPLLTE_NO", reqVO.getUplAppllteNo());
		requestData.put("DISCOUNT_PREMIUM", reqVO.getDiscountPremium());
		requestData.put("DISCOUNT_PREMIUM_CHANGE", reqVO.getDiscountPremiumChange());
		// 信用卡資料
		requestData.put("creditCardNo1", reqVO.getCreditCardNo1());
		requestData.put("creditCardNo2", reqVO.getCreditCardNo2());
		requestData.put("creditCardNo3", reqVO.getCreditCardNo3());
		requestData.put("creditCardNo4", reqVO.getCreditCardNo4());
		requestData.put("creditCardY", reqVO.getCreditCardY());
		requestData.put("creditCardM", reqVO.getCreditCardM());
		requestData.put("creditCardCvc", reqVO.getCreditCardCvc());

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("map", requestData);
		logger.info("T04 : map : " + map);

		String rpjson = insuranceAPIFeignService.getT04(JSON.toJSONString(map));
		logger.info("T04 : result : " + rpjson);

		Map<String, Object> responseMap = returnToMap(rpjson);
		Map<String, Object> detail = (Map<String, Object>) responseMap.get("detail");
		vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		if (StringUtils.equals(String.valueOf(detail.get("RETURN_CODE")), "0000")) {
			BotMessageVO botMessageVO = null;
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("MEMBER_ID", reqVO.getMemberId());
			requireParams.put("MEMBER_TOKEN", reqVO.getMemberToken());
			requireParams.put("RECEIVE_TYPE", "65");
			requireParams.put("SESSION_ID", reqVO.getSessionId());
			requireParams.put("LOGIN_IP", reqVO.getLoginIp());
			requireParams.put("VIEW_DEVICE", reqVO.getViewDevice());
			reqVO.setCmd("TRAVEL");
			botMessageVO = getA05(reqVO, requireParams);
			List<Map<String, String>> getMessageList = (List<Map<String, String>>) botMessageVO.getContent().get(1)
					.get("dataList");
			Map<String, String> resultDetail = new HashMap<String, String>();
			for (Map<String, String> getMessage : getMessageList) {
				if (StringUtils.equals(getMessage.get("POLICY_NO"), reqVO.getPolicyNo())) {
					resultDetail.putAll(getMessage);
					break;
				}
			}
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("MEMBER_TYPE", getString(resultDetail.get("MEMBER_TYPE")));
			parameter.put("POLICY_NO", getString(resultDetail.get("POLICY_NO")));
			parameter.put("APPLLTE_TYPE", getString(resultDetail.get("APPLLTE_TYPE")));

			botResponseUtils.setTextResult(vo, "保險期間變更已完成囉~要看看變更後的保單內容嗎？");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_03, InsQuickReplay.T04_1(parameter),
					InsQuickReplay.OTHERHELP);
			tpiGatewayResult.put("POLICY_NO", reqVO.getPolicyNo());
			tpiGatewayResult.put("PRODUCT_TYPE", "TRAVEL");
			tpiGatewayResult.put("MEMBER_TYPE", getString(resultDetail.get("MEMBER_TYPE")));
			tpiGatewayResult.put("APPLLTE_TYPE", getString(resultDetail.get("APPLLTE_TYPE")));
			return tpiGatewayResult.setData(vo);
		} else {
			if (StringUtils.equals("0", reqVO.getDiscountPremiumChange())) {
				botResponseUtils.setTextResult(vo, String.valueOf(detail.get("RETURN_MSG")));
			} else {
				botResponseUtils.setCreditCardView(vo, reqVO.getUplAppllteNo(),
						String.valueOf(detail.get("RETURN_MSG")));
			}
			return tpiGatewayResult.setData(vo);
		}
	}

	@Override
	public BotMessageVO getTopQuestions(InsuranceRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<TopQuestion> topQuestionList = topQuestionMapper
				.getAll(reqVO.getQuestionType() != null ? reqVO.getQuestionType() : 1);

		if (CollectionUtils.isNotEmpty(topQuestionList)) {
			for (TopQuestion topQuestion : topQuestionList) {
				LinkListItem linkList = vo.new LinkListItem();
				linkList.setLText(topQuestion.getQuestionName());
				linkList.setLTopQuestion(topQuestion.getQuestionText());
				linkList.setLAlt(topQuestion.getQuestionAlt());
				linkList.setLAction(19);
				resultList.add(linkList.getLinkList());
			}
		}
		botResponseUtils.setInsTopQuestion(vo, resultList, "熱門問題");
		return vo;
	}

	public String getString(Object str) {
		String returnStr = "";
		if (str != null) {
			returnStr = str.toString();
		}

		return returnStr;
	}

	public Integer getInteger(Object data) {
		Integer returnInt = 0;
		if (data != null) {
			try {
				returnInt = Integer.parseInt(data.toString());
			} catch (NumberFormatException e) {
				return returnInt;
			}
		}

		return returnInt;
	}

	private URI getURI(String url) {
		URI baseUrl;
		try {
			baseUrl = new URI(url);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage());
		}
		return baseUrl;
	}

	public static Map<String, Object> returnToMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> rumap = new HashMap<String, Object>();
		try {
			rumap = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
			});
			return rumap;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rumap;
	}

	public List<Map<String, Object>> drawCard(BotMessageVO vo, List<Map<String, Object>> arrayList,
			String[] detailArray, String titleKey, String title) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		for (int i = 0; i < arrayList.size(); i++) {
			String idKey = getString(arrayList.get(i).get(titleKey));
			String idName = getString(arrayList.get(i).get("StockName"));

			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			cards.setCTextType("5");
			cards.setCLinkType(1);

			// 牌卡圖片
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl("policy_01.jpg");
			cards.setCImageData(cImageData.getCImageDatas());

			if (!getString(title).equals("")) {
				cards.setCTitle(title);
			} else if (!getString(titleKey).equals("")) {
				cards.setCTitle(idKey);
			}

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
			for (int j = 0; j < detailArray.length; j++) {
				CTextItem cText = vo.new CTextItem();
				String detailMsg = detailArray[j].split(":")[1] + " : "
						+ getString(arrayList.get(i).get(detailArray[j].split(":")[0]));
				cText.setCText(detailMsg);
				cTextList.add(cText.getCTexts());
			}
			cards.setCTexts(cTextList);

			QuickReplay[] link1s = {
					new InsQuickReplay(15, "查看個股資訊", "查看(" + idKey + ")資訊", "RB02_select:" + idKey, "", ""),
					new InsQuickReplay(15, "申購", "申購(" + idKey + ")", "RB02_buy:" + idKey + ":" + idName, "", "") };

			cards.setCLinkList(botResponseUtils.getCLinkList(vo, link1s));

			cardList.add(cards.getCards());
		}

		return cardList;
	}

	public String textRescue() {
		return "<div class=\"txt\" >" + "<h3>服務項目</h3>"
				+ "<p><span style=\"color:#328a28;border-bottom-style:solid;\">道路救援服務項目及收費標準表</span></p>"
				+ "<br> <p>●免費拖吊（超過免費公里數者，每公里收費50元，由保戶自付）；惟拖吊地點須位拖吊車可行駛及作業平面道路（不提供車輛在修車廠內拖吊服務）</p>"
				+ "<p>●免費急修服務（加水、更換備胎、接電啟動、加油但燃料費需自付）</p>" + "<p>●其他諮詢服務（例加油站、醫院位置諮詢、代為聯絡家屬等）</p><br><p>以下地區不提供服務:</p>"
				+ "<p style=\"margin-left:+10px;\"> 1.特殊地區:橫貫公路、國家公園、離島及偏遠山區（例如:合歡山、阿里山；司馬庫斯等）。</p>"
				+ "<p style=\"margin-left:+10px;\"> 2.因淹水、山崩、斷橋等不可避免之天災事變，或經政府公告禁止通行之管制山區及路段。</p>"
				+ "<p style=\"margin-left:+10px;\"> 3.交通管制規則之法令規定無法上下乘客之高速公路及快速公路。</p>"
				+ "</div><div class=\"subview_inner\">" + "<div class=\"sub_button\">"
				+ "<a href=\"#\" class=\"tag\" ms-on-click=\"@send\">我想看詳細內容</a>" + "</div></div>";
	}

}
