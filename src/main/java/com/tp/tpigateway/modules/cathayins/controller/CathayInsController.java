package com.tp.tpigateway.modules.cathayins.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathayins.service.CathayInsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("cathayins")
public class CathayInsController extends BaseController {

    @Autowired
    CathayInsService cathayInsService;

    @RequestMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult hello(@RequestBody BotRequest botRequest) {
        return cathayInsService.hello(botRequest);
    }

    @RequestMapping(value = "/fallback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult fallback(@RequestBody FallBackRequest fallBackRequest) {
        return cathayInsService.fallback(fallBackRequest);
    }

    /*
       組FAQ回答樣式
    */
    @RequestMapping(value = "/handleFaqReplay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult handleFaqReplay(@RequestBody Map<String, Object> request) {
        return cathayInsService.handleFaqReplay(request);
    }

    /*
        服務台反問
     */
    @RequestMapping(value = "/supportNLU", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult supportNLU(@RequestBody Map<String, Object> request) {
        return cathayInsService.supportNLU(request);
    }

    @RequestMapping(value = "/showSurvey", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult showSurvey(@RequestBody BotRequest botRequest) {
        BotMessageVO botMessageVO = cathayInsService.showSurvey(botRequest);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }
    

}
