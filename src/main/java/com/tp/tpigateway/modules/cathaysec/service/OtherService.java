package com.tp.tpigateway.modules.cathaysec.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.cathaysec.DefaultRequest;
import com.tp.common.model.cathaysec.RegularRequest;

import java.util.Map;

public interface OtherService {
	
	/**
	 * For Cmd
	 * 
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	public BotMessageVO chooseCmd(DefaultRequest reqVO);

	/**
	 * For 新手上路(RB20)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB20(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 新手上路_回覆按鈕集中處理(RB20Action)
	 * 
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB20Action(RegularRequest reqVO);
	
}
