package com.tp.tpigateway.modules.cathaysec.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysec.LoginRequest;
import com.tp.common.model.cathaysite.SiteRequest;
import com.tp.tpigateway.modules.cathaysec.model.HelloRequest;

import java.util.Map;

public interface CathaySecService {

    BotMessageVO hello(HelloRequest helloRequest);

    TPIGatewayResult fallback(FallBackRequest fallBackRequest);

    BotMessageVO showSurvey(BotRequest botRequest);

    BotMessageVO cmd(SiteRequest siteRequest);

    TPIGatewayResult handleFaqReplay(Map<String, Object> request);

    TPIGatewayResult supportNLU(Map<String, Object> request);

    TPIGatewayResult doSecSelectAccount(LoginRequest loginRequest);
}
