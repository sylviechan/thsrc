package com.tp.tpigateway.modules.cathaysec.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.RegularRequest;
import com.tp.common.model.cathaysec.StockLotterRequest;

public interface StockLotteryService {

	public BotMessageVO stockLottery(StockLotterRequest reqVO);

	public BotMessageVO showAllCanStockPurchase(StockLotterRequest reqVO);

	public TPIGatewayResult confirmWhetherToPurchase1(StockLotterRequest reqVO);

	public BotMessageVO confirmWhetherToPurchase2(StockLotterRequest reqVO);

	public BotMessageVO commingStock(StockLotterRequest reqVO);

	public BotMessageVO doingStock(StockLotterRequest reqVO);

	public BotMessageVO doingStockDetails(StockLotterRequest reqVO);

	public TPIGatewayResult buyStock(StockLotterRequest reqVO);

	public TPIGatewayResult chooseWebCmd(StockLotterRequest reqVO);

}
