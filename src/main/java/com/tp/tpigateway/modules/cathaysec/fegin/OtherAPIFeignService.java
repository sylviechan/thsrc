package com.tp.tpigateway.modules.cathaysec.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "otherapi", url = "${api.sec.path}/")
@TpSysLog(remark = "OtherAPIFeignService")
public interface OtherAPIFeignService {

	@RequestMapping(value = "eCathayRobo/RB20.svc/RB20Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB20(String json);

	@RequestMapping(value = "eCathayRobo/RB21.svc/RB21Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB21(String json);

	@RequestMapping(value = "eCathayRobo/RB24.svc/RB24Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB24(String json);

	@RequestMapping(value = "LogMonitor/AFALogInfo.svc/AFAErrLog", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> sendeErrorLogRB29(Map<String, Object> dataUtilsAbnormalOrder);

}
