package com.tp.tpigateway.modules.cathaysec.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tp.tpigateway.modules.cathaysec.mybatis.entity.StockData;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author matt
 * @since 2019-09-04
 */
public interface StockDataMapper extends BaseMapper<StockData> {

}
