package com.tp.tpigateway.modules.cathaysec.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "passwordapi", url = "${api.sec.path}/eCathayRobo/")
@TpSysLog(remark = "PasswordAPIFeignService", showParams = false, showResult = false)
public interface PasswordAPIFeignService {
	
	@RequestMapping(value = "RB04.svc/RB04Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Map<String, Object> getRB04(Map<String, String> json);
	
	@RequestMapping(value = "RB05.svc/RB05Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB05(Map<String, String> json);
	
	@RequestMapping(value = "RB06.svc/RB06Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB06(Map<String, String> json);
	
	@RequestMapping(value = "RB07.svc/RB07Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB07(Map<String, String> json);
	
	@RequestMapping(value = "RB08.svc/RB08Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB08(Map<String, String> json);
	
	@RequestMapping(value = "RB09.svc/RB09Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB09(Map<String, String> json);
}
