package com.tp.tpigateway.modules.cathaysec.service.impl;

import com.alibaba.fastjson.JSON;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysec.LoginRequest;
import com.tp.common.model.cathaysec.RegularRequest;
import com.tp.common.model.cathaysite.SiteRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.fegin.OtherAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.model.HelloRequest;
import com.tp.tpigateway.modules.cathaysec.model.SecQuickReplay;
import com.tp.tpigateway.modules.cathaysec.service.CathaySecService;
import com.tp.tpigateway.modules.cathaysec.service.OtherService;
import com.tp.tpigateway.modules.cathaysec.service.RegularService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CathaySecServiceImpl implements CathaySecService {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    BotResponseUtils botResponseUtils;

    @Autowired
    OtherAPIFeignService otherAPIFeignService;

    @Autowired
    OtherService otherService;

    @Autowired
    RegularService regularService;

    private static final Map<String, QuickReplay> INTENT_QR_MAP;

    static {
        INTENT_QR_MAP = new HashMap<>();
        INTENT_QR_MAP.put("定期定額", SecQuickReplay.INIT_REGULAR);
        INTENT_QR_MAP.put("股票抽籤", SecQuickReplay.INIT_LOTTERY);
        INTENT_QR_MAP.put("密碼", SecQuickReplay.INIT_PWD);
        INTENT_QR_MAP.put("個人投資", SecQuickReplay.INIT_DELIVERY);
        INTENT_QR_MAP.put("報價", SecQuickReplay.INIT_TAIEXGOT);
    }

    @Override
    public BotMessageVO hello(HelloRequest helloRequest) {
        log.info("helloRequest.getIdno() : " + helloRequest.getIdno());
        log.info("helloRequest.getId() : " + helloRequest.getId());
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(helloRequest.getChannel(), helloRequest.getRole());
        String idno = helloRequest.getIdno();
        if (StringUtils.isBlank(helloRequest.getMsg())) {
            helloRequest.setMsg("init");
        }
        Map<String, String> requestCathayData = new HashMap<String, String>();
        requestCathayData.put("FunctionID", "RB24");
        requestCathayData.put("UDID", helloRequest.getMsg());
        requestCathayData.put("Channel", PropUtils.getApiChannel());
        Map<String, Object> responseData = otherAPIFeignService.getRB24(JSON.toJSONString(requestCathayData));
        if (StringUtils.equals((String) responseData.get("ResultCode"), "0000")) {
            botResponseUtils.setTextResult(botMessageVO, (String) responseData.get("Grt_Desc"));
        } else {
            botResponseUtils.setTextResult(botMessageVO, "嗨～請問需要阿發幫你做什麼呢？");
        }

        if (StringUtils.isNotBlank(idno)) {
            //TODO 新手上路 RB20
            RegularRequest regularReq = new RegularRequest();
            regularReq.setChannel(helloRequest.getChannel());
            regularReq.setRole(helloRequest.getRole());
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("FunctionID", "RB20");
            requireParams.put("Channel", PropUtils.getApiChannel());
            requireParams.put("ID", helloRequest.getId());
            requireParams.put("functionType", "RB24");

            Map<String, Object> rb20 = otherService.getRB20(regularReq, requireParams);
            BotMessageVO botMessageVOGetRB20 = (BotMessageVO) rb20.get("botMessageVO");

            if (botMessageVOGetRB20 != null) {
                for (Map<String, Object> RB20Content : botMessageVOGetRB20.getContent()) {
                    botMessageVO.getContent().add(RB20Content);
                }
            } else {
                //老手 1.申辦(定期定額、股票抽籤、密碼相關)2.報價(大盤、熱門股、庫存/自選)3.查詢(成交回報、委託查詢、交割款)
                botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getInitCardList(botMessageVO));
            }

        } else {
            //未登入 1.申辦(定期定額、股票抽籤、密碼相關)2.報價(大盤、熱門股、庫存/自選)3.查詢(成交回報、委託查詢、交割款)
            botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getInitCardList(botMessageVO));
        }

        return botMessageVO;
    }

    @Override
    public TPIGatewayResult fallback(FallBackRequest fallBackRequest) {
        BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());

        TPIGatewayResult defaultTpiResult = TPIGatewayResult.ok();
        TPIGatewayResult tpiGatewayResult = null;

        String firstNluCanAnswer = fallBackRequest.getFirstNluCanAnswer();

        if (StringUtils.equals(firstNluCanAnswer, "Y")) { //總機聽的懂, secondBot聽不懂
            //定期定額_操作 密碼_操作 報價_操作  股票抽籤_操作 股票抽籤_查詢_項目 個人投資_操作 個人投資_查詢_項目
            String fallbackNlu = StringUtils.defaultString(fallBackRequest.getFallbackNlu(), "");

            defaultTpiResult.put(TPIConst.SECOND_BOT_ASK, "Y");

            switch (fallbackNlu) {
                case "定期定額_操作":
                    tpiGatewayResult = regularService.rb02Check(fallBackRequest);
                    tpiGatewayResult.put(TPIConst.SECOND_BOT_ASK, "Y");
                    break;
                case "密碼_操作":
                    botResponseUtils.setTextResult(vo, "請問你想要做關於密碼的什麼業務呢？");
                    botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.UPD_PWD, SecQuickReplay.RESEND_PWD);
                    break;
                case "報價_操作":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");

                    List<Map<String, Object>> price_cardList = new ArrayList<>();
                    price_cardList.add(getPriceCardItem(vo).getCards());
                    botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, price_cardList);
                    break;
                case "股票抽籤_操作":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo, SecQuickReplay.INIT_LOTTERY);
                    break;
                case "股票抽籤_查詢_項目":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                    botResponseUtils.setQuickReplayResult(vo, SecQuickReplay.LotteryRecord, SecQuickReplay.ShowAllCanTargets);
                    break;
                case "個人投資_操作":
                case "個人投資_查詢_項目":
                    botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");

                    List<Map<String, Object>> inv_cardList = new ArrayList<>();
                    inv_cardList.add(getInvCardItem(vo).getCards());
                    botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, inv_cardList);
                    break;
                default:
                    tpiGatewayResult = handleDefaultFallback(fallBackRequest);
                    break;
            }
        } else {
            tpiGatewayResult = handleIntentAndFaqCard(fallBackRequest);
        }

        if (tpiGatewayResult != null) {
            return tpiGatewayResult;
        }

        return defaultTpiResult.setData(vo);
    }

    private TPIGatewayResult handleIntentAndFaqCard(FallBackRequest fallBackRequest) {
        BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String msg = fallBackRequest.getMsg();
        List<Map<String, Object>> firstNluIntents = fallBackRequest.getFirstNluIntents();
        List<Map<String, Object>> faqResult = fallBackRequest.getFaqResult();

        List<Map<String, Object>> cardList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(firstNluIntents)) {
            List<QuickReplay> intentCardQRs = new ArrayList<>();

            for (Map map : firstNluIntents) {

                String score = MapUtils.getString(map, "score");

                if (NumberUtils.toDouble(score) <= NumberUtils.toDouble(PropUtils.getCathaySecScoreNluMin())) {
                    continue;
                }

                String intent = MapUtils.getString(map, "intent");
                if (StringUtils.equals(intent, "Hello") || StringUtils.equals(intent, "End") || StringUtils.equals(intent, "真人服務")) {
                    continue;
                }

                intentCardQRs = new ArrayList<>();
                intentCardQRs.add(INTENT_QR_MAP.get(intent));

                if (intentCardQRs.size() >= 3) {
                    break;
                }
            }

            if (intentCardQRs.size() > 0) {
                CardItem cards = buildBaseCard(vo, "card04.png", intentCardQRs.toArray(new QuickReplay[intentCardQRs.size()]));
                cardList.add(cards.getCards());
            }
        }

        if (CollectionUtils.isNotEmpty(faqResult)) {
            //card06.png
            List<QuickReplay> faqCardQRs = new ArrayList<>();

            for (Map map : faqResult) {

                String score = MapUtils.getString(map, "score");

                if (NumberUtils.toDouble(score) < NumberUtils.toDouble(PropUtils.getCathaySecScoreFaqMin())) {
                    continue;
                }

                String text = MapUtils.getString(map, "question");
                String alt = MapUtils.getString(map, "answer");
                //看需求是否需要過濾不顯示的categorie //TODO 要過濾後面是@2以上的
                //List<String> categories = (List<String>) MapUtils.getObject(map, "categories");

                QuickReplay q = new SecQuickReplay(10, text, alt);
                faqCardQRs.add(q);

                if (faqCardQRs.size() >= 3) {
                    break;
                }
            }

            if (faqCardQRs.size() > 0) {
                CardItem cards = buildBaseCard(vo, "card06.png", faqCardQRs.toArray(new QuickReplay[faqCardQRs.size()]));
                cardList.add(cards.getCards());
            }
        }

        if (cardList.size() > 0) {
            botResponseUtils.setTextResult(vo, "阿發猜想你要的答案是");
            // 牌卡
            botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
            tpiGatewayResult.put(TPIConst.ASK_CARD, "Y");

            return tpiGatewayResult.setData(vo);
        } else {
            return handleDefaultFallback(fallBackRequest);
        }
    }

    private CardItem buildBaseCard(BotMessageVO vo, String image, QuickReplay... quickReplays) {
        CardItem cards = vo.new CardItem();
        cards.setCName("");
        cards.setCWidth(BotMessageVO.CWIDTH_200);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CC);
        cImageData.setCImageUrl(image);
        cards.setCImageData(cImageData.getCImageDatas());

        cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplays));

        return cards;
    }

    private TPIGatewayResult handleDefaultFallback(FallBackRequest fallBackRequest) {
        BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        StringBuilder sb = new StringBuilder();
        sb.append("你要問「")
                .append(fallBackRequest.getMsg())
//                .append("（")
//                .append(fallBackRequest.getSecondBot())
//                .append("）")
                .append("」什麼相關內容呢？你可以描述得更完整一些，我會比較容易理解喔！");

        botResponseUtils.setTextResult(vo, sb.toString());

        tpiGatewayResult.put(TPIConst.CAN_NOT_ANS, "Y");

        return tpiGatewayResult.setData(vo);
    }

    private List<Map<String, Object>> getInitCardList(BotMessageVO vo) {
        List<Map<String, Object>> cardList = new ArrayList<>();

        // card1
        CardItem cards1 = getApplyCardItem(vo);

        // card2
        CardItem cards2 = getPriceCardItem(vo);

        // card3
        CardItem cards3 = getInvCardItem(vo);

        cardList.add(cards1.getCards());
        cardList.add(cards2.getCards());
        cardList.add(cards3.getCards());

        return cardList;
    }

    private CardItem getApplyCardItem(BotMessageVO vo) {
        CardItem cards1 = vo.new CardItem();
        cards1.setCName("");
        cards1.setCWidth(BotMessageVO.CWIDTH_200);
        CImageDataItem cImageData1 = vo.new CImageDataItem();
        cImageData1.setCImage(BotMessageVO.IMG_CC);
        cImageData1.setCImageUrl("policy_04.jpg");
        cards1.setCImageData(cImageData1.getCImageDatas());
        cards1.setCTextType("5");
        cards1.setCTitle("申辦");
        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        cards1.setCTexts(cTextList);
        cards1.setCLinkList(botResponseUtils.getCLinkList(vo, SecQuickReplay.INIT_REGULAR, SecQuickReplay.INIT_LOTTERY, SecQuickReplay.INIT_PWD));

        return cards1;
    }

    private CardItem getPriceCardItem(BotMessageVO vo) {
        CardItem cards2 = vo.new CardItem();
        cards2.setCName("");
        cards2.setCWidth(BotMessageVO.CWIDTH_200);
        CImageDataItem cImageData2 = vo.new CImageDataItem();
        cImageData2.setCImage(BotMessageVO.IMG_CC);
        cImageData2.setCImageUrl("policy_01.jpg");
        cards2.setCImageData(cImageData2.getCImageDatas());
        cards2.setCTextType("5");
        cards2.setCTitle("報價");
        List<Map<String, Object>> cText2List = new ArrayList<Map<String, Object>>();
        cards2.setCTexts(cText2List);
        cards2.setCLinkList(botResponseUtils.getCLinkList(vo, SecQuickReplay.INIT_TAIEXGOT, SecQuickReplay.INIT_HOTSTOCK, SecQuickReplay.INIT_STOCK_OPTIONAL));

        return cards2;
    }

    private CardItem getInvCardItem(BotMessageVO vo) {
        CardItem cards3 = vo.new CardItem();
        cards3.setCName("");
        cards3.setCWidth(BotMessageVO.CWIDTH_200);
        CImageDataItem cImageData3 = vo.new CImageDataItem();
        cImageData3.setCImage(BotMessageVO.IMG_CC);
        cImageData3.setCImageUrl("policy_03.jpg");
        cards3.setCImageData(cImageData3.getCImageDatas());
        cards3.setCTextType("5");
        cards3.setCTitle("個人投資");
        List<Map<String, Object>> cText3List = new ArrayList<Map<String, Object>>();
        cards3.setCTexts(cText3List);
        cards3.setCLinkList(botResponseUtils.getCLinkList(vo, SecQuickReplay.INIT_RETURN_TRANSACTION, SecQuickReplay.INIT_ENTRUSTED_INQUIRY, SecQuickReplay.INIT_DELIVERY));

        return cards3;
    }

    @Override
    public BotMessageVO showSurvey(BotRequest botRequest) {
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());

        botResponseUtils.setSurvey(botMessageVO);

        return botMessageVO;
    }

    @Override
    public BotMessageVO cmd(SiteRequest siteRequest) {
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(siteRequest.getChannel(), siteRequest.getRole());
        String cmd = siteRequest.getCmd();

        switch (cmd) {
            case "doSecLogin":
                botResponseUtils.setSecLoginData(botMessageVO);
                break;
            case "loginSuccess":
                botResponseUtils.setTextResult(botMessageVO, "登入成功");
                break;
            case "loginError":
                botResponseUtils.setTextResult(botMessageVO, "登入失敗（" + siteRequest.getMsg() + "）");
                break;
            case "doSecVoucher":
                botResponseUtils.setSecVoucherData(botMessageVO);
                break;
            case "doSecVoucher_check":
                botResponseUtils.setSecVoucherData_check(botMessageVO);
                break;
            case "doSecVoucher_buy":
                List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
                Map<String, Object> dataMap = new HashMap<String, Object>();

                dataMap.put("voucherType", siteRequest.getVoucherType());
                dataMap.put("branchCode", siteRequest.getBranchCode());
                dataMap.put("account", siteRequest.getAccount());
                dataMap.put("symbol", siteRequest.getSymbol());
                dataMap.put("id", siteRequest.getId());
                dataMap.put("chargeAmt", siteRequest.getChargeAmt());
                dataMap.put("chargeDay", siteRequest.getChargeDay());

                dataList.add(dataMap);

                botResponseUtils.setSecVoucherData_buy(botMessageVO, dataList);
                break;
            case "voucherSuccess":
                botResponseUtils.setTextResult(botMessageVO, "憑證申請成功");
                break;
            case "voucherError":
                botResponseUtils.setTextResult(botMessageVO, "憑證申請失敗（" + siteRequest.getMsg() + "）");
                break;
            case "voucherCheck_00":
                botResponseUtils.setTextResult(botMessageVO, "憑證狀態_00");
                break;
            case "voucherCheck_69":
                botResponseUtils.setTextResult(botMessageVO, "需重新申請憑證");
                botResponseUtils.setSecVoucherData(botMessageVO);
                break;
            case "voucherCheck_41":
                botResponseUtils.setTextResult(botMessageVO, "輸入生日錯誤");
                botResponseUtils.setSecVoucherData(botMessageVO);
                break;
            case "voucherCheck_44":
                botResponseUtils.setTextResult(botMessageVO, "我好像發生了一些錯誤...");
                botResponseUtils.setTextResult(botMessageVO, siteRequest.getMsg());
                break;
            case "isExistAgent_Y":
                botResponseUtils.setTextResult(botMessageVO, "哭哭～你的帳號已經被授權給別人了，無法線上投資定期定額喔");
                botResponseUtils.setTextResult(botMessageVO, "但是你可以紙本臨櫃申購唷！");
                botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, SecQuickReplay.AGENT_PLACE
                        , SecQuickReplay.AGENT_FAQ);
                break;
            default:
                botResponseUtils.setTextResult(botMessageVO, "還有什麼需要阿發為你服務的呢？");
                break;
        }

        return botMessageVO;
    }

    @Override
    public TPIGatewayResult handleFaqReplay(Map<String, Object> request) {
        BotMessageVO vo = botResponseUtils.initBotMessage(MapUtils.getString(request, "channel"), MapUtils.getString(request, "role"));

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String faqAns = MapUtils.getString(request, "faqAns");
        botResponseUtils.setTextResult(vo, faqAns);

        return tpiGatewayResult.setData(vo);
    }

    @Override
    public TPIGatewayResult supportNLU(Map<String, Object> request) {
        /*
            總機	            個人投資	    定期定額     報價	    密碼	    股票抽籤	 真人服務	Hello	 End
            服務台	        查詢	        申購	        轉接	    修改	    補發

            個人投資	        查詢	        其他
            個人投資_查詢 	交割款	    成交回報	    委託 	銀行餘額
            定期定額	        查詢	        申購
            報價          	查詢	        其他
            密碼	            補發	        修改
            股票抽籤	        申購	        查詢
            股票抽籤_查詢	    可申購商品	申購記錄
            真人服務	        轉接	        其他

            查詢: 個人投資 定期定額 報價  股票抽籤
            申購: 定期定額 股票抽籤
            修改: 密碼
            補發: 密碼
            轉接: 真人服務
         */
        BotMessageVO vo = botResponseUtils.initBotMessage(MapUtils.getString(request, "channel"), MapUtils.getString(request, "role"));

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String operationIntent = MapUtils.getString(request, "operationIntent", "");
        String msg = MapUtils.getString(request, "msg");
        StringBuilder sb = new StringBuilder();

        switch (operationIntent) {
            case "查詢":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo,
                        SecQuickReplay.INIT_REGULAR,
                        SecQuickReplay.INIT_LOTTERY
                );
                break;
            case "申購":
                QuickReplay q1_buy = new SecQuickReplay(1, "申購定期定額", "申購定期定額");
                QuickReplay q2_buy = new SecQuickReplay(1, "股票抽籤", "股票抽籤");
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, q1_buy, q2_buy);
                break;
            case "修改":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, new SecQuickReplay(1, "修改密碼", "修改密碼"));
                break;
            case "補發":
                botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
                botResponseUtils.setQuickReplayResult(vo, new SecQuickReplay(1, "補發密碼", "補發密碼"));
                break;
            case "轉接":
                sb.append("你要問「")
                        .append(msg)
                        .append("」什麼相關內容呢？你可以描述得更完整一些，我會比較容易理解喔！");
                botResponseUtils.setTextResult(vo, sb.toString());
                break;
            default:
                sb.append("你要問「")
                        .append(msg)
                        .append("」什麼相關內容呢？你可以描述得更完整一些，我會比較容易理解喔！");
                botResponseUtils.setTextResult(vo, sb.toString());

                //botResponseUtils.setTextResult(vo, "TODO 服務台反問 : " + operationIntent);
                break;
        }

        return tpiGatewayResult.setData(vo);
    }

    @Override
    public TPIGatewayResult doSecSelectAccount(LoginRequest loginRequest) {
        BotMessageVO vo = botResponseUtils.initBotMessage(loginRequest.getChannel(), loginRequest.getRole());

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        botResponseUtils.doSecSelectAccount(vo, loginRequest.getAccountDatas());

        return tpiGatewayResult.setData(vo);
    }

}
