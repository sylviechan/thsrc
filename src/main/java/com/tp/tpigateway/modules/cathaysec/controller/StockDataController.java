package com.tp.tpigateway.modules.cathaysec.controller;

import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysec.mybatis.entity.StockData;
import com.tp.tpigateway.modules.cathaysec.mybatis.service.IStockDataService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("stockData")
public class StockDataController extends BaseController {

    @Autowired
    IStockDataService stockDataService;

    @PostMapping(value = "/getStockData")
    public TPIGatewayResult getStockData(@RequestBody Map<String, String> param) {
        String type = param.get("type");
        StockData stockData = null;
        String msg = param.get("msg");

        if (StringUtils.equals(type, "equals")) {
            return handleEqualsType(msg);
        }

        if (StringUtils.equals(type, "stockName")) {

            if (StringUtils.isBlank(msg)) {
                return TPIGatewayResult.error("msg is null");
            }

            Map<String, StockData> allStockNameData = stockDataService.getAllStockNameData();

            if (allStockNameData != null) {
                Set<String> stockNames = allStockNameData.keySet();

                //找到第一個就跳出
                for (String stockName : stockNames) {
                    if (StringUtils.isNotEmpty(stockName) && msg.contains(stockName.trim())) {
                        stockData = allStockNameData.get(stockName);
                        break;
                    }
                }
            }

            if (stockData == null) {
                return TPIGatewayResult.ok();
            }

        } else {
            String stockId = param.get("stockId");
            if (StringUtils.isBlank(stockId)) {
                return TPIGatewayResult.error("stockId is null");
            }
            Map<String, StockData> allStockIdData = stockDataService.getAllStockIdData();

            stockData = allStockIdData == null ? null : allStockIdData.get(stockId);

            if (stockData == null) {
                return TPIGatewayResult.ok();
            }
        }

        return TPIGatewayResult.ok().setData(stockData);
    }

    private TPIGatewayResult handleEqualsType(String msg) {
        if (StringUtils.isBlank(msg)) {
            return TPIGatewayResult.error("msg is null");
        }

        StockData stockData = null;

        msg = msg.trim();

        Map<String, StockData> allStockNameData = stockDataService.getAllStockNameData();
        if (allStockNameData != null) {
            stockData = allStockNameData.get(msg);

            if (stockData == null) {
                Map<String, StockData> allStockIdData = stockDataService.getAllStockIdData();
                if (allStockIdData != null) {
                    stockData = allStockIdData.get(msg);
                }
            }

            if (stockData == null) {
                return TPIGatewayResult.error("查無資料");
            }
        }

        return TPIGatewayResult.ok().setData(stockData);
    }

}
