package com.tp.tpigateway.modules.cathaysec.feginError;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "otherapierror", url = "${api.error.path}/")
@TpSysLog(remark = "OtherAPIFeignErrorService")
public interface OtherAPIFeignErrorService {

	@RequestMapping(value = "LogMonitor/AFALogInfo.svc/AFAErrLog", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> sendeErrorLogRB29(Map<String, Object> dataUtilsAbnormalOrder);

}
