package com.tp.tpigateway.modules.cathaysec.service.impl;

import com.alibaba.fastjson.JSON;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.cathaysec.PasswordRequest;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysec.fegin.PasswordAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.model.SecQuickReplay;
import com.tp.tpigateway.modules.cathaysec.service.PasswordRelatedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("passwordRelatedService")
public class PasswordRelatedServiceImpl implements PasswordRelatedService {

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	PasswordAPIFeignService passwordAPIFeignService;

	@Override
	public BotMessageVO passwordRelated(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		botResponseUtils.setTextResult(botMessageVO, "請問你想要做關於密碼的什麼業務呢？");

		/*
		 * 修改密碼 密碼解鎖 密碼補發
		 */
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.UPD_PWD,
				 SecQuickReplay.RESEND_PWD);

		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdChangeInit(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		List<Map<String, Object>> dataList = new ArrayList<>();

		botResponseUtils.setPasswordRelated(botMessageVO, dataList, "PasswordProcessKey_Change", reqVO.getOldPassword());

		return botMessageVO;
	}

	private BotMessageVO.ContentItem getTextContentItem(BotMessageVO vo, String text) {
		BotMessageVO.ContentItem contentH = vo.new ContentItem();
		contentH.setType(1);
		contentH.setText(text);
		return contentH;
	}

	@Override
	public BotMessageVO pswdChangeSuccess(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		botResponseUtils.setTextResult(botMessageVO, "修改密碼成功！之後記得用新的密碼登入交易平台，還需要什麼服務嗎？");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.INIT,
				SecQuickReplay.END_CHAT);

		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdChangeFail(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "挖！修改密碼系統有點問題，請你稍後再試");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, SecQuickReplay.INIT,
				SecQuickReplay.END_CHAT);

		return botMessageVO;
	}

	@Override
	public BotMessageVO passwordRelatedLock(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "哭哭...我們暫時將你帳戶上鎖，以保護你的安全...");
		botResponseUtils.setTextResult(botMessageVO, "如果離開我了～下次回來要記得先補發唷！");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.RESEND_PWD_NOW, SecQuickReplay.END_CHAT_SUPPLY_AGAIN);

		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdOnLockInit(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		List<Map<String, Object>> dataList = new ArrayList<>();

		botResponseUtils.setPasswordRelated(botMessageVO, dataList, "PasswordProcessKey_OnLock");

		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdSenderPhoneF(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		List<Map<String, Object>> dataList = new ArrayList<>();

		botResponseUtils.setPasswordRelated(botMessageVO, dataList, "PasswordProcessKey_OnLock_Phone");

		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdOnLockSuccess(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "歐耶！成功解鎖");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, SecQuickReplay.INIT,
				SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdOnLockFail(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		List<Map<String, Object>> dataList = new ArrayList<>();
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "挖！密碼解鎖有點問題，請你稍後再試");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, SecQuickReplay.INIT,
				SecQuickReplay.END_CHAT);

		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdOnLockCanNot(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "好像沒辦法幫你解鎖，你可以聯繫我們客服哦～");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.NEED_CS,
				SecQuickReplay.INIT, SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdOnLockFullAmount(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "嗚嗚...今天不能幫你解鎖，明天再試試或補發密碼好嗎？");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.TR_RESEND_PWD,
				SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdReissueInit(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		List<Map<String, Object>> dataList = new ArrayList<>();

		botResponseUtils.setPasswordRelated(botMessageVO, dataList, "PasswordProcessKey_Reissue");

		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdReissueSuccess(PasswordRequest reqVO, String epass) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> paramMap = new HashMap<String, Object>(){{put("oldPassword", epass);}};

		botResponseUtils.setTextResult(botMessageVO, "歐耶！補發成功～要試試看登入嗎[" + epass + "]");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.UPD_PWD_NOW(paramMap, epass),
				SecQuickReplay.INIT, SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdReissueFail(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "挖！密碼補發的系統有點問題，請你稍後再試");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, SecQuickReplay.INIT,
				SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

    @Override
    public BotMessageVO pswdReissueCanNot(PasswordRequest reqVO, String resultCode) {
        String missInfo = "";
        switch (resultCode) {
            case "9916":
                missInfo = "你的帳戶是委任帳戶. 要請你臨櫃辦理密碼補發了. ";
                break;
            case "9917":
                missInfo = "我們找不到你的手機... 可能要請你臨櫃辦理補發密碼了. ";
                break;
            case "9918":
                missInfo = "我們找不到你的E-Mail... 可能要請你臨櫃辦理補發密碼了. ";
                break;
            case "9920":
                missInfo = "我們沒辦法驗證生日... 可能要請你臨櫃辦理補發密碼了. ";
                break;
        }
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        botResponseUtils.setImageResult(botMessageVO, botMessageVO.APOLOGIZE_PIC);
        String textResult = "挖，" + missInfo + "記得要帶證件及原開戶印鑑唷！";
        botResponseUtils.setTextResult(botMessageVO, textResult);
        //9916 多"什麼是委任帳戶"
        if(resultCode.equals("9916")){
            botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.AGENT_FAQ,
                    SecQuickReplay.SEARCH_BUSINESS_BASE);
        }else{
            botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.SEARCH_BUSINESS_BASE);
        }
        return botMessageVO;
    }

	@Override
	public BotMessageVO pswdReissueFullAmount(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "嗚嗚...今天不能幫你補發密碼了，明天再試試好嗎？");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.INIT,
				SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdReissueRt(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.APOLOGIZE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "挖，好像怎樣都驗不過，可以請你重新來一次嗎Q_Q？");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_01, SecQuickReplay.INIT,
				SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO pswdStop(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setImageResult(botMessageVO,botMessageVO.BYE_PIC);
		botResponseUtils.setTextResult(botMessageVO, "下次服務可以再來找阿發哦！");
		// botResponseUtils.setQuickReplayResult(botMessageVO,
		// "我想"+SecQuickReplay.RESEND_PWD, SecQuickReplay.END_CHAT);
		return botMessageVO;
	}

	@Override
	public Map<String, Object> getRB04(PasswordRequest reqVO, Map<String, String> param) {
		Map<String, Object> result = passwordAPIFeignService.getRB04(param);
		if (result == null) {
			result.put("ResultCode", "9999");
			result.put("ResultMessage", JSON.toJSONString(result));
		}

		return result;
	}

	@Override
	public Map<String, Object> getRB05(PasswordRequest reqVO, Map<String, String> param) {
		Map<String, Object> result = passwordAPIFeignService.getRB05(param);
		if (result == null) {
			result.put("ResultCode", "9999");
			result.put("ResultMessage", JSON.toJSONString(result));
		}

		return result;
	}

	@Override
	public Map<String, Object> getRB06(PasswordRequest reqVO, Map<String, String> param) {
		Map<String, Object> result = passwordAPIFeignService.getRB06(param);
		if (result == null) {
			result.put("ResultCode", "9999");
			result.put("ResultMessage", JSON.toJSONString(result));
		}

		return result;
	}

	@Override
	public Map<String, Object> getRB07(PasswordRequest reqVO, Map<String, String> param) {
		Map<String, Object> result = passwordAPIFeignService.getRB07(param);
		if (result == null) {
			result.put("ResultCode", "9999");
			result.put("ResultMessage", JSON.toJSONString(result));
		}

		return result;
	}

	@Override
	public Map<String, Object> getRB08(PasswordRequest reqVO, Map<String, String> param) {
		Map<String, Object> result = passwordAPIFeignService.getRB08(param);
		if (result == null) {
			result.put("ResultCode", "9999");
			result.put("ResultMessage", JSON.toJSONString(result));
		}

		return result;
	}

	@Override
	public Map<String, Object> getRB09(PasswordRequest reqVO, Map<String, String> param) {
		Map<String, Object> result = passwordAPIFeignService.getRB09(param);
		if (result == null) {
			result.put("ResultCode", "9999");
			result.put("ResultMessage", JSON.toJSONString(result));
		}

		return result;
	}

	/**
	 * 語音密碼
	 *
	 * @param reqVO
	 * @return
	 */
	@Override
	public BotMessageVO updSpeechPwd(PasswordRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		botResponseUtils.setTextResult(botMessageVO, "修改語音密碼FAQ");

		return botMessageVO;
	}
}
