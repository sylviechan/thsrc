package com.tp.tpigateway.modules.cathaysec.model;

import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import org.apache.commons.collections.MapUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SecQuickReplay extends QuickReplay {

    //common
    public static final QuickReplay INIT = new SecQuickReplay(1, "看阿發還能幫什麼忙", "看阿發還能幫什麼忙");
    public static final QuickReplay END_CHAT = new SecQuickReplay(1, "不用了, 結束對話", "不用了, 結束對話");
    public static final QuickReplay E_APP_DOWNLOAD = new SecQuickReplay(1, "下載E櫃台APP", "下載E櫃台APP");

    //報價
    public static final QuickReplay PRICE_ASK_LOGIN = new SecQuickReplay(1, "登入看報價", "登入看報價", "報價", "toLogin", MapUtils.EMPTY_MAP);

    //密碼流程
    public static final QuickReplay UPD_PWD = new SecQuickReplay(1, "修改密碼", "修改密碼", "", "", "");

    public static final QuickReplay UPD_PWD_NOW(Map<String, Object> paramMap, String epass) {
        return new SecQuickReplay(1, "立即修改密碼", "修改密碼("+epass+")", "密碼", "修改密碼", paramMap);
    }

    public static final QuickReplay UNLOCK_PWD = new SecQuickReplay(1, "密碼解鎖", "密碼解鎖", "", "", "");
    public static final QuickReplay RESEND_PWD = new SecQuickReplay(1, "密碼補發", "密碼補發", "", "", "");
    public static final QuickReplay RESEND_PWD_NOW = new SecQuickReplay(1, "立即補發密碼", "密碼補發", "", "", "");
    public static final QuickReplay DO_LOGIN = new SecQuickReplay(1, "登入看看", "登入看看", "", "", "");
    public static final QuickReplay TR_RESEND_PWD = new SecQuickReplay(1, "我想補發密碼", "我想補發密碼", "", "", "");
    public static final QuickReplay NEED_CS = new SecQuickReplay(1, "聯繫客服", "聯繫客服", "", "", "");
    public static final QuickReplay SEARCH_BUSINESS_BASE = new SecQuickReplay(4, "查詢營業據點", "查詢營業據點", "", "https://www.cathaysec.com.tw/mobile/Branches.aspx", "");
    public static final QuickReplay END_CHAT_SUPPLY_AGAIN = new SecQuickReplay(1, "不用了, 結束對話", "不用了, 結束對話");

    //定期定額
    public static final QuickReplay RB_MONTH_6 = new SecQuickReplay(1, "6日", "6日", "", "", "");
    public static final QuickReplay RB_MONTH_16 = new SecQuickReplay(1, "16日", "16日", "", "", "");
    public static final QuickReplay RB_MONTH_26 = new SecQuickReplay(1, "26日", "26日", "", "", "");    
    public static final QuickReplay RB03_FLAG_N_N = new SecQuickReplay(1, "我還要再考慮一下", "我還要再考慮一下", "定期定額", "熱門定期定額", Collections.emptyMap());
    public static final QuickReplay rb02_check_1 = new SecQuickReplay(1, "查看我的定期定額", "查看我的定期定額", "定期定額", "查詢", Collections.emptyMap());
    public static final QuickReplay selectStockType = new SecQuickReplay(1, "看全部定期定額商品", "看其他可申購定期定額", "定期定額", "selectStockType", Collections.emptyMap());
    public static final QuickReplay RSP_FAQ = new SecQuickReplay(2, "定期定額是什麼?", "定期定額是什麼?", "", "", ""); //FAQ
    public static final QuickReplay HOT_RSP = new SecQuickReplay(2, "猜你喜歡的標的", "熱門定期定額", "定期定額", "熱門定期定額", Collections.emptyMap());
    public static final QuickReplay RB17_1 = new SecQuickReplay(1, "股票", "股票", "定期定額", "選擇股票類型", getParameter("entity", "股票"));
    public static final QuickReplay RB17_2 = new SecQuickReplay(1, "ETF", "ETF", "定期定額", "選擇股票類型", getParameter("entity", "ETF"));
    public static final QuickReplay RB18_ELSE2 = new SecQuickReplay(1, "看看其他標的", "看其他可申購定期定額", "定期定額", "selectStockType", Collections.emptyMap());
    public static final QuickReplay AGENT_FAQ = new SecQuickReplay(4, "什麼是委任帳戶", "什麼是委任帳戶", "", "https://www.cathaysec.com.tw/mobile/Branches.aspx", "");
    public static final QuickReplay AGENT_PLACE = new SecQuickReplay(4, "查詢營業據點", "查詢營業據點", "", "https://www.cathaysec.com.tw/mobile/Branches.aspx", "");
    public static final QuickReplay RB_NOTE = new SecQuickReplay(1, "定期定額扣款注意事項", "定期定額扣款注意事項"); //FAQ
    public static final QuickReplay RB_RETURN = new SecQuickReplay(18, "回E櫃台看我的定期定額", "回E櫃台看我的定期定額");

    private static Map getParameter(String key, String value) {
        Map<String, Object> parameter = new HashMap();
        parameter.put(key, value);
        return parameter;
    }

//    public static final QuickReplay RB03_FLAG_N = new SecQuickReplay(17, "帶我去簽署合約", "帶我去簽署合約", "", "", "");    
    public static QuickReplay RB03_FLAG_N(String stockId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("stockId", stockId);
        return new SecQuickReplay(2, "帶我去簽署合約", "帶我去簽署合約(" + stockId + ")","定期定額", "toSIP", parameter);
    }
    
    public static QuickReplay MODIFY_RSP(String stockId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("stockId", stockId);
        return new SecQuickReplay(2, "我要修改", "我要修改(" + stockId + ")","定期定額", "toModify", parameter);
    }

    public static QuickReplay RB_BEGIN_BUY(String text, String stockId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("stockId", stockId);
        return new SecQuickReplay(2, text, text + "(" + stockId + ")", "定期定額", "beginBuy", parameter);
    }

    public static QuickReplay RB_BUY(String stockId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("stockId", stockId);
        return new SecQuickReplay(2, "確定申購", "確定申購(" + stockId + ")", "定期定額", "tobuy", parameter);
    }
    
    public static QuickReplay RB_HISTORY(String stockId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("stockId", stockId);
        return new SecQuickReplay(2, "歷史報酬試算", "歷史報酬試算(" + stockId + ")", "定期定額", "toHistory", parameter);
    }

    //新手上路
    public static final QuickReplay RB20_1 = new SecQuickReplay(1, "登入看新手上路", "登入看新手上路", "新手上路", "請求登入", MapUtils.EMPTY_MAP);
//    public static final QuickReplay RB20_2 = new SecQuickReplay(1, "登入看一般人新手上路", "登入看一般人新手上路", "", "", "");
//    public static final QuickReplay RB20_3 = new SecQuickReplay(1, "登入看老手新手上路", "登入看老手新手上路", "", "", "");

    //股票
    public static final QuickReplay LotteryRecord = new SecQuickReplay(1, "看我的抽籤紀錄", "看我的抽籤紀錄", "股票抽籤", "searchMyRecord", MapUtils.EMPTY_MAP);
    public static final QuickReplay PurchaseRules = new SecQuickReplay(1, "申購規則說明", "申購規則說明", "", "", "");  //FAQ
    public static final QuickReplay ShowAllTargets = new SecQuickReplay(2, "顯示所有標的", "顯示所有標的", "股票抽籤", "searchCanBuyStock", MapUtils.EMPTY_MAP);
    public static final QuickReplay ShowAllCanTargets = new SecQuickReplay(2, "顯示所有可抽籤標的", "顯示所有可抽籤標的", "股票抽籤", "searchCanBuyStock", MapUtils.EMPTY_MAP);
    public static final QuickReplay MyComingSoon = new SecQuickReplay(1, "抽籤行事曆", "抽籤行事曆", "股票抽籤", "myComming", MapUtils.EMPTY_MAP);
    public static final QuickReplay LetMeThinkAgain = new SecQuickReplay(1, "否，我再想想", "否，我再想想", "股票抽籤", "noBuy", MapUtils.EMPTY_MAP);
    public static final QuickReplay IWantToBuy = new SecQuickReplay(1, "是，我要抽籤", "是，我要抽籤", "股票抽籤", "toBuy", MapUtils.EMPTY_MAP);
    public static final QuickReplay ShowAllTheLotsOfTheLottery = new SecQuickReplay(1, "顯示所有抽籤標的", "顯示所有抽籤標的", "股票抽籤", "searchCanBuyStock", MapUtils.EMPTY_MAP);
    public static final QuickReplay BankAccountBalance = new SecQuickReplay(1, "查詢交割銀行餘額", "查詢交割銀行餘額", "個人投資", "查詢個人投資", getParameter("searchType", "銀行餘額"));
    public static final QuickReplay MyAccountBalance = new SecQuickReplay(1, "看看我的帳上餘額", "看看我的帳上餘額", "個人投資", "查詢個人投資", getParameter("searchType", "銀行餘額"));
    public static final QuickReplay StillNeedLottery = new SecQuickReplay(2, "我還想抽籤", "股票抽籤", "股票抽籤", "申購", MapUtils.EMPTY_MAP);
    public static final QuickReplay CHECK_STOCK_APP(String symbol) {
        Map<String, Object> parameter = new HashMap();
        parameter.put("symbol", symbol);
        return new SiteQuickReplay(16, "查詢", "查詢(" + symbol + ")", "", "", "", "", "", parameter);
    }
    
    //交割款
    public static final QuickReplay MoneyTransfer = new SecQuickReplay(1, "我要轉帳", "我要轉帳", "個人投資", "我要轉帳", MapUtils.EMPTY_MAP);
    public static final QuickReplay GoToCathayUrlAPP = new SecQuickReplay(4, "是", "是", "", "https://webap.cathaysec.com.tw/General/Afa_cathaybk.html","");
    public static final QuickReplay NotGoToCathayUrlAPP = new SecQuickReplay(2, "否", "看阿發還能幫什麼忙");
    
    
    //招呼語
    public static final QuickReplay INIT_REGULAR = new SecQuickReplay(2, "定期定額", "定期定額", "定期定額", "initCard", MapUtils.EMPTY_MAP);
    public static final QuickReplay INIT_LOTTERY = new SecQuickReplay(2, "股票抽籤", "股票抽籤", "股票抽籤", "申購", MapUtils.EMPTY_MAP);
    public static final QuickReplay INIT_PWD = new SecQuickReplay(2, "密碼相關", "密碼相關", "密碼", "", MapUtils.EMPTY_MAP);
    public static final QuickReplay INIT_TAIEXGOT = new SecQuickReplay(2, "大盤", "大盤", "報價", "報價類型", getPriceTypeParameter("大盤"));
    public static final QuickReplay INIT_HOTSTOCK = new SecQuickReplay(2, "熱門股", "熱門股", "報價", "報價類型", getPriceTypeParameter("熱門股"));
    public static final QuickReplay INIT_STOCK_OPTIONAL = new SecQuickReplay(2, "庫存/自選", "庫存/自選", "報價", "選擇類型", MapUtils.EMPTY_MAP);
    public static final QuickReplay INIT_STOCK_INVENTORY = new SecQuickReplay(2, "庫存", "庫存", "報價", "報價類型", getPriceTypeParameter("庫存"));
    public static final QuickReplay INIT_STOCK_OPTIONAL_SELF = new SecQuickReplay(2, "自選", "自選", "報價", "報價類型", getPriceTypeParameter("自選"));
    public static final QuickReplay INIT_RETURN_TRANSACTION = new SecQuickReplay(2, "成交回報", "成交回報", "個人投資", "查詢個人投資", getSearchTypeParameter("成交"));
    public static final QuickReplay INIT_ENTRUSTED_INQUIRY = new SecQuickReplay(2, "委託查詢", "委託查詢", "個人投資", "查詢個人投資", getSearchTypeParameter("委託"));
    public static final QuickReplay INIT_DELIVERY = new SecQuickReplay(2, "交割款", "交割款", "個人投資", "查詢個人投資", getSearchTypeParameter("交割款"));

    private static Map getPriceTypeParameter(String priceType) {
        Map<String, Object> parameter = new HashMap();
        parameter.put("priceType", priceType);
        return parameter;
    }

    private static Map getSearchTypeParameter(String searchType) {
        Map<String, Object> parameter = new HashMap();
        parameter.put("searchType", searchType);
        return parameter;
    }

    public static QuickReplay RB_BUY_SIP(String stockId, String stockName) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("stockId", stockId);

        return new SecQuickReplay(1, "我要申購" + stockName +"定期定額", "我要申購" + stockName +"定期定額", "定期定額", "beginBuy", parameter);
    }

    public SecQuickReplay(int action, String text, String alt, String reply, String url, String customerAction) {
        super(action, text, alt, reply, url, customerAction);
    }

    public SecQuickReplay(int action, String text, String alt, String reply, String url, String customerAction, String intent, String secondBorIntent, Map<String, Object> parameter) {
        super(action, text, alt, reply, url, customerAction, intent, secondBorIntent, parameter);
    }

    public SecQuickReplay(int action, String text, String alt, String intent, String secondBotIntent, Map<String, Object> parameter) {
        super(action, text, alt, intent, secondBotIntent, parameter);
    }

    public SecQuickReplay(int action, String text, String alt) {
        super(action, text, alt);
    }
}