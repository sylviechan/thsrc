package com.tp.tpigateway.modules.cathaysec.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.cathaysec.SecuritiesRequest;
import com.tp.common.util.DataUtils;
import com.tp.common.util.DateUtils;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.SecTestApiData;
import com.tp.tpigateway.modules.cathaysec.fegin.OtherAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.fegin.RegularAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.fegin.SecuritiesAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.model.SecQuickReplay;
import com.tp.tpigateway.modules.cathaysec.mybatis.entity.StockData;
import com.tp.tpigateway.modules.cathaysec.mybatis.service.IStockDataService;
import com.tp.tpigateway.modules.cathaysec.service.SecuritiesService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;

@Service("securitiesService")
public class SecuritiesServiceImpl implements SecuritiesService{
	private static Logger logger = LoggerFactory.getLogger(SecuritiesServiceImpl.class);
	private static final String[] DETAIL_TYPE = {"0B:現買", "0S:現賣", "3B:資買", "3S:資賣","4B:券買","4S:券賣","10S:現沖賣"};
	private static final String[] RB53_DETAIL_TEST= {"證券帳號/銀行帳戶:,$BranchDesc, ,$Account,<br>,@Bankname, ,@Bankno"
													,"餘額:@Sign,@Balance"};
	
	private static final String[] RB50_THOUSANDS= {"availQty","sumDealQty","notDealQty","avgDealPrice","Balance","expr"};
	private static final String[] RB53_THOUSANDS= {"Balance","expr"};
	private static final String[] RB21_LIST = { "card38-01new.png:查看上市熱門股:上市","card38new.png:查看上櫃熱門股:上櫃"};
	private static final String[] RB21_DETAIL = {"SymbolName:股票名稱", "Symbol:股票代號"};

	@Autowired
    BotResponseUtils botResponseUtils;
	
	@Autowired
	SecuritiesAPIFeignService securitiesAPIFeignService;
	
	@Autowired
	RegularAPIFeignService regularAPIFeignService;

	@Autowired
	OtherAPIFeignService otherAPIFeignService;

	@Autowired
	IStockDataService stockDataService;

	@Override
	public BotMessageVO askLogin(SecuritiesRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		botResponseUtils.setTextResult(vo, "依照規定，即時資訊只能提供給證券客戶，請問你要登入看個股報價嗎");

		botResponseUtils.setQuickReplayResult(vo, SecQuickReplay.PRICE_ASK_LOGIN, SecQuickReplay.END_CHAT);

		return vo;
	}

	@Override
	public BotMessageVO rb21QueryCard(SecuritiesRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());					 			

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		// 牌卡圖片
		for(String param:RB21_LIST) {
			String[] params = param.split(":");
			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_250);
			cards.setCLinkType(2);
			
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CB03);
			cImageData.setCImageTextType(1);
			cImageData.setCImageUrl(params[0]);
			cards.setCImageData(cImageData.getCImageDatas());
			
			List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();

			Map<String, Object> parameter = new HashMap<>();
            parameter.put("hotType", params[2]);
            parameter.put("priceType", "熱門股");

			cLinkContents.add(botResponseUtils.getCLinkListItem(vo,
					new SecQuickReplay(2, "查看", params[1], "報價", "報價類型", parameter)).getCLinkList());

			cards.setCLinkList(cLinkContents);
			
			cardList.add(cards.getCards());
		}
		
		botResponseUtils.setTextResult(vo, "以下是目前的熱門股排行：");
		botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);

		return vo;
	}

	@Override
	public BotMessageVO choose(SecuritiesRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		botResponseUtils.setTextResult(vo, "請問你要查庫存還是自選？"); 

		botResponseUtils.setQuickReplayResult(vo, SecQuickReplay.INIT_STOCK_INVENTORY, SecQuickReplay.INIT_STOCK_OPTIONAL_SELF);

		return  vo;
	}

	//熱門股
	@Override
    public BotMessageVO getRB21(SecuritiesRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = otherAPIFeignService.getRB21(JSON.toJSONString(param));
        
 		if(result != null) {
 			String resultCode = getString(result.get("ResultCode"));
 			
 			if(resultCode.equals("0000")) {	
 				String title ="";
 				List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
 				
				List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get(reqVO.getType());
				
				//title
				switch (reqVO.getType()) {
					case "TAIEX":
						title = "上市熱門股";
					break;
					case "TAISDAQ":
						title = "上櫃熱門股";
					break;
				}
				
				String symbolStr = "";
				for(Map<String, Object> dataMap : dataList) {
					symbolStr += "," + dataMap.get("Symbol");
				}
				
				symbolStr = symbolStr.substring(1);
				
				List<Map<String, Object>> listRB52= getRB52API(reqVO.getChannel(), symbolStr);
				
				//data
				for (Map<String, Object> dataMap : dataList) {
					Map<String, Object> map = rb21mergeRB52list(listRB52,dataMap);
					if (map != null) {
						list.add(map);
					}
				}
				
 				botResponseUtils.setPopularShare(vo, list, title);
 			} else {
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤");
 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
 			} 			 						
		}else {
			String replyText = "RB21 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
        return vo;
    }
	
	public List<Map<String, Object>> getRB52API(String channel,String symbol){
		Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB52");
			requireParams.put("Symbol", symbol);
			requireParams.put("Channel", channel);
			
			Map<String, Object> result2 = securitiesAPIFeignService.getRB52(requireParams);
			if(result2 != null) {
				List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result2.get("quotes");
				if(dataList.size()>0) {
					return dataList;
				} 
			}
		return null;
	}
	
	public Map<String, Object> rb21mergeRB52list(List<Map<String, Object>> listRB52, Map<String, Object> dataMap){
		Map<String, Object> map = new HashMap<String, Object>();
		for (Map<String, Object> rb52map : listRB52) {

			String resultCode = getString(rb52map.get("resultCode"));

			if (StringUtils.equals(resultCode, "0")) {
				if (StringUtils.equals(dataMap.get("Symbol").toString(), rb52map.get("symbol").toString())) {
					map.put("symbol", dataMap.get("Symbol"));
					map.put("symbolName", dataMap.get("SymbolName"));
					map.put("tradingPrice", rb52map.get("tradingPrice"));
					map.put("dealflag", rb52map.get("dealflag"));
				}
			} else {
				logger.info("getRB52API-symbol:" + getString(rb52map.get("symbol")) + ", resultMessage:"
						+ getString(rb52map.get("resultMessage")));
				return null;
			}
		}
		return map;
	}
	
	@Override
	public BotMessageVO getRB25(SecuritiesRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String rpjson = regularAPIFeignService.getRB25(JSON.toJSONString(param));

		if (StringUtils.isNotBlank(rpjson)) {
			Map<String, Object> result = returnToMap(rpjson);
			String resultCode = getString(result.get("ResultCode"));

			if (resultCode.equals("0000")) {
				List<Map> stockList = (List<Map>) result.get("Stock");
				if (stockList.size() > 0) {
					Map<String, String> stockMap = new HashMap<>();
					String symbolStr = "";
					for (Map stock : stockList) {
						String symbol = (String) stock.get("Symbol");
						String symbolName = (String) stock.get("SymbolName");
						stockMap.put(symbol, symbolName);
						symbolStr += "," + symbol;
					}
					symbolStr = symbolStr.substring(1);

					doRB52Window(vo, param.get("Channel"), symbolStr, stockMap, "庫存報價");
				} else {
					botResponseUtils.setTextResult(vo, "查無資料");
				}
			} else {
				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤...");
				botResponseUtils.setTextResult(vo, getString(result.get("ResultMessage")));
			}

		} else {
			String replyText = "RB25 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		return vo;
	}

	@Override
	public BotMessageVO getRB26(SecuritiesRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String rpjson = regularAPIFeignService.getRB26(JSON.toJSONString(param));
		logger.info("rpjson=" + rpjson);

		if (StringUtils.isNotBlank(rpjson)) {
			Map<String, Object> result = returnToMap(rpjson);
			String resultCode = getString(result.get("ResultCode"));

			if (resultCode.equals("0000")) {
				// 組牌卡
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				List<Map> groupset = (List<Map>) result.get("Groupset");
				for (Map<String, Object> group : groupset) {
					String groupName = (String) group.get("GroupName");
					String groupseq = (String) group.get("Groupseq");

					CardItem cards = vo.new CardItem();
					cards.setCLinkType(1);
					cards.setCWidth(BotMessageVO.CWIDTH_250);

					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImageUrl("card39-01.jpg");
					cImageData.setCImageTag(groupName);
					//cImageData.setCImage(BotMessageVO.IMG_GENERIC);
					cImageData.setCImageTextType(2);
					

//					List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
//					CImageTextItem cImageText = vo.new CImageTextItem();
//					
//					cImageText.setCImageTextTitle("　");
//					cImageTexts.add(cImageText.getCImageTexts());
//					cImageText = vo.new CImageTextItem();
//					cImageText.setCImageTextTitle("　");
//					cImageTexts.add(cImageText.getCImageTexts());
//					cImageText = vo.new CImageTextItem();
//					cImageText.setCImageTextTitle("　");
//					cImageTexts.add(cImageText.getCImageTexts());
//
//					cImageText = vo.new CImageTextItem();
//					cImageText.setCImageTextTitle(groupName);//行動自選title
//					cImageTexts.add(cImageText.getCImageTexts());
//
//					cImageData.setCImageTexts(cImageTexts);
					cards.setCImageData(cImageData.getCImageDatas());

					String alt = "自選報價(" + groupseq + ")";
					String intent = "報價";
					String secondBotIntent = "showRB26Detail";
					Map<String, Object> parameter = new HashMap<>();
					parameter.put("groupseq", groupseq);

					QuickReplay q1 = new SecQuickReplay(2, "看全部", alt, "", "", "", intent, secondBotIntent, parameter);
					cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1));

					List<Map<String,Object>> cTexts = new ArrayList<>();

					List<Map> stockList = (List<Map>) group.get("_Stock");
					if (stockList.size() > 0) {
						Map<String, String> stockMap = new HashMap<>();
						String symbolStr = "";
						for (Map stock : stockList) {
							String symbol = (String) stock.get("Symbol");
							String symbolName = (String) stock.get("SymbolName");
							stockMap.put(symbol, symbolName);
							symbolStr += "," + symbol;
						}
						symbolStr = symbolStr.substring(1);
						cTexts = cTextsListForRB26(param.get("Channel"), symbolStr, stockMap);
					}
					cards.setCTextType("13");
					cards.setCTexts(cTexts);

					cardList.add(cards.getCards());
				}
				botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
			} else {
				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤...");
				botResponseUtils.setTextResult(vo, getString(result.get("ResultMessage")));
			}
		} else {
			String replyText = "RB26 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		return vo;
	}

	@Override
	public BotMessageVO showRB26Detail(SecuritiesRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String rpjson = regularAPIFeignService.getRB26(JSON.toJSONString(param));
		logger.info("rpjson=" + rpjson);

		if (StringUtils.isNotBlank(rpjson)) {
			Map<String, Object> result = returnToMap(rpjson);
			String resultCode = getString(result.get("ResultCode"));

			if (resultCode.equals("0000")) {
				// 組牌卡
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				List<Map> groupset = (List<Map>) result.get("Groupset");
				for (Map<String, Object> group : groupset) {
					String groupseq = (String) group.get("Groupseq");
					if (StringUtils.equals(groupseq, reqVO.getGroupseq())) {
						List<Map> stockList = (List<Map>) group.get("_Stock");
						if (stockList.size() > 0) {
							Map<String, String> stockMap = new HashMap<>();
							String symbolStr = "";
							for (Map stock : stockList) {
								String symbol = (String) stock.get("Symbol");
								String symbolName = (String) stock.get("SymbolName");
								stockMap.put(symbol, symbolName);
								symbolStr += "," + symbol;
							}
							symbolStr = symbolStr.substring(1);

							doRB52Window(vo, param.get("Channel"), symbolStr, stockMap, "自選報價" + groupseq);
						} else {
							botResponseUtils.setTextResult(vo, "查無資料");
						}
						break;
					}
				}
				if(CollectionUtils.isNotEmpty(cardList))
					botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
			} else {
				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤...");
				botResponseUtils.setTextResult(vo, getString(result.get("ResultMessage")));
			}
		} else {
			String replyText = "RB26 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

		return vo;
	}
	
	/**
	 * 報價浮出視窗
	 * 
	 * @param vo
	 * @param channel
	 * @param symbolStr 股票號碼 逗號隔開
	 * @param stockMap  自定義股票名稱
	 * @param title     視窗標題
	 */
	protected void doRB52Window(BotMessageVO vo, String channel, String symbolStr, Map<String, String> stockMap,
			String title) {
		Map<String, StockData> allStockIdData = null;
		Map<String, String> param2 = new HashMap<>();
		param2.put("FunctionID", "RB52");
		param2.put("Channel", channel);
		param2.put("Symbol", symbolStr);
		Map<String, Object> result = securitiesAPIFeignService.getRB52(param2);
		logger.info("result=" + result);

		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> quotes = (List<Map<String, Object>>) result.get("quotes");
		if (quotes.size() > 0) {
			for (Map<String, Object> quote : quotes) {
				String resultCode = getString(quote.get("resultCode"));
				if (StringUtils.equals(resultCode, "0")) {

					String symbol = getString(quote.get("symbol"));
					String dealflag = getString(quote.get("dealflag"));
					String dealpoint = getString(quote.get("dealpoint"));
					String deallimit = getString(quote.get("deallimit"));
					String tradingPrice = getString(quote.get("tradingPrice"));
					Map<String, Object> data = new HashMap<String, Object>();
					dataList.add(data);
					data.put("symbol", symbol);

					String symbolNm = stockMap.get(symbol);
					if (StringUtils.isBlank(symbolNm)) {
						// 取得股票名稱 DB
						if (allStockIdData == null)
							allStockIdData = stockDataService.getAllStockIdData();
						StockData stockData = allStockIdData.get(symbol);
						symbolNm = stockData.getStockName();
					}
					data.put("symbolNm", symbolNm);

					switch (dealflag) {
					case "0":
						data.put("quoteChange", dealpoint);
						data.put("quoteChangeColor", "black");
						break;
					case "1":
					case "3":
						data.put("quoteChange", "▲ " + dealpoint);
						data.put("quoteChangeColor", "red");
						break;
					case "2":
					case "4":
						data.put("quoteChange", "▼ " + dealpoint);
						data.put("quoteChangeColor", "green");
					}
					data.put("quoteChangePercent", deallimit);
					data.put("price", tradingPrice);
				} else {
					logger.info("doRB52Window-symbol:" + getString(quote.get("symbol")) + ", resultMessage:"
							+ getString(quote.get("resultMessage")));
				}
			}
			botResponseUtils.setRegular52Query(vo, dataList, title);
		} else {
			String replyText = "查無資料";
			botResponseUtils.setTextResult(vo, replyText);
		}
	}
	
	@Override
    public  BotMessageVO getRB50(SecuritiesRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = securitiesAPIFeignService.getRB50(param);
        
//        if((List<Map<String, Object>>) result.get("orderSumarrys")==null || ((List<Map<String, Object>>) result.get("orderSumarrys")).size()==0) {
//        	result = JSON.parseObject(secTestApiData.getRB50ResponseString(),Map.class);
//        }
       
        
 		if(result != null) { 	
 			result.put("resultCode", "0");
 			String resultCode = getString(result.get("resultCode"));
 			
 			List<Map<String, Object>> dataList = (List<Map<String, Object>>) result.get("orderSumarrys");
 			
 			if(resultCode.equals("0")) {
 				if(dataList.size()>0) {
 					for(Map<String, Object>dataMap:dataList) {
 						if(dataMap.get("orderType")!=null) {
 							for(String detail:DETAIL_TYPE) {
 								String[] details = detail.split(":");
 								if(StringUtils.equals(details[0], dataMap.get("orderType").toString())) {
 									dataMap.put("orderType",details[1]);
 								}
 							}
 						}
 						
 						for(String key:RB50_THOUSANDS) {
 							if(dataMap.containsKey(key)) {
 								dataMap.put(key, DataUtils.handleMoney(dataMap.get(key).toString()));	
 							}
 						}
 					}
 					botResponseUtils.setEntrustRet(vo, dataList, "委託查詢");
 				} else {
 					botResponseUtils.setTextResult(vo, "你的委託目前還沒有成交唷！");
 					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
 				}	
 			} else {
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤");
 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
 			} 			 						
		}else {
			String replyText = "RB50 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
        return vo;
    }
	
	@Override
    public BotMessageVO getRB51(SecuritiesRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());									 		
        Map<String, Object> result = securitiesAPIFeignService.getRB51(param);
        
//        if((List<Map<String, Object>>) result.get("execuSummarys")==null || ((List<Map<String, Object>>) result.get("execuSummarys")).size()==0) {
//        	 result = JSON.parseObject(secTestApiData.getRB51ResponseString(),Map.class);
//        }
       
        
 		if(result != null) {
 			result.put("resultCode", "0");
 			String resultCode = getString(result.get("resultCode"));
 			
 			List<Map<String, Object>> dataList = (List<Map<String, Object>>) result.get("execuSummarys");
 			
 			if(resultCode.equals("0")) {
 				if(dataList.size()>0) {
 					for(Map<String, Object>dataMap:dataList) {
 						if(dataMap.get("orderType")!=null) {
 							for(String detail:DETAIL_TYPE) {
 								String[] details = detail.split(":");
 								if(StringUtils.equals(details[0], dataMap.get("orderType").toString())) {
 									dataMap.put("orderType",details[1]);
 								}
 							}
 						}
 						
 						for(String key:RB50_THOUSANDS) {
 							if(dataMap.containsKey(key)) {
 								dataMap.put(key, DataUtils.handleMoney(dataMap.get(key).toString()));	
 							}
 						}
 					}
 					botResponseUtils.setClinchRtn(vo, dataList, "成交回報");
 				} else {
 					botResponseUtils.setTextResult(vo, "你目前沒有成交紀錄");
 					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
 				}	
 			} else {
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤");
 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
 			} 					 						
		}else {
			String replyText = "RB51 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
        return vo;
    }
	
	@Override
    public BotMessageVO getRB52(SecuritiesRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());									 		
        Map<String, Object> result = securitiesAPIFeignService.getRB52(param);
        
		if (result != null) {
			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("quotes");

			if (dataList.size() > 0) {
				for (int i = 0; i < dataList.size(); i++) {
					String resultCode = getString(dataList.get(i).get("resultCode"));

					if (StringUtils.equals(resultCode, "0")) {
						String quoteChange = getString(dataList.get(i).get("quoteChange"));
						String quoteChangePercent = getString(dataList.get(i).get("quoteChangePercent"));

						if ("".equals(quoteChange)) {
							dataList.get(i).put("quoteChange", "0");
							dataList.get(i).put("quoteChangeColor", "black");
						} else if (quoteChange.indexOf("-") > -1) {
							// 跌幅
							quoteChange = quoteChange.replace("-", "");
							quoteChangePercent = quoteChangePercent.replace("-", "");
							dataList.get(i).put("quoteChange", "▼ " + quoteChange);
							dataList.get(i).put("quoteChangeColor", "green");
						} else {
							// 漲幅
							dataList.get(i).put("quoteChange", "▲ " + quoteChange);
							dataList.get(i).put("quoteChangeColor", "red");
						}
						dataList.get(i).put("quoteChangePercent", quoteChangePercent + "％");
					} else {
						logger.info("getRB52-symbol:" + getString(dataList.get(i).get("symbol")) + ", resultMessage:"
								+ getString(dataList.get(i).get("resultMessage")));
					}
				}
				botResponseUtils.setRegular52Query(vo, dataList, "庫存報價");
			} else {
				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤");
				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
			}
		} else {
			String replyText = "RB53 api error(null)";
			botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
		}
 		
        return vo;
    }
	
	@Override
    public BotMessageVO getRB52Card(SecuritiesRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		
		String isOpen = "N";
		
		Map<String, StockData> allStockIdData = null;
		String symbol = getString(reqVO.getSymbol());
		String symbolNm = "";
		String tradingDate = "";
		if(getString(reqVO.getSymbol()).equals("T001")) {
			symbolNm = "加權指數";
		} else if(getString(reqVO.getSymbol()).equals("O001")) {
			symbolNm = "櫃買指數";
		}
		if (StringUtils.isBlank(symbolNm)) {
			// 取得股票名稱 DB
			if (allStockIdData == null)
				allStockIdData = stockDataService.getAllStockIdData();
			
			//不能確定使用者輸入的是股票名稱還是股票代號，所以2個都要判斷
			if(allStockIdData.containsKey(symbol)) {
				StockData stockData = allStockIdData.get(symbol);
				symbolNm = stockData.getStockName();
			}else {
				for(String key:allStockIdData.keySet()) {
					StockData stockData = allStockIdData.get(key);
					if(StringUtils.equals(symbol, stockData.getStockName())) {
						symbolNm = stockData.getStockName();
						symbol = stockData.getStockId();
						param.put("Symbol", symbol);
						break;
					}
				}
			}
			
		}
		
		if(getString(reqVO.getCmd()).equals("RB52Card1")) {
			botResponseUtils.setTextResult(vo, "讓我為你獻上" + symbolNm);
		} else if(getString(reqVO.getCmd()).equals("RB52Card2")) {
			//CALL RB10
			Map<String, Object> result = new HashMap<String, Object>(); 
			Map<String, Object> param10 = new HashMap<String, Object>(); 
			param10.put("FunctionID", "RB10");
			param10.put("Channel", "1");
			param10.put("Symbol", symbol);
			String rpjson = regularAPIFeignService.getRB10(JSON.toJSONString(param10));
		    
			if(StringUtils.isNotBlank(rpjson)) {
	 			result= returnToMap(rpjson); 	
	 			String resultCode = getString(result.get("ResultCode"));
	 			String resultMessage = getString(result.get("ResultMessage"));
	 			
	 			if(resultCode.equals("0000")) {
	 				if(StringUtils.isNotBlank(getString(result.get("IsOpen")))) {
	 					isOpen = getString(result.get("IsOpen"));
	 				}
	 			} else {
	 				botResponseUtils.setTextResult(vo, "RB10 api error[" + resultMessage + "]");
	 			} 			
			} else {
				String replyText = "RB10 api error";
				botResponseUtils.setTextResult(vo, replyText);	
			}
		}
		
		
		
		
		Map<String, Object> result = securitiesAPIFeignService.getRB52(param);
		
		if(result != null) { 	
 			String resultCode = getString(result.get("ResultCode"));
 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("quotes");
 			
 			// TODO 範例資料沒有ResultCode
 			if( true || resultCode.equals("0000")) {

	 			if(dataList.size() > 0) {
	 				if(getString(reqVO.getCmd()).equals("RB52Card2")) {
	 					botResponseUtils.setTextResult(vo, "以下是你查詢的股票的目前行情");
	 				}
	 				
	 				
	 				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

 	 				String[] label = {"股票代號", "即時成交行情-價格", "撮合日期", "撮合時間", "開盤價價格", "當日最高價", "當日最低價", "參考價"};
 	 				String[] text = {"symbol", "tradingPrice", "tradingDate", "tradingTime", 
 	 						"openingPrice", "highPrice", "lowPrice",  "referencePrice"};
 	 				
	 					
 	 				for(int i=0;i< dataList.size();i++) {
// 	 					if(!getString(dataList.get(i).get("resultCode")).equals("0")) {
// 	 						continue;
// 	 					}
 	 					String dealflag = getString(dataList.get(i).get("dealflag"));
 	 					String dealpoint = getString(dataList.get(i).get("dealpoint"));
 	 					String deallimit = getString(dataList.get(i).get("deallimit"));
 	 					String imageTextColor = "black";
 	 					if("0".equals(dealflag)) {
 	 						dealpoint += " ";
 	 						imageTextColor = "black";
						} else if("2".equals(dealflag) || "4".equals(dealflag)) {
		 					// 跌幅
							dealpoint += " ▼";
							imageTextColor = "green";
		 				} else if("1".equals(dealflag) || "3".equals(dealflag)) {
		 					// 漲幅
		 					dealpoint += " ▲";
		 					imageTextColor = "red";
		 				}
 	 					CImageDataItem cImageData = vo.new CImageDataItem();
	 					cImageData.setCImage(BotMessageVO.IMG_CC);
	 					cImageData.setCImageUrl("card37-01.png");
	 					cImageData.setCImageTextType(7);
 	 					List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();
 	 					//List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
 		 				//List<Map<String, Object>> cTextList2 = new ArrayList<Map<String, Object>>();
 		 				CardItem cards = vo.new CardItem();
 		 				cards.setCName("");
 	 					cards.setCWidth(BotMessageVO.CWIDTH_200);
// 	 					cards.setCMessage(dataList.get(i).get(text[2]) + " " + dataList.get(i).get(text[3]));
// 	 					cards.setCTextType("111");
 	 					
 	 					
		 				CImageTextItem cImageText = vo.new  CImageTextItem();
		 				
		 				tradingDate = dataList.get(i).get(text[2]).toString();
		 				if(getString(reqVO.getSymbol()).equals("T001") || getString(reqVO.getSymbol()).equals("O001")) {
		 					cImageText.setCImageTextTitle(symbolNm);
		 				}else {
 	 						cImageText.setCImageTextTitle(getString(symbolNm + " <span  style=\"color:#939ca0\">" + dataList.get(i).get(text[0])+"</span>"));
 	 					}
		 				
		 				String date = tradingDate.replaceAll("([^0-9])", "");
		 				String dataTitle = "";
		 				String dataText = "";
		 				String thatDaymax = "";
		 				String thatDaymin = "";
						if (getString(dataList.get(i).get("resultCode")).equals("0")) {
							date = date.substring(0, 4) + "/" + date.substring(4, 6) + "/" + date.substring(6, 8);
							date = date + " " + dataList.get(i).get(text[3]).toString().substring(0, 5);
							dataTitle = getString(dataList.get(i).get(text[1])) + "::#e65100";
							dataText = dealpoint + deallimit + "::" + imageTextColor;
							thatDaymax = getString(dataList.get(i).get(text[5]));
							thatDaymin = getString(dataList.get(i).get(text[6]));
						} else {
							date = "-- --";
							dataTitle = "--";
							dataText = "-- --";
							thatDaymax = "--";
							thatDaymin = "--";
						}
		 				
						cImageText.setCImageText(date);
		 				cImageText.setCImageTextTag("Label");
		 				cImageTextList.add(cImageText.getCImageTexts());
		 				
		 				CImageTextItem cImageText3 = vo.new  CImageTextItem();	
		 				cImageText3.setCImageTextTitle(dataTitle);
		 				cImageText3.setCImageText(dataText);
		 				cImageText3.setCImageTextTag("data");
		 				cImageTextList.add(cImageText3.getCImageTexts());
		 				
 						CImageTextItem cImageText2 = vo.new  CImageTextItem();	
	 					cImageText2.setCImageText("最高 "+thatDaymax+" 最低 "+thatDaymin);
	 					cImageText2.setCImageTextTag("thatDay");
		 				cImageTextList.add(cImageText2.getCImageTexts());
		 				
		 				cImageData.setCImageTexts(cImageTextList);
 	 					
//		 				Float referencePrice = Float.valueOf(getString(dataList.get(i).get(text[7])));
		 				
//		 				for(int j=4;j<label.length;j++) {
// 	 						Float price = Float.valueOf(getString(dataList.get(i).get(text[j])));
// 
// 	 						CTextItem cText = vo.new CTextItem();
// 		 					cText.setCLabel(label[j]);
//		 					cText.setCText(getString(dataList.get(i).get(text[j])));
//		 					String textColor = "black";
// 	 						if(price > referencePrice) {
// 	 							textColor = "red";
// 	 						} else if(price < referencePrice) {
// 	 							textColor = "green";
// 	 						} else if(price == referencePrice) {
// 	 							textColor = "black";
// 	 						}
// 	 						
// 	 						cText.setCTextClass(textColor);
//		 					
// 							cTextList.add(cText.getCTexts());
//
//	 					}
 	 					cards.setCImageData(cImageData.getCImageDatas());
 	 					
 	 					//cards.setCTexts(cTextList);
		 				cardList.add(cards.getCards());
 	 				}

 					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList); 
 					if(!(getString(reqVO.getSymbol()).equals("T001") || getString(reqVO.getSymbol()).equals("O001"))) {
 						botResponseUtils.setTextResult(vo,"以下是到昨天為止的日K圖唷！");
 						botResponseUtils.setPicComponentResult3(vo, "http://webap.cathaysec.com.tw" + "/TreasuryStock/kchart/lastclose/" + symbol + ".png",true);
 					}
 					
	 			} else {
	 				botResponseUtils.setTextResult(vo, "你查詢的股票沒有資料");
	 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.selectStockType);
	 			}
	 			
	 			
 			} else {
 				String replyText = "RB52 api error(" + resultCode + ")";
 				botResponseUtils.setTextResult(vo, replyText);
 			} 		
 			
 			if(getString(reqVO.getCmd()).equals("RB52Card1")) {
 				Map<String, Object> parameter = new HashMap<>();
 				parameter.put("priceType", "櫃買");
 				SecQuickReplay otherCard1 = new SecQuickReplay(1, "看看櫃買指數", "看看櫃買指數", "報價", "報價類型", parameter);
 				if(getString(reqVO.getSymbol()).equals("T001")) {
 					parameter.put("priceType", "櫃買");
 					otherCard1 = new SecQuickReplay(1, "看看櫃買指數", "看看櫃買指數", "報價", "報價類型", parameter);
 				} else if(getString(reqVO.getSymbol()).equals("O001")) {
 					parameter.put("priceType", "加權");
 					otherCard1 = new SecQuickReplay(1, "看看加權指數", "看看加權指數", "報價", "報價類型", parameter);
 				}
 				botResponseUtils.setQuickReplayResult(vo, otherCard1, SecQuickReplay.INIT);
 			} else if(getString(reqVO.getCmd()).equals("RB52Card2")) {
 				//CALL RB10
 				if(isOpen.equals("Y")) {
 					Map<String, Object> parameter = new HashMap<>();
 					parameter.put("stockId", symbol);
 					botResponseUtils.setQuickReplayResult(vo, new SecQuickReplay(1, "我要申購" + symbolNm +"定期定額", "我要申購" + symbolNm +"定期定額", "定期定額", "beginBuy", parameter));
 				}
 			}
		}else {
			String replyText = "RB52 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
        return vo;
    }

	
	@Override
    public BotMessageVO getRB53(SecuritiesRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());									 		
        Map<String, Object> result = securitiesAPIFeignService.getRB53(param);
        
//        if((List<Map<String, Object>>) result.get("invois")==null || ((List<Map<String, Object>>) result.get("invois")).size()==0) {
//        	result = JSON.parseObject(secTestApiData.getRB53ResponseString(),Map.class);
//        }
        
 		if(result != null) {
 			result.put("resultCode", "0");
 			String resultCode = getString(result.get("resultCode"));
 			
 			List<Map<String, Object>> dataList = (List<Map<String, Object>>) result.get("invois");
 			
 			if(resultCode.equals("0")) {
 				int i = 0;
	 			for(Map<String, Object>dataMap:dataList) {
	 				i += Integer.parseInt(dataMap.get("expr").toString());
	 				
	 				//加千分位
					for (String key : RB53_THOUSANDS) {
						if (dataMap.containsKey(key)) {
							dataMap.put(key, DataUtils.handleMoney(dataMap.get(key).toString()));
						}
					}
	 			}
	 			if(i!=0) {
	 				Map<String, String> requireParams = new HashMap<String, String>();
	 				requireParams.put("FunctionID", "RB12");
	 				requireParams.put("BranchCode", reqVO.getBranchCode());
	 				requireParams.put("Account", reqVO.getAccount());			
	 				requireParams.put("Channel", PropUtils.getApiChannel());	
	 				
	 				String rpjson = regularAPIFeignService.getRB12(JSON.toJSONString(requireParams));
	 				getRB53Card(vo,param,rpjson,dataList);
	 			} else {
	 				botResponseUtils.setTextResult(vo, "近3日無交割款");
	 			}	
 			} else {
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤");
 			} 			 						
		}else {
			String replyText = "RB53 api error(null)";
			botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤");
		}
 		
        return vo;
    }
	
    public BotMessageVO getRB53Card(BotMessageVO vo,Map<String, String> param, String rpjsonRB12, List<Map<String, Object>> dataListRB53) {
        
 		if(StringUtils.isNotBlank(rpjsonRB12)) {
 			 Map<String, Object> result = JSON.parseObject(rpjsonRB12,Map.class); 				 			

			if (StringUtils.equals(result.get("ResultCode").toString(), "0000")) {
				botResponseUtils.setTextResult(vo, "來看看你的近三日股票吧");
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				CardItem cards = vo.new CardItem();
				cards.setCName("");
				cards.setCWidth(BotMessageVO.CWIDTH_250);
				cards.setCTextType("11");
				cards.setCLinkType(2);

				// 牌卡圖片
				CImageDataItem cImageData = vo.new CImageDataItem();
				cImageData.setCImage(BotMessageVO.IMG_CB03);
				cImageData.setCImageTextType(13);
				List<Map<String, Object>> cImageTextList = new ArrayList<>();

				// 加千分位
				for (String key : RB53_THOUSANDS) {
					if (result.containsKey(key)) {
						result.put(key, DataUtils.handleMoney(result.get(key).toString()));
					}
				}

				// 組牌卡
				for (String detail : RB53_DETAIL_TEST) {
					CImageTextItem cImageText = vo.new CImageTextItem();
					String title = "";
					String text = "";
					String[] details = detail.split(":");

					for (String key : details[0].split(",")) {
						if (key.indexOf("$") > -1) {
							title += param.get(key.substring(1, key.length()));
						} else if (key.indexOf("@") > -1) {
							if (!(key.substring(1).equals("Sign") && result.get(key.substring(1)).equals("+"))) {
								title += result.get(key.substring(1, key.length()));
							}
						} else {
							title += key;
						}
					}

					for (String key : details[1].split(",")) {
						if (key.indexOf("$") > -1) {
							text += param.get(key.substring(1, key.length()));
						} else if (key.indexOf("@") > -1) {
							if (!(key.substring(1).equals("Sign") && result.get(key.substring(1)).equals("+"))) {
								text += result.get(key.substring(1, key.length()));
							}
						} else {
							text += key;
						}
					}

					if (StringUtils.isNotBlank(title)) {
						cImageText.setCImageTextTitle(title);
					}
					if (StringUtils.isNotBlank(text)) {
						cImageText.setCImageText(text);
					}
					cImageTextList.add(cImageText.getCImageTexts());
				}

				cImageData.setCImageTexts(cImageTextList);
				cards.setCImageData(cImageData.getCImageDatas());

				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

				String exdt = "";
				String expr = "";

				for (Map<String, Object> data : dataListRB53) {
					exdt += "<p>" + data.get("exdt").toString().substring(0, 4) + "/"
							+ data.get("exdt").toString().substring(4, 6) + "/"
							+ data.get("exdt").toString().substring(6, 8) + "</p>";
					expr += "<p style = 'text-align:right;' >" + data.get("expr").toString() + "&emsp;</p>";
				}

				CTextItem cText = vo.new CTextItem();
				cText.setCTextClass("1");
				cText.setCLabel("交割日");
				cText.setCText(exdt);
				cTextList.add(cText.getCTexts());

				cText = vo.new CTextItem();
				cText.setCTextClass("2");
				cText.setCLabel("淨收付金額");
				cText.setCText(expr);
				cTextList.add(cText.getCTexts());

				cards.setCTexts(cTextList);
				
				cards.setCLinkList(botResponseUtils.getCLinkList(vo, SecQuickReplay.MoneyTransfer));
				
				cardList.add(cards.getCards());

				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
			} else {
				botResponseUtils.setTextResult(vo, result.get("ResultMessage").toString());
			}

		}else {
			String replyText = "RB11 api error";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
        return vo;
    }
	
    public static Map<String, Object> returnToMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> rumap = new HashMap<String, Object>();
		try {
			rumap = mapper.readValue(json, new TypeReference<HashMap<String,Object>>(){});
			return rumap;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rumap;
	}
    
	public String getString(Object str){
		String returnStr = "";
		if(str != null){
			returnStr = str.toString();			
		}
		
		return returnStr;
	}

	private List<Map<String,Object>> cTextsListForRB26( String channel, String symbolStr, Map<String, String> stockMap) {
		Map<String, StockData> allStockIdData = null;
		Map<String, String> param2 = new HashMap<>();
		param2.put("FunctionID", "RB52");
		param2.put("Channel", channel);
		param2.put("Symbol", symbolStr);
		Map<String, Object> result = securitiesAPIFeignService.getRB52(param2);
		logger.info("result=" + result);

		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> quotes = (List<Map<String, Object>>) result.get("quotes");
		if (quotes.size() > 0) {
			for (Map<String, Object> quote : quotes) {
				if(dataList.size()==3){
                    return dataList;
				}
				String symbol = getString(quote.get("symbol"));
				String dealflag = getString(quote.get("dealflag"));
				String dealpoint = getString(quote.get("dealpoint"));
				String deallimit = getString(quote.get("deallimit"));
				String tradingPrice = getString(quote.get("tradingPrice"));
				Map<String, Object> data = new HashMap<String, Object>();
				dataList.add(data);
				data.put("symbol", symbol);

				String symbolNm = stockMap.get(symbol);
				if (StringUtils.isBlank(symbolNm)) {
					// 取得股票名稱 DB
					if (allStockIdData == null)
						allStockIdData = stockDataService.getAllStockIdData();
					StockData stockData = allStockIdData.get(symbol);
					symbolNm = stockData.getStockName();
				}
				data.put("symbolNm", symbolNm);

				switch (dealflag) {
					case "0":
						if(StringUtils.isBlank(dealpoint)||StringUtils.equals(DataUtils.handleMoney2(dealpoint.replace("%", "")),"0.00")) {
							dealpoint = "--";
						}
						data.put("quoteChange", dealpoint);
						data.put("quoteChangeColor", "black");
						break;
					case "1":
					case "3":
						data.put("quoteChange", "▲ " + dealpoint);
						data.put("quoteChangeColor", "red");
						break;
					case "2":
					case "4":
						data.put("quoteChange", "▼ " + dealpoint);
						data.put("quoteChangeColor", "green");
				}
				
				if(StringUtils.isBlank(deallimit)||StringUtils.equals(DataUtils.handleMoney2(deallimit.replace("%", "")),"0.00")) {
					deallimit = "--";
				}
				
				data.put("quoteChangePercent", deallimit);
				if(StringUtils.isBlank(tradingPrice)||StringUtils.equals(DataUtils.handleMoney2(tradingPrice.replace("%", "")),"0.00")) {
					tradingPrice = "--";
				}
				data.put("price", tradingPrice);
			}
			return dataList;
		} else {
			return dataList;
		}
	}

	@Override
	public BotMessageVO iNeedMoneyTransfer(SecuritiesRequest reqVO) {

		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "開啟國泰世華APP");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.GoToCathayUrlAPP,
				SecQuickReplay.NotGoToCathayUrlAPP);
		
		return botMessageVO;
	}

}
