package com.tp.tpigateway.modules.cathaysec.service.impl;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotMessageVO.ContentItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.StockLotterRequest;
import com.tp.common.util.DataUtils;
import com.tp.common.util.DateUtils;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.fegin.StockLotteryAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.model.SecQuickReplay;
import com.tp.tpigateway.modules.cathaysec.service.StockLotteryService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;

@SuppressWarnings("unchecked")
@Service("stockLottery")
public class StockLotteryServiceImpl implements StockLotteryService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	StockLotteryAPIFeignService stockLotteryAPIFeignService;

	private String fee = "20";// 手續費
	private String serviceFee = "50";// 中籤手續費
	
	@Override
	public TPIGatewayResult chooseWebCmd(StockLotterRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		String cmd = reqVO.getCmd();

		switch (cmd) {
		case "StockProcessKey_ShowAllCanStockPurchase1":
			botResponseUtils.setTextResult(vo, "阿！接下來的任務要在APP才能完成");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.E_APP_DOWNLOAD,SecQuickReplay.INIT);
			break;
		default:
			botResponseUtils.setTextResult(vo, "還有什麼需要阿發為你服務的呢？");
			break;
		}

		return tpiGatewayResult.setData(vo);
	}

	/**
	 * 股票抽籤 (流程圖階段一)
	 */
	@Override
	public BotMessageVO stockLottery(StockLotterRequest reqVO) {

		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		// 取資訊 RB14
		Map<String, String> result14 = new HashMap<String, String>();
		result14.put("FunctionID", "RB14");

		String id = "";
		if (StringUtils.isNotBlank(reqVO.getId())) {
			id = reqVO.getId();
		}
		result14.put("ID", id);

		String branchCode = "";
		if (StringUtils.isNotBlank(reqVO.getBranchCode())) {
			branchCode = reqVO.getBranchCode();
		}
		result14.put("BranchCode", branchCode);

		String udid = "";
		if (StringUtils.isNotBlank(reqVO.getSessionId())) {
			udid = reqVO.getSessionId();
		}
		result14.put("UDID", udid);

		result14.put("Channel", PropUtils.getApiChannel());

		String account = "";
		if (StringUtils.isNotBlank(reqVO.getAccount())) {
			account = reqVO.getAccount();
		}
		result14.put("Account", account);

		Map<String, Object> resp14 = stockLotteryAPIFeignService.getRB14(result14);

		List<Map<String, Object>> data14List = (List<Map<String, Object>>) resp14.get("SubStockData");

		List<Map<String, Object>> comparisonList = new ArrayList<>();// 比對13跟14後的資料

		Boolean nb = false;
		if (data14List.size() > 0) {

			Map<String, String> result13 = new HashMap<String, String>();
			result13.put("FunctionID", "RB13");
			result13.put("ID", id);
			result13.put("UDID", udid);
			result13.put("Channel", PropUtils.getApiChannel());
			Map<String, Object> resp13 = stockLotteryAPIFeignService.getRB13(result13);
			// System.out.println("檢查2" + resp13.get("StockList").toString());
			List<Map<String, Object>> data13List = (List<Map<String, Object>>) resp13.get("StockList");
//			List<Map<String, Object>> data13List = JSONArray.parseObject(resp13.get("StockList").toString(),
//					new TypeReference<List<Map<String, Object>>>() {
//					});
			if (data13List.size() > 0) {
				for (Map<String, Object> map13 : data13List) {
					for (Map<String, Object> map14 : data14List) {
						if (StringUtils.equals(MapUtils.getString(map13, "StockId"),
								MapUtils.getString(map14, "Symbol"))) {
							comparisonList.add(map14);
						}
					}
				}
			} else {
				nb = true;
			}

		} else {
			nb = true;
		}

		if (comparisonList.size() == 0) {
			nb = true;
		} else if (comparisonList.size() > 0 && comparisonList.size() <= 3) {
			// 小於三筆
			botResponseUtils.setTextResult(botMessageVO, "以下是可抽籤的股票商品喔！");
			botMessageVO = combinationData(botMessageVO, comparisonList);
//			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.LotteryRecord,
//					SecQuickReplay.MyComingSoon, SecQuickReplay.PurchaseRules);

		} else {
			// 大於三筆
			botResponseUtils.setTextResult(botMessageVO, "以下是可抽籤的股票商品喔！");
			botMessageVO = combinationData(botMessageVO, comparisonList);
//			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, SecQuickReplay.PurchaseRules,
//					SecQuickReplay.ShowAllTargets, SecQuickReplay.LotteryRecord, SecQuickReplay.MyComingSoon);

		}

		if (nb) {
			botMessageVO = notCanBuy(botMessageVO);
		} else {
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, SecQuickReplay.PurchaseRules,
					SecQuickReplay.ShowAllTargets, SecQuickReplay.LotteryRecord, SecQuickReplay.MyComingSoon);
		}

		return botMessageVO;
	}

	/**
	 * 顯示所有的標 (流程圖階段二)
	 */
	@Override
	public BotMessageVO showAllCanStockPurchase(StockLotterRequest reqVO) {

		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		// 取資訊 RB14
		Map<String, String> result14 = new HashMap<String, String>();
		result14.put("FunctionID", "RB14");

		String id = "";
		if (StringUtils.isNotBlank(reqVO.getId())) {
			id = reqVO.getId();
		}
		result14.put("ID", id);

		String branchCode = "";
		if (StringUtils.isNotBlank(reqVO.getBranchCode())) {
			branchCode = reqVO.getBranchCode();
		}
		result14.put("BranchCode", branchCode);

		String udid = "";
		if (StringUtils.isNotBlank(reqVO.getSessionId())) {
			udid = reqVO.getSessionId();
		}
		result14.put("UDID", udid);

		result14.put("Channel", PropUtils.getApiChannel());

		String account = "";
		if (StringUtils.isNotBlank(reqVO.getAccount())) {
			account = reqVO.getAccount();
		}
		result14.put("Account", account);

		Map<String, Object> resp14 = stockLotteryAPIFeignService.getRB14(result14);

		List<Map<String, Object>> data14List = (List<Map<String, Object>>) resp14.get("SubStockData");

//		List<Map<String, Object>> data14List = JSONArray.parseObject(resp14.get("SubStockData").toString(),
//				new TypeReference<List<Map<String, Object>>>() {
//				});

		if (data14List.size() > 0) {
			for (Map<String, Object> map : data14List) {
				String tempRefer = DataUtils.handleMoney2(MapUtils.getString(map, "TempRefer"));
				map.put("TempRefer", tempRefer);

				String ratioColor = "black";
				Double priceDiffRatio = Double.valueOf(MapUtils.getString(map, "PriceDiffRatio"));
				if (MapUtils.getString(map, "PriceDiffRatio").indexOf("+") > -1
						|| (priceDiffRatio != null && priceDiffRatio > 0)) {
					ratioColor = "red";
				} else if (MapUtils.getString(map, "PriceDiffRatio").indexOf("-") > -1
						|| (priceDiffRatio != null && priceDiffRatio < 0)) {
					ratioColor = "green";
					map.put("PriceDiffRatio", MapUtils.getString(map, "PriceDiffRatio"));
				}
				String priceDiffStr = DataUtils.handleMoney(MapUtils.getString(map, "PriceDiff"))
						+ "元<br><span style='color:" + ratioColor + ";'>("
						+ DataUtils.handleMoney2(MapUtils.getString(map, "PriceDiffRatio")) + "％)" + "</span>";
				map.put("PriceDiffStr", priceDiffStr);
			}
			List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
			dataList.addAll(data14List);
			botResponseUtils.setDoSelectSearchComp(botMessageVO, dataList, "所有可抽籤標的", "1", "", "請選擇下方商品");
		} else {
			botMessageVO = notCanBuy(botMessageVO);
		}

		return botMessageVO;

	}

	/**
	 * 確認是否申購 {組1} 否，我再想想 / 是，我要購買
	 */
	@Override
	public TPIGatewayResult confirmWhetherToPurchase1(StockLotterRequest reqVO) {

		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		// 取資訊 RB14
		Map<String, String> result14 = new HashMap<String, String>();
		result14.put("FunctionID", "RB14");

		String id = "";
		if (StringUtils.isNotBlank(reqVO.getId())) {
			id = reqVO.getId();
		}
		result14.put("ID", id);

		String branchCode = "";
		if (StringUtils.isNotBlank(reqVO.getBranchCode())) {
			branchCode = reqVO.getBranchCode();
		}
		result14.put("BranchCode", branchCode);

		String udid = "";
		if (StringUtils.isNotBlank(reqVO.getSessionId())) {
			udid = reqVO.getUdid();
		}
		result14.put("UDID", udid);
		result14.put("Channel", PropUtils.getApiChannel());

		String account = "";
		if (StringUtils.isNotBlank(reqVO.getAccount())) {
			account = reqVO.getAccount();
		}
		result14.put("Account", account);

		Map<String, Object> resp14 = stockLotteryAPIFeignService.getRB14(result14);
		List<Map<String, Object>> data14List = (List<Map<String, Object>>) resp14.get("SubStockData");
		for (Map<String, Object> map : data14List) {

			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
			if (StringUtils.equals(reqVO.getSymbol(), MapUtils.getString(map, "Symbol"))) {
				// 初始分組
				ContentItem content = botMessageVO.new ContentItem();
				content.setType(19);

				// 牌卡
				CardItem cards = botMessageVO.new CardItem();

				// 組底下資訊
				List<Map<String, Object>> cTextsList = new ArrayList<>();
				cards.setCLinkType(8);
				cards.setCTitle("費用詳情");

				Map<String, Object> m = new HashMap<>();
				String mStr = "";
				mStr += "申購金額";
				mStr += "<br />";

				String amount = "0";
				if (map.containsKey("AdvAmount")) {
					amount = MapUtils.getString(map, "AdvAmount");
				}

				if (map.containsKey("Fee")) {
					fee = MapUtils.getString(map, "Fee");
				}

				if (map.containsKey("ServiceFee")) {
					serviceFee = MapUtils.getString(map, "ServiceFee");
				}

				float amountInt = Float.parseFloat(amount);
				float feeInt = Float.parseFloat(fee);
				float serviceFeeInt = Float.parseFloat(serviceFee);

				String price = String.valueOf((amountInt - (feeInt + serviceFeeInt)));

				mStr += "價金" + DataUtils.handleMoney(price) + "＋處理費" + DataUtils.handleMoney(fee)
				+ "<br />＋中籤手續費" + DataUtils.handleMoney(serviceFee);
				
				m.put("cText", mStr);
				cTextsList.add(m);
				cards.setCTexts2(cTextsList);

				// 組牌卡
				CImageDataItem cImageData = botMessageVO.new CImageDataItem();
				cImageData.setCImageTextType(9);
				List<Map<String, Object>> cImageTexts = new ArrayList<>();
				Map<String, Object> cardWord = new HashMap<>();
				cardWord.put("cImageTextTitle", "股票名稱");
				cardWord.put("cImageText", MapUtils.getString(map, "SymbolName") + MapUtils.getString(map, "Symbol"));
				cImageTexts.add(cardWord);

				cardWord = new HashMap<>();
				cardWord.put("cImageTextTitle", "申購金額");
				cardWord.put("cImageText", DataUtils.handleMoney(MapUtils.getString(map, "AdvAmount")) + "元");
				cImageTexts.add(cardWord);

				cImageData.setCImageTexts(cImageTexts);

				cards.setCImageData(cImageData.getCImageDatas());// 組裝起來

				// 中間
				List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

				cards.setCTextType("1");
				cards.setCWidth("segment secard");

				CTextItem cText = botMessageVO.new CTextItem();

				cText = botMessageVO.new CTextItem();
				cText.setCLabel("張數");
				cText.setCText(MapUtils.getString(map, "Share") + "張");
				cTexts.add(cText.getCTexts());
				cards.setCTexts(cTexts);// 組裝起來

				cardList.add(cards.getCards());

				content.setCards(cardList);

				botMessageVO.getContent().add(content.getContent());
				botResponseUtils.setTextResult(botMessageVO, "＊提醒您，每檔股票僅可在一家券商進行抽籤。");

				QuickReplay q1 = SecQuickReplay.LetMeThinkAgain;
				QuickReplay q2 = SecQuickReplay.IWantToBuy;

				botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, q1, q2);

				tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
				tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));
			}
		}

		return tpiGatewayResult.setData(botMessageVO);
	}

	/**
	 * 確認是否申購 {組2} 顯示所有抽籤的標 / 看阿發還能幫什麼忙
	 */
	@Override
	public BotMessageVO confirmWhetherToPurchase2(StockLotterRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04,
				SecQuickReplay.ShowAllTheLotsOfTheLottery, SecQuickReplay.INIT);
		return botMessageVO;
	}

	// 組牌卡
	private BotMessageVO combinationData(BotMessageVO botMessageVO, List<Map<String, Object>> comparisonList) {

		int maximum = 3;
		int nowmum = 0;

		// 初始分組
		ContentItem content = botMessageVO.new ContentItem();
		content.setType(19);

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : comparisonList) {

			// 限制只能最大三筆
			if (nowmum > maximum) {
				break;
			}

			nowmum++;

			// 牌卡
			CardItem cards = botMessageVO.new CardItem();
			List<Map<String, Object>> cTextsList = new ArrayList<>();

			// 組底下資訊
			cards.setCLinkType(8);
			cards.setCTitle("費用詳情");

			Map<String, Object> m = new HashMap<>();
			
			String mStr = "";
			mStr += "申購金額";
			mStr += "<br />";

			String amount = "0";
			if (map.containsKey("AdvAmount")) {
				amount = MapUtils.getString(map, "AdvAmount");
			}

			if (map.containsKey("Fee")) {
				fee = MapUtils.getString(map, "Fee");
			}

			if (map.containsKey("ServiceFee")) {
				serviceFee = MapUtils.getString(map, "ServiceFee");
			}

			float amountInt = Float.parseFloat(amount);
			float feeInt = Float.parseFloat(fee);
			float serviceFeeInt = Float.parseFloat(serviceFee);

			String price = String.valueOf((amountInt - (feeInt + serviceFeeInt)));

			mStr += "價金" + DataUtils.handleMoney(price) + "＋處理費" + DataUtils.handleMoney(fee) + "<br />＋中籤手續費"
					+ DataUtils.handleMoney(serviceFee);
			m.put("cText", mStr);
			cTextsList.add(m);

			cards.setCTexts2(cTextsList);

			// 組牌卡-------

			// 圖
			storckTreeCard(botMessageVO, cards, map, "", "combination", "");
//			Map<String, Object> cardWord = new HashMap<>();
//			cardWord.put("cImageTextTag", "截止日" + MapUtils.getString(map, "StopDate"));
//			cardWord.put("cImageTextTitle", MapUtils.getString(map, "SymbolName"));
//			cardWord.put("cImageText", MapUtils.getString(map, "Symbol"));
//			cImageData.setCImageTexts(cImageTexts);
//			cards.setCImageData(cImageData.getCImageDatas());// 組裝起來

			// 中間
			List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

			cards.setCTextType("1");
			cards.setCWidth(botMessageVO.CWIDTH_250);

			CTextItem cText = botMessageVO.new CTextItem();

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("申購價");
			cText.setCText(DataUtils.handleMoney2(MapUtils.getString(map, "TempRefer")));
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			String lsdd = MapUtils.getString(map, "LastTDDate");
			if (StringUtils.isNotBlank(lsdd)) {
				cText.setCLabel("收盤價(" + lsdd.substring(5) + ")");
			} else {
				cText.setCLabel("收盤價");
			}

			cText.setCText(DataUtils.handleMoney2(MapUtils.getString(map, "LastTDPrice")));

			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("張數");
			cText.setCText(map.get("Share") + "張");
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("價差");

//			if (MapUtils.getString(map, "PriceDiffRatio").indexOf("+") > -1) {
//				cText.setCTextClass("1");
//			} else if (MapUtils.getString(map, "PriceDiffRatio").indexOf("-") > -1) {
//				cText.setCTextClass("2");
//			}

			cText.setCText(DataUtils.handleMoney(MapUtils.getString(map, "PriceDiff")) + "元("
					+ DataUtils.handleMoney2(MapUtils.getString(map, "PriceDiffRatio")) + "％)");

			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("申購金額");
			cText.setCText(DataUtils.handleMoney(MapUtils.getString(map, "AdvAmount")) + "元");
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			// 底下按鈕
			List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();

			Map<String, Object> parameter = new HashMap<>();
			parameter.put("stockId", map.get("Symbol"));

			QuickReplay q1 = new SiteQuickReplay(2, "立即申購", "申購" + map.get("Symbol"), "股票抽籤", "beginBuy", parameter);

			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q1).getCLinkList());

			cards.setCLinkList(cLinkContents);// 組裝起來

			cardList.add(cards.getCards());

		}

		content.setCards(cardList);

		botMessageVO.getContent().add(content.getContent());

		return botMessageVO;
	}

	/**
	 * 目前無可申購商品牌卡
	 * 
	 * @param botMessageVO
	 * @return
	 */
	private BotMessageVO notCanBuy(BotMessageVO botMessageVO) {
		botResponseUtils.setTextResult(botMessageVO, "目前無可抽籤標的!");

		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.INIT,
				SecQuickReplay.LotteryRecord);

		return botMessageVO;
	}

	/**
	 * RB16 comming 組牌卡
	 * 
	 * @param type
	 */
	private BotMessageVO combinationDataForCommingStock(BotMessageVO botMessageVO,
			List<Map<String, Object>> commingList) {

		// 初始分組
		ContentItem content = botMessageVO.new ContentItem();
		content.setType(19);

//		Map<String, Object> demo = new HashMap<>();
//		demo.put("SymbolName", "有獎徵答股");
//		demo.put("Symbol", "3345678");
//		demo.put("Status", "扣款");
//		demo.put("StatusDate", "2019/03/05");
//		demo.put("Share", "10");
//		demo.put("AdvAmount", "120");
//		commingList.add(demo);
//
//		Map<String, Object> demo2 = new HashMap<>();
//		demo2.put("SymbolName", "有獎徵答股");
//		demo2.put("Symbol", "3345678");
//		demo2.put("Status", "抽籤");
//		demo2.put("StatusDate", "2019/03/05");
//		demo2.put("Share", "10");
//		demo2.put("PriceDiff", "170");
//		demo2.put("PriceDiffRatio", "30");
//		commingList.add(demo2);

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : commingList) {
			// 1 = 已申購，尚未扣款 2 = 已申購，已扣款，尚未抽籤 3 = 未中籤 4 = 已中籤
			String statusIdx = MapUtils.getString(map, "StatusIdx");

			// 牌卡
			CardItem cards = botMessageVO.new CardItem();

			// 組牌卡-------
			String status = MapUtils.getString(map, "Status");
			String statusDate = MapUtils.getString(map, "StatusDate");

			// 圖
//			CImageDataItem cImageData = botMessageVO.new CImageDataItem();
//			cImageData.setCImageUrl("card37.png");
//			cImageData.setCImageTextType(10);
//			List<Map<String, Object>> cImageTexts = new ArrayList<>();
//			Map<String, Object> m = new HashMap<>();
//			m.put("cImageTextTitle", "商品名稱" + MapUtils.getString(map, "SymbolName"));
//			m.put("cImageText", "股號" + MapUtils.getString(map, "Symbol"));
//			m.put("cImageTextTag", status + "日");
//			m.put("cImageTextTag2", MapUtils.getString(map, "StatusDate"));
//			cImageTexts.add(m);
//			cImageData.setCImageTexts(cImageTexts);
//
//			cards.setCImageData(cImageData.getCImageDatas());// 組裝起來

			// 中間
			List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
			cards.setCTextType("15");
			cards.setCWidth(botMessageVO.CWIDTH_250);

			CTextItem cText = botMessageVO.new CTextItem();
			cText.setCLabel(status);
			cText.setCText(MapUtils.getString(map, "SymbolName") + " " + MapUtils.getString(map, "Symbol"));
			cText.setCTextClass("1");
			cTexts.add(cText.getCTexts());

			// def
			cText = botMessageVO.new CTextItem();
			cText.setCLabel("張數");
			cText.setCText(MapUtils.getString(map, "Share") + "張");
			cText.setCTextClass("2");
			cTexts.add(cText.getCTexts());

			String statusIdxType = "日";

			if (StringUtils.equals(statusIdx, "1")) {
				statusIdxType = "扣款日";
			} else if (StringUtils.equals(statusIdx, "2")) {
				statusIdxType = "抽籤日";
			} else if (StringUtils.equals(statusIdx, "3")) {
				statusIdxType = "退款日";
			} else if (StringUtils.equals(statusIdx, "4")) {
				statusIdxType = "撥券日";
			}

			if (StringUtils.equals(statusIdx, "1") || StringUtils.equals(statusIdx, "3")) {
				cText = botMessageVO.new CTextItem();
				cText.setCLabel(statusIdxType);
				Date date = DateUtils.stringToDate(statusDate, "yyyy/MM/dd");
				cText.setCText(DateUtils.format(date, "M月d日"));
				cText.setCTextClass("2");
				cTexts.add(cText.getCTexts());

				cText = botMessageVO.new CTextItem();
				if (StringUtils.equals(statusIdx, "1")) {
					cText.setCLabel("抽籤金額");
					cText.setCTextStyle("red");
					int days = countdownDays(statusDate);
					if (days > 0) {
						cText.setCText("倒數" + days + "天");
					}
				} else {
					cText.setCLabel("退款金額");
					cText.setCTextStyle("green");
				}

				cText.setCUnit("NT＄");
				cText.setCAmount(DataUtils.handleMoney(MapUtils.getString(map, "AdvAmount")));
				cText.setCTextClass("3");
				cTexts.add(cText.getCTexts());

			} else if (StringUtils.equals(statusIdx, "2")) {
				cText = botMessageVO.new CTextItem();

				cText.setCLabel("價差");
				cText.setCText("NT＄" + DataUtils.handleMoney(MapUtils.getString(map, "PriceDiff")) + "::("
						+ DataUtils.handleMoney2(MapUtils.getString(map, "PriceDiffRatio")) + "％)");
				cText.setCTextClass("2");
				cTexts.add(cText.getCTexts());
				cText = botMessageVO.new CTextItem();

				int days = countdownDays(statusDate);
				if (days > 0) {
					cards.setCBreakLine("倒數" + days + "天");
				}

				Date date = DateUtils.stringToDate(statusDate, "yyyy/MM/dd");
				cText.setCAmount(DateUtils.format(date, "M月d日"));
				cText.setCLabel(statusIdxType + "期");
				cText.setCTextClass("3");
				cTexts.add(cText.getCTexts());

			} else if (StringUtils.equals(statusIdx, "4")) {
				cText = botMessageVO.new CTextItem();
				cText.setCLabel("抽籤價格／昨收價格");
				cText.setCText(DataUtils.handleMoney(MapUtils.getString(map, "PriceDiff")) + "／"
						+ DataUtils.handleMoney(MapUtils.getString(map, "LastTDPrice")));
				cText.setCTextClass("2");
				cTexts.add(cText.getCTexts());

				cText = botMessageVO.new CTextItem();
				cText.setCLabel("價差");
				cText.setCUnit("NT＄");
				cText.setCAmount(DataUtils.handleMoney(MapUtils.getString(map, "PriceDiff")));
				cText.setCText("(" + DataUtils.handleMoney2(MapUtils.getString(map, "PriceDiffRatio")) + "％)");
				cText.setCTextClass("4");
				cTexts.add(cText.getCTexts());
			}
			cards.setCTexts(cTexts);// 組裝起來
			cardList.add(cards.getCards());

		}

		content.setCards(cardList);

		botMessageVO.getContent().add(content.getContent());

		return botMessageVO;
	}

	/** RB16 doing 組牌卡 */
	private BotMessageVO combinationDataForDoing(BotMessageVO botMessageVO, List<Map<String, Object>> comparisonList,
			String symbol) {

		// 初始分組
		ContentItem content = botMessageVO.new ContentItem();
		content.setType(19);

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : comparisonList) {

			if (MapUtils.getString(map, "Symbol").equals(symbol)) {

				// 型態資訊 1 = 已申購，尚未扣款 , 2 = 已申購，扣款失敗 , 3 = 已申購，已扣款，尚未抽籤 , 4 = 未中籤 ,5 = 已中籤
				String statusIdx = MapUtils.getString(map, "StatusIdx");

				// 牌卡
				CardItem cards = botMessageVO.new CardItem();
				List<Map<String, Object>> cTextsList = new ArrayList<>();
				String status = MapUtils.getString(map, "Status");
				// 組底下資訊
				cards.setCLinkType(8);
				cards.setCTitle("費用詳情");

				Map<String, Object> m = new HashMap<>();
				
				String mStr = "";
				mStr += "申購金額";
				mStr += "<br />";

				String amount = "0";
				if (map.containsKey("AdvAmount")) {
					amount = MapUtils.getString(map, "AdvAmount");
				}

				float amountInt = Float.parseFloat(amount);
				float feeInt = Float.parseFloat(fee);
				float serviceFeeInt = Float.parseFloat(serviceFee);

				String price = String.valueOf((amountInt - (feeInt + serviceFeeInt)));

				mStr += "價金" + DataUtils.handleMoney(price) + "＋處理費" + DataUtils.handleMoney(fee)
				+ "<br />＋中籤手續費" + DataUtils.handleMoney(serviceFee);
				
				if (StringUtils.equals(statusIdx, "4")) {
					mStr += "<br />";

					mStr += "退款金額";
					mStr += "<br />";
					
					mStr += "價金" + DataUtils.handleMoney(price) + "＋中籤手續費" + DataUtils.handleMoney(serviceFee);
					
				}
				m.put("cText", mStr);
				cTextsList.add(m);
				
				cards.setCTexts2(cTextsList);

				// 組牌卡-------

				// 圖
//				CImageDataItem cImageData = botMessageVO.new CImageDataItem();
//				cImageData.setCImageUrl("card37-01.png");
//				cImageData.setCImageTextType(7);
//				List<Map<String, Object>> cImageTexts = new ArrayList<>();
//				CImageTextItem cImageText=botMessageVO.new CImageTextItem();
//				
//				Map<String, Object> cardWord = new HashMap<>();
//				cardWord.put("cImageTextTag", MapUtils.getString(map, "Status"));
//				cardWord.put("cImageTextTitle", MapUtils.getString(map, "SymbolName"));
//				cardWord.put("cImageText", MapUtils.getString(map, "Symbol"));
//				cImageTexts.add(cardWord);
//				cImageData.setCImageTexts(cImageTexts);
//
//				cards.setCImageData(cImageData.getCImageDatas());// 組裝起來

				storckTreeCard(botMessageVO, cards, map, status, "Doing", statusIdx);// 圖片建置

				// 中間
				List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

				cards.setCTextType("1");
				cards.setCWidth(botMessageVO.CWIDTH_250);

				CTextItem cText = botMessageVO.new CTextItem();

				// def value
				cText = botMessageVO.new CTextItem();
				cText.setCLabel("抽籤價");
				cText.setCText(DataUtils.handleMoney2(MapUtils.getString(map, "TempRefer")));
				cTexts.add(cText.getCTexts());
				cards.setCTexts(cTexts);// 組裝起來

				cText = botMessageVO.new CTextItem();
				String ld = MapUtils.getString(map, "LastTDDate");
				if (StringUtils.isNotBlank(ld) && ld.length() >= 5) {
					ld = ld.substring(5);
				}
				cText.setCLabel("昨收價(" + ld + ")");
				cText.setCText(DataUtils.handleMoney2(MapUtils.getString(map, "LastTDPrice")));
				cTexts.add(cText.getCTexts());
				cards.setCTexts(cTexts);// 組裝起來

				cText = botMessageVO.new CTextItem();
				cText.setCLabel("張數");
				cText.setCText(MapUtils.getString(map, "Share") + "張");
				cTexts.add(cText.getCTexts());
				cards.setCTexts(cTexts);// 組裝起來

				cText = botMessageVO.new CTextItem();
				cText.setCLabel("價差");
				cText.setCText(DataUtils.handleMoney(MapUtils.getString(map, "PriceDiff")) + "元("
						+ MapUtils.getString(map, "PriceDiffRatio") + "％)");
				cTexts.add(cText.getCTexts());
				cards.setCTexts(cTexts);// 組裝起來

				// statusIdx change value

				if (!StringUtils.equals(statusIdx, "2")) {
					String statusIdxType = "日";

					if (StringUtils.equals(statusIdx, "1")) {
						statusIdxType = "扣款日";
					} else if (StringUtils.equals(statusIdx, "3")) {
						statusIdxType = "抽籤日";
					} else if (StringUtils.equals(statusIdx, "4")) {
						statusIdxType = "退款日";
					} else if (StringUtils.equals(statusIdx, "5")) {
						statusIdxType = "撥券日";
					}
					cText = botMessageVO.new CTextItem();
					cText.setCLabel(statusIdxType);
					cText.setCText(MapUtils.getString(map, "StatusDate"));
					cTexts.add(cText.getCTexts());
					cards.setCTexts(cTexts);// 組裝起來

					cText = botMessageVO.new CTextItem();
					cText.setCLabel("抽籤金額");
					cText.setCText(DataUtils.handleMoney(MapUtils.getString(map, "AdvAmount")) + "元");
					cTexts.add(cText.getCTexts());
					cards.setCTexts(cTexts);// 組裝起來

					if (StringUtils.equals(statusIdx, "4")) {
						cText = botMessageVO.new CTextItem();
						cText.setCLabel("退款金額");
						float i = Integer.parseInt(MapUtils.getString(map, "AdvAmount")) - feeInt;
						cText.setCText(DataUtils.handleMoney(String.valueOf(i)) + "元");
						cTexts.add(cText.getCTexts());
						cards.setCTexts(cTexts);// 組裝起來
					}

				}

				// 底下按鈕
				// cards.setCLinkList(botResponseUtils.getCLinkList(botMessageVO,
				// SecQuickReplay.CHECK_STOCK_APP(symbol)));
				cardList.add(cards.getCards());
			}
		}

		content.setCards(cardList);

		botMessageVO.getContent().add(content.getContent());

		return botMessageVO;
	}

	/**
	 * 即將到來的股票 抽籤行事曆
	 */
	@Override
	public BotMessageVO commingStock(StockLotterRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		// 取資訊 RB14
		Map<String, String> result16 = new HashMap<String, String>();
		result16.put("FunctionID", "RB16");

		String id = "";
		if (StringUtils.isNotBlank(reqVO.getId())) {
			id = reqVO.getId();
		}
		result16.put("ID", id);

		String udid = "";
		if (StringUtils.isNotBlank(reqVO.getSessionId())) {
			udid = reqVO.getSessionId();
		}
		result16.put("UDID", udid);

		String branchCode = "";
		if (StringUtils.isNotBlank(reqVO.getBranchCode())) {
			branchCode = reqVO.getBranchCode();
		}
		result16.put("BranchCode", branchCode);

		String account = "";
		if (StringUtils.isNotBlank(reqVO.getAccount())) {
			account = reqVO.getAccount();
		}
		result16.put("Account", account);

		result16.put("Channel", PropUtils.getApiChannel());

		Map<String, Object> resp16 = stockLotteryAPIFeignService.getRB16(result16);

		List<Map<String, Object>> data16CommingList = (List<Map<String, Object>>) resp16.get("Comming");

		if (data16CommingList.size() > 0) {
			botResponseUtils.setTextResult(botMessageVO, "你好，以下是您即將開始的活動");
			combinationDataForCommingStock(botMessageVO, data16CommingList);
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.LotteryRecord,
					SecQuickReplay.BankAccountBalance, SecQuickReplay.ShowAllCanTargets);
		} else {
			botResponseUtils.setTextResult(botMessageVO, "目前您並未有即將開始的活動");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.LotteryRecord,
					SecQuickReplay.MyAccountBalance, SecQuickReplay.ShowAllTargets);
		}

		return botMessageVO;
	}

	/**
	 * 看我的抽籤紀錄
	 */
	@Override
	public BotMessageVO doingStock(StockLotterRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		// 取資訊 RB14
		// {"FunctionID":"RB16","ID":"G221491662","UDID":"123","BranchCode":"8880","Account":"9807390","Channel":"DQ"}
		Map<String, String> result16 = new HashMap<String, String>();
		result16.put("FunctionID", "RB16");

		String id = "";
		if (StringUtils.isNotBlank(reqVO.getId())) {
			id = reqVO.getId();
		}
		result16.put("ID", id);

		String udid = "";
		if (StringUtils.isNotBlank(reqVO.getSessionId())) {
			udid = reqVO.getSessionId();
		}
		result16.put("UDID", udid);

		String branchCode = "";
		if (StringUtils.isNotBlank(reqVO.getBranchCode())) {
			branchCode = reqVO.getBranchCode();
		}
		result16.put("BranchCode", branchCode);

		String account = "";
		if (StringUtils.isNotBlank(reqVO.getAccount())) {
			account = reqVO.getAccount();
		}
		result16.put("Account", account);

		result16.put("Channel", PropUtils.getApiChannel());

		Map<String, Object> resp16 = stockLotteryAPIFeignService.getRB16(result16);

		List<Map<String, Object>> data16DoingList = (List<Map<String, Object>>) resp16.get("Doing");

		List<Map<String, Object>> doingSortList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : data16DoingList) {
			String statusDate = MapUtils.getString(map, "StatusDate");
			if (StringUtils.isNotBlank(statusDate) && statusDate.length() == 10) {
				statusDate = statusDate.substring(5);
				map.put("StatusDate", statusDate);//日期格式矯正
			}
			doingSortList.add(map);
		}

		if (doingSortList.size() > 0) {
			botResponseUtils.setDoSelectSearchComp(botMessageVO, doingSortList, "我的紀錄", "2", "", "請選擇想要查詢的標的"); // 20191118呂長紘修改，原標題為所有可抽籤的標的
		} else {
			botResponseUtils.setTextResult(botMessageVO, "您目前沒有申購紀錄唷！");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, SecQuickReplay.ShowAllTargets,
					SecQuickReplay.INIT);
		}

		return botMessageVO;
	}

	/**
	 * 看我的抽籤紀錄
	 */
	@Override
	public BotMessageVO doingStockDetails(StockLotterRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		// 取資訊 RB14
		Map<String, String> result16 = new HashMap<String, String>();
		result16.put("FunctionID", "RB16");

		String id = "";
		if (StringUtils.isNotBlank(reqVO.getId())) {
			id = reqVO.getId();
		}
		result16.put("ID", id);

		String udid = "";
		if (StringUtils.isNotBlank(reqVO.getSessionId())) {
			udid = reqVO.getSessionId();
		}
		result16.put("UDID", udid);

		String branchCode = "";
		if (StringUtils.isNotBlank(reqVO.getBranchCode())) {
			branchCode = reqVO.getBranchCode();
		}
		result16.put("BranchCode", branchCode);

		String account = "";
		if (StringUtils.isNotBlank(reqVO.getAccount())) {
			account = reqVO.getAccount();
		}
		result16.put("Account", account);

		result16.put("Channel", PropUtils.getApiChannel());
		String symbol = reqVO.getSymbol();
		Map<String, Object> resp16 = stockLotteryAPIFeignService.getRB16(result16);
		List<Map<String, Object>> data16DoingList = (List<Map<String, Object>>) resp16.get("Doing");
		combinationDataForDoing(botMessageVO, data16DoingList, symbol);
		return botMessageVO;
	}

	@Override
	public TPIGatewayResult buyStock(StockLotterRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		// 取資訊 RB14
		Map<String, String> result14 = new HashMap<String, String>();
		result14.put("FunctionID", "RB14");

		String id = "";
		if (StringUtils.isNotBlank(reqVO.getId())) {
			id = reqVO.getId();
		}
		result14.put("ID", id);

		String branchCode = "";
		if (StringUtils.isNotBlank(reqVO.getBranchCode())) {
			branchCode = reqVO.getBranchCode();
		}
		result14.put("BranchCode", branchCode);

		String udid = "";
		if (StringUtils.isNotBlank(reqVO.getSessionId())) {
			udid = reqVO.getSessionId();
		}
		result14.put("UDID", udid);

		result14.put("Channel", PropUtils.getApiChannel());

		String account = "";
		if (StringUtils.isNotBlank(reqVO.getAccount())) {
			account = reqVO.getAccount();
		}
		result14.put("Account", account);

		Map<String, Object> resp14 = stockLotteryAPIFeignService.getRB14(result14);
		List<Map<String, Object>> data14List = (List<Map<String, Object>>) resp14.get("SubStockData");
		String symbolName = "";
		String share = "";
		String advAmount = "";
		String item = "";
		String debitDate = "";
		String drawDate = "";

		Boolean rb15isOK = false;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		String stopDate = "";
		Map<String, Object> resp15 = new HashMap<>();
		logger.info("檢查reqVO.Symbol:" + reqVO.getSymbol());
		for (Map<String, Object> map : data14List) {
			if (StringUtils.equals(MapUtils.getString(map, "Symbol"), reqVO.getSymbol())) {
				rb15isOK = true;
				symbolName = MapUtils.getString(map, "SymbolName") + " " + MapUtils.getString(map, "Symbol");
				share = MapUtils.getString(map, "Share");
				advAmount = MapUtils.getString(map, "AdvAmount");
				String amount = "0";
				if (map.containsKey("AdvAmount")) {
					amount = MapUtils.getString(map, "AdvAmount");
				}

				if (map.containsKey("Fee")) {
					fee = MapUtils.getString(map, "Fee");
				}

				if (map.containsKey("ServiceFee")) {
					serviceFee = MapUtils.getString(map, "ServiceFee");
				}

				if (map.containsKey("DebitDate")) {
					debitDate = MapUtils.getString(map, "DebitDate");
				}

				if (map.containsKey("DrawDate")) {
					drawDate = MapUtils.getString(map, "DrawDate");
				}

				float amountInt = Float.parseFloat(amount);
				float feeInt = Float.parseFloat(fee);
				float serviceFeeInt = Float.parseFloat(serviceFee);

				String price = String.valueOf((amountInt - (feeInt + serviceFeeInt)));

				item = "價金" + DataUtils.handleMoney(price) + "＋處理費" + DataUtils.handleMoney(fee) + "<br />＋中籤手續費"
						+ DataUtils.handleMoney(serviceFee);
				requireParams.put("FunctionID", "RB15");
				requireParams.put("ID", reqVO.getId());
				requireParams.put("UDID", reqVO.getUdid());
				requireParams.put("Symbol", reqVO.getSymbol());
				requireParams.put("Market", MapUtils.getString(map, "Market"));
				requireParams.put("BranchCode", reqVO.getBranchCode());
				requireParams.put("Account", reqVO.getAccount());
				requireParams.put("Type", "0");
				requireParams.put("OrderType", "0");
				requireParams.put("OrderState", "10");
				requireParams.put("PriceType", "0");
				requireParams.put("Price", MapUtils.getString(map, "AdvAmount"));
				requireParams.put("Quantity", MapUtils.getString(map, "TxunShare"));
				requireParams.put("OrderSide", "1");

				stopDate = MapUtils.getString(map, "StopDate");
				if (StringUtils.isNotBlank("stopDate")) {
					String yl = stopDate.substring(0, 4);

					int ylInt = Integer.parseInt(yl);
					ylInt = ylInt - 1911;

					stopDate = String.valueOf(ylInt) + stopDate.substring(4);
				}
				requireParams.put("StopDate", stopDate);// 要求民國年

				requireParams.put("CN", reqVO.getCn());
				requireParams.put("ApplySource", "S");
				requireParams.put("PlainText", reqVO.getPlainText());
				requireParams.put("P1Sign", reqVO.getP1Sign());
				requireParams.put("Channel", PropUtils.getApiChannel());
				requireParams.put("ClientIP", reqVO.getClientIp());
				resp15 = stockLotteryAPIFeignService.getRB15(requireParams);
				break;
			}
		}

		if (resp15.get("ResultCode").equals("0000")) {
			botResponseUtils.setTextResult(botMessageVO, "申購成功");

			// 初始分組
			ContentItem content = botMessageVO.new ContentItem();
			content.setType(19);

			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
			Map<String, String> param_12 = new HashMap<>();
			param_12.put("FunctionID", "RB12");
			param_12.put("Channel", PropUtils.getApiChannel());
			param_12.put("Account", reqVO.getAccount());
			param_12.put("BranchCode", reqVO.getBranchCode());

			Map<String, Object> resp12 = stockLotteryAPIFeignService.getRB12(param_12);

			// 牌卡
			CardItem cards = botMessageVO.new CardItem();
			List<Map<String, Object>> cTextsList = new ArrayList<>();

			// 組底下資訊
			cards.setCLinkType(8);
			cards.setCTitle("費用詳情");

			Map<String, Object> m = new HashMap<>();
			
			String mStr = "";
			
			mStr += "申購金額";
			mStr += "<br />";

			mStr += item;
			m.put("cText", mStr);
			cTextsList.add(m);

			cards.setCTexts2(cTextsList);

			// 組牌卡-------

			// 圖
			CImageDataItem cImageData = botMessageVO.new CImageDataItem();
			cImageData.setCImageUrl("card333.png");
			cImageData.setCImageTextType(1);
			cards.setCImageData(cImageData.getCImageDatas());// 組裝起來

			// 中間
			List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

			cards.setCTextType("1");
			cards.setCWidth("segment secard");

			CTextItem cText = botMessageVO.new CTextItem();

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("股票名稱");
			cText.setCText(symbolName);
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("張數");
			cText.setCText(share + "張");
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("證券帳戶");
			cText.setCText(MapUtils.getString(resp12, "Bankno"));
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("銀行帳戶餘額");
			cText.setCText(DataUtils.handleMoney(MapUtils.getString(resp12, "Balance")) + "元");
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("申購金額");
			cText.setCText(DataUtils.handleMoney(advAmount) + "元");
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("扣款日期");
			cText.setCText(debitDate);
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cText = botMessageVO.new CTextItem();
			cText.setCLabel("抽籤日期");
			cText.setCText(drawDate);
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來

			cardList.add(cards.getCards());

			content.setCards(cardList);

			botMessageVO.getContent().add(content.getContent());

			QuickReplay q1 = SecQuickReplay.StillNeedLottery;

			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, q1, SecQuickReplay.MyComingSoon,
					SecQuickReplay.INIT);

			tpiGatewayResult.setMsgList(Arrays.asList(q1.getText()));
			tpiGatewayResult.setOptionList(Arrays.asList(q1));

		} else {
			botResponseUtils.setTextResult(botMessageVO, "好像發生了一些錯誤..." + resp15.get("ResultMessage"));
		}

		return tpiGatewayResult.setData(botMessageVO);
	}

	/**
	 * 抽籤樹精靈
	 * 
	 * @param vo
	 * @param cards
	 * @param map
	 * @param status
	 * @param type
	 * @return
	 */
	private CardItem storckTreeCard(BotMessageVO vo, CardItem cards, Map<String, Object> map, String status,
			String type, String statusIdx) {
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB);
		cImageData.setCImageUrl("card37-01.png");
		cImageData.setCImageTextType(7);
		List<Map<String, Object>> cImageTexts = new ArrayList<>();

		if (StringUtils.equals(statusIdx, "2")) {
			CImageTextItem cImageText = vo.new CImageTextItem();
			cImageText.setCImageTextTitle(MapUtils.getString(map, "SymbolName") + " <span style=\"color:#939ca0\">"
					+ MapUtils.getString(map, "Symbol") + "</span>");
			cImageText.setCImageText("抽籤狀態::handhand.png");
			cImageText.setCImageTextTag("Label");
			cImageTexts.add(cImageText.getCImageTexts());

			CImageTextItem cImageText2 = vo.new CImageTextItem();
			cImageText2.setCImageTextTitle("扣款失敗::red");
			cImageText2.setCImageTextTag("data2");
			cImageTexts.add(cImageText2.getCImageTexts());
		} else {
			CImageTextItem cImageText = vo.new CImageTextItem();
			cImageText.setCImageTextTitle(MapUtils.getString(map, "SymbolName") + " <span  style=\"color:#939ca0\">"
					+ MapUtils.getString(map, "Symbol") + "</span>");

//			if (StringUtils.isBlank(status)) {
//				statusDate = "截止日期";
//			} else if (StringUtils.equals(status, "未中籤") || StringUtils.equals(status, "已申購")) {
//				statusDate = "抽籤日期";
//			} else if (StringUtils.equals(status, "已得標")) {
//				statusDate = "撥券日期";
//			} else if (StringUtils.equals(status, "將退款/已退款紀錄")) {
//				statusDate = "退款日期";
//			} else {
//				statusDate = status + "日期";
//			}

			String statusDate = "";

			if (StringUtils.isBlank(statusIdx)) {
				statusDate = "截止日期";
			} else if (StringUtils.equals(statusIdx, "1")) {
				statusDate = "扣款日";
			} else if (StringUtils.equals(statusIdx, "3")) {
				statusDate = "抽籤日";
			} else if (StringUtils.equals(statusIdx, "4")) {
				statusDate = "退款日";
			} else if (StringUtils.equals(statusIdx, "5")) {
				statusDate = "撥券日";
			} else {
				statusDate = status;
			}

			cImageText.setCImageText(statusDate + "::handhand.png");
			cImageText.setCImageTextTag("Label");
			cImageTexts.add(cImageText.getCImageTexts());

			String stockDate = StringUtils.equals(type, "combination") ? MapUtils.getString(map, "StopDate")
					: MapUtils.getString(map, "StatusDate");

			CImageTextItem cImageText2 = vo.new CImageTextItem();
			cImageText2.setCImageTextTitle(stockDate.substring(5));

			if (StringUtils.equals(statusIdx, "4") || StringUtils.equals(type, "combination")) {
				cImageText2.setCImageTextTag("data");
				cImageTexts.add(cImageText2.getCImageTexts());

				int days = countdownDays(stockDate);
				if (days > 0) {
					CImageTextItem cImageText3 = vo.new CImageTextItem();
					cImageText3.setCImageText("倒數" + days + "天");
					cImageText3.setCImageTextTag("thatDay2");
					cImageTexts.add(cImageText3.getCImageTexts());
				}

			} else {
				cImageText2.setCImageTextTag("data2");
				cImageTexts.add(cImageText2.getCImageTexts());
			}
		}

		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());// 組裝起來
		return cards;
	}

	private Integer countdownDays(String endDate) {
		if (StringUtils.isNotBlank(endDate)) {
			DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDate date = LocalDate.now();
			LocalDate stopDate = LocalDate.parse(endDate, pattern);
			Period period = Period.between(date, stopDate);
			return (period.getDays() + 1);
		}
		return null;
	}

}
