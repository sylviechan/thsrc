package com.tp.tpigateway.modules.cathaysec.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.cathaysec.SecuritiesRequest;

import java.util.Map;

public interface SecuritiesService {

	BotMessageVO askLogin(SecuritiesRequest reqVO);

	/**
	 * For 熱門股:牌卡(RB21)
	 * 
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	BotMessageVO rb21QueryCard(SecuritiesRequest reqVO);

	BotMessageVO choose(SecuritiesRequest reqVO);
	
	/**
	 * For 熱門股:浮動視窗(RB21)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB21(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 庫存查詢(RB25)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB25(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 自選查詢(RB26)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB26(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 自選查詢(RB26)明細
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO showRB26Detail(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 證券委託回報彙總查詢(當盤有效委託)(RB50)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB50(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 證券成交回報彙總查詢(RB51)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB51(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 證券行情查詢(RB52)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB52(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 個股報價、大盤報價(RB52)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB52Card(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 交割款查詢(RB53)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB53(SecuritiesRequest reqVO, Map<String, String> param);
	
	/**
	 * For 我要轉帳
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO iNeedMoneyTransfer(SecuritiesRequest reqVO);

}
