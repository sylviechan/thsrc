package com.tp.tpigateway.modules.cathaysec.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.tp.common.mybatis.entity.BaseEntity;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author matt
 * @since 2019-09-04
 */
public class StockData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    private String stockId;

    private String stockName;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public Long getId() {
        return id;
    }

    public StockData setId(Long id) {
        this.id = id;
        return this;
    }
    public String getStockId() {
        return stockId;
    }

    public StockData setStockId(String stockId) {
        this.stockId = stockId;
        return this;
    }
    public String getStockName() {
        return stockName;
    }

    public StockData setStockName(String stockName) {
        this.stockName = stockName;
        return this;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public StockData setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public StockData setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "StockData{" +
        "id=" + id +
        ", stockId=" + stockId +
        ", stockName=" + stockName +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
