package com.tp.tpigateway.modules.cathaysec.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "securitiesapi", url = "${api.secfut.path}/")
@TpSysLog(remark = "SecuritiesAPIFeignService")
public interface SecuritiesAPIFeignService {

	@RequestMapping(value = "soms/query/orderSummary", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB50(@RequestParam Map<String, String> map);
	
	@RequestMapping(value = "soms/query/execuSummary", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB51(@RequestParam Map<String, String> map);
	
	@RequestMapping(value = "soms/trading/quote", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB52(@RequestParam Map<String, String> map);
	
	@RequestMapping(value = "soms/query/invoi", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB53(@RequestParam Map<String, String> map);
	
}
