package com.tp.tpigateway.modules.cathaysec.fegin;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tp.common.annotation.TpSysLog;

@FeignClient(name = "loginapi", url = "${api.sec.path}/AFaService/")
@TpSysLog(remark = "LoginOtherAPIFeignService")
public interface LoginAPIFeignService {

	@RequestMapping(value = "RB28.svc/RB28Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> loginForOtherDevice(Map<String, String> json);
}
