package com.tp.tpigateway.modules.cathaysec.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.SecuritiesRequest;
import com.tp.common.util.DateUtils;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.service.SecuritiesService;

@RestController
@RequestMapping("/Securities")
public class SecuritiesController extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(SecuritiesController.class);
	private static final String SOURCE = "securities";

	@Autowired
	SecuritiesService securitiesService;

	@RequestMapping(value = "/cmd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult chooseCmd(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();
		String cmd = reqVO.getCmd();
		Map<String, Object> result = null;

		try {
			switch (cmd) {
			case "rb21_query1":
				botMessageVO = securitiesService.rb21QueryCard(reqVO);
				break;
			case "rb50_query1":
				break;
			}
		} finally {
			logger.error("[" + SOURCE + "_cmd(" + reqVO.getCmd() + ")] total cost time = "
					+ (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 選擇報價類型
	@RequestMapping(value = "/choose", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult choose(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = securitiesService.choose(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 詢問登入
	@RequestMapping(value = "/askLogin", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult askLogin(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = securitiesService.askLogin(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 熱門股
	@RequestMapping(value = "/RB21", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB21(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB21");
			// requireParams.put("Date", reqVO.getDate());
			requireParams.put("Date", DateUtils.format(new Date(), DateUtils.DATE_SLASH_PATTERN));
			requireParams.put("Channel", reqVO.getChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB21(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB21] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	/** 庫存查詢(RB25) */
	@RequestMapping(value = "/RB25", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB25(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB25");
			requireParams.put("ID", reqVO.getId());
			requireParams.put("BranchCode", reqVO.getBranchCode());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB25(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB25] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	/** 自選查詢(RB26) */
	@RequestMapping(value = "/RB26", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB26(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB26");
			requireParams.put("ID", reqVO.getId());
			requireParams.put("Channel", PropUtils.getApiChannel());
			requireParams.put("IP", reqVO.getIp());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB26(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB26] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	/** 自選查詢(RB26)明細 */
	@RequestMapping(value = "/showRB26Detail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult showRB26Detail(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB26");
			requireParams.put("ID", reqVO.getId());
			requireParams.put("Channel", PropUtils.getApiChannel());
			requireParams.put("IP", reqVO.getIp());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.showRB26Detail(reqVO, requireParams);
			}

		} finally {
			logger.error("[" + SOURCE + "_showRB26Detail] total cost time = "
					+ (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 證券委託回報
	@RequestMapping(value = "/RB50", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB50(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB50");
			requireParams.put("BrokerID", reqVO.getBranchCode());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("Channel", reqVO.getChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB50(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB50] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 證券成交回報
	@RequestMapping(value = "/RB51", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB51(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB51");
			requireParams.put("BrokerID", reqVO.getBranchCode());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("Channel", reqVO.getChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB51(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB51] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/RB52", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB52(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB52");
			requireParams.put("Symbol", reqVO.getSymbol());
			requireParams.put("Channel", reqVO.getChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB52(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB52] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// RB52 個股報價、大盤報價
	@RequestMapping(value = "/RB52Card", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB52Card(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB52");
			requireParams.put("Channel", reqVO.getChannel());
			requireParams.put("Symbol", reqVO.getSymbol());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB52Card(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB52] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 證券交割查詢
	@RequestMapping(value = "/RB53", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult RB53(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = null;
		long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;

		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB53");
			requireParams.put("BrokerID", reqVO.getBranchCode());
			requireParams.put("BranchDesc", reqVO.getBranchDesc());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("Channel", reqVO.getChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				botMessageVO = securitiesService.getRB53(reqVO, requireParams);
			}

		} finally {
			logger.error(
					"[" + SOURCE + "_RB53] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 我要轉帳
	@RequestMapping(value = "/INeedMoneyTransfer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult iNeedMoneyTransfer(@RequestBody SecuritiesRequest reqVO) {
		BotMessageVO botMessageVO = securitiesService.iNeedMoneyTransfer(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}
}
