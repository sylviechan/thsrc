package com.tp.tpigateway.modules.cathaysec.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysec.RegularRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.service.RegularService;


@RestController
@RequestMapping("/Regular")
public class RegularController extends BaseController{
	private static Logger logger = LoggerFactory.getLogger(RegularController.class);
	private static final String SOURCE = "regular";
	
	@Autowired
	RegularService regularService;
	
	@RequestMapping(value="/RB01", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB01(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
		String sipFlag = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB01");
			requireParams.put("BranchCode", reqVO.getBranchCode());
			requireParams.put("ID", reqVO.getId());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("Symbol", reqVO.getSymbol());
			requireParams.put("ChargeAmt", reqVO.getChargeAmt());
			requireParams.put("ChargeDay", reqVO.getChargeDay());
			requireParams.put("CN", reqVO.getCn());
			requireParams.put("PlainText", reqVO.getPlainText());
			requireParams.put("P1Sign", reqVO.getP1Sign());
			requireParams.put("CaNum", reqVO.getCaNum());
			requireParams.put("PlainTextUnCode", reqVO.getPlainTextUnCode());						
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());
			
			// 必要參數檢核
			if (checkParams(requireParams)) {					
				result = regularService.getRB01(reqVO, requireParams);
				botMessageVO = (BotMessageVO) result.get("botMessageVO");
				sipFlag = result.get("sipFlag").toString();
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB01] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO).put("sipFlag", sipFlag);
    }
	
	@RequestMapping(value="/RB27", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB27(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;		
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB27");
			requireParams.put("BranchCode", reqVO.getBranchCode());
			requireParams.put("ID", reqVO.getId());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("Symbol", reqVO.getSymbol());
			requireParams.put("ChargeAmt", reqVO.getChargeAmt());
			requireParams.put("ChargeDay", reqVO.getChargeDay());
			requireParams.put("CN", reqVO.getCn());
			requireParams.put("PlainText", reqVO.getPlainText());
			requireParams.put("P1Sign", reqVO.getP1Sign());
			requireParams.put("CaNum", reqVO.getCaNum());
			requireParams.put("PlainTextUnCode", reqVO.getPlainTextUnCode());						
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());
			
			requireParams.put("SignDate", reqVO.getSignDate());
			requireParams.put("CType", "D6");	// 定期定額固定帶D6
			
			// 必要參數檢核
			if (checkParams(requireParams)) {									
				result = regularService.getRB27(reqVO, requireParams);
				botMessageVO = (BotMessageVO) result.get("botMessageVO");
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB27] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB02", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB02(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB02");			
			requireParams.put("UDID", reqVO.getUdid());
			requireParams.put("Channel", PropUtils.getApiChannel());
			
			// 必要參數檢核
			if (checkParams(requireParams)) {
				requireParams.put("ID", reqVO.getId());	// 可為空值				
				
				botMessageVO = regularService.getRB02(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB02] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB03", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB03(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
		String sipFlag = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();			
			requireParams.put("FunctionID", "RB03");
			requireParams.put("BranchCode", reqVO.getBranchCode());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				result = regularService.getRB03(reqVO, requireParams);
				botMessageVO = (BotMessageVO) result.get("botMessageVO");
				sipFlag = result.get("sipFlag").toString();
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB03] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO).put("sipFlag", sipFlag);
    }
			
	@RequestMapping(value="/RB10", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB10(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB10");
			requireParams.put("Symbol", reqVO.getSymbol());
			requireParams.put("Channel",PropUtils.getApiChannel());	
			
			// 必要參數檢核
			if (checkParams(requireParams)) {				
				botMessageVO = regularService.getRB10(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB10] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB11", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB11(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB11");
			requireParams.put("BranchCode", reqVO.getBranchCode());
			requireParams.put("Account", reqVO.getAccount());
			requireParams.put("Symbol", reqVO.getSymbol());
			requireParams.put("Channel", PropUtils.getApiChannel());	
			
			// 必要參數檢核
			if (checkParams(requireParams)) {				
				botMessageVO = regularService.getRB11(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB11] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB12", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB12(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB12");
			requireParams.put("BranchCode", reqVO.getBranchCode());
			requireParams.put("Account", reqVO.getAccount());			
			requireParams.put("Channel", PropUtils.getApiChannel());	
			
			// 必要參數檢核
			if (checkParams(requireParams)) {				
				botMessageVO = regularService.getRB12(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB12] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB17", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB17(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB17");			
			requireParams.put("Channel", PropUtils.getApiChannel());
			requireParams.put("Kind", reqVO.getKind());
			
			// 必要參數檢核
			if (checkParams(requireParams)) {				
				botMessageVO = regularService.getRB17(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB17] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB18", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB18(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB18");			
			requireParams.put("Symbol", reqVO.getSymbol());
//			requireParams.put("ChargeDay", reqVO.getChargeDay());
//			requireParams.put("Amount", reqVO.getAmount());
			
			// 改成試算(輸入金額日期之前, 所以購買金額和日期的值寫死)
			requireParams.put("ChargeDay", "6");
			requireParams.put("Amount", "3000");	
			requireParams.put("Channel",PropUtils.getApiChannel());
			
			// 必要參數檢核
			if (checkParams(requireParams)) {				
				botMessageVO = regularService.getRB18(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB18] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB19", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB19(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB19");
			requireParams.put("BranchCode", reqVO.getBranchCode());
			requireParams.put("Account", reqVO.getAccount());			
			requireParams.put("Channel", PropUtils.getApiChannel());
			
			// 必要參數檢核
			if (checkParams(requireParams)) {	
				requireParams.put("Sort", reqVO.getSort());
				requireParams.put("Status", reqVO.getStatus());
				
				botMessageVO = regularService.getRB19(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB19] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB22", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB22(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB22");
			requireParams.put("Channel", PropUtils.getApiChannel());
			
			// 未登入無使用者資訊
			reqVO.setId(StringUtils.defaultString(reqVO.getId(), "none"));
			reqVO.setUdid(StringUtils.defaultString(reqVO.getUdid(), "none"));
			
			// 必要參數檢核
			if (checkParams(requireParams)) {	
				requireParams.put("UDID", reqVO.getUdid());
				botMessageVO = regularService.getRB22(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB22] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/RB23", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB23(@RequestBody RegularRequest reqVO) {        
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
		Map<String, Object> result = null;
        
		try {			
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("FunctionID", "RB23");
			requireParams.put("ID", reqVO.getId());
			requireParams.put("UDID", reqVO.getUdid());
			requireParams.put("Question", reqVO.getQuestion());
			requireParams.put("Answer", reqVO.getAnswer());			
			requireParams.put("Channel", PropUtils.getApiChannel());
			
			// 必要參數檢核
			if (checkParams(requireParams)) {				
				botMessageVO = regularService.getRB23(reqVO, requireParams);
			}
			
		} finally {
			logger.error("[" + SOURCE + "_RB23] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
		}

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

	@RequestMapping(value="/rb02_buy_ok", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult rb02BuyOk(@RequestBody RegularRequest reqVO) {
		return regularService.rb02BuyOk(reqVO);
	}

    @RequestMapping(value="/rb02_check", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult rb02_check(@RequestBody FallBackRequest reqVO) {
        return regularService.rb02Check(reqVO);
    }

	@RequestMapping(value="/cmd", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult cmd(@RequestBody RegularRequest reqVO) {        
		return regularService.chooseCmd(reqVO);
    }
	
	//抽籤定期定額相關-WEB動作
	@RequestMapping(value="/WebCmd", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult WebCmd(@RequestBody RegularRequest reqVO) {        
		return regularService.chooseWebCmd(reqVO);
    }
}
