package com.tp.tpigateway.modules.cathaysec.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.RegularRequest;
import com.tp.common.model.cathaysec.StockLotterRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysec.service.StockLotteryService;

/*
   股票抽籤
 */

@RestController
@RequestMapping("/StockLottery")
public class StockLotteryController extends BaseController {

	@Autowired
	StockLotteryService stockLotteryService;

	@RequestMapping(value = "/related", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult related(@RequestBody StockLotterRequest reqVO) {
		String cmd = reqVO.getCmd();
		BotMessageVO botMessageVO = null;

		// 股票抽籤(流程圖階段一) RB13 RB14
		if (cmd.equals("StockProcessKey")) {
			
			botMessageVO = stockLotteryService.stockLottery(reqVO);
			//botMessageVO = stockLotteryService.confirmWhetherToPurchase1(reqVO);
			// botMessageVO = stockLotteryService.showAllCanStockPurchase(reqVO);
		}

		// 顯示所有標的(流程圖階段二) RB14
		else if (cmd.equals("StockProcessKey_ShowAllCanStockPurchase")) {
			botMessageVO = stockLotteryService.showAllCanStockPurchase(reqVO);
		}

		// 顯示所有標的(流程圖階段三) 確認是否申購 {組1} 否，我再想想 / 是，我要購買
		else if (cmd.equals("StockProcessKey_ShowAllCanStockPurchase1")) {
			return stockLotteryService.confirmWhetherToPurchase1(reqVO);
		}

		// 顯示所有標的(流程圖階段三) 確認是否申購 {組2} 顯示所有抽籤的標 / 看阿發還能幫什麼忙
		else if (cmd.equals("StockProcessKey_ShowAllCanStockPurchase2")) {
			botMessageVO = stockLotteryService.confirmWhetherToPurchase2(reqVO);
		}

		// 看我的抽籤紀錄 RB16
		else if (cmd.equals("StockProcessKey_DoingStock")) {
			botMessageVO = stockLotteryService.doingStock(reqVO);
		}

		// 我的即將到來 RB16
		else if (cmd.equals("StockProcessKey_CommingStock")) {
			botMessageVO = stockLotteryService.commingStock(reqVO);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	//抽籤相關-WEB動作
	@RequestMapping(value="/WebCmd", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult WebCmd(@RequestBody StockLotterRequest reqVO) {        
		return stockLotteryService.chooseWebCmd(reqVO);
    }
	
	// 看我的抽籤紀錄-查看詳情
	@RequestMapping(value = "/doingStockDetails", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult doingStockDetails(@RequestBody StockLotterRequest reqVO) {
		BotMessageVO botMessageVO = null;

		botMessageVO = stockLotteryService.doingStockDetails(reqVO);

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// RB15購買股票流程
	@RequestMapping(value = "/buyStock", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult buyStock(@RequestBody StockLotterRequest reqVO) {
		return stockLotteryService.buyStock(reqVO);
	}

}
