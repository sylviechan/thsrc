package com.tp.tpigateway.modules.cathaysec.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSON;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CLinkListItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.cathaysec.DefaultRequest;
import com.tp.common.model.cathaysec.RegularRequest;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysec.fegin.OtherAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.model.SecQuickReplay;
import com.tp.tpigateway.modules.cathaysec.service.OtherService;

@Service("otherService")
public class OtherServiceImpl implements OtherService {
    private static Logger logger = LoggerFactory.getLogger(OtherServiceImpl.class);

    @Autowired
    BotResponseUtils botResponseUtils;

    @Autowired
    OtherAPIFeignService otherAPIFeignService;

    @Override
    public BotMessageVO chooseCmd(DefaultRequest reqVO) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        String cmd = getString(reqVO.getCmd()).toLowerCase();

        switch (cmd) {
            //RB20-新手上路 顯示文字訊息
            case "rb20_1":
                botResponseUtils.setTextResult(vo, "登入後阿發可以給你個人化新手上路唷！");
                botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.RB20_1);
                break;
            case "rb20_firstbuy":
                vo = rb20_firstbuy(reqVO);
                break;
            case "rb20_stockloan":
                botResponseUtils.setTextResult(vo, "TODO [FAQ] 股票貸款是什麼");
                break;
            case "rb20_whencanbuy":
                botResponseUtils.setTextResult(vo, "TODO [FAQ] 什麼時候可以買賣股票？");
                break;
            case "rb20_howmuch":
                botResponseUtils.setTextResult(vo, "TODO [FAQ] 股票每張多少錢？");
                break;
            case "rb20_howtochoose":
                botResponseUtils.setTextResult(vo, "TODO [FAQ] 如何選擇股票");
                break;
            case "rb20_queryaccount":
                botResponseUtils.setTextResult(vo, "TODO 查詢證券帳戶及銀行餘額");
                break;
            default:
                botResponseUtils.setTextResult(vo, "還有什麼需要阿發為你服務的呢？");
                break;
        }

        return vo;

    }

    @Override
    public Map<String, Object> getRB20(RegularRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = otherAPIFeignService.getRB20(JSON.toJSONString(param));
        Map<String, Object> returnResult = new HashMap<>();

        if (result != null) {
            String resultCode = getString(result.get("ResultCode"));

            if (resultCode.equals("0000")) {
                //StringUtils.isNotBlank(param.get("functionType"))判斷是否為阿發招呼語
                if (StringUtils.isNotBlank(param.get("functionType")) && StringUtils.equals((String) result.get("IsProvide"), "Y")) {
                    vo = null;
                    returnResult.put("botMessageVO", vo);
                    return returnResult;
                }

                List<Map<String, Object>> accountList = (List<Map<String, Object>>) result.get("Accounts");
                List<Map<String, Object>> dataList = new ArrayList<>();
                Map<String, Object> data = new HashMap<>();
                if (resultCode.equals("0000")) {
                    List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
                    List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

                    //傳回去給flow記住
                    Map<String, Object> accountDataMap = new HashMap<>();

                    for (Map<String, Object> account : accountList) {
                        String branchName = getString(account.get("BranchName"));
                        String accountNum = getString(account.get("Bhno")) + "-" + getString(account.get("Cseq"));

                        String telName = "數位服務專員/分公司電話";
                        String tel = getString(account.get("AEPhone"));

                        String cusStatus = getString(result.get("CusStatus"));
                        //測試階段，根據點選的快速回覆按鈕不同，回傳不同的新手上路
 	 	 	 			/*if(reqVO.getCmd().indexOf("A1") > -1) {
 	 	 	 				cusStatus = "A1";
 	 	 	 			} else if(reqVO.getCmd().indexOf("A2") > -1) {
 	 	 	 				cusStatus = "A2";
 	 	 	 			} else if(reqVO.getCmd().indexOf("A3") > -1) {
 	 	 	 				cusStatus = "A3";
 	 	 	 			}*/
                        String isProvide = getString(result.get("IsProvide"));

                        String bankStr = "銀行交割帳戶(國泰世華)";
                        String bankName = getString(account.get("BankAccount"));

                        String tradeLimit = "單日交易門檻";
                        String tradeLimitNum = getString(account.get("Ivam")) + "萬";

                        //資料暫存
                        data.put("account", bankName);
                        data.put("phone", tel);
                        dataList.add(data);

                        Map<String, Object> cImageData = new HashMap<String, Object>();
                        cImageData.put("cImageUrl", "openaccount.png");
                        cImageData.put("cImage", BotMessageVO.IMG_CB);
                        cImageData.put("cImageTextType", 1);
                        CardItem cards = vo.new CardItem();
                        cards.setCImageData(cImageData);
                        cards.setCName("");
                        cards.setCWidth(BotMessageVO.CWIDTH_250);
                        cards.setCTextType("3");
                        cards.setCLinkType(1);

                        String[] label = {branchName, telName, bankStr, tradeLimit};
                        String[] text = {"帳號" + accountNum, tel, bankName, tradeLimitNum};
                        for (int i = 0; i < label.length; i++) {
                            CTextItem cText = vo.new CTextItem();
                            cText.setCLabel(label[i]);
                            cText.setCText(getString(text[i]));
                            cTextList.add(cText.getCTexts());
                        }

                        cards.setCTexts(cTextList);

 	 					/*
							"Accounts": [
								{
									"AEPhone": "02-77326889",
									"BankAccount": "營業部699500153915",
									"Bhno": "8888",
									"BranchName": "敦南分公司",
									"Cseq": "0002196",
									"Ivam": ""
								}
							],
 	 					 */

 	 					//丟回給flow紀錄
                        Map<String, String> accountData = new HashMap<>();
                        accountData.put("account", getString(account.get("Cseq")));
                        accountData.put("branchCode", getString(account.get("Bhno")));
                        accountData.put("phone", getString(account.get("AEPhone")));
                        accountDataMap.put(cusStatus, accountData);

                        //埋cusStatus
                        Map<String, Object> parameter = new HashMap<>();
                        parameter.put("cusStatus", cusStatus);

                        if (StringUtils.equals(cusStatus, "A1")) {

                            SecQuickReplay[] link1s = {
                                    new SecQuickReplay(2, "證券帳號可以做什麼 ？", "證券帳號可以做什麼", "新手上路", "rb20_action1", parameter),
                                    new SecQuickReplay(2, "怎麼買股票 ？", "怎麼買股票", "新手上路", "rb20_action2", parameter),
                                    new SecQuickReplay(2, "股款怎麼匯 ？", "股款怎麼匯", "新手上路", "rb20_action3", parameter)
                            };

                            cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));
                        } else if (StringUtils.equals(cusStatus, "A2")) {
                            SecQuickReplay[] link1s = {
                                    new SecQuickReplay(2, "本公司提供的加值服務", "本公司提供的加值服務", "新手上路", "rb20_action4", parameter),
                                    new SecQuickReplay(2, "怎麼做定期定額", "怎麼做定期定額", "新手上路", "rb20_action5", parameter),
                                    new SecQuickReplay(2, "和阿發聊聊天", "和阿發聊聊天", "阿發問你", "", parameter)
                            };

                            cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));
                        } else if (StringUtils.equals(cusStatus, "A3")) {
                            SecQuickReplay[] link1s = {
                                    new SecQuickReplay(2, "本公司提供的加值服務", "本公司提供的加值服務", "新手上路", "rb20_action4", parameter),
                                    new SecQuickReplay(2, "如何提高額度", "如何提高額度", "新手上路", "rb20_action7", parameter),
                                    new SecQuickReplay(2, "手續費折扣", "手續費折扣", "新手上路", "rb20_action8", parameter)
                            };

                            cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));
                        }

                        cardList.add(cards.getCards());
                    }

                    // 牌卡
                    botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
                    if (vo.getContent().size() > 0) {
                        vo.getContent().get(0).put("dataList", dataList);
                    }

                    returnResult.put("accountDataMap", accountDataMap);

                } else {
                    String replyText = "RB20 api error(" + resultCode + ")";
                    botResponseUtils.setTextResult(vo, replyText);
                }
            } else {
                String replyText = "RB20 api error(" + resultCode + ")";
                botResponseUtils.setTextResult(vo, replyText);
            }
        } else {
            String replyText = "RB20 api error(null)";
            botResponseUtils.setTextResult(vo, replyText);
        }

        returnResult.put("botMessageVO", vo);

        return returnResult;
    }

    @Override
    public BotMessageVO getRB20Action(RegularRequest reqVO) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        if (StringUtils.isNotBlank(reqVO.getCmd())) {

            String account = reqVO.getAccount();
            String branchCode = reqVO.getBranchCode();
            String phone = reqVO.getPhone();

            switch (reqVO.getCmd()) {
                case "rb20_action1":
                    botResponseUtils.setTextResult(vo, "當你持有證券帳戶後，可以開始買股票，也可以定期定額存股唷！<a href=\"javascript:void(0)\" onclick=\"chatWidge(this,'getStockInfo');\">了解更多股票基本知識</a>");
                    break;
                case "rb20_action2":
                    botResponseUtils.setTextResult(vo, "阿發向你推薦2種買股票的方式：");
                    botResponseUtils.setTextResult(vo, "（1）若你要一般股票交易，可使用樹精靈APP~【<a href=\"javascript:void(0)\" onclick=\"window.open('https://webap.cathaysec.com.tw/General/OSU.html');\">立即開啟</a>】");
                    botResponseUtils.setTextResult(vo, "（2）若你要定期定額存股，我來幫你～<a href=\"javascript:void(0)\" onclick=\"App.sendMsg('定期定額')\">立即申購</a>");
                    break;
                case "rb20_action3":
                    botResponseUtils.setTextResult(vo, "若股票已成交，隔日務必確認銀行交割帳戶存款足夠唷！");
                    botResponseUtils.setTextResult(vo, "若不處理將導致違約，影響日後的信用～<a href=\"javascript:void(0)\" onclick=\"chatWidge(this,'queryAccountInfo', ['" + account +"', '" + branchCode + "'])\">查詢銀行交割帳戶及餘額</a>");
                    break;
                case "rb20_action4":
                    botResponseUtils.setTextResult(vo, "我們提供多種線上服務～");
                    botResponseUtils.setTextResult(vo, "（1）選股：協助你篩選出標的～快選App【<a href=\"javascript:void(0)\" onclick=\"window.open('https://webap.cathaysec.com.tw/General/QuickApp.html');\">立即下載</a>】");
                    botResponseUtils.setTextResult(vo, "（2）貸款：有庫存即可申貸，立即撥款解決資金缺口～<a href=\"javascript:void(0)\" onclick=\"App.showUserMsg('股票貸款是什麼');App.sendMsgToChatWeb('rb20_stockloan');\">了解更多</a>");
                    botResponseUtils.setTextResult(vo, "（3）簽屬：還沒簽風險預告書嗎？簽屬立即生效～【<a href=\"javascript:void(0)\" onclick=\"App.showMsg(App.getTxtData('請稍等～現在帶你過去簽'));launchPage('ESERVICE','0,Si,,,,,');\">立即簽屬</a>】");
                    break;
                case "rb20_action5":
                    botResponseUtils.setTextResult(vo, "每月投資1,000元，即可成為台積電或鴻海股東，輕鬆儲蓄你的夢想～<a href=\"javascript:void(0)\" onclick=\"App.sendMsg('定期定額')\">立即申購</a>");
                    break;
                case "rb20_action7":
                    botResponseUtils.setTextResult(vo, "若您僅需100萬內額度，可電洽數位服務專員：<a href=\"tel:" + phone + "\">" + phone + "</a>");
                    botResponseUtils.setTextResult(vo, "若您需要更高的額度，請攜帶雙證件臨櫃任一<a href=\"javascript:void(0)\" onclick=\"window.open('https://www.cathaysec.com.tw/mobile/Branches.aspx');\">營業據點</a>辦理");
                    break;
                case "rb20_action8":
                    botResponseUtils.setTextResult(vo, "如果想申請折扣，可以親臨國泰證券任一分公司或者聯絡營業員申請唷！");

                    List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
                    List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();

                    Map<String, Object> cImageData = new HashMap<String, Object>();
                    cImageData.put("cImageUrl", "card02.png");
                    cImageData.put("cImage", BotMessageVO.IMG_CC);
                    cImageData.put("cImageTextType", 1);
                    CardItem cards = vo.new CardItem();
                    cards.setCImageData(cImageData);
                    cards.setCName("");
                    cards.setCWidth(BotMessageVO.CWIDTH_200);
                    cards.setCTextType("3");
                    cards.setCLinkType(1);

                    String[] label = {"交割銀行帳戶", "專屬優惠"};
                    String[] text = {account, "手續費65折"};

                    for (int i = 0; i < label.length; i++) {
                        CTextItem cText = vo.new CTextItem();
                        cText.setCLabel(label[i]);
                        cText.setCText(getString(text[i]));
                        cTextList.add(cText.getCTexts());
                    }

                    cards.setCTexts(cTextList);

                    SecQuickReplay[] link1s = {
                            new SecQuickReplay(4, "查詢營業據點", "查詢營業據點", "", "https://www.cathaysec.com.tw/mobile/Branches.aspx", ""),
                            new SecQuickReplay(12, "打給數位服務專員／分公司電話", phone, "", "", "")
                    };

                    cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));

                    cardList.add(cards.getCards());

                    // 牌卡
                    botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
                    break;
            }
        }


        return vo;
    }

    public BotMessageVO rb20_firstbuy(DefaultRequest reqVO) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();

        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
        CardItem cards = vo.new CardItem();
        cards.setCName("");
        cards.setCWidth(BotMessageVO.CWIDTH_200);
        cards.setCTextType("3");
        cards.setCLinkType(1);

        // 牌卡圖片
        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CC);
        cImageData.setCImageUrl("card06.png");
        cImageData.setCImageTextType(1);
        cards.setCImageData(cImageData.getCImageDatas());

        List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
        CTextItem cText = vo.new CTextItem();
        cText.setCLabel("第一次買股票就上手");
        cText.setCText("這裡有各種疑難雜症的傳送門～");
		/*
		cText.setCText("這裡有各種疑難雜症的傳送門～"
				+ "<br /><br />" 
				+ "<a href=\"javascript:void(0)\" onclick=\"App.showUserMsg('什麼時候可以買賣股票');App.sendMsgToChatWeb('什麼時候可以買賣股票');\">什麼時候可以買賣股票</a>"
				+ "<br /><a href=\"javascript:void(0)\" onclick=\"App.showUserMsg('股票每張多少元');App.sendMsgToChatWeb('股票每張多少元');\">股票每張多少元</a>"
				+ "<br /><a href=\"javascript:void(0)\" onclick=\"App.showUserMsg('如何選擇股票');App.sendMsgToChatWeb('如何選擇股票');\">如何選擇股票</a>");
		*/
        cTextList.add(cText.getCTexts());

        SecQuickReplay[] link1s = {
                new SecQuickReplay(2, "什麼時候可以買賣股票？", "什麼時候可以買賣股票"),
                new SecQuickReplay(2, "股票每張多少元？", "股票每張多少元"),
                new SecQuickReplay(2, "如何選擇股票？", "如何選擇股票")
        };

        cards.setCLinkList(this.getCLinkList(vo, Arrays.asList(link1s)));

        cards.setCTexts(cTextList);
        cardList.add(cards.getCards());

        botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

        return vo;
    }

    public String getString(Object str) {
        String returnStr = "";
        if (str != null) {
            returnStr = str.toString();
        }

        return returnStr;
    }

    private List<Map<String, Object>> getCLinkList(BotMessageVO vo, List<SecQuickReplay> links) {
        List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
        for (SecQuickReplay link : links)
            cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());
        return cLinkContentList;
    }

    private CLinkListItem getCLinkListItem(BotMessageVO vo, SecQuickReplay link) {
        return botResponseUtils.getCLinkListItem(vo, link);
    }
}
