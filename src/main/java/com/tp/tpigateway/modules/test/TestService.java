package com.tp.tpigateway.modules.test;

import java.util.Map;

/**
* @author wj
* @version 建立時間:Dec 27, 2019 11:59:49 AM
* 類說明
*/
public interface TestService {

	Map<String,Object> test(Object test);
}
