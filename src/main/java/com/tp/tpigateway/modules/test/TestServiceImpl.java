package com.tp.tpigateway.modules.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tp.tpigateway.modules.thsr.model.BasicFare;
import com.tp.tpigateway.modules.thsr.model.Records;
import com.tp.tpigateway.modules.thsr.model.Station;
import com.tp.tpigateway.modules.thsr.model.TimeTable;
import com.tp.tpigateway.modules.thsr.model.Train;

/**
 * @author wj
 * @version 建立時間:Dec 27, 2019 12:00:53 PM 類說明
 */

@Service("testService")
public class TestServiceImpl implements TestService {

	@Override
	public Map<String,Object> test(Object test) {

		TimeTable timeTable = new TimeTable();
		Train train = new Train();
		List<Station> station = new ArrayList<>();
		Station st = new Station();
		st.setArrTime("06:10:15");
		st.setCode("12");
		st.setDepTime("06:40:50");
		st.setSeq("1");

		station.add(st);

		train.setDirection("0");
		train.setNrCars(3);
		train.setNumber("0202");
		train.setStation(station);
		timeTable.setOpDate("20190931");
		timeTable.setTrain(train);
		
		BasicFare  bf = new BasicFare();
		List<Records>recordList = new ArrayList<>();
		
		Records record = new Records();
		record.setArriveStation("05");
		record.setDepartStation("01");
		record.setEffectedDate("20190101");
		record.setFareId("1");
		record.setFareName("對好商務艙票價");
		record.setPrice("700");
		record.setVersionNo("41");
		
		recordList.add(record);
		
		bf.setRecCount("35");
		bf.setRecords(recordList);
		
		Map resultMap = new HashMap<>();
		resultMap.put("openDate", "20190809");
		resultMap.put("timeTable", timeTable);
		resultMap.put("basicFare",bf);
		
		Map<String,Object>bodyMap = new HashMap<>();
		bodyMap.put("responseCode", "0");
		bodyMap.put("responeseMsg","success");
		bodyMap.put("result", resultMap);
		System.err.println("bodyMap:"+bodyMap.toString());
		return bodyMap;
	}

}
