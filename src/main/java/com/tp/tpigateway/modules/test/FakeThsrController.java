package com.tp.tpigateway.modules.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//wayne
//20191227
@RestController
@RequestMapping("fake")
public class FakeThsrController {
	
	@PostMapping("/lfs/v1/POST/createLostForm")
	public Map<String, Object> createLostForm(@RequestBody String params) {
		return new HashMap<String, Object>(){{
				put("formNo", "L07031599002");
		}};
	}
	
	@PostMapping("/ens/v1/POST/sendSMS")
	public void sendSMS(@RequestBody String params) {
		System.err.println("================> Thsr system has sent SMS.");
	}
}
