package com.tp.tpigateway.modules.test;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.FloatRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.modules.thsr.service.FloatService;

/**
 * @author West
 * @version 建立時間:Dec 27, 2019 11:55:14 AM 類說明
 */

@RestController
@RequestMapping("test")
public class TestController {

	@Autowired
	TestService testService;
	
	@RequestMapping(value = "/hpi",method = RequestMethod.POST)
	public Map<String,Object> test(@RequestBody String json) {
		System.err.println("------------"+json.toString());
		
		return testService.test(json);
	}
}
