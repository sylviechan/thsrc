package com.tp.tpigateway.modules.cathaysite.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.service.SiteInvestmentService;

/*
 *  投資相關
 *  
 */

@RestController
@RequestMapping("cathaysite/investment")
public class SiteInvestmentController extends BaseController{
	
	@Autowired
	SiteInvestmentService siteInvestmentService;
	@Autowired
	BotResponseUtils botResponseUtils;
	
	
	@PostMapping(value="/getQuota",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getQuota(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			
			if(!StringUtils.equals("", reqVO.getType())) {
				String[] typeArray = reqVO.getType().split(",");
				List<Map<String, Object>> mapList = new ArrayList<Map<String,Object>>();
				for(int i=0 ; i < typeArray.length ; i++) {
					if(typeArray[i].equals("3")) {
						requireParams.put("types", typeArray[i] + "," + typeArray[i++]);
					}else{
						requireParams.put("types", typeArray[i]);
					}
					Map<String, Object> obj = new HashMap<String, Object>();
					obj.put("obj", requireParams);
					reqVO.setType(typeArray[i]);
					List<Map<String, Object>> cards = (List<Map<String, Object>>) siteInvestmentService.getQuota(reqVO, obj).getContent().get(2).get("cards");
					mapList.add(cards.get(0));
				}
				botResponseUtils.setCardsResult31(botMessageVO, "card", mapList);
			}else {
				requireParams.put("types", reqVO.getType());
				Map<String, Object> obj = new HashMap<String, Object>();
				obj.put("obj", requireParams);
				botMessageVO = siteInvestmentService.getQuota(reqVO, obj);
			}
		}
		
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	
}
