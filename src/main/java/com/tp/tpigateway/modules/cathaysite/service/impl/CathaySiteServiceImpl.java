package com.tp.tpigateway.modules.cathaysite.service.impl;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.*;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysite.CustomerServiceRequest;
import com.tp.common.model.cathaysite.PwdRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.BotResponseUtils.Widget;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysite.fegin.CtiAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.fegin.CustAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import com.tp.tpigateway.modules.cathaysite.service.CathaySiteService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.tp.common.util.cathaysite.SiteApiUtils.*;

@Service("cathaySiteService")
public class CathaySiteServiceImpl implements CathaySiteService {
	private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    BotResponseUtils botResponseUtils;

    @Autowired
    CtiAPIFeignService ctiAPIFeignService;

	@Autowired
	CustAPIFeignService custAPIFeignService;

	private static final Map<String, QuickReplay> INTENT_QR_MAP;

	static {
		INTENT_QR_MAP = new HashMap<>();
		INTENT_QR_MAP.put("基金", SiteQuickReplay.fund_q3);
		INTENT_QR_MAP.put("密碼", SiteQuickReplay.pwd_q1);
		INTENT_QR_MAP.put("帳務", SiteQuickReplay.account_q2);
		INTENT_QR_MAP.put("開戶", SiteQuickReplay.ecopen_q1);
		INTENT_QR_MAP.put("電子帳單", SiteQuickReplay.bill_q1);
		INTENT_QR_MAP.put("個人投資", SiteQuickReplay.ivn_q1);
	}

    @Override
    public BotMessageVO hello(BotRequest botRequest) {
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());

        botResponseUtils.setTextResult(botMessageVO, "嗨~");

        // 牌卡
        botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getInitCardList(botMessageVO));

        return botMessageVO;
    }

	@Override
	public TPIGatewayResult fallback(FallBackRequest fallBackRequest) {
		BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());

		TPIGatewayResult defaultTpiResult = TPIGatewayResult.ok();
		TPIGatewayResult tpiGatewayResult = null;

		String firstNluCanAnswer = fallBackRequest.getFirstNluCanAnswer();

		if (StringUtils.equals(firstNluCanAnswer, "Y")) { //總機聽的懂, secondBot聽不懂

			String fallbackNlu = StringUtils.defaultString(fallBackRequest.getFallbackNlu(), "");

			defaultTpiResult.put(TPIConst.SECOND_BOT_ASK, "Y");

			//		public static final QuickReplay fund_q1 = new SiteQuickReplay(1, "查詢基金淨值", "查詢基金淨值");
			//		public static final QuickReplay fund_q2 = new SiteQuickReplay(1, "查詢基金配息", "查詢基金配息");
			//		public static final QuickReplay fund_q3 = new SiteQuickReplay(1, "推薦基金", "推薦基金");
			//		public static final QuickReplay fund_q4 = new SiteQuickReplay(1, "申購單筆基金", "申購單筆基金");
			//		public static final QuickReplay fund_q5 = new SiteQuickReplay(1, "新定期定額申請", "新定期定額申請");
			//		public static final QuickReplay fund_q6 = new SiteQuickReplay(1, "新委託交易查詢", "新委託交易查詢");
			//		public static final QuickReplay fund_q7 = new SiteQuickReplay(1, "買回轉申購", "買回轉申購");
			//		public static final QuickReplay account_q1 = new SiteQuickReplay(1, "申請恢復扣款", "申請恢復扣款");
			//		public static final QuickReplay account_q2 = new SiteQuickReplay(1, "約定帳號查詢", "約定帳號查詢");
			//		public static final QuickReplay bill_q1 = new SiteQuickReplay(1, "補發電子帳單", "補發電子帳單");
			//		public static final QuickReplay pwd_q1 = new SiteQuickReplay(1, "補發密碼", "補發密碼");
			//		public static final QuickReplay ivn_q1 = new SiteQuickReplay(1, "查詢我的投資", "查詢我的投資");
			//		public static final QuickReplay ecopen_q1 = new SiteQuickReplay(1, "開戶進度查詢", "開戶進度查詢");

			switch (fallbackNlu) {
				case "基金_操作":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					List<Map<String, Object>> cardList = new ArrayList<>();
					CardItem cards1 = buildBaseCard(vo, "基金", "", SiteQuickReplay.fund_q1, SiteQuickReplay.fund_q2, SiteQuickReplay.fund_q3,
							SiteQuickReplay.fund_q4, SiteQuickReplay.fund_q5, SiteQuickReplay.fund_q6, SiteQuickReplay.fund_q7);
					cardList.add(cards1.getCards());
					botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
					break;
				case "基金_查詢_項目":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.fund_q1, SiteQuickReplay.fund_q2);
					break;
				case "基金_申購_項目":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.fund_q4, SiteQuickReplay.fund_q5);
					break;
				case "密碼_操作":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.pwd_q1);
					break;
				case "帳務_操作":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.account_q1, SiteQuickReplay.account_q1);
					break;
				case "開戶_操作":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.ecopen_q1);
					break;
				case "電子帳單_操作":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.bill_q1);
					break;
				case "個人投資_操作":
					botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
					botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.ivn_q1);
					break;
				default:
					tpiGatewayResult = handleDefaultFallback(fallBackRequest);
					break;
			}
		} else {
			tpiGatewayResult = handleIntentAndFaqCard(fallBackRequest);
		}

		if (tpiGatewayResult != null) {
			return tpiGatewayResult;
		}

		return defaultTpiResult.setData(vo);
	}

	private TPIGatewayResult handleIntentAndFaqCard(FallBackRequest fallBackRequest) {
		BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		String msg = fallBackRequest.getMsg();
		List<Map<String, Object>> firstNluIntents = fallBackRequest.getFirstNluIntents();
		List<Map<String, Object>> faqResult = fallBackRequest.getFaqResult();

		List<Map<String, Object>> cardList = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(firstNluIntents)) {
			List<QuickReplay> intentCardQRs = new ArrayList<>();

			for (Map map : firstNluIntents) {

				String score = MapUtils.getString(map, "score");

				if (NumberUtils.toDouble(score) <= NumberUtils.toDouble(PropUtils.getCathaySiteScoreNluMin())) {
					continue;
				}

				String intent = MapUtils.getString(map, "intent");
				if (StringUtils.equals(intent, "Hello") || StringUtils.equals(intent, "End") || StringUtils.equals(intent, "真人服務")) {
					continue;
				}

				intentCardQRs = new ArrayList<>();
				intentCardQRs.add(INTENT_QR_MAP.get(intent));

				if (intentCardQRs.size() >= 3) {
					break;
				}
			}

			if (intentCardQRs.size() > 0) {
				CardItem cards = buildBaseCard(vo, "card04.png", intentCardQRs.toArray(new QuickReplay[intentCardQRs.size()]));
				cardList.add(cards.getCards());
			}
		}

		if (CollectionUtils.isNotEmpty(faqResult)) {
			//card06.png
			List<QuickReplay> faqCardQRs = new ArrayList<>();

			for (Map map : faqResult) {

				String score = MapUtils.getString(map, "score");

				if (NumberUtils.toDouble(score) < NumberUtils.toDouble(PropUtils.getCathaySiteScoreFaqMin())) {
					continue;
				}

				String text = MapUtils.getString(map, "question");
				String alt = MapUtils.getString(map, "answer");
				//看需求是否需要過濾不顯示的categorie //TODO 要過濾後面是@2以上的
				//List<String> categories = (List<String>) MapUtils.getObject(map, "categories");

				QuickReplay q = new SiteQuickReplay(10, text, alt);
				faqCardQRs.add(q);

				if (faqCardQRs.size() >= 3) {
					break;
				}
			}

			if (faqCardQRs.size() > 0) {
				CardItem cards = buildBaseCard(vo, "card06.png", faqCardQRs.toArray(new QuickReplay[faqCardQRs.size()]));
				cardList.add(cards.getCards());
			}
		}

		if (cardList.size() > 0) {
			botResponseUtils.setTextResult(vo, "阿發猜想你要的答案是");
			// 牌卡
			botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);

			tpiGatewayResult.put(TPIConst.ASK_CARD, "Y");

			return tpiGatewayResult.setData(vo);
		} else {
			return handleDefaultFallback(fallBackRequest);
		}
	}

	private CardItem buildBaseCard(BotMessageVO vo, String image, QuickReplay... quickReplays) {
		CardItem cards = vo.new CardItem();
		cards.setCName("");
		cards.setCWidth(BotMessageVO.CWIDTH_200);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CC);
		cImageData.setCImageUrl(image);
		cards.setCImageData(cImageData.getCImageDatas());

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplays));

		return cards;
	}

	private TPIGatewayResult handleDefaultFallback(FallBackRequest fallBackRequest) {
		BotMessageVO vo = botResponseUtils.initBotMessage(fallBackRequest.getChannel(), fallBackRequest.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		StringBuilder sb = new StringBuilder();
		sb.append("你要問「")
				.append(fallBackRequest.getMsg())
//                .append("(")
//                .append(fallBackRequest.getSecondBot())
//                .append(")")
				.append("」什麼相關內容呢？你可以描述得更完整一些，我會比較容易理解喔！");

		botResponseUtils.setTextResult(vo, sb.toString());

		tpiGatewayResult.put(TPIConst.CAN_NOT_ANS, "Y");

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public TPIGatewayResult handleFaqReplay(Map<String, Object> request) {
		BotMessageVO vo = botResponseUtils.initBotMessage(MapUtils.getString(request, "channel"), MapUtils.getString(request, "role"));

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		String faqAns = MapUtils.getString(request, "faqAns");
		botResponseUtils.setTextResult(vo, faqAns);

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public TPIGatewayResult supportNLU(Map<String, Object> request) {
		/*
			總機				基金		電子帳單		密碼		開戶		帳務		個人投資
			服務台			申請		申購		補發		查詢		買回		推薦

			基金_操作		申購		買回		推薦		查詢
			基金_查詢_項目	淨值		配息		新委託交易
			基金_申購_項目	定期定額	單筆
			電子帳單_操作		補發		其他
			密碼_操作		補發		其他
			帳務_操作		申請_扣款恢復		查詢_約定帳號
			個人投資_操作		查詢		其他
			開戶_操作		查詢_申請進度		其他

			推薦: 基金_操作
			申請: 帳務_操作
			申購: 基金_操作
			補發: 電子帳單_操作  密碼_操作
			查詢: 基金_操作  帳務_操作  個人投資_操作  開戶_操作
			買回: 基金_操作
		 */
		BotMessageVO vo = botResponseUtils.initBotMessage(MapUtils.getString(request, "channel"), MapUtils.getString(request, "role"));

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		String operationIntent = MapUtils.getString(request, "operationIntent", "");
		String msg = MapUtils.getString(request, "msg");
		StringBuilder sb = new StringBuilder();

//		public static final QuickReplay fund_q1 = new SiteQuickReplay(1, "查詢基金淨值", "查詢基金淨值");
//		public static final QuickReplay fund_q2 = new SiteQuickReplay(1, "查詢基金配息", "查詢基金配息");
//		public static final QuickReplay fund_q3 = new SiteQuickReplay(1, "推薦基金", "推薦基金");
//		public static final QuickReplay fund_q4 = new SiteQuickReplay(1, "申購單筆基金", "申購單筆基金");
//		public static final QuickReplay fund_q5 = new SiteQuickReplay(1, "新定期定額申請", "新定期定額申請");
//		public static final QuickReplay fund_q6 = new SiteQuickReplay(1, "新委託交易查詢", "新委託交易查詢");
//		public static final QuickReplay fund_q7 = new SiteQuickReplay(1, "買回轉申購", "買回轉申購");
//		public static final QuickReplay account_q1 = new SiteQuickReplay(1, "申請恢復扣款", "申請恢復扣款");
//		public static final QuickReplay account_q2 = new SiteQuickReplay(1, "約定帳號查詢", "約定帳號查詢");
//		public static final QuickReplay bill_q1 = new SiteQuickReplay(1, "補發電子帳單", "補發電子帳單");
//		public static final QuickReplay pwd_q1 = new SiteQuickReplay(1, "補發密碼", "補發密碼");
//		public static final QuickReplay ivn_q1 = new SiteQuickReplay(1, "查詢我的投資", "查詢我的投資");
//		public static final QuickReplay ecopen_q1 = new SiteQuickReplay(1, "開戶進度查詢", "開戶進度查詢");
		switch (operationIntent) {
			case "查詢":
				botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
				botResponseUtils.setQuickReplayResult(vo,
						SiteQuickReplay.fund_q1,
						SiteQuickReplay.fund_q2,
						SiteQuickReplay.fund_q6,
						SiteQuickReplay.account_q2,
						SiteQuickReplay.ivn_q1,
						SiteQuickReplay.ecopen_q1)
				;
				break;
			case "推薦":
				botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
				botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.fund_q3);
				break;
			case "申請":
				botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
				botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.account_q1);
				break;
			case "申購":
				botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
				botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.fund_q4, SiteQuickReplay.fund_q5);
				break;
			case "補發":
				botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
				botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.bill_q1, SiteQuickReplay.pwd_q1);
				break;
			case "買回":
				botResponseUtils.setTextResult(vo, "請問你要問阿發什麼服務嗎？");
				botResponseUtils.setQuickReplayResult(vo, SiteQuickReplay.fund_q7);
				break;
			default:
				sb.append("你要問「")
						.append(msg)
						.append("」什麼相關內容呢？你可以描述得更完整一些，我會比較容易理解喔！");
				botResponseUtils.setTextResult(vo, sb.toString());

				//botResponseUtils.setTextResult(vo, "TODO 服務台反問 : " + operationIntent);
				break;
		}

		return tpiGatewayResult.setData(vo);
	}

    private List<Map<String, Object>> getInitCardList(BotMessageVO vo) {
        List<Map<String, Object>> cardList = new ArrayList<>();

		CardItem cards1 = buildBaseCard(vo, "基金", "基金流程測試", SiteQuickReplay.fund_q1, SiteQuickReplay.fund_q2, SiteQuickReplay.fund_q3,
				SiteQuickReplay.fund_q4, SiteQuickReplay.fund_q5, SiteQuickReplay.fund_q6, SiteQuickReplay.fund_q7);

		//card2
		CardItem cards2 = buildBaseCard(vo, "帳務", "帳務流程測試", SiteQuickReplay.account_q1, SiteQuickReplay.account_q2);

        //card3
		CardItem cards3 = buildBaseCard(vo, "電子帳單", "電子帳單流程測試", SiteQuickReplay.bill_q1);

		//card4
		CardItem cards4 = buildBaseCard(vo, "密碼", "密碼流程測試", SiteQuickReplay.pwd_q1);

		//card5
		CardItem cards5 = buildBaseCard(vo, "個人投資", "個人投資流程測試", SiteQuickReplay.ivn_q1);

		//card6
		CardItem cards6 = buildBaseCard(vo, "開戶", "開戶流程測試", SiteQuickReplay.ecopen_q1);

        cardList.add(cards1.getCards());
        cardList.add(cards2.getCards());
        cardList.add(cards3.getCards());
		cardList.add(cards4.getCards());
		cardList.add(cards5.getCards());
		cardList.add(cards6.getCards());

        return cardList;
    }

    private CardItem buildBaseCard(BotMessageVO vo, String title, String text, QuickReplay... quickReplays) {
		CardItem cards = vo.new CardItem();
		cards.setCName("");
		cards.setCWidth(BotMessageVO.CWIDTH_200);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CC);
		cImageData.setCImageUrl("policy_03.jpg");
		cards.setCImageData(cImageData.getCImageDatas());

		if (StringUtils.isBlank(title)) {
			cards.setCTitle(title);
		}

		if (StringUtils.isBlank(text)) {
			List<Map<String, Object>> cTextList = new ArrayList<>();
			CTextItem cText = vo.new CTextItem();
			cText.setCText(text);
			cTextList.add(cText.getCTexts());
			cards.setCTexts(cTextList);
		}

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplays));

		return cards;
	}

    @Override
    public BotMessageVO showSurvey(BotRequest botRequest) {
        BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());

        botResponseUtils.setSurvey(botMessageVO);

        return botMessageVO;
    }

	@Override
	public BotMessageVO showSiteCustomerServiceOffTime(BotRequest botRequest) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());

		botResponseUtils.setTextResult(botMessageVO, "很抱歉，現在非為上班時段，請於營業日8:30-18:00撥打客服專線(02)7713-3000洽詢，我們將盡快為你服務。");

		return botMessageVO;
	}

	@Override
	public BotMessageVO showSiteCustomerServiceForm(BotRequest botRequest) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());

		ContentItem content = botMessageVO.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSiteCustomerServiceForm.name());
		botMessageVO.getContent().add(content.getContent());

		return botMessageVO;
	}

	@Override
	public BotMessageVO ctiSetAdd(CustomerServiceRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = ctiAPIFeignService.ctiSetAdd(param, reqVO.getToken());
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, Object> dataMap = getDataMap(data);
				botResponseUtils.setTextResult(vo, "阿發收到你的留言了，會請客服人員盡速與你聯絡");
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	/**
	 * <pre>
	 * 聯絡客服牌卡
	 * </pre>
	 * <ul>
	 * <li>撥打客服專線</li>
	 * <li>留言給客服</li>
	 * </ul>
	 */
	@Override
	public BotMessageVO setSiteCustomerService(BotRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		// XXX BotRequest without clientIP
		setSiteCustomerService(vo, reqVO.getSessionId(), "1.1.1.1");
		return vo;
	}

	/**
	 * <pre>
	 * 聯絡客服牌卡
	 * </pre>
	 * <ul>
	 * <li>撥打客服專線</li>
	 * <li>留言給客服</li>
	 * </ul>
	 */
	@Override
	public void setSiteCustomerService(BotMessageVO vo, String sessionID, String clientIP,
			QuickReplay... quickReplaysPrepend) {
		botResponseUtils.setTextResult(vo, "請於營業日8:30-18:00撥打客服專線(02)7713-3000洽詢，我們將盡快為你服務。");

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCLinkType(1);
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImageUrl("card06.png");
		cards.setCImageData(cImageData.getCImageDatas());

		List<QuickReplay> quickReplayList = new ArrayList<>();
		for (QuickReplay quickReplay : quickReplaysPrepend)
			quickReplayList.add(quickReplay);
		if (isSiteOnTime(sessionID, clientIP))
			quickReplayList.add(SiteQuickReplay.CUSTOMER_SERVICE_ON_TIME);
		quickReplayList.add(SiteQuickReplay.CUSTOMER_SERVICE_FORM);

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplayList.toArray(new QuickReplay[0])));
		cardList.add(cards.getCards());
		botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, SiteQuickReplay.INIT,
				SiteQuickReplay.END_CHAT);
	}

	/**
	 * <pre>
	 * 判斷是否為可接聽電話時間
	 * </pre>
	 */
	protected boolean isSiteOnTime(String sessionID, String clientIP) {
		Boolean dayValid = null;
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", sessionID);
		requireParams.put("ClientIP", clientIP);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("obj", requireParams);

		Map<String, Object> result = ctiAPIFeignService.getBusinessDays(param);
		logger.info("result=" + result);
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				List<Map<String, Object>> list = getList(data);
				dayValid = false;
				String today = DateFormatUtils.format(new Date(), "yyyy-MM-dd");
				for (Map<String, Object> map : list) {
					String bday = (String) map.get("bday");
					if (StringUtils.equals(today, bday)) {
						dayValid = true;
						break;
					}
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
			}
		}
		if (dayValid == null) {
			// 預設使用星期六日判斷
			Calendar today = Calendar.getInstance();
			int day_of_week = today.get(Calendar.DAY_OF_WEEK);
			dayValid = day_of_week != Calendar.SATURDAY && day_of_week != Calendar.SUNDAY;
		}
		logger.info("dayValid=" + dayValid);
		if (!dayValid)
			return false;

		String startTime = "08:30", endTime = "18:00";
		String now = DateFormatUtils.format(new Date(), "HH:mm");
		return now.compareTo(startTime) >= 0 && now.compareTo(endTime) <= 0;
	}

	@Override
	public BotMessageVO getPwdIdCheckForm(PwdRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(vo, "請先輸入下列資料，阿發才能幫你補發喔！");
		botResponseUtils.setTextResult(vo, "提醒您，每日線上補發密碼最多三次！");
		botResponseUtils.setIdentityCheckForm(vo, "核對身分", "pwd");
		return vo;
	}
	
	@Override
	public BotMessageVO getOpenProcessPwdIdCheckForm(PwdRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(vo, "請先輸入下列資料，阿發才能幫你查詢喔！");		
		botResponseUtils.setIdentityCheckForm(vo, "核對身分", "ecopen");
		return vo;
	}

	@Override
	public TPIGatewayResult sendPwdIdCheck(PwdRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult defaultTpiResult = TPIGatewayResult.ok();
		TPIGatewayResult tpiGatewayResult = null;

		Map<String, Object> result = custAPIFeignService.sendPwdIdCheck(param);
		logger.info("result=" + result);
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, Object> dataMap = getDataMap(data);
				logger.info("dataMap=" + dataMap);
				String email = (String) dataMap.get("email");
				// boolean is_cphone_CAPTCHA = (boolean) dataMap.get("is_cphone_CAPTCHA");
				String cphone = (String) dataMap.get("cphone");

				QuickReplay q1 = new SiteQuickReplay(2, "簡訊補發", "簡訊補發", "密碼", "sendPwdBySms", MapUtils.EMPTY_MAP);
				QuickReplay q2 = new SiteQuickReplay(2, "Email補發", "email補發", "密碼", "sendPwdByEmail",
						MapUtils.EMPTY_MAP);

				List<QuickReplay> quickReplayList = new ArrayList<>();
				if (StringUtils.isNotBlank(cphone))
					quickReplayList.add(q1);
				if (StringUtils.isNotBlank(email))
					quickReplayList.add(q2);

				switch (StringUtils.defaultString(reqVO.getSendType())) {
				case "1":
					if (StringUtils.isNotBlank(email)) {
						Map<String, String> l1Obj = (Map<String, String>) param.get("obj");
						l1Obj.put("SendType", "1");
						tpiGatewayResult = sendPwd(reqVO, param);
					} else {
						botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
						botResponseUtils.setTextResult(vo, "請嘗試其他補發密碼方式");
					}
					break;
				case "2":
					if (StringUtils.isNotBlank(cphone)) {
						vo = getPwdCphoneForm(reqVO, param);
					} else {
						botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
						botResponseUtils.setTextResult(vo, "請嘗試其他補發密碼方式");
					}
					break;
				default:
					if (quickReplayList.size() >= 2) {
						botResponseUtils.setTextResult(vo, "希望阿發把密碼補發到你留存的手機還是Email呢?");
						botResponseUtils.setQuickReplayResult(vo,
								(quickReplayList.size() == 2 ? BotMessageVO.LTYPE_01 : BotMessageVO.LTYPE_04),
								quickReplayList.toArray(new QuickReplay[0]));

						// 傳給flow紀錄
						defaultTpiResult.put("waitForSendType", "Y");
						defaultTpiResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
						defaultTpiResult.setOptionList(Arrays.asList(q1, q2));

					} else if (StringUtils.isNotBlank(email)) {
						Map<String, String> l1Obj = (Map<String, String>) param.get("obj");
						l1Obj.put("SendType", "1");
						tpiGatewayResult = sendPwd(reqVO, param);
					} else if (StringUtils.isNotBlank(cphone)) {
						vo = getPwdCphoneForm(reqVO, param);
					}
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				switch (code) {
				case "W006":
					botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
					botResponseUtils.setTextResult(vo, StringUtils.defaultString(msg));
					QuickReplay q1 = new SiteQuickReplay(2, "重新輸入", "補發密碼");
					QuickReplay q2 = new SiteQuickReplay(2, "開戶進度查詢", "開戶進度查詢");
					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q1, q2);
					break;
				case "W007":
					botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
					botResponseUtils.setTextResult(vo, StringUtils.defaultString(msg));
					break;
				default:
					botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
					botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
				}
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		if (tpiGatewayResult != null) {
			return tpiGatewayResult;
		}

		return defaultTpiResult.setData(vo);
	}

	@Override
	public TPIGatewayResult sendPwd(PwdRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = custAPIFeignService.sendPwd(param);
		logger.info("result=" + result);
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, Object> dataMap = getDataMap(data);
				logger.info("dataMap=" + dataMap);
				String sendType = (String) dataMap.get("SendType");
				String sendTime = (String) dataMap.get("SendTime");
				if (StringUtils.equals(sendType, "1")) {
					String email = (String) dataMap.get("email");
					botResponseUtils.setTextResult(vo, "阿發已於" + sendTime + "，將新密碼寄送到你的" + "E-Mail信箱囉! 快去收E-Mail看看吧!");

					List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

					CardItem cards = vo.new CardItem();
					cards.setCLinkType(1);
					cards.setCWidth(BotMessageVO.CWIDTH_250);

					// 牌卡圖片
					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND_FH);
					cImageData.setCImageTextType(1);

					List<Map<String, Object>> cImageTextList = new ArrayList<>();
					CImageTextItem cImageText = vo.new CImageTextItem();
					cImageText.setCImageTextTitle("會寄到：");
					cImageText.setCImageText(email);
					cImageTextList.add(cImageText.getCImageTexts());
					cImageData.setCImageTexts(cImageTextList);
					cards.setCImageData(cImageData.getCImageDatas());

					cardList.add(cards.getCards());

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

					botResponseUtils.setTextResult(vo, "還需要阿發其他幫忙嗎？");
				} else if (StringUtils.equals(sendType, "2")) {
					String cphone = (String) dataMap.get("cphone");
					botResponseUtils.setTextResult(vo,
							"阿發已於" + sendTime + "，將新密碼寄送到你的" + "手機號碼" + cphone + "囉! 快去收簡訊看看吧!");
				}

				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, SiteQuickReplay.INIT,
						SiteQuickReplay.END_CHAT);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				if (StringUtils.equals("W006", code)) {
					W006(vo, msg, param, tpiGatewayResult);
				} else {
					botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
					botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
				}
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO getPwdCphoneForm(PwdRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = custAPIFeignService.sendPwdIdCheck(param);
		logger.info("result=" + result);
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, Object> dataMap = getDataMap(data);
				logger.info("dataMap=" + dataMap);
				boolean is_cphone_CAPTCHA = (boolean) dataMap.get("is_cphone_CAPTCHA");
				if (is_cphone_CAPTCHA) {
					ContentItem content = vo.new ContentItem();
					content.setType(12);
					content.setWidget(Widget.doCphoneForm.name());
					vo.getContent().add(content.getContent());
				} else {
					ContentItem content = vo.new ContentItem();
					content.setType(12);
					content.setWidget(Widget.doCphoneCAPTCHA.name());
					vo.getContent().add(content.getContent());
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public TPIGatewayResult sendCphoneForCaptcha(PwdRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = custAPIFeignService.sendCphoneCAPTCHA(param);
		logger.info("result=" + result);
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				doCphoneCAPTCHACheck(vo, reqVO.getCphone(), "");
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				if (StringUtils.equals("W006", code)) {
					W006(vo, msg, param, tpiGatewayResult);
				} else {
					botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
					botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
				}
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	protected void W006(BotMessageVO vo, String msg, Map<String, Object> param, TPIGatewayResult tpiGatewayResult) {
		botResponseUtils.setTextResult(vo, StringUtils.defaultString(msg));

		QuickReplay q1 = new SiteQuickReplay(2, "重新輸入", "簡訊補發密碼", "密碼", "sendPwdBySms", MapUtils.EMPTY_MAP);
		QuickReplay q2 = new SiteQuickReplay(2, "改Email補發", "改Email補發", "密碼", "sendPwdByEmail", MapUtils.EMPTY_MAP);
		QuickReplay q3 = new SiteQuickReplay(2, "聯絡客服", "找客服", "", "", "");

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

		List<QuickReplay> quickReplayList = new ArrayList<>();
		quickReplayList.add(q1);

		Map<String, Object> result = custAPIFeignService.sendPwdIdCheck(param);
		Map<String, Object> data = getData(result);
		if (isSuccess(data)) {
			Map<String, Object> dataMap = getDataMap(data);
			String email = (String) dataMap.get("email");
			if (StringUtils.isNotBlank(email))
				quickReplayList.add(q2);
		}

		quickReplayList.add(q3);
		botResponseUtils.setQuickReplayResult(vo,
				(quickReplayList.size() == 3 ? BotMessageVO.LTYPE_02 : BotMessageVO.LTYPE_01),
				quickReplayList.toArray(new QuickReplay[0]));
	}

	protected void doCphoneCAPTCHACheck(BotMessageVO vo, String cphone, String errMsg) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doCphoneCAPTCHACheck.name());
		content.getContent().put("cphone", cphone);
		content.getContent().put("errMsg", errMsg);
		vo.getContent().add(content.getContent());
	}

	@Override
	public TPIGatewayResult checkCphoneCaptcha(PwdRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult defaultTpiResult = TPIGatewayResult.ok();
		TPIGatewayResult tpiGatewayResult = null;

		Map<String, Object> result = custAPIFeignService.checkCphoneCAPTCHA(param);
		logger.info("result=" + result);
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, String> l1Obj = (Map<String, String>) param.get("obj");
				l1Obj.put("SendType", "2");
				tpiGatewayResult = sendPwd(reqVO, param);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				if (StringUtils.equals("W006", code)) {
					doCphoneCAPTCHACheck(vo, reqVO.getCphone(), msg);
				} else {
					botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
					botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
				}
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		if (tpiGatewayResult != null) {
			return tpiGatewayResult;
		}

		return defaultTpiResult.setData(vo);
	}
}
