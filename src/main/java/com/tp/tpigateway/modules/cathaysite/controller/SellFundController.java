package com.tp.tpigateway.modules.cathaysite.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.SellFundRequest;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysite.service.SellFundService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("cathaysite/fund")
public class SellFundController extends BaseController {
	@Autowired
	SellFundService sellFundService;

	@RequestMapping(value = "/getSellFundList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getSellFundList(@RequestBody SellFundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
//		requireParams.put("SendType", reqVO.getSendType());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = sellFundService.getSellFundList(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/getSellFundView", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getSellFundView(@RequestBody SellFundRequest reqVO) {

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return sellFundService.getSellFundView(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/getSellFundForm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getSellFundForm(@RequestBody SellFundRequest reqVO) {
		return sellFundService.getSellFundForm(reqVO);
	}

	@RequestMapping(value = "/getSellFundTxpwd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getSellFundTxpwd(@RequestBody SellFundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = sellFundService.getSellFundTxpwd(reqVO, "");
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/sellSetAdd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult sellSetAdd(@RequestBody SellFundRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("tcode", reqVO.getTcode());
			if (StringUtils.isNotBlank(reqVO.getRunit()))
				requireParams.put("runit", reqVO.getRunit());
			if (StringUtils.isNotBlank(reqVO.getRtype()))
				requireParams.put("rtype", reqVO.getRtype());
			if (StringUtils.isNotBlank(reqVO.getRamt()))
				requireParams.put("ramt", reqVO.getRamt());
			requireParams.put("trcode", reqVO.getTrcode());
			switch (reqVO.getTrcode()) {
			case "1":
				requireParams.put("bact", reqVO.getSno() + "-" + reqVO.getAcnt());
				break;
			case "8":
				((Map) requireParams).put("trfund", new String[] { reqVO.getTrfund() });
				((Map) requireParams).put("trfund_p", new int[] { 100 });
				break;
			}
			requireParams.put("pwd", reqVO.getPwd());

			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return sellFundService.sellSetAdd(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/waitSellFundTcode", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitSellFundTcode(@RequestBody SellFundRequest reqVO) {

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return sellFundService.waitSellFundTcode(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/waitSellFundRunit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitSellFundRunit(@RequestBody SellFundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = sellFundService.waitSellFundRunit(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/validateSellFundRunit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult validateSellFundRunit(@RequestBody SellFundRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return sellFundService.validateSellFundRunit(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/waitSellFundRtype", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitSellFundRtype(@RequestBody SellFundRequest reqVO) {
        return sellFundService.waitSellFundRtype(reqVO);
	}

	@RequestMapping(value = "/waitSellFundRamt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitSellFundRamt(@RequestBody SellFundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = sellFundService.waitSellFundRamt(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/validateSellFundRamt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult validateSellFundRamt(@RequestBody SellFundRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return sellFundService.validateSellFundRamt(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/waitSellFundTrcode", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitSellFundTrcode(@RequestBody SellFundRequest reqVO) {
        return sellFundService.waitSellFundTrcode(reqVO);
	}

	@RequestMapping(value = "/waitSellFundAccount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitSellFundAccount(@RequestBody SellFundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = sellFundService.waitSellFundAccount(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/waitSellFundTrfund", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitSellFundTrfund(@RequestBody SellFundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = sellFundService.waitSellFundTrfund(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/modifySellFund", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult modifySellFund(@RequestBody SellFundRequest reqVO) {
		return sellFundService.modifySellFund(reqVO);
	}
}