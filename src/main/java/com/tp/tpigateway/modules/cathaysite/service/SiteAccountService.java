package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.cathaysite.SiteAccountRequest;

import java.util.Map;

public interface SiteAccountService {

	/**
	 * For 約定帳號類型快速回應
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getCusbankType(SiteAccountRequest reqVO, Map<String, Object> param);

	/**
	 * For 約定帳號清單
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getCusbank(SiteAccountRequest reqVO, Map<String, Object> param);
}