package com.tp.tpigateway.modules.cathaysite.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.service.SiteLoginService;

@Service("siteLoginService")
public class SiteLoginServiceImpl implements SiteLoginService{
	
	private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    BotResponseUtils botResponseUtils;
	
	@Override
    public BotMessageVO getLogin(BotRequest botRequest) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());
		botResponseUtils.setLogin(botMessageVO);
		
		return botMessageVO;
    }

}
