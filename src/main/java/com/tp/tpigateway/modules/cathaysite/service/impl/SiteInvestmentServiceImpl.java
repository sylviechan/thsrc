package com.tp.tpigateway.modules.cathaysite.service.impl;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CLinkListItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotMessageVO.ContentItem;
import com.tp.common.model.BotMessageVO.LinkListItem;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.fegin.FundAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import com.tp.tpigateway.modules.cathaysite.service.CathaySiteService;
import com.tp.tpigateway.modules.cathaysite.service.SiteInvestmentService;

/*
 *  投資相關
 *  
 */

@Service("investmentService")
public class SiteInvestmentServiceImpl implements SiteInvestmentService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	FundAPIFeignService fundAPIFeignService;

	@Autowired
	CathaySiteService cathaySiteService;

	@Override
	public BotMessageVO getQuota(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		logger.info("查詢條件 : " + JSON.toJSONString(param));
		String searchType = "全部";
		param.put("types", reqVO.getType());

		if (StringUtils.equals(reqVO.getType(), "1"))
			searchType = "單筆";
		else if (StringUtils.equals(reqVO.getType(), "2"))
			searchType = "定期";
		else if (StringUtils.equals(reqVO.getType(), "3,4"))
			searchType = "約定";
		Map<String, Object> result = fundAPIFeignService.getQuota(param, reqVO.getToken());
		Map<String, Object> resultData = JSON.parseObject(result.get("d").toString(),
				new TypeReference<Map<String, Object>>() {
				});
		// S001:查詢成功 E003:登入逾時
		if ("S001".equals(resultData.get("code").toString())) {
			Map<String, Object> dataValue = (Map<String, Object>) resultData.get("DataValue");
			List<Object> list = new ArrayList<Object>();
			list = JSON.parseArray((String) dataValue.get("list"));
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			Map<String, Object> parameter;
			logger.info("查尋筆數 : " + list.size());
			if (list.size() > 0) {
				botResponseUtils.setTextResult(botMessageVO, "阿發幫你找到你的投資如下：");
				botResponseUtils.setTextResult(botMessageVO, "請選擇你要查詢的基金交易類型");
				for (Object mapList : list) {
					// 1.fund_name.equals(((Map<String, String>)mapList).get("fund_name"))比對基金名稱
					// 2.fund_name.equals("")全部查詢
					if (StringUtils.equals(reqVO.getFund(), ((Map<String, String>) mapList).get("fund"))
							|| StringUtils.equals(reqVO.getType(), "")) {

						CardItem cardList = botMessageVO.new CardItem();
						cardList.setCTexts(new ArrayList<Map<String, Object>>());
						cardList.setCName("");
						cardList.setCWidth("segment");
						cardList.setCTextType("1");

						// "cImageData"
						CImageDataItem cImage = botMessageVO.new CImageDataItem();
						cImage.setCImageTag(searchType);
						cImage.setCImage("img-credbank00");
						cImage.setCImageTextType(5);
						CImageTextItem cImageText = botMessageVO.new CImageTextItem();
						cImageText.setCImageTextTitle("基金名稱");
						cImageText.setCImageText(((Map<String, String>) mapList).get("fund_name"));
						cImage.setCImageTexts(new ArrayList<Map<String, Object>>());
						cImage.getCImageTexts().add(cImageText.getCImageTexts());
						cardList.setCImageData(cImage.getCImageDatas());

						// "cTexts"
						CTextItem cText = botMessageVO.new CTextItem();
						
						cText.getCTexts().put("updateTime", "資訊更新 " + toDateString(mapList,"ndate"));
						cText.getCTexts().put("updateTimeClass", "failure");
						cardList.getCTexts().add(cText.getCTexts());
						String[] cTextLabelArray = { "投資成本:", "持有單位數:", "最新淨值:", "市價預估:", "約當台幣", "投資損益:", "投資報酬率:",
								"含息報酬率:" };
						String[] cTextArray = { "cost", "unit", "nav", "val", "TWDval", "pl", "pl_rate",
								"pl_int_rate" };
						NumberFormat numberFormat = NumberFormat.getNumberInstance();
						for (int i = 0; i < cTextLabelArray.length; i++) {
							cText = botMessageVO.new CTextItem();
							cText.setCLabel(cTextLabelArray[i]);
							if(((Map<String, Object>) mapList).get(cTextArray[i])!=null) {
								switch (cTextArray[i]) {
								case "cost":
								case "val":
								case "TWDval":
									cText.setCText("NT. " + String.valueOf(numberFormat
											.format(((Map<String, BigDecimal>) mapList).get(cTextArray[i]).intValue())));
									break;
								case "pl":
									cText.setCText("NT. " + String.valueOf(numberFormat
											.format(((Map<String, BigDecimal>) mapList).get(cTextArray[i]).intValue())));
									break;
								case "pl_rate":
								case "pl_int_rate":
									cText.setCText(
											String.valueOf(((Map<String, BigDecimal>) mapList).get(cTextArray[i])) + "%");
									break;
								case "unit":
								case "nav":
									cText.setCText((String.valueOf(numberFormat
											.format(((Map<String, BigDecimal>) mapList).get(cTextArray[i]).floatValue()))));
									break;
								}
								// 用cAmount判斷正/負(0代表正，1代表負)
								if (((Map<String, BigDecimal>) mapList).get(cTextArray[i]).floatValue() >= 0)
									cText.setCAmount("0");
								else
									cText.setCAmount("1");
							}else {
								cText.setCText("");
							}
							cardList.getCTexts().add(cText.getCTexts());

						}

						// 判斷是否為全部查詢，如果是全部查詢則顯示
						if (StringUtils.equals(reqVO.getType(), "")) {
							cardList.setCLinkList(new ArrayList<Map<String, Object>>());
							if(((Map<String, String>) mapList).get("typelist") != null) {
								String strTypeList = ((Map<String, String>) mapList).get("typelist");
								// 查詢基金筆數超過5筆要多"繼續查詢其他基金"選項
								CLinkListItem cLinkList = botMessageVO.new CLinkListItem();
								cLinkList.setClAction(2);
								cLinkList.setClText("查詢單筆投資");
								cLinkList.setClAlt(
										"查詢我的投資:" + ((Map<String, String>) mapList).get("fund_name") + ":" + strTypeList);
								cLinkList.setClIntent("個人投資");
								cLinkList.setClSecondBotIntent("query_mySite");
								parameter = new HashMap<String, Object>();
								parameter.put("fund", ((Map<String, String>) mapList).get("fund"));
								parameter.put("type", strTypeList);
								cLinkList.setClParameter(parameter);
								cardList.getCLinkList().add(cLinkList.getCLinkList());
							}else {
								CLinkListItem cLinkList = botMessageVO.new CLinkListItem();
								cLinkList.setClAction(2);
								cLinkList.setClText("繼續查詢其他基金");
								cLinkList.setClAlt("查詢我的投資");
								cardList.getCLinkList().add(cLinkList.getCLinkList());
							}
						}

						resultList.add(cardList.getCards());
					}
				}
				botResponseUtils.setCardsResult31(botMessageVO, "card", resultList);

				// 如果不是全部查詢，需要加快速回復
				if (!StringUtils.equals(reqVO.getType(), "")) {
					Map<String, Object> checkTypeResultData = JSON.parseObject(result.get("d").toString(),
							new TypeReference<Map<String, Object>>() {
							});
					Map<String, Object> checkTypelistDataValue = (Map<String, Object>) checkTypeResultData
							.get("DataValue");
					List<Object> checkTypeList = JSON.parseArray((String) checkTypelistDataValue.get("list"));
					String strTypeList = "";
					for (Object mapList : checkTypeList) {
						if (StringUtils.equals(reqVO.getFund(), ((Map<String, String>) mapList).get("fund"))
								|| StringUtils.equals(reqVO.getType(), "")) {
							strTypeList = ((Map<String, String>) mapList).get("typelist");
						}
					}
					ContentItem quickReply = botMessageVO.new ContentItem();
					quickReply.setLinkList(new ArrayList<Map<String, Object>>());
					quickReply.setType(4);
					quickReply.setLinkList(new ArrayList<Map<String, Object>>());
					quickReply.setLType("select-tag04");

					LinkListItem lList;
					// 如果查詢單一基金有其他交易類型，則需加此快速回復選項
					if (strTypeList.indexOf("3") > -1) {
						if (strTypeList.length() > 3) {
							quickReply.setLType("select-tag01");
							lList = botResponseUtils.getLinkListItem(botMessageVO,
									new SiteQuickReplay(2, "繼續查詢其他投資", "查詢我的投資"));
							quickReply.getLinkList().add(lList.getLinkList());
						}
					} else {
						if (strTypeList.length() > 1) {
							quickReply.setLType("select-tag01");
							lList = botResponseUtils.getLinkListItem(botMessageVO,
									new SiteQuickReplay(2, "繼續查詢其他投資", "查詢我的投資"));
							quickReply.getLinkList().add(lList.getLinkList());
						}
					}

					lList = botResponseUtils.getLinkListItem(botMessageVO, SiteQuickReplay.INIT);
					quickReply.getLinkList().add(lList.getLinkList());

					botMessageVO.getContent().add(quickReply.getContent());
				}
			} else {
				ContentItem contentItem = botMessageVO.new ContentItem();
				contentItem.setType(1);
				contentItem.setText("糟糕，找不到你要查詢的庫存基金資料!");
				botMessageVO.getContent().add(contentItem.getContent());
				cathaySiteService.setSiteCustomerService(botMessageVO, reqVO.getSessionId(), reqVO.getClientIp());
			}
		}
		return botMessageVO;
	}

	private String toDateString(Object mapList, String objectkey) {
		String ndateString = "";
		Map<String, Object> map = (Map<String, Object>) mapList;
		if (map.containsKey(objectkey)) {

			if (map.get(objectkey) != null) {
				if (map.get(objectkey).getClass().toString().indexOf("BigDecimal") > -1)
					ndateString = StringUtils.defaultString(String.valueOf((map.get(objectkey))), "");
				else
					ndateString = (String) map.get(objectkey);
			}

			if (StringUtils.isNotBlank(ndateString) && ndateString.length() >= 8) {
				ndateString = ndateString.substring(0, 4) + "/" + ndateString.substring(4, 6) + "/"
						+ ndateString.substring(6, 8);
			}
		}

		return ndateString;
	}
}
