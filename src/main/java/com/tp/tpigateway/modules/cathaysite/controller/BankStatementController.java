package com.tp.tpigateway.modules.cathaysite.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.BankStatementRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysite.service.BankStatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/*
    電子帳單
 */
@RestController
@RequestMapping("cathaysite/statement")
public class BankStatementController extends BaseController {
	
	@Autowired
	BankStatementService bankStatementService;
	
	@RequestMapping(value = "/getBillIdCheckForm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getBillIdCheckForm(@RequestBody BankStatementRequest reqVO) {
		BotMessageVO botMessageVO = null;
		botMessageVO = bankStatementService.getBillIdCheckForm(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	//補寄對帳單核對身份
	@RequestMapping(value = "/getBillIdCheck", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getBillIdCheck(@RequestBody BankStatementRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		
		requireParams.put("birth", reqVO.getBirth());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("uid", reqVO.getUid());
			
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return bankStatementService.getBillIdCheck(reqVO, obj);
		} else {
			return TPIGatewayResult.error("缺少必要參數");
		}
	}

	//補寄對帳單
	@RequestMapping(value = "/getBillSend", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getBillSend(@RequestBody BankStatementRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("birth", reqVO.getBirth());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("uid", reqVO.getUid());
			requireParams.put("sdate", reqVO.getSdate());
			requireParams.put("edate", reqVO.getEdate());
			
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = bankStatementService.getBillSend(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
}
