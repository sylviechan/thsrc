package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.common.model.cathaysite.FundStopRequest;

import java.util.Map;

public interface FundService {

	/**
	 * For 基金回覆
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO webFundKey(FundRequest reqVO);

	/**
	 * For 查詢基金列表
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getWebFund(FundRequest reqVO, Map<String, Object> param);

	/**
	 * For 查詢基金淨值
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult getWebFundNav(FundRequest reqVO, Map<String, Object> param);

	/**
	 * For 查詢基金配息
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult getWebFundAllotinterest(FundRequest reqVO, Map<String, Object> param);

	/**
	 * For 恢復扣款-定額基金停扣列表
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getFundStopList(FundStopRequest reqVO, Map<String, Object> param);

	/**
	 * For 恢復扣款-定額基金停扣明細
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult getFundStopDetail(FundStopRequest reqVO, Map<String, Object> param);

	/**
	 * For 恢復扣款-輸入交易認證碼
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getFundStopTxPwd(FundStopRequest reqVO, Map<String, Object> param);

	/**
	 * For 恢復扣款-定額基金恢復扣款
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO setFundStatusRestore(FundStopRequest reqVO, Map<String, Object> param);

	/**
	 * For 申購基金清單
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getFundList(FundRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 申購基金明細資料
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getFundDetail(FundRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 申購單筆基金
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult setAdd(FundRequest reqVO, Map<String, Object> param);

	TPIGatewayResult chooseCmd(FundRequest reqVO);
	
	/**
	 * For 查詢推薦基金
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getWebFundChoice(FundRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 申購基金取得資訊
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult getApplyFund(FundRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 注意事項內容
	 * 測試用
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getOne(FundRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 取得委託單群組
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getOrderGroup(FundRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 取得委託單明細
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getOrderDetail(FundRequest reqVO);
	
	/**
	 * For 開戶進度查詢
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getECopenProcess(FundRequest reqVO, Map<String, Object> param);
	
	/**
	 * For 基金清單 關閉
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	BotMessageVO closeFundList(FundRequest reqVO);
}
