package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysite.CustomerServiceRequest;
import com.tp.common.model.cathaysite.PwdRequest;
import com.tp.common.util.QuickReplay;

import java.util.Map;

public interface CathaySiteService {

	BotMessageVO hello(BotRequest botRequest);

	TPIGatewayResult fallback(FallBackRequest fallBackRequest);

	TPIGatewayResult handleFaqReplay(Map<String, Object> request);

	TPIGatewayResult supportNLU(Map<String, Object> request);

	BotMessageVO showSurvey(BotRequest botRequest);

	/**
	 * For 撥打客服專線(下班時間)
	 *
	 * @param botRequest
	 * @return
	 * @throws Exception
	 */
	BotMessageVO showSiteCustomerServiceOffTime(BotRequest botRequest);

	/**
	 * For 留言給客服
	 *
	 * @param botRequest
	 * @return
	 * @throws Exception
	 */
	BotMessageVO showSiteCustomerServiceForm(BotRequest botRequest);

	/**
	 * For 新增留言客服資料
	 * 
	 * @param botRequest
	 * @param param
	 * @return
	 */
	BotMessageVO ctiSetAdd(CustomerServiceRequest botRequest, Map<String, Object> param);

	/**
	 * <pre>
	 * For 聯絡客服牌卡
	 * </pre>
	 * <ul>
	 * <li>撥打客服專線</li>
	 * <li>留言給客服</li>
	 * </ul>
	 */
	BotMessageVO setSiteCustomerService(BotRequest reqVO);

	/**
	 * <pre>
	 * For 聯絡客服牌卡
	 * </pre>
	 * <ul>
	 * <li>撥打客服專線</li>
	 * <li>留言給客服</li>
	 * </ul>
	 */
	public void setSiteCustomerService(BotMessageVO vo, String sessionID, String clientIP, QuickReplay... quickReplaysPrepend);

	/**
	 * <pre>
	 * For 補發密碼驗身表單
	 * </pre>
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	BotMessageVO getPwdIdCheckForm(PwdRequest reqVO);

	/**
	 * <pre>
	 * For 補發密碼核對身份
	 * </pre>
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult sendPwdIdCheck(PwdRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 補發密碼
	 * </pre>
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult sendPwd(PwdRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 簡訊補發密碼 輸入手機表單
	 * </pre>
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	BotMessageVO getPwdCphoneForm(PwdRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 簡訊補發密碼 傳送手機
	 * </pre>
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult sendCphoneForCaptcha(PwdRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 簡訊補發密碼 傳送驗證碼
	 * </pre>
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult checkCphoneCaptcha(PwdRequest reqVO, Map<String, Object> param);
	
	/**
	 * <pre>
	 * For 開戶進度查詢驗身表單
	 * </pre>
	 * 
	 * @param reqVO
	 * @return
	 */
	BotMessageVO getOpenProcessPwdIdCheckForm(PwdRequest reqVO);
}