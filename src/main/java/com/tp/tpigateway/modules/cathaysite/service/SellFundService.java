package com.tp.tpigateway.modules.cathaysite.service;

import java.util.List;
import java.util.Map;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.SellFundRequest;

/**
 * 贖回基金(買回轉申購)
 */
public interface SellFundService {
	/**
	 * For 查詢可買回基金資訊清單‹買回轉申購
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> getSellFundList(SellFundRequest reqVO, Map<String, Object> param, BotMessageVO vo);

	/**
	 * For 贖回基金清單
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getSellFundList(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * For 贖回基金明細
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult getSellFundView(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * For 買回轉申購 贖回前的確認頁
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult getSellFundForm(SellFundRequest reqVO);

	/**
	 * For 交易認證視窗
	 * 
	 * @param reqVO
	 * @param errMsg
	 * @return
	 */
	BotMessageVO getSellFundTxpwd(SellFundRequest reqVO, String errMsg);

	/**
	 * For 申購贖回基金
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult sellSetAdd(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 買回轉申購 選擇買回單位數方式
	 * </pre>
	 * <ul>
	 * <li>0：全數庫存單位數買回</li>
	 * <li><s>1：單筆全部庫存單位數買回</s></li>
	 * <li><s>2：定額全部庫存單位數買回</s></li>
	 * <li>3：部份買回</li>
	 * <li>4：金額買回</li>
	 * </ul>
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult waitSellFundTcode(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 買回轉申購 輸入部份買回單位數
	 * </pre>
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO waitSellFundRunit(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * For 買回轉申購 驗證部份買回單位數
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult validateSellFundRunit(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 買回轉申購 選擇指定買回類別
	 * </pre>
	 * <ul>
	 * <li>1：先單筆後定額</li>
	 * <li>2：先定額後單筆</li>
	 * </ul>
	 */
	TPIGatewayResult waitSellFundRtype(SellFundRequest reqVO);

	/**
	 * <pre>
	 * For 買回轉申購 輸入贖回金額
	 * </pre>
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO waitSellFundRamt(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * For 買回轉申購 驗證贖回金額
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult validateSellFundRamt(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 買回轉申購 選擇買回領取方式
	 * </pre>
	 * <ul>
	 * <li>1：匯款</li>
	 * <li>8：轉申購</li>
	 * </ul>
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult waitSellFundTrcode(SellFundRequest reqVO);

	/**
	 * For 買回轉申購 選擇扣款帳號
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO waitSellFundAccount(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * For 買回轉申購 轉申購基金清單
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO waitSellFundTrfund(SellFundRequest reqVO, Map<String, Object> param);

	/**
	 * For 買回轉申購 選擇修改項目
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult modifySellFund(SellFundRequest reqVO);
}