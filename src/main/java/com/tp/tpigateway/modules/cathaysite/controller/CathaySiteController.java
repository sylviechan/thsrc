package com.tp.tpigateway.modules.cathaysite.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysite.CustomerServiceRequest;
import com.tp.common.model.cathaysite.PwdRequest;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysite.service.CathaySiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 投信共用模組
 */
@RestController
@RequestMapping("cathaysite")
public class CathaySiteController extends BaseController {

	@Autowired
	CathaySiteService cathaySiteService;

	@RequestMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult hello(@RequestBody BotRequest botRequest) {
		BotMessageVO botMessageVO = cathaySiteService.hello(botRequest);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/fallback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult fallback(@RequestBody FallBackRequest fallBackRequest) {
		return cathaySiteService.fallback(fallBackRequest);
	}

	/*
       組FAQ回答樣式
    */
	@RequestMapping(value = "/handleFaqReplay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult handleFaqReplay(@RequestBody Map<String, Object> request) {
		return cathaySiteService.handleFaqReplay(request);
	}

	/*
        服務台反問
     */
	@RequestMapping(value = "/supportNLU", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult supportNLU(@RequestBody Map<String, Object> request) {
		return cathaySiteService.supportNLU(request);
	}

	@RequestMapping(value = "/showSurvey", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult showSurvey(@RequestBody BotRequest botRequest) {
		BotMessageVO botMessageVO = cathaySiteService.showSurvey(botRequest);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/setSiteCustomerService", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult setSiteCustomerService(@RequestBody BotRequest botRequest) {
		BotMessageVO botMessageVO = cathaySiteService.setSiteCustomerService(botRequest);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/showSiteCustomerServiceOffTime", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult showSiteCustomerServiceOffTime(@RequestBody BotRequest botRequest) {
		BotMessageVO botMessageVO = cathaySiteService.showSiteCustomerServiceOffTime(botRequest);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/showSiteCustomerServiceForm", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult showSiteCustomerServiceForm(@RequestBody BotRequest botRequest) {
		BotMessageVO botMessageVO = cathaySiteService.showSiteCustomerServiceForm(botRequest);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/ctiSetAdd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult ctiSetAdd(@RequestBody CustomerServiceRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("q1", reqVO.getQ1());
		requireParams.put("tel", reqVO.getTel());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("uname", reqVO.getUname());
			requireParams.put("email", reqVO.getEmail());

			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = cathaySiteService.ctiSetAdd(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/getPwdIdCheckForm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getPwdIdCheckForm(@RequestBody PwdRequest reqVO) {
		BotMessageVO botMessageVO = cathaySiteService.getPwdIdCheckForm(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}
	
	@RequestMapping(value = "/getOpenProcessPwdIdCheckForm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getOpenProcessPwdIdCheckForm(@RequestBody PwdRequest reqVO) {
		BotMessageVO botMessageVO = cathaySiteService.getOpenProcessPwdIdCheckForm(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/sendPwdIdCheck", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult sendPwdIdCheck(@RequestBody PwdRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("uid", reqVO.getUid());
		requireParams.put("birth", reqVO.getBirth());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return cathaySiteService.sendPwdIdCheck(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/sendPwd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult sendPwd(@RequestBody PwdRequest reqVO) {

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("uid", reqVO.getUid());
		requireParams.put("birth", reqVO.getBirth());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("SendType", reqVO.getSendType());
			requireParams.put("cphone", reqVO.getCphone());

			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return cathaySiteService.sendPwd(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/getPwdCphoneForm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getPwdCphoneForm(@RequestBody PwdRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("uid", reqVO.getUid());
		requireParams.put("birth", reqVO.getBirth());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = cathaySiteService.getPwdCphoneForm(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/sendCphoneForCaptcha", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult sendCphoneForCaptcha(@RequestBody PwdRequest reqVO) {

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("uid", reqVO.getUid());
		requireParams.put("birth", reqVO.getBirth());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("cphone", reqVO.getCphone());

			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return cathaySiteService.sendCphoneForCaptcha(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/checkCphoneCaptcha", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult checkCphoneCaptcha(@RequestBody PwdRequest reqVO) {

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("uid", reqVO.getUid());
		requireParams.put("birth", reqVO.getBirth());
		requireParams.put("cphone", reqVO.getCphone());
		requireParams.put("CAPTCHA", reqVO.getCaptcha());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return cathaySiteService.checkCphoneCaptcha(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

	}
}
