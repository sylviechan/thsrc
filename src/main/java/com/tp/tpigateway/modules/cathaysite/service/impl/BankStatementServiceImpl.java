package com.tp.tpigateway.modules.cathaysite.service.impl;

import static com.tp.common.util.cathaysite.SiteApiUtils.getCode;
import static com.tp.common.util.cathaysite.SiteApiUtils.getData;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDataMap;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDataValue;
import static com.tp.common.util.cathaysite.SiteApiUtils.getMsg;
import static com.tp.common.util.cathaysite.SiteApiUtils.isSuccess;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.BankStatementRequest;
import com.tp.common.util.DateUtils;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.fegin.CustAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import com.tp.tpigateway.modules.cathaysite.service.BankStatementService;

/*
 * 帳單相關
 *
 */

@Service
public class BankStatementServiceImpl implements BankStatementService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    BotResponseUtils botResponseUtils;

    @Autowired
    CustAPIFeignService custAPIFeignService;

    @Override
    public BotMessageVO getBillIdCheckForm(BankStatementRequest reqVO) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        botResponseUtils.setTextResult(vo, "請先輸入下列資料，阿發才能幫你補發喔！");
        botResponseUtils.setIdentityCheckForm(vo, "核對身分",reqVO.getType());
        return vo;
    }

    @Override
    public TPIGatewayResult getBillIdCheck(BankStatementRequest reqVO, Map<String, Object> param) {
        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

        Map<String, Object> result = custAPIFeignService.getBillIdCheck(param, reqVO.getToken());

        if (result != null) {
            Map<String, Object> data = getData(result);
            if (isSuccess(data)) {
                logger.info("data=" + data);
                List<Object> list = JSON.parseObject(getDataValue(data).get("data").toString(), List.class);
                if (list.size() > 0) {
                    botResponseUtils.setTextResult(vo, "請選擇想要寄發的對帳單交易期間！");
                    List<QuickReplay> secQuickReplays = new ArrayList<>();
                    List<String> msgList = new ArrayList<>();

                    Map<String, Object> dateMap = new HashMap<>();

                    for (Object object : list) {
                        Map<String, Object> map = JSON.parseObject(object.toString(), Map.class);
                        String text = map.get("date_txt").toString();

                        if (StringUtils.equals(text, "一個月內") || StringUtils.equals(text, "三個月內") || StringUtils.equals(text, "六個月內")) {
                            Map<String, Object> parameter = new HashMap<>();
                            
							parameter.put("sdate", map.get("sdate"));
							parameter.put("edate", map.get("edate"));

                            dateMap.put(text, parameter);

                            QuickReplay q = SiteQuickReplay.BILL_TIME(text);
                            secQuickReplays.add(q);
                            msgList.add(q.getText());
                        }
                    }

                    //傳給flow紀錄
                    tpiGatewayResult.put("dateMap", dateMap);
                    tpiGatewayResult.setMsgList(msgList);
                    tpiGatewayResult.setOptionList(secQuickReplays);

                    botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, secQuickReplays);
                }
            } else {
                String code = getCode(data), msg = getMsg(data);
                logger.error("code = {}, msg = {}", code, msg);
                botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
                botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
            }
        } else {
            logger.error("result is null");
            botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
            botResponseUtils.setTextResult(vo, "API failed!");
        }

//		botResponseUtils.setTextResult(vo, "請選擇要查詢的帳號類型");
//
//		List<QuickReplay> quickReplayList = new ArrayList<>();
//		for (String[] arr : cusbankTypeArr)
//			quickReplayList.add(new SiteQuickReplay(2, arr[1], arr[2], "", "", ""));
//
//		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_02, quickReplayList.toArray(new QuickReplay[0]));

        return tpiGatewayResult.setData(vo);
    }

    @Override
    public BotMessageVO getBillSend(BankStatementRequest reqVO, Map<String, Object> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

        Map<String, Object> result = custAPIFeignService.getBillSend(param, reqVO.getToken());

        if (result != null) {
            Map<String, Object> data = getData(result);
            if (isSuccess(data)) {
                logger.info("data=" + data);
                Map<String, Object> map = getDataMap(data);
                if (map != null) {
					Date sdate = DateUtils.stringToDate(reqVO.getSdate(), "yyyyMMdd");
					Date edate = DateUtils.stringToDate(reqVO.getEdate(), "yyyyMMdd");
					String sdateStr = DateUtils.format(sdate, "yyyy/MM/dd");
					String edateStr = DateUtils.format(edate, "yyyy/MM/dd");
                    botResponseUtils.setTextResult(vo,
                            "好的，阿發將提供給你" + sdateStr + "~" + edateStr + "的電子對帳單資訊");

                    getBillSendCard(vo, map);

                    botResponseUtils.setTextResult(vo, "還需要阿發其他幫忙嗎?");
                    botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SiteQuickReplay.INIT, SiteQuickReplay.END_CHAT);
                }
            } else {
                String code = getCode(data), msg = getMsg(data);
                logger.error("code = {}, msg = {}", code, msg);
                botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
                botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
            }
        } else {
            logger.error("result is null");
            botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
            botResponseUtils.setTextResult(vo, "API failed!");
        }

        return vo;
    }

    /**
     * 產生牌卡
     */
    protected void getBillSendCard(BotMessageVO vo, Map<String, Object> map) {
        // 組牌卡
        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
        CardItem cards = vo.new CardItem();
        cards.setCLinkType(1);
        cards.setCWidth(BotMessageVO.CWIDTH_250);

        CImageDataItem cImageData = vo.new CImageDataItem();
        cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND_FH);
        cImageData.setCImageTextType(1);

        List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();

        CImageTextItem cImageText = vo.new CImageTextItem();
        cImageText = vo.new CImageTextItem();
        cImageText.setCImageTextTitle("會寄到:");
        cImageText.setCImageText(map.get("email").toString());
        cImageTexts.add(cImageText.getCImageTexts());

        cImageData.setCImageTexts(cImageTexts);
        cards.setCImageData(cImageData.getCImageDatas());
        cardList.add(cards.getCards());
        botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
    }
}
