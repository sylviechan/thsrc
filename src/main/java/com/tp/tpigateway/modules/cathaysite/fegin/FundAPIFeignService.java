package com.tp.tpigateway.modules.cathaysite.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "fundapi", url = "${cathaysite.fund.path}")
@TpSysLog(remark = "FundAPIFeignService")
public interface FundAPIFeignService {

	@RequestMapping(value = "online/api/fund_query.asmx/getWebFund", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getWebFund(Map<String, Object> obj);
	
	@RequestMapping(value = "online/api/fund_query.asmx/getWebFundNav", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getWebFundNav(Map<String, Object> obj);
	
	@RequestMapping(value = "online/api/fund_query.asmx/getWebFundAllotinterest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getWebFundAllotinterest(Map<String, Object> obj);
	
	@RequestMapping(value = "online/api/rsp.asmx/getFundStopList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getFundStopList(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/rsp.asmx/setFundStatusRestore", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> setFundStatusRestore(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fundb.asmx/getFundList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getSellFundList(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fund.asmx/getFundList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getFundList(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fundp.asmx/getFundDetail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getFundDetail(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fundb.asmx/getFundDetail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getSellFundDetail(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fundp.asmx/setAdd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> setAdd(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fundb.asmx/setAdd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> sellSetAdd(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fund_query.asmx/getWebFundChoice", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getWebFundChoice(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/rsp.asmx/getRspFundDetail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getRspFundDetail(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/rsp.asmx/getRspDay", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getRspDay(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/rsp.asmx/setAdd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> rspSetAdd(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/declare.asmx/getOne", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getOne(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/code.asmx/getLinkOne", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getLinkOne(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/quota.asmx/getQuota", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getQuota(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/QueryAcc.asmx/getOrderGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getOrderGroup(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/QueryAcc.asmx/getOrderDetail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getOrderDetail(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/fund_query.asmx/getECopenProcess", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getECopenProcess(Map<String, Object> obj);
}
