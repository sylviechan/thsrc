package com.tp.tpigateway.modules.cathaysite.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.SiteAccountRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysite.service.SiteAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/*
    帳務相關
 */
@RestController
@RequestMapping("cathaysite/account")
public class SiteAccountController extends BaseController {

	@Autowired
	SiteAccountService siteAccountService;

	@Deprecated
	@RequestMapping(value = "/getCusbankType", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getCusbankType(@RequestBody SiteAccountRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = siteAccountService.getCusbankType(reqVO, obj);
		}
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/getCusbank", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getCusbank(@RequestBody SiteAccountRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = siteAccountService.getCusbank(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
}
