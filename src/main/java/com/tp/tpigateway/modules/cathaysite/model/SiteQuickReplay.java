package com.tp.tpigateway.modules.cathaysite.model;

import com.tp.common.util.QuickReplay;
import org.apache.commons.collections.MapUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SiteQuickReplay extends QuickReplay {

    //common
	public static final QuickReplay INIT = new SiteQuickReplay(1, "看看阿發還能做什麼?", "看看阿發還能做什麼?");
    //public static final QuickReplay INIT = new SiteQuickReplay(2, "看看阿發還能做什麼?", "看看阿發還能做什麼?");
    public static final QuickReplay END_CHAT = new SiteQuickReplay(1, "不用了, 結束對話", "不用了, 結束對話");

    //init card
    public static final QuickReplay fund_q1 = new SiteQuickReplay(1, "查詢基金淨值", "查詢基金淨值");
    public static final QuickReplay fund_q2 = new SiteQuickReplay(1, "查詢基金配息", "查詢基金配息");
    public static final QuickReplay fund_q3 = new SiteQuickReplay(1, "推薦基金", "推薦基金");
    public static final QuickReplay fund_q4 = new SiteQuickReplay(1, "申購單筆基金", "申購單筆基金");
    public static final QuickReplay fund_q5 = new SiteQuickReplay(1, "新定期定額申請", "新定期定額申請");
    public static final QuickReplay fund_q6 = new SiteQuickReplay(1, "新委託交易查詢", "新委託交易查詢");
    public static final QuickReplay fund_q7 = new SiteQuickReplay(1, "買回轉申購", "買回轉申購");
    public static final QuickReplay account_q1 = new SiteQuickReplay(1, "申請恢復扣款", "申請恢復扣款");
    public static final QuickReplay account_q2 = new SiteQuickReplay(1, "約定帳號查詢", "約定帳號查詢");
    public static final QuickReplay bill_q1 = new SiteQuickReplay(1, "補發電子帳單", "補發電子帳單");
    public static final QuickReplay pwd_q1 = new SiteQuickReplay(1, "補發密碼", "補發密碼");
    public static final QuickReplay ivn_q1 = new SiteQuickReplay(1, "查詢我的投資", "查詢我的投資");
    public static final QuickReplay ecopen_q1 = new SiteQuickReplay(1, "開戶進度查詢", "開戶進度查詢");

    //投信
    /**撥打客服專線 上線時間*/
    public static final QuickReplay CUSTOMER_SERVICE_ON_TIME = new SiteQuickReplay(12, "撥打客服專線", "0277133000");

    @Deprecated
    /** 撥打客服專線 下線時間 */
    // 透過大腦
    public static final QuickReplay CUSTOMER_SERVICE_OFF_TIME = new SiteQuickReplay(1, "撥打客服專線", "撥打客服專線", "非服務時間", "", Collections.emptyMap());
    // 繞過大腦
//	public static final QuickReplay CUSTOMER_SERVICE_OFF_TIME = new SiteQuickReplay(5, "撥打客服專線",
//			"$.ajax({type: 'POST',cache: false,url: 'cathaysite/showSiteCustomerServiceOffTime',data: {sessionId: App.sessionId},dataType: 'json'});",
//			"", "", "");

    /** 留言給客服 */
    // 透過大腦
    public static final QuickReplay CUSTOMER_SERVICE_FORM = new SiteQuickReplay(1, "留言給客服", "留言給客服", "留言給客服", "", Collections.emptyMap());
    // 繞過大腦
//	public static final QuickReplay CUSTOMER_SERVICE_FORM = new SiteQuickReplay(5, "留言給客服",
//			"$.ajax({type: 'POST',cache: false,url: 'cathaysite/showSiteCustomerServiceForm',data: {sessionId: App.sessionId},dataType: 'json'});",
//			"", "", "");

    public static final QuickReplay QE_FUND_NAV = new SiteQuickReplay(1, "淨值基金查詢", "淨值基金查詢");
    public static final QuickReplay QE_FUND_ALLOTINTEREST = new SiteQuickReplay(1, "配息基金查詢", "配息基金查詢");
    public static final QuickReplay ORDER_DETAIL_ELSE = new SiteQuickReplay(1, "查詢其他新委託", "查詢其他新委託");
    public static final QuickReplay FUND_NAV_ELSE = new SiteQuickReplay(2, "繼續查詢其他基金", "查詢基金淨值", "基金", "基金淨值選單", MapUtils.EMPTY_MAP);
    public static final QuickReplay FUND_ALLOTINTEREST_ELSE = new SiteQuickReplay(2, "繼續查詢其他基金", "查詢基金配息", "基金", "基金配息選單", MapUtils.EMPTY_MAP);

    public static final QuickReplay FUND_STOP_YES(String tno) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("tno", tno);

        String alt = "沒錯! 我要恢復扣款(" + tno + ")";
        String intent = "帳務";
        String secondBotIntent = "clickFundCard";

        return new SiteQuickReplay(2, "是的! 我要恢復扣款", alt, intent, secondBotIntent, parameter);
    }
    public static final QuickReplay FUND_STOP_RE = new SiteQuickReplay(1, "選擇其他基金", "選擇其他基金", "帳務", "getFundStopList", MapUtils.EMPTY_MAP);
    public static final QuickReplay FUND_STOP_NO = new SiteQuickReplay(1, "我不恢復扣款", "看看阿發還能做什麼?");

	public static final QuickReplay BUT_OTHER_FUND_STOP = new SiteQuickReplay(1, "恢復其他基金", "恢復其他基金","帳務", "getFundStopList", MapUtils.EMPTY_MAP);

    //單筆申購
    public static final QuickReplay BUY_ONE_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "好的！立即申購", "好的！立即申購(" + fund +")", "基金", "toBuy_oneFund", parameter);
    }

    public static final QuickReplay MODIFY_ONE_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "我要修改", "我要修改(" + fund + ")", "基金", "toModify_oneFund", parameter);
    }

    public static final QuickReplay CANCEL_ONE_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "我不申購了", "我不申購了", "基金", "toCancel_oneFund", parameter);
    }

    //電子帳單區間
    public static final QuickReplay BILL_TIME(String text) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("text", text);
        return new SiteQuickReplay(2, text, text, "電子帳單", "selectTime", parameter);
    }

    //單筆申購
    public static final QuickReplay AUTO_DELAY_ONE_FUND = new SiteQuickReplay(1, "好呀！就這麼辦", "好呀！就這麼辦", "基金", "toAutoDelay_oneFund",MapUtils.EMPTY_MAP);
    public static final QuickReplay RETRY_ONE_FUND = new SiteQuickReplay(2, "重新申購", "重新申購", "基金", "getFundList_oneFund", MapUtils.EMPTY_MAP);
    public static final QuickReplay BUT_OTHER_ONE_FUND = new SiteQuickReplay(1, "申購其他基金", "申購其他基金", "基金", "getFundList_oneFund", MapUtils.EMPTY_MAP);

    //定期(不)定額申購
    public static final QuickReplay BUT_OTHER_RSP_FUND = new SiteQuickReplay(1, "申購其他基金", "申購其他基金", "基金", "getFundList_rspFund", MapUtils.EMPTY_MAP);
    public static final QuickReplay TRADETYPE1_RSP_FUND = new SiteQuickReplay(1, "定期定額", "定期定額");
    public static final QuickReplay TRADETYPE2_RSP_FUND = new SiteQuickReplay(1, "定期不定額", "定期不定額");

    //stype
    public static final QuickReplay STYPE1_RSP_FUND = new SiteQuickReplay(1, "遇假日順延扣款", "遇假日順延扣款");
    public static final QuickReplay STYPE2_RSP_FUND = new SiteQuickReplay(1, "遇假日不扣款", "遇假日不扣款");

    //贖回基金
    @Deprecated
    public static final QuickReplay AUTO_DELAY_SELL_FUND = new SiteQuickReplay(1, "好呀！就這麼辦", "好呀！就這麼辦", "基金", "toAutoDelay_sellFund",MapUtils.EMPTY_MAP);
    @Deprecated
    public static final QuickReplay RETRY_SELL_FUND = new SiteQuickReplay(2, "重新申購", "重新申購", "基金", "getFundList_sellFund", MapUtils.EMPTY_MAP);
    public static final QuickReplay BUT_OTHER_SELL_FUND = new SiteQuickReplay(1, "買回其他基金", "買回其他基金", "基金", "getFundList_sellFund", MapUtils.EMPTY_MAP);
    public static final QuickReplay INIT_SELL_FUND = new SiteQuickReplay(2, "我不買回基金了", "看看阿發還能做什麼?");
    //tcode
    public static final QuickReplay TCODE1_SELL_FUND = new SiteQuickReplay(1, "全部買回", "全部單位數買回", "基金", "doSelectTcode_sellFund" , getParameter("entity", "全部單位數買回"));
    public static final QuickReplay TCODE2_SELL_FUND = new SiteQuickReplay(1, "部分單位數買回", "部分單位數買回", "基金", "doSelectTcode_sellFund", getParameter("entity", "部分單位數買回"));
    public static final QuickReplay TCODE3_SELL_FUND = new SiteQuickReplay(1, "部分金額買回", "部分金額買回", "基金", "doSelectTcode_sellFund", getParameter("entity", "部分金額買回"));
    //rtype
    public static final QuickReplay RTYPE1_SELL_FUND = new SiteQuickReplay(1, "先定額後單筆", "先定額後單筆", "基金", "doSelectRtype_sellFund" , getParameter("entity", "先定額後單筆"));
    public static final QuickReplay RTYPE2_SELL_FUND = new SiteQuickReplay(1, "先單筆後定額", "先單筆後定額", "基金", "doSelectRtype_sellFund", getParameter("entity", "先單筆後定額"));
    //trcode
    public static final QuickReplay TRCODE1_SELL_FUND = new SiteQuickReplay(1, "領取匯款", "領取匯款", "基金", "doSelectTrcode_sellFund" , getParameter("entity", "領取匯款"));
    public static final QuickReplay TRCODE2_SELL_FUND = new SiteQuickReplay(1, "轉申購基金", "轉申購基金", "基金", "doSelectTrcode_sellFund", getParameter("entity", "轉申購基金"));
    public static final QuickReplay SELECT_SELL_FUND = new SiteQuickReplay(2, "重新選擇買回基金", "重新選擇買回基金", "基金", "getFundList_sellFund", MapUtils.EMPTY_MAP);

    public static final QuickReplay CHOOSE_SELL_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "買回基金", "買回基金", "基金", "doChooseFund_sellFund", parameter);
    }

    //修改選項
    public static final QuickReplay MODIFY_ONE_FUND = new SiteQuickReplay(1, "申購基金", "申購基金", "基金", "setModifyType_oneFund", getModifyTypeParameter("申購基金"));
    public static final QuickReplay MODIFY_RSP_FUND = new SiteQuickReplay(1, "申購基金", "申購基金", "基金", "setModifyType_rspFund", getModifyTypeParameter("申購基金"));
    public static final QuickReplay MODIFY_SELL_FUND = new SiteQuickReplay(2, "重新選擇買回基金", "申購基金", "基金", "setModifyType_sellFund", getModifyTypeParameter("申購基金"));

    public static final QuickReplay MODIFY_SELL_TRFUND = new SiteQuickReplay(2, "轉申購其他基金", "轉申購基金", "基金", "setModifyType_sellFund", getModifyTypeParameter("轉申購基金"));
    public static final QuickReplay MODIFY_SELL_TCODE = new SiteQuickReplay(2, "買回單位數", "買回單位數", "基金", "setModifyType_sellFund", getModifyTypeParameter("買回單位數"));

	public static final QuickReplay MODIFY_SELL_TRCODE1() {
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("trcode", "1");
		return new SiteQuickReplay(2, "改領取匯款", "領取匯款", "基金", "toUpdateTrcode_sellFund", parameter);
	}

	public static final QuickReplay MODIFY_SELL_TRCODE8() {
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("trcode", "8");
		return new SiteQuickReplay(2, "轉申購基金", "轉申購基金", "基金", "toUpdateTrcode_sellFund", parameter);
	}
    
    public static final QuickReplay MODIFY_ONE_ACCOUNT = new SiteQuickReplay(1, "扣款帳號", "扣款帳號", "基金", "setModifyType_oneFund", getModifyTypeParameter("扣款帳號"));
    public static final QuickReplay MODIFY_RSP_ACCOUNT = new SiteQuickReplay(1, "扣款帳號", "扣款帳號", "基金", "setModifyType_rspFund", getModifyTypeParameter("扣款帳號"));
    public static final QuickReplay MODIFY_SELL_ACCOUNT = new SiteQuickReplay(1, "約定買回帳號", "約定買回帳號", "基金", "setModifyType_sellFund", getModifyTypeParameter("約定買回帳號"));

    public static final QuickReplay MODIFY_ONE_DATE = new SiteQuickReplay(1, "申購日期", "申購日期", "基金", "setModifyType_oneFund", getModifyTypeParameter("申購日期"));
    public static final QuickReplay MODIFY_RSP_DATE = new SiteQuickReplay(1, "申購日期", "申購日期", "基金", "setModifyType_rspFund", getModifyTypeParameter("申購日期"));

    public static final QuickReplay MODIFY_ONE_MONEY = new SiteQuickReplay(1, "申購金額", "申購金額", "基金", "setModifyType_oneFund", getModifyTypeParameter("申購金額"));
    public static final QuickReplay MODIFY_RSP_MONEY = new SiteQuickReplay(1, "申購金額", "申購金額", "基金", "setModifyType_rspFund", getModifyTypeParameter("申購金額"));

    private static Map getModifyTypeParameter(String modifyType) {
        Map<String, Object> parameter = new HashMap();
        parameter.put("modifyType", modifyType);
        return parameter;
    }

    private static Map getParameter(String key, String value) {
        Map<String, Object> parameter = new HashMap();
        parameter.put(key, value);
        return parameter;
    }

    public static final QuickReplay BUY_RSP_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "是的!我要立即申購", "好的！立即申購(" + fund + ")", "基金", "toBuy_rspFund", parameter);
    }

    public static final QuickReplay MODIFY_RSP_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "我要修改", "我要修改(" + fund + ")", "基金", "toModify_rspFund", parameter);
    }

    public static final QuickReplay CANCEL_RSP_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "我不申購了", "我不申購了", "基金", "toCancel_rspFund", parameter);
    }

    public static final QuickReplay BUY_SELL_FUND(String fund, String text) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, text, "好的！立即贖回(" + fund + ")", "基金", "toBuy_sellFund", parameter);
    }

    public static final QuickReplay MODIFY_SELL_FUND(String fund) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, "我要修改", "我要修改(" + fund + ")", "基金", "toModify_sellFund", parameter);
    }

    public static final QuickReplay CANCEL_SELL_FUND(String fund, String text) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("fund", fund);
        return new SiteQuickReplay(2, text, text, "基金", "toCancel_sellFund", parameter);
    }

	public static final QuickReplay APPLY_ONE_FUND(String fund) {
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("fund", fund);
		parameter.put("action", "oneFund");// 識別：單筆申購
		parameter.put("sendType", "1");
		return new SiteQuickReplay(2, "我要單筆申購", "我要單筆申購(" + fund + ")", "基金", "doApplyFund", parameter);
	}

	public static final QuickReplay APPLY_RSP_FUND(String fund) {
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("fund", fund);
		parameter.put("action", "rspFund");// 識別：定期定額申購
		parameter.put("sendType", "4");
		return new SiteQuickReplay(2, "我要定期(不)定額申購", "我要定期(不)定額申購(" + fund + ")", "基金", "doApplyFund", parameter);
	}

    public SiteQuickReplay(int action, String text, String alt, String reply, String url, String customerAction) {
        super(action, text, alt, reply, url, customerAction);
    }

    public SiteQuickReplay(int action, String text, String alt, String reply, String url, String customerAction, String intent, String secondBorIntent, Map<String, Object> parameter) {
        super(action, text, alt, reply, url, customerAction, intent, secondBorIntent, parameter);
    }

    public SiteQuickReplay(int action, String text, String alt, String intent, String secondBotIntent, Map<String, Object> parameter) {
        super(action, text, alt, intent, secondBotIntent, parameter);
    }

    public SiteQuickReplay(int action, String text, String alt) {
        super(action, text, alt);
    }
}