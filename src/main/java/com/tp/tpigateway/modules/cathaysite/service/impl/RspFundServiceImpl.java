package com.tp.tpigateway.modules.cathaysite.service.impl;

import static com.tp.common.util.cathaysite.SiteApiUtils.getCode;
import static com.tp.common.util.cathaysite.SiteApiUtils.getData;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDataMap;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDataValue;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDetail;
import static com.tp.common.util.cathaysite.SiteApiUtils.getList;
import static com.tp.common.util.cathaysite.SiteApiUtils.getMsg;
import static com.tp.common.util.cathaysite.SiteApiUtils.isPwdError;
import static com.tp.common.util.cathaysite.SiteApiUtils.isSuccess;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.fegin.FundAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import com.tp.tpigateway.modules.cathaysite.service.CathaySiteService;
import com.tp.tpigateway.modules.cathaysite.service.RspFundService;

@Service("rspFundService")
public class RspFundServiceImpl implements RspFundService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	FundAPIFeignService fundAPIFeignService;

	@Autowired
	CathaySiteService cathaySiteService;

	@Value("${cathaysite.fund.path}")
	private String cathaysiteFundPath;

	@Override
	public TPIGatewayResult rsp_fund_detail(BotMessageVO vo, FundRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		String tradetype_txt = "", amtText = "";
		switch (reqVO.getTradetype()) {
		case "1":
			tradetype_txt = "定期定額";
			amtText = changeMoneyString(reqVO.getAmt()) + "元";
			break;
		case "2":
			tradetype_txt = "定期不定額";
			amtText += "高:" + changeMoneyString(reqVO.getHighamt()) + "元<br>";
			amtText += "中:" + changeMoneyString(reqVO.getMidamt()) + "元<br>";
			amtText += "低:" + changeMoneyString(reqVO.getLowamt()) + "元";
			break;
		}
		botResponseUtils.setTextResult(vo, "請確認你所申購" + tradetype_txt + "基金的扣款資訊：");

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCLinkType(1);
		cards.setCTextType("3");
		cards.setCWidth("segment");

//		// 牌卡下縮放文字
//		List<Map<String, Object>> cTextList2 = new ArrayList<Map<String, Object>>();
//		cards.setCTitle("注意事項");
//		Map<String, Object> m = null;
//		if ("".equals(StringUtils.defaultString(reqVO.getNote()))) {
//			m = new HashMap<>();
//			m.put("cText", "無注意事項");
//			cTextList2.add(m);
//		} else {
//			cards.setCLinkType(8);
//			String note = reqVO.getNote().replaceAll("<br />", "<br>").replaceAll("<br/>", "<br>");
//			String[] noteAry = note.split("<br>");
//
//			for (int i = 0; i < noteAry.length; i++) {
//				m = new HashMap<>();
//				m.put("cText", noteAry[i]);
//				cTextList2.add(m);
//			}
//		}
//		cards.setCTexts2(cTextList2);

		// 牌卡圖片
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		cImageData.setCImageTag(reqVO.getFundkindTxt());

		List<Map<String, Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("基金名稱");
		cImageText.setCImageText(reqVO.getFundName());
		cImageTextList.add(cImageText.getCImageTexts());
		cImageData.setCImageTexts(cImageTextList);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		cards.setCTexts(cTextList);

		CTextItem cText = vo.new CTextItem();
		cText.setCLabel("每月扣款日期");
		cText.setCText(reqVO.getSday());
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("扣款模式");
		cText.setCText(tradetype_txt);
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("每次扣款金額");
		cText.setCText(amtText);
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("扣款方式");
		cText.setCText(StringUtils.equals(reqVO.getStype(), "0") ? "遇假日順延扣款" : "遇假日不扣款");
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("扣款帳號");
		cText.setCText(reqVO.getAcntName());
		cText.setCUnit(reqVO.getAcnt());
		cTextList.add(cText.getCTexts());

		String fund = reqVO.getFund();

		QuickReplay q1 = SiteQuickReplay.BUY_RSP_FUND(fund);
		QuickReplay q2 = SiteQuickReplay.MODIFY_RSP_FUND(fund);
		QuickReplay q3 = SiteQuickReplay.CANCEL_RSP_FUND(fund);

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2, q3));
		cardList.add(cards.getCards());

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3));

		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO getBuyRspFunTxPwd(FundRequest reqVO, String errMsg) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		List<Map> linkList = new ArrayList<>();

		Map<String, Object> param = new HashMap<>();
		Map<String, String> l1obj = new HashMap<String, String>();
		l1obj.put("SessionID", reqVO.getSessionId());
		l1obj.put("ClientIP", reqVO.getClientIp());
		l1obj.put("fund", reqVO.getFund());
		param.put("obj", l1obj);
		Map<String, Object> result = fundAPIFeignService.getRspFundDetail(param, reqVO.getToken());
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, Object> dataMap = getDetail(data);

				String fund_doc = dataMap.get("fund_doc").toString();
				if (StringUtils.isNotBlank(fund_doc)) {
					String fund_doc_url = cathaysiteFundPath + fund_doc;
					linkList.add(botResponseUtils
							.getLinkListItem(vo, new SiteQuickReplay(4, "公開說明書", "", "", fund_doc_url, ""))
							.getLinkList());
				}

				Map<String, String> declare = (Map<String, String>) dataMap.get("Declare");
				for (String key : declare.keySet()) {
					l1obj.put("query", key);
					Map<String, Object> resultOne = fundAPIFeignService.getOne(param, reqVO.getToken());
					if (resultOne != null) {
						Map<String, Object> dataOne = getData(resultOne);
						String title = "", alt = "";
						if (isSuccess(dataOne)) {
							Map<String, Object> dvOne = getDataValue(dataOne);
							Map<String, Object> mapDataOne = (Map<String, Object>) dvOne.get("data");
							title = (String) mapDataOne.get("title");
							alt = (String) mapDataOne.get("txt");
							String stype = (String) mapDataOne.get("stype");

							if (StringUtils.equals(stype, "2")) {
								// 純文字
								alt = alt.replaceAll("\r\n", "<br>");
							}

						} else {
							String code = getCode(dataOne), msg = getMsg(dataOne);
							logger.error("code = {}, msg = {}", code, msg);
							alt = code + ":" + StringUtils.defaultString(msg);
						}
						linkList.add(botResponseUtils
								.getLinkListItem(vo, new SiteQuickReplay(0, title, alt, "", "", "")).getLinkList());
					}
				}

				botResponseUtils.doTxPwd(vo, "確認申購", "點選確認申購(視窗)", "基金", "doBuyFund_rspFund", MapUtils.EMPTY_MAP,
						linkList, errMsg);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	protected Integer getInteger(Object data) {
		Integer returnInt = 0;
		if (data != null) {
			try {
//				returnInt = Integer.parseInt(data.toString());
				returnInt = (int) Double.parseDouble(data.toString());
			} catch (NumberFormatException e) {
				return returnInt;
			}
		}

		return returnInt;
	}

	protected String changeMoneyString(String money) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		return numberFormat.format(getInteger(money));
	}

	@Override
	public BotMessageVO getRspFundList(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getFundList(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				List<Map<String, Object>> list = getList(data);
				if (list.size() > 0) {
					botResponseUtils.setTextResult(vo, "請挑選你想要申購的基金：");
					botResponseUtils.setSiteRspFundList(vo, list);
				} else {
					botResponseUtils.setTextResult(vo, "你沒有可申購的基金資訊。");
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public TPIGatewayResult waitRspFundTradetype(FundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		botResponseUtils.setTextResult(vo, "你想選哪種扣款模式呢?");

		QuickReplay q1 = SiteQuickReplay.TRADETYPE1_RSP_FUND;
		QuickReplay q2 = SiteQuickReplay.TRADETYPE2_RSP_FUND;

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q1, q2);

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO waitRspFundMoney(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getRspFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				doWaitRspFundMoney(vo, reqVO.getTradetype(), dataMap, false);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	protected void doWaitRspFundMoney(BotMessageVO vo, String tradetype, Map<String, Object> dataMap,
			boolean isShowUpperAmt) {
		String moneyType = dataMap.get("currency_txt").toString();
		String moneyTypeEng = "";

		if (moneyType.indexOf("台幣") > -1) {
			moneyTypeEng = "NTD";
		} else if (moneyType.indexOf("美金") > -1 || moneyType.indexOf("美元") > -1) {
			moneyTypeEng = "USD";
		} else if (moneyType.indexOf("人民幣") > -1) {
			moneyTypeEng = "RMB";
		}

		switch (tradetype) {
		case "1":
			String msg = "請選擇每次扣款金額，或輸入其他金額(千元為單位)。阿發提醒定期定額扣款金額不能小於" + moneyType
					+ changeMoneyString(dataMap.get("rsplow_amt").toString()) + "元。";
			if (isShowUpperAmt)
				msg += "輸入扣款金額不能超過申購金額上限" + moneyType + changeMoneyString(dataMap.get("upper_amt").toString()) + "元。";
			botResponseUtils.setTextResult(vo, msg);

			JSONArray moneyAry = (JSONArray) dataMap.get("range_amt");
			Integer money1 = getInteger(JSON.parseObject(moneyAry.getString(0), String.class));
			Integer money2 = getInteger(JSON.parseObject(moneyAry.getString(1), String.class));
			Integer money3 = getInteger(JSON.parseObject(moneyAry.getString(2), String.class));

			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_02,
					new SiteQuickReplay(1, moneyTypeEng + " " + changeMoneyString(money1.toString()), money1.toString(),
							"", "", ""),
					new SiteQuickReplay(1, moneyTypeEng + " " + changeMoneyString(money2.toString()), money2.toString(),
							"", "", ""),
					new SiteQuickReplay(1, moneyTypeEng + " " + changeMoneyString(money3.toString()), money3.toString(),
							"", "", ""));
			break;
		case "2":
			botResponseUtils.setTextResult(vo, "請輸入一組每次扣款金額(千元為單位)。阿發提醒定期不定額扣款金額不能小於" + moneyType
					+ changeMoneyString(dataMap.get("rsplow_amt").toString()) + "元。");

			// 浮出視窗輸入三種金額
			botResponseUtils.setSiteRspvFundAmtForm(vo, changeMoneyString(dataMap.get("rsplow_amt").toString()),
					changeMoneyString(dataMap.get("upper_amt").toString()), moneyType);

			break;
		}
	}

	@Override
	public TPIGatewayResult validateRspFundMoney(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = fundAPIFeignService.getRspFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				Integer rsplow_amt = getInteger(dataMap.get("rsplow_amt").toString());
				Integer upper_amt = getInteger(dataMap.get("upper_amt").toString());

				String checkResult = "Y";

				List<String> amtList = new ArrayList<>();
				switch (reqVO.getTradetype()) {
				case "1":
					amtList.add(reqVO.getAmt());
					break;
				case "2":
					amtList.add(reqVO.getHighamt());
					amtList.add(reqVO.getMidamt());
					amtList.add(reqVO.getLowamt());
					break;
				}
				for (String amt : amtList) {
					// 金錢檢核
					amt = amt.replaceAll(",", "").replaceAll("NTD", "").replaceAll(" ", "");
					Integer amtInt = getInteger(amt);
					if (amtInt == 0 || amtInt % 1000 > 0) {
						doWaitRspFundMoney(vo, reqVO.getTradetype(), dataMap, false);
						checkResult = "N";
					} else if (amtInt < rsplow_amt) {
						doWaitRspFundMoney(vo, reqVO.getTradetype(), dataMap, false);
						checkResult = "N";
					} else if (amtInt > upper_amt) {
						doWaitRspFundMoney(vo, reqVO.getTradetype(), dataMap, true);
						checkResult = "N";
					}
					if (!StringUtils.equals(checkResult, "Y"))
						break;
				}
				tpiGatewayResult.put("checkResult", checkResult);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO waitRspFundAccount(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getRspFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				JSONArray acnts = (JSONArray) dataMap.get("acnts");

				botResponseUtils.setTextResult(vo, "要用哪個約定扣款帳戶呢?");

				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				for (int i = 0; i < acnts.size(); i++) {
					Map<String, Object> acntsMap = JSON.parseObject(((JSONArray) dataMap.get("acnts")).getString(i),
							Map.class);

					if (acntsMap.size() > 0) {
						String acnt = (String) acntsMap.get("acntno");
						String acntName = (String) acntsMap.get("bdesc");
						String sno = (String) acntsMap.get("sno");

						CardItem cardItem = vo.new CardItem();
						cardItem.setCLinkType(1);
						cardItem.setCWidth(BotMessageVO.CWIDTH_250);

						CImageDataItem cImageData = vo.new CImageDataItem();
						cImageData.setCImage(BotMessageVO.IMG_CB03);
						cImageData.setCImageTextType(1);

						List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
						CImageTextItem cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("銀行");
						cImageText.setCImageText(acntName);
						cImageTexts.add(cImageText.getCImageTexts());

						cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("帳號");
						cImageText.setCImageText(acnt);
						cImageTexts.add(cImageText.getCImageTexts());

						cImageData.setCImageTexts(cImageTexts);
						cardItem.setCImageData(cImageData.getCImageDatas());

						String intent = "基金";
						String secondBotIntent = "toUpdateAccount_rspFund";
						Map<String, Object> parameter = new HashMap<>();
						parameter.put("fund", reqVO.getFund());
						parameter.put("sno", sno);
						parameter.put("acnt", acnt);
						parameter.put("acntName", acntName);

						QuickReplay q1 = new SiteQuickReplay(2, "好啊！就用這個扣款", sno + "-" + acnt, intent, secondBotIntent,
								parameter);

						cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, q1));
						cardList.add(cardItem.getCards());
					}
				}

				botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public TPIGatewayResult waitRspFundStype(FundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		botResponseUtils.setTextResult(vo, "請選擇扣款方式");

		QuickReplay q1 = SiteQuickReplay.STYPE1_RSP_FUND;
		QuickReplay q2 = SiteQuickReplay.STYPE2_RSP_FUND;

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q1, q2);

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO waitRspFundSday(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getRspDay(param, reqVO.getToken());
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);

				if (StringUtils.equals(reqVO.getStype(), "0"))
					botResponseUtils.setTextResult(vo, "好的!如果遇到扣款日為假日, 會幫你順延至次一營業日扣款喔!");
				botResponseUtils.setTextResult(vo, "你想要每月幾號扣款呢?");

				List<Map<String, Object>> list = new ArrayList<>(); // 送出物件

				Map<String, Object> dataMap = getDetail(data);
				List<Map<String, Object>> lst_sday = (List<Map<String, Object>>) dataMap.get("lst_sday");
//				List<Integer> sdayList = new ArrayList<>();
//				for (Map<String, Object> row : lst_sday) {
//					String sday = (String) row.get("sday");
//					boolean isUsed = (boolean) row.get("isUsed");
//					if (!isUsed)
//						sdayList.add(Integer.parseInt(sday));
//				}
				List<Integer> sdayList = lst_sday.stream().filter(row -> !(boolean) row.get("isUsed"))
						.map(row -> Integer.parseInt((String) row.get("sday"))).collect(Collectors.toList());

				Map<String, Object> map = null;
				List<Map<String, String>> txList = null;
				for (int i = 1; i <= 31; i++) {
					if (i == 1 || i == 11 || i == 21) {
						map = new HashMap<String, Object>();
						txList = new ArrayList<>();
						map.put("content", txList);
						list.add(map);
					}
					if (i == 1) {
						map.put("title", "每月01號-每月10號");
					} else if (i == 11) {
						map.put("title", "每月11號-每月20號");
					} else if (i == 21) {
						map.put("title", "每月21號-每月31號");
					}

					Map<String, String> txMap = new HashMap<String, String>();
					txList.add(txMap);
					txMap.put("txt", "每月" + String.valueOf(i) + "號");
					txMap.put("keyValue", String.valueOf(i));
					String status = "def";

					if (!sdayList.contains(i))
						status = "lock";

					txMap.put("status", status);
				}

				botResponseUtils.setDeductionDate(vo, "扣款日期", list, "");
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public TPIGatewayResult rspSetAdd(FundRequest reqVO, Map<String, Object> param) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = fundAPIFeignService.rspSetAdd(param, reqVO.getToken());

		boolean isOverTime = false;
		boolean isSuccess = false;

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				isSuccess = true;
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDataMap(data);
				if (dataMap.size() > 0) {
					List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
					CardItem cards = vo.new CardItem();
					cards.setCName("");
					cards.setCWidth(BotMessageVO.CWIDTH_250);
					cards.setCTextType("12");
					cards.setCLinkType(1);

					// 牌卡圖片
					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_GENERIC);
					cImageData.setCImageUrl("card33-02.png");
					cImageData.setCImageTextType(1);
					cards.setCImageData(cImageData.getCImageDatas());

					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
					CTextItem cText = vo.new CTextItem();
					cText.setCLabel("基金名稱");
					cText.setCText(dataMap.get("fund_name").toString());
					cTextList.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("每月扣款日期");
					cText.setCText(dataMap.get("sday").toString());
					cTextList.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("扣款模式");
					cText.setCText(dataMap.get("tradetype_txt").toString());
					cTextList.add(cText.getCTexts());

					String amtText = "";
					switch (reqVO.getTradetype()) {
					case "1":
						amtText = changeMoneyString(dataMap.get("samt").toString()) + "元";
						break;
					case "2":
						amtText += "高:" + changeMoneyString(dataMap.get("highamt").toString()) + "元<br>";
						amtText += "中:" + changeMoneyString(dataMap.get("midamt").toString()) + "元<br>";
						amtText += "低:" + changeMoneyString(dataMap.get("lowamt").toString()) + "元";
						break;
					}
					cText = vo.new CTextItem();
					cText.setCLabel("每次扣款金額");
					cText.setCText(amtText);
					cTextList.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("扣款方式");
					cText.setCText(dataMap.get("stype_txt").toString());
					cTextList.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("扣款帳號");
					cText.setCText(dataMap.get("branch").toString());
					cText.setCUnit(dataMap.get("acnt").toString());
					cTextList.add(cText.getCTexts());

					String CurrentTime = (String) dataMap.get("CurrentTime");
					if (StringUtils.isNotBlank(CurrentTime)) {
						String d = CurrentTime.substring(0, CurrentTime.indexOf(" "));
						String t = CurrentTime.substring(CurrentTime.indexOf(" ") + 1, CurrentTime.indexOf("."));
						cText = vo.new CTextItem();
						cText.setCLabel("委託時間");
						cText.setCText(d);
						cText.setCUnit(t);
						cTextList.add(cText.getCTexts());
					}

					cards.setCTexts(cTextList);
					cardList.add(cards.getCards());

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

					String email = (String) dataMap.get("email");
					if (StringUtils.isNotBlank(email)) {
						botResponseUtils.setTextResult(vo, "等下會寄委託通知單到你的約定信箱");

						cardList = new ArrayList<Map<String, Object>>();

						cards = vo.new CardItem();
						cards.setCLinkType(1);
						cards.setCWidth(BotMessageVO.CWIDTH_250);

						// 牌卡圖片
						cImageData = vo.new CImageDataItem();
						cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND_FH);
						cImageData.setCImageTextType(1);

						List<Map<String, Object>> cImageTextList = new ArrayList<>();
						CImageTextItem cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("Email：");
						cImageText.setCImageText(email);
						cImageTextList.add(cImageText.getCImageTexts());
						cImageData.setCImageTexts(cImageTextList);
						cards.setCImageData(cImageData.getCImageDatas());

						cardList.add(cards.getCards());

						botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
					}

					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, SiteQuickReplay.BUT_OTHER_RSP_FUND,
							SiteQuickReplay.INIT);
				} else {
					botResponseUtils.setTextResult(vo, "你沒有可恢復扣款的停扣基金資訊。");
				}
			} else if (isPwdError(data)) {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				vo = getBuyRspFunTxPwd(reqVO, StringUtils.defaultString(msg));
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, /* code + ":" + */ StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		tpiGatewayResult.put("isOverTime", isOverTime);
		tpiGatewayResult.put("isSuccess", isSuccess);

		return tpiGatewayResult.setData(vo);
	}
}
