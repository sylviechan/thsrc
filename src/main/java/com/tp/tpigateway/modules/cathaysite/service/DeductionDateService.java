package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.cathaysec.DefaultRequest;

public interface DeductionDateService {

	BotMessageVO deductionDate(DefaultRequest reqVO);

}
