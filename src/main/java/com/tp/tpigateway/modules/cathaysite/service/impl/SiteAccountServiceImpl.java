package com.tp.tpigateway.modules.cathaysite.service.impl;

import static com.tp.common.util.cathaysite.SiteApiUtils.getCode;
import static com.tp.common.util.cathaysite.SiteApiUtils.getData;
import static com.tp.common.util.cathaysite.SiteApiUtils.getList;
import static com.tp.common.util.cathaysite.SiteApiUtils.getMsg;
import static com.tp.common.util.cathaysite.SiteApiUtils.isSuccess;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.cathaysite.SiteAccountRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.fegin.CustAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import com.tp.tpigateway.modules.cathaysite.service.CathaySiteService;
import com.tp.tpigateway.modules.cathaysite.service.SiteAccountService;

@Service
public class SiteAccountServiceImpl implements SiteAccountService {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	CustAPIFeignService custAPIFeignService;

	@Autowired
	CathaySiteService cathaySiteService;

	protected String[][] cusbankTypeArr = { { "1", "約定扣款/買回帳號", "查詢約定扣款帳號" }, { "2", "約定買回帳號", "查詢約定買回帳號" },
			{ "3", "收益分配帳號", "查詢收益分配帳號" } };

	@Deprecated
	protected List<Map<String, Object>> statusFilter(List<Map<String, Object>> list) {
		List<Map<String, Object>> dList = new ArrayList<>();
		for (Map<String, Object> row : list) {
			String status = (String) row.get("status");
			if (!StringUtils.equals(status, "核印成功"))
				dList.add(row);
		}
		list.removeAll(dList);
		return list;
	}

	protected List<Map<String, Object>> getCusbankList(Map<String, Object> data) {
		List<Map<String, Object>> list = getList(data);
		// list = statusFilter(list);
		return list;
	}

	@Override
	public BotMessageVO getCusbankType(SiteAccountRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = custAPIFeignService.getCusbank(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				List<Map<String, Object>> list = getCusbankList(data);

				Set<String> typeSet = new HashSet();
				for (Map<String, Object> row : list) {
					String rowType = (String) row.get("type");
					typeSet.add(rowType);
				}

				if (typeSet.size() >= 2) {
					botResponseUtils.setTextResult(vo, "請選擇要查詢的帳號類型");

					List<QuickReplay> quickReplayList = new ArrayList<>();
					for (String[] arr : cusbankTypeArr)
						if (typeSet.contains(arr[0]))
							quickReplayList.add(new SiteQuickReplay(2, arr[1], arr[2], "", "", ""));

					botResponseUtils.setQuickReplayResult(vo,
							(typeSet.size() >= 3 ? BotMessageVO.LTYPE_04 : BotMessageVO.LTYPE_01),
							quickReplayList.toArray(new QuickReplay[0]));
				} else {
					String type = "";
					if (typeSet.size() > 0)
						type = typeSet.toArray(new String[0])[0];
					doCusbank(reqVO, vo, type, list);

					if (list.size() > 0)
						botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SiteQuickReplay.INIT);
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public BotMessageVO getCusbank(SiteAccountRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = custAPIFeignService.getCusbank(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				List<Map<String, Object>> list = getCusbankList(data);

				if (StringUtils.isNotBlank(reqVO.getType())) {
					List<Map<String, Object>> tmpList = new ArrayList<>();
					tmpList.addAll(list);
					list.clear();
					for (Map<String, Object> row : tmpList) {
						String rowType = (String) row.get("type");
						if (StringUtils.equals(reqVO.getType(), rowType))
							list.add(row);
					}
				}

				doCusbank(reqVO, vo, reqVO.getType(), list);

				if (list.size() > 0)
					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01,
							new SiteQuickReplay(1, "查詢其他類型帳戶", "約定帳號查詢", "", "", ""), SiteQuickReplay.INIT);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	/** 產生牌卡 */
	protected void doCusbank(SiteAccountRequest reqVO, BotMessageVO vo, String type, List<Map<String, Object>> list) {
		if (list.size() > 0) {
			String typeTxt = "約定帳號";
			if (StringUtils.isNotBlank(type))
				for (String[] arr : cusbankTypeArr)
					if (StringUtils.equals(arr[0], type)) {
						typeTxt = arr[1];
						break;
					}

			botResponseUtils.setTextResult(vo, "以下是你的" + typeTxt + "資料如下：");
			botResponseUtils.setImageResult(vo, BotMessageVO.HAPPY_PIC);

			// 組牌卡
			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
			for (Map<String, Object> row : list) {
				CardItem cardItem = vo.new CardItem();
				cardItem.setCLinkType(1);
				cardItem.setCWidth(BotMessageVO.CWIDTH_250);

				CImageDataItem cImageData = vo.new CImageDataItem();
				cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND);
				cImageData.setCImageTextType(1);

				List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
				CImageTextItem cImageText = vo.new CImageTextItem();
				cImageText.setCImageTextTitle("銀行");
				cImageText.setCImageText(row.get("bank").toString() + " " + row.get("branch").toString());
				cImageTexts.add(cImageText.getCImageTexts());

				cImageText = vo.new CImageTextItem();
				cImageText.setCImageTextTitle("帳號");
				cImageText.setCImageText(row.get("acnt").toString());
				cImageTexts.add(cImageText.getCImageTexts());

				cImageData.setCImageTexts(cImageTexts);
				cardItem.setCImageData(cImageData.getCImageDatas());

				cardList.add(cardItem.getCards());
			}
			botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
		} else {
			botResponseUtils.setImageResult(vo, BotMessageVO.EMBARRASSED_PIC);
			botResponseUtils.setTextResult(vo, "糟糕，找不到你要查詢的約定帳戶資訊？");
			cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());
		}
	}
}
