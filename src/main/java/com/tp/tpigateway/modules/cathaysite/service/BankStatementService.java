package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.BankStatementRequest;

import java.util.Map;

public interface BankStatementService {
	
	
	/**
	 * For 補寄對帳單
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getBillIdCheckForm(BankStatementRequest reqVO);

	/**
	 * For 補寄對帳單核對身份
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult getBillIdCheck(BankStatementRequest reqVO, Map<String, Object> param);

	/**
	 * For 補寄對帳單
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getBillSend(BankStatementRequest reqVO, Map<String, Object> param);
}