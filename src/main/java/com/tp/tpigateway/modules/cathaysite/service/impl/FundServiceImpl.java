package com.tp.tpigateway.modules.cathaysite.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.*;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.common.model.cathaysite.FundStopRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.fegin.FundAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import com.tp.tpigateway.modules.cathaysite.service.CathaySiteService;
import com.tp.tpigateway.modules.cathaysite.service.FundService;
import com.tp.tpigateway.modules.cathaysite.service.RspFundService;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.tp.common.util.cathaysite.SiteApiUtils.*;

@Service("fundService")
public class FundServiceImpl implements FundService {

	private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    BotResponseUtils botResponseUtils;

    @Autowired
    FundAPIFeignService fundAPIFeignService;
    
    @Autowired
	CathaySiteService cathaySiteService;
    
    @Autowired
    RspFundService rspFundService;

    @Value("${cathaysite.fund.path}")
	private String cathaysiteFundPath;

    @Override
	public BotMessageVO webFundKey(FundRequest reqVO) {
    	BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
    	
    	botResponseUtils.setTextResult(vo, "想查詢那個基金");
    	botResponseUtils.setQuickReplayResult(vo,BotMessageVO.LTYPE_03, SiteQuickReplay.QE_FUND_NAV, SiteQuickReplay.QE_FUND_ALLOTINTEREST);

        return vo;
	}

    @Override
    public BotMessageVO getWebFund(FundRequest reqVO, Map<String, Object> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        String searchType = reqVO.getSearchType();

        Map<String, Object> result = fundAPIFeignService.getWebFund(param);

        if (result != null) {
            Map<String, Object> data = getData(result);
			
            if (isSuccess(data)) {
                botResponseUtils.setSiteFundList(vo, searchType, getList(data));
            } else {
                logger.error("code = {}, msg = {}", getCode(data), getMsg(data));
                botResponseUtils.setSiteFundList(vo, searchType, new ArrayList<>());
            }
        } else {
            logger.error("result is null");
            botResponseUtils.setSiteFundList(vo, searchType, new ArrayList<>());
        }

        return vo;
    }

	@Override
	public TPIGatewayResult getWebFundNav(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = fundAPIFeignService.getWebFundNav(param);
		if (result != null) {
			Map<String, Object> resultd = JSON.parseObject(result.get("d").toString(),
					new TypeReference<Map<String, Object>>() {
					});

			if (isSuccess(resultd)) {
				Map<String, Object> dataValue = JSON.parseObject(resultd.get("DataValue").toString(),
						new TypeReference<Map<String, Object>>() {
						});
				List<Map<String, Object>> dataList = JSONArray.parseObject(dataValue.get("list").toString(),
						new TypeReference<List<Map<String, Object>>>() {
						});

				if (dataList.size() > 0) {
					Map<String, Object> data = dataList.get(0);
					JSONObject link = (JSONObject) data.get("link");
					String fund_info_url = StringUtils.defaultString(link.getString("fund_info_url"));
					
					// 組牌卡
					List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
					CardItem cards = vo.new CardItem();
					cards.setCLinkType(1);
					cards.setCTextType("1");
					cards.setCWidth(BotMessageVO.CWIDTH_250);

					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_CB03);
					cImageData.setCImageTextType(1);

					List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
					CImageTextItem cImageText = vo.new CImageTextItem();
					if (StringUtils.equals((String) data.get("fund_type"), "25")) {
						cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("基金名稱");
						cImageText.setCImageText(data.get("fund_name").toString());
						cImageTexts.add(cImageText.getCImageTexts());

						if (StringUtils.isNotBlank(data.get("stock_code").toString())) {
							cImageText = vo.new CImageTextItem();
							cImageText.setCImageTextTitle("股票代號");
							cImageText.setCImageText(data.get("stock_code").toString());
							cImageTexts.add(cImageText.getCImageTexts());
						}
						
						List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();
						if (StringUtils.equals(link.getString("isallot"), "1")) {

							String fund = reqVO.getFund();
							String intent = "基金";
							String secondBotInter = "查詢基金配息";
							Map<String, Object> parameter = new HashMap<>();
							parameter.put("fund", fund);
							String alt = "查詢配息資訊(" + fund + ")";

							SiteQuickReplay clink = new SiteQuickReplay(2, "查詢配息資訊", alt, "", "", "", intent,
									secondBotInter, parameter);

							cLinkContents.add(botResponseUtils.getCLinkListItem(vo, clink).getCLinkList());
						}
						
						if (StringUtils.isNotBlank(fund_info_url))
							cLinkContents.add(botResponseUtils
									.getCLinkListItem(vo, new SiteQuickReplay(4, "了解更多", "了解更多", "", fund_info_url, ""))
									.getCLinkList());
						cards.setCLinkList(cLinkContents);
					} else {
						cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("基金名稱");
						cImageText.setCImageText(data.get("fund_name").toString());
						cImageTexts.add(cImageText.getCImageTexts());

//						cImageText = vo.new CImageTextItem();
//						cImageText.setCImageTextTitle("股票代號");
//						cImageText.setCImageText("");
//						cImageTexts.add(cImageText.getCImageTexts());
						
						List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();
						if (StringUtils.equals(link.getString("isallot"), "1")) {

							String fund = reqVO.getFund();
							String intent = "基金";
							String secondBotInter = "查詢基金配息";
							Map<String, Object> parameter = new HashMap<>();
							parameter.put("fund", fund);
							String alt = "查詢配息資訊(" + fund + ")";

							SiteQuickReplay clink = new SiteQuickReplay(2, "查詢配息資訊", alt, "", "", "", intent,
									secondBotInter, parameter);

							cLinkContents.add(botResponseUtils.getCLinkListItem(vo, clink).getCLinkList());
						}
						
						if (StringUtils.equals(link.getString("ispur"), "1"))
							cLinkContents.add(botResponseUtils
									.getCLinkListItem(vo, SiteQuickReplay.APPLY_ONE_FUND(reqVO.getFund())).getCLinkList());
						if (StringUtils.equals(link.getString("isrsp"), "1"))
							cLinkContents.add(botResponseUtils
									.getCLinkListItem(vo, SiteQuickReplay.APPLY_RSP_FUND(reqVO.getFund())).getCLinkList());
						if (StringUtils.isNotBlank(fund_info_url))
							cLinkContents.add(botResponseUtils
									.getCLinkListItem(vo, new SiteQuickReplay(4, "了解更多", "了解更多", "", fund_info_url, ""))
									.getCLinkList());
						cards.setCLinkList(cLinkContents);
					}

					cImageData.setCImageTexts(cImageTexts);
					cards.setCImageData(cImageData.getCImageDatas());

					List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
					CTextItem cText = vo.new CTextItem();
					cText.setCLabel("淨值日期");
					cText.setCText(data.get("wdate").toString());
					cTexts.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("最新淨值");
					cText.setCText(data.get("nav").toString());
					cTexts.add(cText.getCTexts());

					String udcode;
					if (StringUtils.equals(data.get("udcode").toString(), "U")) {
						udcode = "+";
					} else {
						udcode = "-";
					}

					cText = vo.new CTextItem();
					cText.setCLabel("日漲跌");
					cText.setCText(udcode + data.get("xnav").toString());
					cTexts.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("漲跌幅");
					cText.setCText(udcode + data.get("rate").toString() + "%");
					cTexts.add(cText.getCTexts());

					cards.setCTexts(cTexts);

					cardList.add(cards.getCards());
					botResponseUtils.setFundCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
					botResponseUtils.setTextResult(vo, "還需要阿發查其他基金嗎?");

					QuickReplay q1 = SiteQuickReplay.FUND_NAV_ELSE;
					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_03, q1, SiteQuickReplay.END_CHAT);

					tpiGatewayResult.setMsgList(Arrays.asList(q1.getText()));
					tpiGatewayResult.setOptionList(Arrays.asList(q1));

				} else {
					botResponseUtils.setTextResult(vo, "查無資料");
				}
			} else {
				logger.error("code = {}, msg = {}", getCode(resultd), getMsg(resultd));
			}
		} else {
			logger.error("result is null");
		}

		return tpiGatewayResult.setData(vo);
	}

    @Override
    public TPIGatewayResult getWebFundAllotinterest(FundRequest reqVO, Map<String, Object> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = fundAPIFeignService.getWebFundAllotinterest(param);
        if (result != null) {
        	Map<String, Object> resultd = JSON.parseObject(result.get("d").toString(), new TypeReference<Map<String,Object>>(){});
        	
            if (isSuccess(resultd)) {
            	Map<String, Object> dataValue = JSON.parseObject(resultd.get("DataValue").toString(), new TypeReference<Map<String,Object>>(){});
            	List<Map<String, Object>> dataList = JSONArray.parseObject(dataValue.get("list").toString(), new TypeReference<List<Map<String, Object>>>(){});
            	
				if (dataList.size() > 0) {
					Map<String, Object> data = dataList.get(0);

					// TODO 組牌卡
					List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
					CardItem cards = vo.new CardItem();
					cards.setCLinkType(1);
					cards.setCTextType("1");
					cards.setCWidth(BotMessageVO.CWIDTH_250);

					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_CB03);
					cImageData.setCImageTextType(1);

					List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
					CImageTextItem cImageText = vo.new CImageTextItem();
					if (StringUtils.equals((String)data.get("fund_type"),"25")) {
						cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("基金名稱");
						cImageText.setCImageText(data.get("fund_name").toString());
						cImageTexts.add(cImageText.getCImageTexts());

						if (StringUtils.isNotBlank(data.get("stock_code").toString())) {
							cImageText = vo.new CImageTextItem();
							cImageText.setCImageTextTitle("股票代號");
							cImageText.setCImageText(data.get("stock_code").toString());
							cImageTexts.add(cImageText.getCImageTexts());
						}
					} else {
						cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("基金名稱");
						cImageText.setCImageText(data.get("fund_name").toString());
						cImageTexts.add(cImageText.getCImageTexts());

//						cImageText = vo.new CImageTextItem();
//						cImageText.setCImageTextTitle("股票代號");
//						cImageText.setCImageText("");
//						cImageTexts.add(cImageText.getCImageTexts());
					}

					cImageData.setCImageTexts(cImageTexts);
					cards.setCImageData(cImageData.getCImageDatas());

					List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
					CTextItem cText = vo.new CTextItem();
					cText.setCLabel("配息年月");
					cText.setCText(data.get("everyallotym").toString());
					cTexts.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("每單位分配金額(元)");
					if (StringUtils.equals("新台幣", data.get("vmoney_type").toString())) {
						cText.setCText(data.get("allotmoney").toString());
					} else {
						cText.setCText(data.get("allotmoney").toString());
					}
					
					cTexts.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("當期配息率");
					cText.setCText(data.get("mallot").toString() + "%");
					cTexts.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("年化配息率");
					cText.setCText(data.get("yallot").toString() + "%");
					cTexts.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("配息基準日淨值(元)");
					if (StringUtils.equals("新台幣", data.get("vmoney_type").toString())) {
						cText.setCText(data.get("basedate_nav").toString());	
					} else {
						cText.setCText(data.get("basedate_nav").toString());
					}
					
					cTexts.add(cText.getCTexts());
					if (StringUtils.equals(data.get("fund_type").toString(),"25")) {
						cText = vo.new CTextItem();
						cText.setCLabel("除息交易日");
						cText.setCText(data.get("tradedate").toString());
						cTexts.add(cText.getCTexts());
					} else {
						cText = vo.new CTextItem();
						cText.setCLabel("配息基準日");
						cText.setCText(data.get("basedate").toString());
						cTexts.add(cText.getCTexts());
					}

					cText = vo.new CTextItem();
					cText.setCLabel("配息發放日");
					cText.setCText(data.get("lendingdate").toString());
					cTexts.add(cText.getCTexts());

					cText = vo.new CTextItem();
					cText.setCLabel("配息頻率");
					cText.setCText(data.get("allotyes").toString());
					cTexts.add(cText.getCTexts());

					cards.setCTexts(cTexts);

					JSONObject link = (JSONObject) data.get("link");
					String fund_info_url = StringUtils.defaultString(link.getString("fund_info_url"));
					List<Map<String, Object>> cLinkContents = new ArrayList<>();

					if (StringUtils.isNotBlank(fund_info_url))
						cLinkContents.add(botResponseUtils.getCLinkListItem(vo, new SiteQuickReplay(StringUtils.isNotBlank(fund_info_url) ? 4 : 1,
									"了解更多", "了解更多", "", fund_info_url, "")).getCLinkList());

					cards.setCLinkList(cLinkContents);
					cardList.add(cards.getCards());
					botResponseUtils.setFundCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
					botResponseUtils.setTextResult(vo, "還需要阿發查其他基金嗎?");

					QuickReplay q1 = SiteQuickReplay.FUND_ALLOTINTEREST_ELSE;
					botResponseUtils.setQuickReplayResult(vo,BotMessageVO.LTYPE_03, q1, SiteQuickReplay.END_CHAT);

					tpiGatewayResult.setMsgList(Arrays.asList(q1.getText()));
					tpiGatewayResult.setOptionList(Arrays.asList(q1));

				} else {
					botResponseUtils.setTextResult(vo, "該基金尚未開始配息，查無配息資訊");

					QuickReplay q1 = SiteQuickReplay.FUND_ALLOTINTEREST_ELSE;
					botResponseUtils.setQuickReplayResult(vo,BotMessageVO.LTYPE_03, q1, SiteQuickReplay.END_CHAT);

					tpiGatewayResult.setMsgList(Arrays.asList(q1.getText()));
					tpiGatewayResult.setOptionList(Arrays.asList(q1));					
				}
            }  else {
                logger.error("code = {}, msg = {}", getCode(resultd), getMsg(resultd));
            }
        } else {
            logger.error("result is null");
        }

        return tpiGatewayResult.setData(vo);
    }
    
	@Override
	public BotMessageVO getFundStopList(FundStopRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getFundStopList(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				List<Map<String, Object>> list = getList(data);
				if (list.size() > 0) {
					botResponseUtils.setSiteFundStopList(vo, list);
				} else {
					botResponseUtils.setTextResult(vo, "你沒有可恢復扣款的停扣基金資訊。");
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public TPIGatewayResult getFundStopDetail(FundStopRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = fundAPIFeignService.getFundStopList(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				List<Map<String, Object>> list = getList(data);
				if (list.size() > 0) {
					for (Map<String, Object> row : list) {
						String tno = (String) row.get("tno");
						if (StringUtils.equals(tno, reqVO.getTno())) {
							botResponseUtils.setTextResult(vo, "請確認你要恢復扣款的停扣基金資訊:");

							// 組牌卡
							List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
							CardItem cards = vo.new CardItem();
							cards.setCLinkType(1);
							cards.setCTextType("3");
							cards.setCWidth(BotMessageVO.CWIDTH_250);

							CImageDataItem cImageData = vo.new CImageDataItem();
							cImageData.setCImage(BotMessageVO.IMG_CB03);
							cImageData.setCImageTextType(1);
							cImageData.setCImageTag("定期");

							List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
							CImageTextItem cImageText = vo.new CImageTextItem();
							cImageText.setCImageTextTitle("基金名稱");
							cImageText.setCImageText(row.get("fund_name").toString());
							cImageTexts.add(cImageText.getCImageTexts());

							Object sday = row.get("sday");
							cImageText = vo.new CImageTextItem();
							cImageText.setCImageTextTitle("扣款日期");
							cImageText.setCImageText("每月" + sday + "號");
							cImageTexts.add(cImageText.getCImageTexts());

							cImageData.setCImageTexts(cImageTexts);
							cards.setCImageData(cImageData.getCImageDatas());

							List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
							String pay_date = row.get("pay_date").toString();
							try {
								Date d = DateUtils.parseDate(pay_date, "yyyyMMdd");
								pay_date = DateFormatUtils.format(d, "yyyy/MM/dd");
							} catch (ParseException e) {
								logger.error("pay_date:'" + pay_date + "' " + e.getClass() + ":" + e.getMessage());
								e.printStackTrace();
							}
							CTextItem cText = vo.new CTextItem();
							cText.setCLabel("預計扣款日");
							cText.setCText(pay_date);
							cTexts.add(cText.getCTexts());

							cText = vo.new CTextItem();
							cText.setCLabel("扣款模式");
							cText.setCText(row.get("tradetype_txt").toString());
							cTexts.add(cText.getCTexts());

							String tradetype = row.get("tradetype").toString();
							// 根據tradetype顯示 扣款金額 或 低中高金額
							String amtText = "";
							switch (tradetype) {
							case "1":
								Object samt = row.get("samt");
								if (samt instanceof Number)
									samt = ((Number) samt).intValue();
								amtText = changeMoneyString(String.valueOf(samt)) + "元";
								break;
							case "2":
								Object highamt = row.get("highamt");
								if (highamt instanceof Number)
									highamt = ((Number) highamt).intValue();
								Object midamt = row.get("midamt");
								if (midamt instanceof Number)
									midamt = ((Number) midamt).intValue();
								Object lowamt = row.get("lowamt");
								if (lowamt instanceof Number)
									lowamt = ((Number) lowamt).intValue();
								amtText += "高:" + changeMoneyString(String.valueOf(highamt)) + "元<br>";
								amtText += "中:" + changeMoneyString(String.valueOf(midamt)) + "元<br>";
								amtText += "低:" + changeMoneyString(String.valueOf(lowamt)) + "元";
								break;
							}

							cText = vo.new CTextItem();
							cText.setCLabel("每次扣款金額");
							cText.setCText(amtText);
							cTexts.add(cText.getCTexts());

							cText = vo.new CTextItem();
							cText.setCLabel("扣款方式");
							cText.setCText(row.get("stype_txt").toString());
							cTexts.add(cText.getCTexts());

							String branch = (String) row.get("branch");
							String acnt = (String) row.get("acnt");
							String bact = branch + "<br>" + acnt;
							cText = vo.new CTextItem();
							cText.setCLabel("扣款帳號");
							cText.setCText(bact);
							cTexts.add(cText.getCTexts());

							cards.setCTexts(cTexts);

							QuickReplay q1 = SiteQuickReplay.FUND_STOP_YES(tno);
							QuickReplay q2 = SiteQuickReplay.FUND_STOP_RE;
							QuickReplay q3 = SiteQuickReplay.FUND_STOP_NO;
							cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2, q3));

							tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText()));
							tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3));

							cardList.add(cards.getCards());
							botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
							break;
						}
					}
				} else {
					botResponseUtils.setTextResult(vo, "你沒有可恢復扣款的停扣基金資訊。");
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	/**
	 * 恢復扣款 交易認證碼浮出視窗
	 * 
	 * @param vo
	 * @param tno
	 * @param linkList
	 * @param errMsg
	 */
	protected void doFundStatusRestore(BotMessageVO vo, String tno, List<Map> linkList, String errMsg) {
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("tno", tno);

		botResponseUtils.doTxPwd(vo, "確認恢復扣款", "執行恢復扣款(" + tno + ")", "帳務", "doFundStatusRestore", parameter, linkList,
				errMsg);
	}

	@Override
	public BotMessageVO getFundStopTxPwd(FundStopRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		return getFundStopTxPwd(vo, reqVO, param, "");
	}

	protected BotMessageVO getFundStopTxPwd(BotMessageVO vo, FundStopRequest reqVO, Map<String, Object> param,
			String errorMsg) {
		Map<String, Object> result = fundAPIFeignService.getFundStopList(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				List<Map<String, Object>> list = getList(data);
				if (list.size() > 0) {
					for (Map<String, Object> row : list) {
						String tno = (String) row.get("tno");
						if (StringUtils.equals(tno, reqVO.getTno())) {
							List<Map> linkList = new ArrayList<>();

							//String fund_name = (String) row.get("fund_name");
							String fund_doc = (String) row.get("fund_doc");
							if (StringUtils.isNotBlank(fund_doc)) {
								String fund_doc_url = cathaysiteFundPath + fund_doc;
								linkList.add(botResponseUtils
										.getLinkListItem(vo,
												new SiteQuickReplay(4, "公開說明書", "", "", fund_doc_url, ""))
										.getLinkList());
							}

							Map<String, String> declare = (Map<String, String>) getDataValue(data).get("Declare");
							for (String key : declare.keySet()) {
								Map<String, String> l1obj = (Map<String, String>) param.get("obj");
								l1obj.put("query", key);
								Map<String, Object> resultOne = fundAPIFeignService.getOne(param, reqVO.getToken());
								if (resultOne != null) {
									Map<String, Object> dataOne = getData(resultOne);
									String title = "", alt = "";
									if (isSuccess(dataOne)) {
										Map<String, Object> dvOne = getDataValue(dataOne);
										Map<String, Object> mapDataOne = (Map<String, Object>) dvOne.get("data");
										title = (String) mapDataOne.get("title");
										alt = (String) mapDataOne.get("txt");
										String stype = (String) mapDataOne.get("stype");

										if (StringUtils.equals(stype, "2")) {
											// 純文字
											alt = alt.replaceAll("\r\n", "<br>");
										}

									} else {
										String code = getCode(dataOne), msg = getMsg(dataOne);
										logger.error("code = {}, msg = {}", code, msg);
										alt = code + ":" + StringUtils.defaultString(msg);
									}
									linkList.add(botResponseUtils
											.getLinkListItem(vo, new SiteQuickReplay(0, title, alt, "", "", ""))
											.getLinkList());
								}
							}

							doFundStatusRestore(vo, reqVO.getTno(), linkList, errorMsg);
							break;
						}
					}
				} else {
					botResponseUtils.setTextResult(vo, "你沒有可恢復扣款的停扣基金資訊。");
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public BotMessageVO setFundStatusRestore(FundStopRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.setFundStatusRestore(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, Object> dataMap = getDataMap(data);
				// 組牌卡
				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				CardItem cards = vo.new CardItem();
				cards.setCWidth(BotMessageVO.CWIDTH_250);
				cards.setCTextType("12");
				cards.setCLinkType(1);

				CImageDataItem cImageData = vo.new CImageDataItem();
				cImageData.setCImage(BotMessageVO.IMG_GENERIC);
				cImageData.setCImageUrl("card33-01.png");
				cImageData.setCImageTextType(1);
				cards.setCImageData(cImageData.getCImageDatas());

				List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
				CTextItem cText = vo.new CTextItem();
				cText.setCLabel("基金名稱");
				cText.setCText(dataMap.get("fund_name").toString());
				cTexts.add(cText.getCTexts());

				Object sday = dataMap.get("sday");
				cText = vo.new CTextItem();
				cText.setCLabel("扣款日期");
				cText.setCText("每月" + sday + "號");
				cTexts.add(cText.getCTexts());

				String pay_day = dataMap.get("pay_date").toString();
				try {
					Date d = DateUtils.parseDate(pay_day, "yyyyMMdd");
					pay_day = DateFormatUtils.format(d, "yyyy/MM/dd");
				} catch (ParseException e) {
					e.printStackTrace();
				}
				cText = vo.new CTextItem();
				cText.setCLabel("預計扣款日");
				cText.setCText(pay_day);
				cTexts.add(cText.getCTexts());

				cText = vo.new CTextItem();
				cText.setCLabel("扣款模式");
				cText.setCText(dataMap.get("tradetype_txt").toString());
				cTexts.add(cText.getCTexts());

				String tradetype = dataMap.get("tradetype").toString();
				// 根據tradetype顯示 扣款金額 或 低中高金額
				String amtText = "";
				switch (tradetype) {
				case "1":
					Object samt = dataMap.get("samt");
					if (samt instanceof Number)
						samt = ((Number) samt).intValue();
					amtText = changeMoneyString(String.valueOf(samt)) + "元";
					break;
				case "2":
					Object highamt = dataMap.get("highamt");
					if (highamt instanceof Number)
						highamt = ((Number) highamt).intValue();
					Object midamt = dataMap.get("midamt");
					if (midamt instanceof Number)
						midamt = ((Number) midamt).intValue();
					Object lowamt = dataMap.get("lowamt");
					if (lowamt instanceof Number)
						lowamt = ((Number) lowamt).intValue();
					amtText += "高:" + changeMoneyString(String.valueOf(highamt)) + "元<br>";
					amtText += "中:" + changeMoneyString(String.valueOf(midamt)) + "元<br>";
					amtText += "低:" + changeMoneyString(String.valueOf(lowamt)) + "元";
					break;
				}

				cText = vo.new CTextItem();
				cText.setCLabel(/* "每次"+ */"扣款金額");
				cText.setCText(amtText);
				cTexts.add(cText.getCTexts());

				cText = vo.new CTextItem();
				cText.setCLabel("扣款方式");
				cText.setCText(dataMap.get("stype_txt").toString());
				cTexts.add(cText.getCTexts());

				String branch = (String) dataMap.get("branch");
				String acnt = (String) dataMap.get("acnt");
				String bact = branch + "<br>" + acnt;
				cText = vo.new CTextItem();
				cText.setCLabel("扣款帳號");
				cText.setCText(bact);
				cTexts.add(cText.getCTexts());

				cards.setCTexts(cTexts);

				cardList.add(cards.getCards());
				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

				String email = (String) dataMap.get("email");
				if (StringUtils.isNotBlank(email)) {
					botResponseUtils.setTextResult(vo, "等下會寄委託通知單到你的約定信箱");

					cardList = new ArrayList<Map<String, Object>>();

					cards = vo.new CardItem();
					cards.setCLinkType(1);
					cards.setCWidth(BotMessageVO.CWIDTH_250);

					// 牌卡圖片
					cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND_FH);
					cImageData.setCImageTextType(1);

					List<Map<String, Object>> cImageTextList = new ArrayList<>();
					CImageTextItem cImageText = vo.new CImageTextItem();
					cImageText.setCImageTextTitle("Email：");
					cImageText.setCImageText(email);
					cImageTextList.add(cImageText.getCImageTexts());
					cImageData.setCImageTexts(cImageTextList);
					cards.setCImageData(cImageData.getCImageDatas());

					cardList.add(cards.getCards());

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
				}

				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_03, SiteQuickReplay.BUT_OTHER_FUND_STOP,
						SiteQuickReplay.INIT);
			} else if (isPwdError(data)) {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				getFundStopTxPwd(vo, reqVO, param, StringUtils.defaultString(msg));
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
				botResponseUtils.setTextResult(vo, /* code + ":" + */ StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setImageResult(vo, BotMessageVO.APOLOGIZE_PIC);
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public BotMessageVO getFundList(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getFundList(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				List<Map<String, Object>> list = getList(data);
				if (list.size() > 0) {
					botResponseUtils.setTextResult(vo, "請挑選你想要申購的基金：");
					botResponseUtils.setSiteFundList2(vo, list);
				} else {
					botResponseUtils.setTextResult(vo, "很抱歉！目前無可申購的基金！");
					cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}
	
	@Override
	public Map<String, Object> getFundDetail(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());		
		Map<String, Object> result = fundAPIFeignService.getFundDetail(param, reqVO.getToken());
		Map<String, Object> returnResult = new HashMap<String, Object>();
		String rDate = "";
		String amt = "";
		String acnt = "";
		String acntName = "";
		String sno = "";
		String note = "";
		String fundDoc = "";
		String declare = "";

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				note = String.valueOf(dataMap.get("notes"));
				fundDoc = String.valueOf(dataMap.get("fund_doc"));				
				
				Map<String, String> declareMap = (Map<String, String>) dataMap.get("Declare");
				for (String key : declareMap.keySet()) {
					if(!"".equals(declare)) {
						declare += ",";
					}
					
					declare += key;
				}
						
				if (dataMap.size() > 0) {
					if("".equals(StringUtils.defaultString(reqVO.getAcnt()))) {
						// 初始扣款帳號
						Map<String, Object> acntMap = JSON.parseObject(((JSONArray)dataMap.get("acnts")).getString(0), Map.class);
						if(acntMap.size() > 0) {
							acnt = (String) acntMap.get("acntno");
							acntName = (String) acntMap.get("bdesc");
							sno = (String) acntMap.get("sno");
						}
					}					
					
					if("date".equals(reqVO.getType())) {	// 申購日期
						JSONArray canBuyDays = (JSONArray)dataMap.get("tDays");										
						rDate = reqVO.getRdate();
						
						if("".equals(StringUtils.defaultString(rDate))) {
							botResponseUtils.setTextResult(vo, "阿發幫你找出最接近的可申購日期，或你可自己輸入其他日期喔！例如2019年7月3號，請輸入20190703");
						} else {
							// 日期檢核
							rDate = rDate.replaceAll("/", "").replaceAll("-", "");
							
							for(int i=0; i<canBuyDays.size(); i++) {
								if(rDate.equals(canBuyDays.getString(i))) {
									reqVO.setPass(true);
									break;
								}														
							}
							
							if(!reqVO.isPass()) {
								botResponseUtils.setTextResult(vo, "你選擇的申購日為當地的非營業時間喔，請挑選鄰近的可申購日期，或再輸入其他日期");
							}
						}
						
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
						
						if(reqVO.isPass()) {
							// 日期檢核通過
							try {
								rDate = sdf2.format(sdf.parse(rDate));
							} catch (ParseException e) {
								e.printStackTrace();
							}
						} else {
							try {		
								String canButDays1 = sdf2.format(sdf.parse(canBuyDays.getString(0)));
								String canButDays2 = sdf2.format(sdf.parse(canBuyDays.getString(1)));
								String canButDays3 = sdf2.format(sdf.parse(canBuyDays.getString(2)));
								botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_02, 
										new SiteQuickReplay(1, canButDays1, canButDays1, "", "", ""),
										new SiteQuickReplay(1, canButDays2, canButDays2, "", "", ""),
										new SiteQuickReplay(1, canButDays3, canButDays3, "", "", ""));
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					} else if("dateDelay".equals(reqVO.getType())) {	// 申購日期延期
						JSONArray canBuyDays = (JSONArray)dataMap.get("tDays");										
						rDate = StringUtils.defaultString(reqVO.getRdate());
						
						if(!"".equals(rDate)) {
							// 日期延期
							if(canBuyDays.size() > 0) {
								reqVO.setPass(true);
								rDate = canBuyDays.getString(0);
							} else {
								reqVO.setPass(false);
								botResponseUtils.setTextResult(vo, "沒有下一個申購日期了喔！");
							}
							
							/*
							rDate = rDate.replaceAll("/", "").replaceAll("-", "");
							int dateIndex = 0;
							
							for(int i=0; i<canBuyDays.size(); i++) {
								if(rDate.equals(canBuyDays.getString(i))) {
									dateIndex = i;
									reqVO.setPass(true);
									break;
								}														
							}
							
							dateIndex += 1;
							if(dateIndex < canBuyDays.size() && reqVO.isPass()) {
								rDate = canBuyDays.getString(dateIndex);
							} else {
								reqVO.setPass(false);
								botResponseUtils.setTextResult(vo, "沒有下一個申購日期了喔！");
							}
							*/
						} else {
							botResponseUtils.setTextResult(vo, "阿發失憶了喔！不記得你所選的申購日期了");
						}
					} else if("money".equals(reqVO.getType())) {	// 申購金額
						amt = StringUtils.defaultString(reqVO.getAmt());
						Integer slow_amt = getInteger(dataMap.get("slow_amt").toString());
						Integer upper_amt = getInteger(dataMap.get("upper_amt").toString());
						String moneyType = dataMap.get("currency_txt").toString();
						String moneyTypeEng = "";
						
						if(moneyType.indexOf("台幣") > -1) {
							moneyTypeEng = "NTD";
						} else if (moneyType.indexOf("美金") > -1 || moneyType.indexOf("美元") > -1) {
							moneyTypeEng = "USD";
						} else if (moneyType.indexOf("人民幣") > -1) {
							moneyTypeEng = "RMB";
						}
						
						// 金錢檢核
						amt = amt.replaceAll(",", "").replaceAll("NTD", "").replaceAll(" ", "");						
						if(getInteger(amt) == 0 || getInteger(amt)%1000 > 0 || getInteger(amt) < slow_amt || getInteger(amt) > upper_amt) {
							String noPassMsg = "請選擇扣款金額，或輸入其他金額(千元為單位)。提醒你扣款金額不能低於" + moneyType + changeMoneyString(slow_amt.toString()) + "元";							

							if(getInteger(amt) > upper_amt) {
								noPassMsg += "，輸入金額最高" + moneyType + changeMoneyString(upper_amt.toString()) + "元";
							}	
							
							botResponseUtils.setTextResult(vo, noPassMsg);
						} else {
							reqVO.setPass(true);
						}
						
						if(reqVO.isPass()) {
							// 金額檢核通過
							
						} else {																				
							JSONArray moneyAry = (JSONArray)dataMap.get("range_amt");
							Integer money1 = getInteger(JSON.parseObject(moneyAry.getString(0), String.class));
							Integer money2 = getInteger(JSON.parseObject(moneyAry.getString(1), String.class));
							Integer money3 = getInteger(JSON.parseObject(moneyAry.getString(2), String.class));
							
							botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_02, 
									new SiteQuickReplay(1, moneyTypeEng + " " + changeMoneyString(money1.toString()), money1.toString(), "", "", ""),
									new SiteQuickReplay(1, moneyTypeEng + " " + changeMoneyString(money2.toString()), money2.toString(), "", "", ""),
									new SiteQuickReplay(1, moneyTypeEng + " " + changeMoneyString(money3.toString()), money3.toString(), "", "", ""));
						}																		
					} else if("account".equals(reqVO.getType())) {	// 申購帳號
						JSONArray acnts = (JSONArray)dataMap.get("acnts");
						
						botResponseUtils.setTextResult(vo, "要用哪個約定扣款帳戶呢?");
						
						List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
						for(int i=0; i<acnts.size(); i++) {
							Map<String, Object> acntsMap = JSON.parseObject(((JSONArray)dataMap.get("acnts")).getString(i), Map.class);
							
							if(acntsMap.size() > 0) {
								acnt = (String) acntsMap.get("acntno");
								acntName = (String) acntsMap.get("bdesc");
								sno = (String) acntsMap.get("sno");
								
								CardItem cardItem = vo.new CardItem();
								cardItem.setCLinkType(1);
								cardItem.setCWidth(BotMessageVO.CWIDTH_250);
								
								CImageDataItem cImageData = vo.new CImageDataItem();
								cImageData.setCImage(BotMessageVO.IMG_CB03);
								cImageData.setCImageTextType(1);

								List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
								CImageTextItem cImageText = vo.new CImageTextItem();
								cImageText.setCImageTextTitle("銀行");
								cImageText.setCImageText(acntName);
								cImageTexts.add(cImageText.getCImageTexts());
								
								cImageText = vo.new CImageTextItem();
								cImageText.setCImageTextTitle("帳號");
								cImageText.setCImageText(acnt);
								cImageTexts.add(cImageText.getCImageTexts());
														
								cImageData.setCImageTexts(cImageTexts);
								cardItem.setCImageData(cImageData.getCImageDatas());						

								String intent = "基金";
								String secondBotIntent = "toUpdateAccount_oneFund";
								Map<String, Object> parameter = new HashMap<>();
								parameter.put("fund", reqVO.getFund());
								parameter.put("sno", sno);
								parameter.put("acnt", acnt);
								parameter.put("acntName", acntName);

								QuickReplay q1 = new SiteQuickReplay(2, "好啊！就用這個扣款", sno + "-" + acnt, intent, secondBotIntent, parameter);

								cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, q1));
								cardList.add(cardItem.getCards());																
							}
						}

						botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList); 
					}

				} else {
					botResponseUtils.setTextResult(vo, "該基金沒有任何明細資訊喔。");
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		returnResult.put("botMessageVO", vo);
 		returnResult.put("isPass", reqVO.isPass());
 		returnResult.put("type", reqVO.getType());
 		returnResult.put("rDate", StringUtils.defaultString(rDate));
 		returnResult.put("amt", StringUtils.defaultString(amt));
 		returnResult.put("acnt", StringUtils.defaultString(acnt));
 		returnResult.put("acntName", StringUtils.defaultString(acntName));
 		returnResult.put("sno", StringUtils.defaultString(sno));
 		returnResult.put("note", StringUtils.defaultString(note));
 		returnResult.put("fundDoc", StringUtils.defaultString(fundDoc));
 		returnResult.put("declare", StringUtils.defaultString(declare));
 		
		return returnResult;
	}
	
	@Override
	public TPIGatewayResult setAdd(FundRequest reqVO, Map<String, Object> param) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = fundAPIFeignService.setAdd(param, reqVO.getToken());

		boolean isOverTime = false;
		boolean isSuccess = false;

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				isSuccess = true;
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDataMap(data);
				if (dataMap.size() > 0) {
					List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
					CardItem cards = vo.new CardItem();
					cards.setCName("");
					cards.setCWidth(BotMessageVO.CWIDTH_250);
					cards.setCTextType("12");

					// 注意事項
					cards.setCLinkType(8);

					// 牌卡圖片
					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_GENERIC);
					cImageData.setCImageUrl("card33-02.png");
					cImageData.setCImageTextType(1);
					cards.setCImageData(cImageData.getCImageDatas());

					// 牌卡下縮放文字 start
					// notes參數改為 setadd api回傳
					List<Map<String, Object>> cTextList2 = new ArrayList<Map<String, Object>>();
					cards.setCTitle("注意事項");
					Map<String, Object> m = null;

					String getNotes = dataMap.get("notes") == null ? "" : dataMap.get("notes").toString();
					if ("".equals(getNotes)) {
						m = new HashMap<>();
						m.put("cText", "無注意事項");
						cTextList2.add(m);
					} else {
						getNotes = getNotes.replaceAll("<br />", "<br>").replaceAll("<br/>", "<br>");
						String[] noteAry = getNotes.split("<br>");

						for (int i = 0; i < noteAry.length; i++) {
							m = new HashMap<>();
							m.put("cText", noteAry[i]);
							cTextList2.add(m);
						}
					}
					cards.setCTexts2(cTextList2);
					// 牌卡下縮放文字 end

					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
					for (int i = 1; i <= 5; i++) {
						CTextItem cText = vo.new CTextItem();
						if (i == 1) {
							cText.setCLabel("基金名稱");
							cText.setCText(dataMap.get("fund_name").toString());
						} else if (i == 2) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
							SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
							cText.setCLabel("申購日期");
							try {
								cText.setCText(sdf2.format(sdf.parse(dataMap.get("rdate").toString())));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else if (i == 3) {
							cText.setCLabel("申購金額");
							cText.setCText(changeMoneyString(dataMap.get("amt").toString()));
						} else if (i == 4) {
							cText.setCLabel("扣款帳號");
//							cText.setCText(dataMap.get("acnt_txt").toString());
							cText.setCText(dataMap.get("branch").toString());
							cText.setCUnit(dataMap.get("acnt").toString());
						} else if (i == 5) {
							String CurrentTime = (String) dataMap.get("CurrentTime");
							
							if (StringUtils.isNotBlank(CurrentTime)) {
								String d = CurrentTime.substring(0, CurrentTime.indexOf(" "));
								String t = CurrentTime.substring(CurrentTime.indexOf(" ") + 1, CurrentTime.indexOf("."));								
								cText.setCLabel("委託時間");
								cText.setCText(d);
								cText.setCUnit(t);								
							}
						}
						cTextList.add(cText.getCTexts());
					}
					cards.setCTexts(cTextList);
					cardList.add(cards.getCards());

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
					botResponseUtils.setTextResult(vo, "等下會寄委託通知單到你的約定信箱");

					cardList = new ArrayList<Map<String, Object>>();
					cards = vo.new CardItem();
					cards.setCLinkType(1);
					cards.setCWidth("segment");

					// 牌卡圖片
					cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND_FH);
					cImageData.setCImageTextType(1);

					List<Map<String, Object>> cImageTextList = new ArrayList<>();
					CImageTextItem cImageText = vo.new CImageTextItem();
					cImageText.setCImageTextTitle("Email：");
					cImageText.setCImageText(dataMap.get("email").toString());
					cImageTextList.add(cImageText.getCImageTexts());
					cImageData.setCImageTexts(cImageTextList);
					cards.setCImageData(cImageData.getCImageDatas());

					cardList.add(cards.getCards());

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, SiteQuickReplay.BUT_OTHER_ONE_FUND,
							SiteQuickReplay.INIT);
				} else {
					botResponseUtils.setTextResult(vo, "你沒有可恢復扣款的停扣基金資訊。");
				}
			} else if (isOverTime(data)) {
				isOverTime = true;
				botResponseUtils.setImageResult(vo, BotMessageVO.EMBARRASSED_PIC);
				botResponseUtils.setTextResult(vo, "Sorry！交易時間已超過今天的交易截止時間(下午4:00)，你今天的此筆申購失敗！");
				botResponseUtils.setTextResult(vo, "需要阿發自動幫你遞延此交易到下一個營業日嗎！");

				QuickReplay q1 = SiteQuickReplay.AUTO_DELAY_ONE_FUND;
				QuickReplay q2 = SiteQuickReplay.RETRY_ONE_FUND;

				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, SiteQuickReplay.AUTO_DELAY_ONE_FUND,
						SiteQuickReplay.RETRY_ONE_FUND);

				tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
				tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

			} else if (isPwdError(data)) {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				vo = getBuySingleFunTxPwd(reqVO, StringUtils.defaultString(msg));
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, /* code + ":" + */ StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		tpiGatewayResult.put("isOverTime", isOverTime);
		tpiGatewayResult.put("isSuccess", isSuccess);

		return tpiGatewayResult.setData(vo);
	}
	
	@Override
	public BotMessageVO getWebFundChoice(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = fundAPIFeignService.getWebFundChoice(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				List<Map<String, Object>> dataList = getList(data);
				if (dataList.size() > 0) {
					List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

					for (int i = 0; i < dataList.size(); i++) {
						Map<String, Object> dataMap = dataList.get(i);
						String choice_type = dataMap.get("choice_type").toString();
						String imgUrl = "";

						// TODO 圖片測試用
						if (choice_type.indexOf("股票") > -1) {
							imgUrl = "card22.png";
						} else if (choice_type.indexOf("債券") > -1) {
							imgUrl = "card25.jpg";
						} else if (choice_type.indexOf("平衡") > -1) {
							imgUrl = "card34.png";
						} else if (choice_type.indexOf("多重資產") > -1) {
							imgUrl = "card30.jpg";
						} else if (choice_type.indexOf("退休導向") > -1) {
							imgUrl = "card16.png";
						}

						CardItem cards = vo.new CardItem();
						cards.setCName("");
						cards.setCWidth(BotMessageVO.CWIDTH_200);
						cards.setCTextType("3");
						cards.setCLinkType(1);

						// 牌卡圖片
						CImageDataItem cImageData = vo.new CImageDataItem();
						cImageData.setCImage(BotMessageVO.IMG_CC);
						cImageData.setCImageUrl(imgUrl);
						cImageData.setCImageTextType(1);
						cards.setCImageData(cImageData.getCImageDatas());

						List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
						CTextItem cText = vo.new CTextItem();
						cText.setCLabel(dataMap.get("fund_name").toString());
						cText.setCText(dataMap.get("title").toString());
						cTextList.add(cText.getCTexts());
						cards.setCTexts(cTextList);

						String fund = dataMap.get("fund").toString();

						List<QuickReplay> link1s = new ArrayList<>();

						Map<String, String> link = (Map<String, String>) dataMap.get("link");
						if (link != null) {
							String fund_info_url = link.get("fund_info_url");
							if (StringUtils.isNotBlank(fund_info_url))
								link1s.add(new SiteQuickReplay(4, "了解更多", "了解更多", "", fund_info_url, ""));
							String ispur = link.get("ispur"), isrsp = link.get("isrsp");
							if (StringUtils.equals(ispur, "1"))
								link1s.add(SiteQuickReplay.APPLY_ONE_FUND(fund));
							if (StringUtils.equals(isrsp, "1"))
								link1s.add(SiteQuickReplay.APPLY_RSP_FUND(fund));
						}

						cards.setCLinkList(this.getCLinkList(vo, link1s));

						cardList.add(cards.getCards());
					}

					botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
				} else {
					botResponseUtils.setTextResult(vo, "沒有推薦基金資訊。");
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}
	
	@Override
	public TPIGatewayResult getApplyFund(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		tpiGatewayResult.put("checkResult", "N");

		Map<String, Object> result = fundAPIFeignService.getFundList(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				List<Map<String, Object>> list = getList(data);
				if (list.size() > 0) {
					tpiGatewayResult.put("checkResult", "Y");
					for (Map<String, Object> map : list) {
						tpiGatewayResult.put("fund", map.get("fund"));
						tpiGatewayResult.put("fundName", map.get("fund_name"));
						tpiGatewayResult.put("fundkindTxt", map.get("fundkind_txt"));
						Object isrsp = map.get("isrsp");
						try {
							if (isrsp instanceof String)
								isrsp = Integer.parseInt((String) isrsp);
						} catch (NumberFormatException e) {
						}
						tpiGatewayResult.put("isrsp", isrsp);
						Object isrspv = map.get("isrspv");
						try {
							if (isrspv instanceof String)
								isrspv = Integer.parseInt((String) isrspv);
						} catch (NumberFormatException e) {
						}
						tpiGatewayResult.put("isrspv", isrspv);
						tpiGatewayResult.put("kyctypeTxt", map.get("kyc_type_txt"));// unused
					}
				} else {
					botResponseUtils.setTextResult(vo, "你的投資風險屬性低於該基金的風險報酬等級，請先更新投資風險屬性才能繼續做交易");
					botResponseUtils.setTextResult(vo, "更新投資屬性後，要再回來按一次你想做的交易，阿發才能繼續作業喔~");

					List<QuickReplay> qrList = new ArrayList<>();
					qrList.add(fundKycQuickReplay(reqVO.getSessionId(), reqVO.getClientIp(), reqVO.getToken(),
							reqVO.isMobile()));

					cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp(),
							qrList.toArray(new QuickReplay[0]));
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public TPIGatewayResult chooseCmd(FundRequest reqVO) {
		TPIGatewayResult defaultResult = TPIGatewayResult.ok();
		TPIGatewayResult tpiGatewayResult = null;

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String cmd = reqVO.getCmd();
	
		switch (cmd) {        	
        case "getFundList_click":          	
        	botResponseUtils.setTextResult(vo, "你要購買基金[" + reqVO.getFund() + "] " + reqVO.getFundName() + " 嗎?");
        	botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, 
					new SiteQuickReplay(1, "是", "是", "", "", ""),
					new SiteQuickReplay(1, "否", "否", "", "", ""));
            break;
        case "single_fund_detail":
			tpiGatewayResult = single_fund_detail(vo, reqVO);
            break;
        case "single_fund_detail_edit":
			botResponseUtils.setTextResult(vo, "告訴阿發你要修改什麼資訊");

			QuickReplay q1 = SiteQuickReplay.MODIFY_ONE_FUND;
			QuickReplay q2 = SiteQuickReplay.MODIFY_ONE_ACCOUNT;
			QuickReplay q3 = SiteQuickReplay.MODIFY_ONE_DATE;
			QuickReplay q4 = SiteQuickReplay.MODIFY_ONE_MONEY;

			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q1, q2, q3, q4);

			defaultResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText(), q4.getText()));
			defaultResult.setOptionList(Arrays.asList(q1, q2, q3, q4));
			break;
        case "rsp_fund_detail_edit":
			botResponseUtils.setTextResult(vo, "告訴阿發你要修改什麼資訊");

			QuickReplay rsp_q1 = SiteQuickReplay.MODIFY_RSP_FUND;
			QuickReplay rsp_q2 = SiteQuickReplay.MODIFY_RSP_ACCOUNT;
			QuickReplay rsp_q3 = SiteQuickReplay.MODIFY_RSP_DATE;
			QuickReplay rsp_q4 = SiteQuickReplay.MODIFY_RSP_MONEY;

			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, rsp_q1, rsp_q2, rsp_q3, rsp_q4);

			defaultResult.setMsgList(Arrays.asList(rsp_q1.getText(), rsp_q2.getText(), rsp_q3.getText(), rsp_q4.getText()));
			defaultResult.setOptionList(Arrays.asList(rsp_q1, rsp_q2, rsp_q3, rsp_q4));
            break;
		case "single_fund_txpwd":
			vo = getBuySingleFunTxPwd(reqVO, null);
			break;
		case "rsp_fund_detail":
			tpiGatewayResult = rspFundService.rsp_fund_detail(vo, reqVO);
			break;
		case "rsp_fund_txpwd":
			vo = rspFundService.getBuyRspFunTxPwd(reqVO, null);
			break;
		case "A20_confirm":
			botResponseUtils.setTextResult(vo, "告訴阿發你要買回的單位數，例如:1234.5，或可全部買回"); 
			botResponseUtils.setTextResult(vo, "阿發提醒你，部分買回最低單位數為30");
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, 
					new SiteQuickReplay(1, "全部買回", "全部買回", "", "", ""),
					new SiteQuickReplay(1, "部分買回", "部分買回", "", "", ""),
					new SiteQuickReplay(1, "取消買回", "取消買回", "", "", ""));
		break;
		case "getOrderGroup_up5":	
			String returnMsg = "你有" + reqVO.gettCount() + "筆" + reqVO.getFundName() + reqVO.getOtype() + "委託交易" + reqVO.gettType();
			botResponseUtils.setTextResult(vo, returnMsg);
			
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("dataString", reqVO.getDataString());
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, 
					new SiteQuickReplay(1, "查看委託詳情", "查看委託詳情", "", "", "", "基金", "新委託交易查詢明細", param),
					new SiteQuickReplay(1, "繼續查詢其他新委託", "繼續查詢其他新委託", "", "", ""));
			
			
		break;
        default:
        	botResponseUtils.setTextResult(vo, "還有什麼需要阿發為你服務的呢?"); 
            break;
		}

		if (tpiGatewayResult == null) {
			return defaultResult.setData(vo);
		}
		else {
			return tpiGatewayResult;
		}
	}
	
	public TPIGatewayResult single_fund_detail(BotMessageVO vo, FundRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		botResponseUtils.setTextResult(vo, "請確認你的基金申購資訊：");
		
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCLinkType(1);
		cards.setCTextType("3");
		cards.setCWidth("segment");
		
		// 牌卡下縮放文字
//		List<Map<String, Object>> cTextList2 = new ArrayList<Map<String, Object>>();
//		cards.setCTitle("注意事項");
//		Map<String, Object> m = null;		
//		if("".equals(StringUtils.defaultString(reqVO.getNote()))) {
//			m = new HashMap<>();
//			m.put("cText", "無注意事項");
//			cTextList2.add(m);
//		} else {
//			String note = reqVO.getNote().replaceAll("<br />", "<br>").replaceAll("<br/>", "<br>");
//			String[] noteAry = note.split("<br>");
//			
//			for(int i=0; i<noteAry.length; i++) {
//				m = new HashMap<>();
//				m.put("cText", noteAry[i]);
//				cTextList2.add(m);
//			}						
//		}				
//		cards.setCTexts2(cTextList2);
		
		// 牌卡圖片
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);		
		cImageData.setCImageTextType(1);
		cImageData.setCImageTag(StringUtils.defaultString(reqVO.getFundkindTxt()));
		
		List<Map<String,Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();					
		cImageText.setCImageTextTitle("申購基金");
		cImageText.setCImageText(reqVO.getFundName());						
		cImageTextList.add(cImageText.getCImageTexts());					
		cImageData.setCImageTexts(cImageTextList);												
		cards.setCImageData(cImageData.getCImageDatas());
		
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		for (int i = 1; i <= 3; i++) {
			CTextItem cText = vo.new CTextItem();
			if (i == 1) {								
				cText.setCLabel("申購日期");
				cText.setCText(reqVO.getRdate());
			} else if (i == 2) {
				cText.setCLabel("申購金額");
				cText.setCText("NT$ " + changeMoneyString(reqVO.getAmt()));
			} else if (i == 3) {
				cText.setCLabel("扣款帳號");
				cText.setCText(reqVO.getAcntName());
				cText.setCUnit(reqVO.getAcnt());
			}
			cTextList.add(cText.getCTexts());
		}
		cards.setCTexts(cTextList);

		String fund = reqVO.getFund();

		QuickReplay q1 = SiteQuickReplay.BUY_ONE_FUND(fund);
		QuickReplay q2 = SiteQuickReplay.MODIFY_ONE_FUND(fund);
		QuickReplay q3 = SiteQuickReplay.CANCEL_ONE_FUND(fund);

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2, q3));
		
		cardList.add(cards.getCards());

		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(),q2.getText(),q3.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3));

		return tpiGatewayResult.setData(vo);
	}

	private BotMessageVO getBuySingleFunTxPwd(FundRequest reqVO, String errMsg) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> parameter = new HashMap<>();
		
		List<Map> linkList = new ArrayList<>();					
		if (StringUtils.isNotBlank(reqVO.getFundDoc())) {	// 公開說明書
			String fund_doc_url = cathaysiteFundPath + reqVO.getFundDoc();
			linkList.add(botResponseUtils
					.getLinkListItem(vo,
							new SiteQuickReplay(4, "公開說明書", "", "", fund_doc_url, ""))
					.getLinkList());
		}
		
		if (StringUtils.isNotBlank(reqVO.getDeclare())) {	// 注意事項
			String[] declare = reqVO.getDeclare().split(",");
			
			for(String a : declare) {
				Map<String, String> requireParams = new HashMap<String, String>();
				requireParams.put("SessionID", reqVO.getSessionId());
				requireParams.put("ClientIP", reqVO.getClientIp());
				requireParams.put("query", a);
				Map<String, Object> obj = new HashMap<String, Object>();
				obj.put("obj", requireParams);
				
				Map<String, Object> resultOne = fundAPIFeignService.getOne(obj, reqVO.getToken());
				
				if (resultOne != null) {
					Map<String, Object> dataOne = getData(resultOne);
					String title = "", alt = "";
					if (isSuccess(dataOne)) {
						Map<String, Object> dvOne = getDataValue(dataOne);
						Map<String, Object> mapDataOne = (Map<String, Object>) dvOne.get("data");
						title = (String) mapDataOne.get("title");
						alt = (String) mapDataOne.get("txt");
						String stype = (String) mapDataOne.get("stype");

						if (StringUtils.equals(stype, "2")) {
							// 純文字
							alt = alt.replaceAll("\r\n", "<br>");
						}

					} else {
						String code = getCode(dataOne), msg = getMsg(dataOne);
						logger.error("code = {}, msg = {}", code, msg);
						alt = code + ":" + StringUtils.defaultString(msg);
					}
					linkList.add(botResponseUtils
							.getLinkListItem(vo, new SiteQuickReplay(0, title, alt, "", "", ""))
							.getLinkList());
				}
			}
						
		}

		botResponseUtils.doTxPwd(vo, "確認申購", "點選確認申購(視窗)", "基金", "doBuyFund_oneFund", parameter, linkList, errMsg);

		return vo;
	}
	
	@Override
	public BotMessageVO getOrderGroup(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String dataString = "";		

		Map<String, Object> result = fundAPIFeignService.getOrderGroup(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);				
				List<Map<String, Object>> dataList = getList(data);
								
				if (dataList.size() > 0) {
					Map<String, Object> dataMap = null;		
					int tCountSum = 0;
					
					for(int i=0; i<dataList.size(); i++) {
						dataMap = dataList.get(i);
						
						dataList.get(i).put("id", i);						
						
						// 轉換顯示日期
						dataList.get(i).put("addt2", dataMap.get("addt").toString().replaceAll("-", "").replaceAll("/", ""));
						
						// 轉換交易類型名稱
						dataList.get(i).put("otypeName", replaceData("otype", dataMap.get("otype").toString()));
						
						// 轉換交易狀態名稱
						dataList.get(i).put("tTypeName", replaceData("tType", dataMap.get("tType").toString()));							
						
						if(i != 0) {
							dataString += ";";
						}
						
						dataString += dataMap.get("otype").toString() + ":" + dataMap.get("tnos").toString();							
						tCountSum += Integer.parseInt(dataMap.get("tCount").toString());
					}													
					
					if(tCountSum <= 5) {							
						reqVO.setDataString(dataString);
						
						if(tCountSum == 1) {
							reqVO.setOnlyOne(true);
						}
						
						vo = getOrderDetail(reqVO);						
					} else {
						// 組清單
						botResponseUtils.setSiteOrderGroupList(vo, dataList);
					}													
				} else {
					// TODO 客服共用						
					botResponseUtils.setTextResult(vo, "你沒有委託單資料欸！");
					cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());
				}				
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}	
				
		return vo;
	}				
	
	@Override
	public BotMessageVO getOrderDetail(FundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String type = StringUtils.defaultString(reqVO.getType());
		String[] dataStringAry = reqVO.getDataString().split(";");
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();		
		String otype = "";
		
		for(int i=0; i<dataStringAry.length; i++) {
			otype = dataStringAry[i].split(":")[0];
			String tnos = dataStringAry[i].split(":")[1];
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("SessionID", reqVO.getSessionId());
			requireParams.put("ClientIP", reqVO.getClientIp());
			requireParams.put("otype", otype);
			requireParams.put("tnos", tnos);
			
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);

			Map<String, Object> result = fundAPIFeignService.getOrderDetail(obj, reqVO.getToken());

			if (result != null) {
				Map<String, Object> data = getData(result);
				if (isSuccess(data)) {
					logger.info("data=" + data);
					
					List<Map<String, Object>> dataListTrans = new ArrayList<Map<String, Object>>();
					if("1".equals(otype)) {			// 申購		
						dataListTrans = getListPur(data);						
					} else if("2".equals(otype)) {	// 買回轉申購
						dataListTrans = getListRede(data);						
					} else if("3".equals(otype)) {	// 定期(不)定額
						dataListTrans = getListRsp(data);						
					}	
					
					if(dataListTrans.size() > 0) {
						dataList.addAll(dataListTrans);
					}
				} else {
					String code = getCode(data), msg = getMsg(data);
					logger.error("code = {}, msg = {}", code, msg);
					botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
				}
			} else {
				logger.error("result is null");
				botResponseUtils.setTextResult(vo, "API failed!");
			}					
		}
		
		if(dataList.size() <= 5) {	
			if(dataList.size() != 0) {
				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, drawOrderDetailCard(vo, otype, dataList));				
			} else {
				botResponseUtils.setTextResult(vo, "查無委託明細資料！");
			}
			
			if(reqVO.isOnlyOne()) {
				botResponseUtils.setQuickReplayResult(vo,BotMessageVO.LTYPE_04, SiteQuickReplay.INIT);
			} else {
				botResponseUtils.setQuickReplayResult(vo,BotMessageVO.LTYPE_04, SiteQuickReplay.ORDER_DETAIL_ELSE
						, SiteQuickReplay.INIT);
			}			
		} else {
			botResponseUtils.setSiteOrderDetailList(vo, otype, dataList);
		}		

		return vo;
	}
	
	protected List<Map<String, Object>> drawOrderDetailCard(BotMessageVO vo, String otype, List<Map<String, Object>> dataList) {		
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		
		for(Map<String, Object> dataMap : dataList) {					
				
			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_250);
			cards.setCTextType("3");	
			cards.setCLinkType(2);
			
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CB03);
			cImageData.setCImageTextType(1);
			
			List<Map<String,Object>> cImageTextList = new ArrayList<Map<String,Object>>();			
			String[] imageTextAry = {"基金名稱:fund_name", "狀態:tType"}; 

			for(String s : imageTextAry) {
				CImageTextItem cImageText = vo.new CImageTextItem();
				
				String title = s.split(":")[0];
				String key = s.split(":")[1];
				String text = replaceData(key, dataMap.get(key).toString());
				
				cImageText.setCImageTextTitle(title);
				cImageText.setCImageText(text);
				
				cImageTextList.add(cImageText.getCImageTexts());
			}
			
			cImageData.setCImageTexts(cImageTextList);
			
			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();	
			if("1".equals(otype)) {			// 申購
				cImageData.setCImageTag("申購");
								
				String[] typeAry = {"交易日期:addt", "預計申購日期:rdate", "申購金額:amt", "手續費:camt", "申購總金額:amtSum", "預計扣款日:rdate", "扣款帳號:acnt"};
				String[] moneyAry = {"amt", "camt", "amtSum"};
				String[] dateAry = {"addt", "rdate"};
								
				for(String s : typeAry) {
					CTextItem cText = vo.new CTextItem();
					String title = s.split(":")[0];
					String key = s.split(":")[1];
					String text = String.valueOf(dataMap.get(key));
					
					if(Arrays.asList(moneyAry).contains(key)) {
						// 金額
						String moneyTypeEng = dataMap.get("currency_txt").toString().indexOf("台幣") > -1 ? "NT." : "";
						
						if("amtSum".equals(key)) {
							text = String.valueOf(getInteger(dataMap.get("amt")) + getInteger(dataMap.get("camt")));
						}
						
						text = moneyTypeEng + changeMoneyString(text);						
					} else if (Arrays.asList(dateAry).contains(key)) {
						// 日期		
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");	
						
						text = text.replaceAll("-", "").replaceAll("/", "");
						
						try {
							text = sdf2.format(sdf.parse(text));							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if ("acnt".equals(key)) {
						cText.setCUnit(text);
						text = dataMap.get("branch").toString();
					}
					
					cText.setCLabel(title);
					cText.setCText(text);	
					cTextList.add(cText.getCTexts());
				}								
			} else if("2".equals(otype)) {	// 買回轉申購			
				cImageData.setCImageTag("買回");
				
				String trcode = String.valueOf(dataMap.get("trcode"));
				String[] typeAry = {"交易日期:rdate", "指定買回方式:rtype_txt", "買回單位數:runit", "買回金額:ramt", "買回領取方式:trcode"};
				String[] moneyAry = {"ramt"};
				String[] dateAry = {"rdate"};
								
				for(String s : typeAry) {
					CTextItem cText = vo.new CTextItem();
					String title = s.split(":")[0];
					String key = s.split(":")[1];
					String text = String.valueOf(dataMap.get(key));
					String tcode = dataMap.get("tcode").toString();
					
					if(Arrays.asList(moneyAry).contains(key)) {
						// 金額
						String moneyTypeEng = dataMap.get("currency_txt").toString().indexOf("台幣") > -1 ? "NT." : "";
						text = moneyTypeEng + changeMoneyString(text);
						
						if(!"4".equals(tcode)) {
							key = null;
						}
					} else if (Arrays.asList(dateAry).contains(key)) {
						// 日期		
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");						
						
						text = text.replaceAll("-", "").replaceAll("/", "");
						
						try {	
							text = sdf2.format(sdf.parse(text));							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if ("runit".equals(key)) {						
						if("4".equals(tcode)) {
							key = null;
						}
					} else if ("trcode".equals(key)) {
						// 1:匯款, 8:轉申購, 9:匯款+轉申購 						
						JSONArray trfund_p = (JSONArray)dataMap.get("trfund_p");
						JSONArray trfund_txt = (JSONArray)dataMap.get("trfund_txt");
						
						if("1".equals(text) || "9".equals(text)) {
							cText = vo.new CTextItem();
							cText.setCLabel("匯款");
								
							String cTxt = "{" + trfund_p.getString(0) + "}% " + String.valueOf(dataMap.get("branch"));
															
							cText.setCText(cTxt);							
							cText.setCUnit(String.valueOf(dataMap.get("acnt")));
							cTextList.add(cText.getCTexts());							
						}												
						
						if("8".equals(text) || "9".equals(text)) {
							cText = vo.new CTextItem();
							cText.setCLabel("轉申購");
							
							String cTxt = "";
							for(int i=0; i<trfund_txt.size(); i++) {
								String trfund_p_data = "";
								if("9".equals(text) && i==0) {
									trfund_p_data = trfund_p.getString(i + 1);
								} else {
									trfund_p_data = trfund_p.getString(i);
								}
								
								cTxt += "{" + trfund_p_data + "}% 轉申購基金：<br>" + trfund_txt.getString(i);
								
								if(i != (trfund_txt.size()-1)) {
									cTxt += "<br><br>";
								}
							}							
							
							cText.setCText(cTxt);
							cTextList.add(cText.getCTexts());
						}												
											
						key = null;
					}
					
					if(key != null) {
						cText.setCLabel(title);
						cText.setCText(text);
						cTextList.add(cText.getCTexts());
					}
				}	
			} else if("3".equals(otype)) {	// 定期(不)定額
				cImageData.setCImageTag("定期");
				
				String[] typeAry = {"交易日期:addt", "扣款方式:stype_txt", "扣款模式:tradetype_txt", "扣款金額:samt", "每月扣款日:sday", "扣款帳號:acnt"};
				String[] moneyAry = {"samt"};
				String[] dateAry = {"addt"};
								
				for(String s : typeAry) {
					CTextItem cText = vo.new CTextItem();
					String title = s.split(":")[0];
					String key = s.split(":")[1];
					String text = String.valueOf(dataMap.get(key));
					
					if(Arrays.asList(moneyAry).contains(key)) {
						// 金額
						String moneyTypeEng = dataMap.get("currency_txt").toString().indexOf("台幣") > -1 ? "NT." : "";
						String tradetype = dataMap.get("tradetype").toString();						
						
						if("1".equals(tradetype)) {			// 定期定額				
							text = moneyTypeEng + changeMoneyString(text);
						} else if ("2".equals(tradetype)) {	// 定期不定額					
							text = "高金額 : " + changeMoneyString(String.valueOf(dataMap.get("highamt"))) + " 元" + "<br>"
								 + "中金額 : " + changeMoneyString(String.valueOf(dataMap.get("midamt"))) + " 元" + "<br>"
								 + "低金額 : " + changeMoneyString(String.valueOf(dataMap.get("lowamt"))) + " 元";
						}																
					} else if (Arrays.asList(dateAry).contains(key)) {
						// 日期		
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");						
						
						text = text.replaceAll("-", "").replaceAll("/", "");
						
						try {	
							text = sdf2.format(sdf.parse(text));							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if ("acnt".equals(key)) {
						cText.setCUnit(text);
						text = dataMap.get("branch").toString();
					} else if ("sday".equals(key)) {
						text = getInteger(text).toString();
					}
					
					cText.setCLabel(title);
					cText.setCText(text);	
					cTextList.add(cText.getCTexts());
				}
			}
			
			cards.setCImageData(cImageData.getCImageDatas());	
			cards.setCTexts(cTextList);
			cardList.add(cards.getCards());
		}			
												
		return cardList;
	}
	
	public String replaceData(String type, String value){
		String returnStr = "";
		
		if("otype".equals(type)) {
			switch(value) {
				case "1":
					returnStr = "申購";
					break;
				case "2":
					returnStr = "買回轉申購";					
					break;
				case "3":
					returnStr = "定期(不)定額";					
					break;
				default :
					returnStr = "尚未歸類";					
					break;						
			}
		} else if("tType".equals(type)) {
			switch(value) {
				case "1":
					returnStr = "新委託";
					break;
				case "2":
					returnStr = "處理中";					
					break;
				default :
					returnStr = "委託失敗";					
					break;						
			}
		} else {
			returnStr = value;
		}
		
		return returnStr;
	}
	
	@Override
	public BotMessageVO getECopenProcess(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());			
		Map<String, Object> result = fundAPIFeignService.getECopenProcess(param);

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);				
				List<Map<String, Object>> dataList = getList(data);

				if (dataList.size() > 0) {
					Map<String, Object> dataMap = dataList.get(0);
					if(dataMap.get("ecopen_status") != null || dataMap.get("ecopen_efamily_status") != null) {						
						// 一般開戶訊息
						String ecopenMsg = dataMap.get("ecopen_msg") != null ? dataMap.get("ecopen_msg").toString() : "";
						
						// 國泰E家人開戶訊息
						String ecopenEfamilyMsg = dataMap.get("ecopen_efamily_msg") != null ? dataMap.get("ecopen_efamily_msg").toString() : "";
						
						if(StringUtils.isNotBlank(ecopenMsg)) {
							botResponseUtils.setTextResult(vo, ecopenMsg);
						}
												
						if(StringUtils.isNotBlank(ecopenEfamilyMsg)) {
							botResponseUtils.setTextResult(vo, ecopenEfamilyMsg);
						}

						String url = "";
						if(((Map)dataMap.get("link")).get("fund_documents_url") != null) {
							url = ((Map)dataMap.get("link")).get("fund_documents_url").toString();
						}
						
						QuickReplay fund_q1 = new SiteQuickReplay(1, "單筆申購", "單筆申購");
				        QuickReplay fund_q2 = new SiteQuickReplay(1, "定期(不)定額申請", "定期(不)定額申請");
				        QuickReplay fund_q3 = new SiteQuickReplay(1, "推薦基金", "推薦基金");
				        QuickReplay fund_q4 = new SiteQuickReplay(4, "基金申購(基金表單連結)", "基金申購(基金表單連結)", "", url, "");
				        
				        
						if("1".equals(dataMap.get("ecopen_efamily_status")) && "1".equals(dataMap.get("ecopen_status"))) {					        
					        botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, fund_q1, fund_q2, fund_q3);
						} else if("1".equals(dataMap.get("ecopen_status"))) {
							botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, fund_q3, fund_q4);
						} else {
							cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());							
						}							
					} else {
						// 查無客戶資料 會給一個空的map
						botResponseUtils.setTextResult(vo, "阿發找不到你的開戶資料欸！");
						cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());
					}										
				} else {						
					botResponseUtils.setTextResult(vo, "阿發找不到你的開戶資料欸！");
					cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());
				}				
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}					
		
		return vo;
	}
	
	public Integer getInteger(Object data){
		Integer returnInt = 0;
		if(data != null){
			try {
//				returnInt = Integer.parseInt(data.toString());
				returnInt = (int) Double.parseDouble(data.toString());					
			}catch (NumberFormatException e) {
				return returnInt;
			}
		}
				
		return returnInt;
	}
	
	private String changeMoneyString(String money) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();		
		return numberFormat.format(getInteger(money));
	}
	
	private List<Map<String, Object>> getCLinkList(BotMessageVO vo, List<QuickReplay> links) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<Map<String, Object>>();
		for (QuickReplay link : links)
			cLinkContentList.add(botResponseUtils.getCLinkListItem(vo, link).getCLinkList());
		return cLinkContentList;
	}
	
	@Override
	public BotMessageVO getOne(FundRequest reqVO, Map<String, Object> param) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = fundAPIFeignService.getOne(param, reqVO.getToken());
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				Map<String, Object> list = getDataValue(data);
				Map<String, Object> mapData = (Map<String, Object>) list.get("data");
				String txt = (String) mapData.get("txt");
				String stype= (String) mapData.get("stype");
				if(StringUtils.equals(stype, "2")) {
					//XXX 純文字
					txt=txt.replaceAll("\r\n", "<br>");
				}
				botResponseUtils.setTextResult(botMessageVO, txt);
				
				return botMessageVO;
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(botMessageVO, code + ":" + StringUtils.defaultString(msg));
			}
		}
		return botMessageVO;
	}
	
	/**
	 * For 基金清單 關閉
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public BotMessageVO closeFundList(FundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		if ("single".equals(reqVO.getType())) {
			botResponseUtils.setTextResult(vo, "找不到想申購的基金嗎? 有些基金尚未開放電子交易或你的投資風險屬性低於想申購的基金風險報酬等級");
			botResponseUtils.setTextResult(vo, "請先更新投資屬性後，再回來按一次單筆申購，阿發才能繼續作業喔~");

			List<QuickReplay> qrList = new ArrayList<>();
			qrList.add(
					fundKycQuickReplay(reqVO.getSessionId(), reqVO.getClientIp(), reqVO.getToken(), reqVO.isMobile()));

			cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp(),
					qrList.toArray(new QuickReplay[0]));
		} else if ("rspFund".equals(reqVO.getType())) {
			botResponseUtils.setTextResult(vo, "找不到想申購的基金嗎? 有些基金未開放定期(不)定額交易或你的投資風險屬性低於想申購的基金風險報酬等級");
			botResponseUtils.setTextResult(vo, "請先更新投資屬性後，再回來按一次定期(不)定額，阿發才能繼續作業喔~");

			List<QuickReplay> qrList = new ArrayList<>();
			qrList.add(
					fundKycQuickReplay(reqVO.getSessionId(), reqVO.getClientIp(), reqVO.getToken(), reqVO.isMobile()));

			cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp(),
					qrList.toArray(new QuickReplay[0]));
		} else if ("sellFundTrfund".equals(reqVO.getType())) {
			botResponseUtils.setTextResult(vo, "找不到想申購的基金嗎? 有些基金未開放電子交易或你的投資風險屬性低於想申購的基金風險報酬等級");
			botResponseUtils.setTextResult(vo, "請先更新投資屬性後，再回來按一次買回轉申購，阿發才能繼續作業喔~");

			List<QuickReplay> qrList = new ArrayList<>();
			qrList.add(
					fundKycQuickReplay(reqVO.getSessionId(), reqVO.getClientIp(), reqVO.getToken(), reqVO.isMobile()));

			cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp(),
					qrList.toArray(new QuickReplay[0]));
		} else {
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, SiteQuickReplay.FUND_NAV_ELSE,
					SiteQuickReplay.INIT);
		}

		return vo;
	}

	protected QuickReplay fundKycQuickReplay(String sessionId, String clientIp, String token, boolean isMobile) {
		Map<String, String> l1obj = new HashMap<String, String>();
		l1obj.put("SessionID", sessionId);
		l1obj.put("ClientIP", clientIp);
		l1obj.put("cnum", "03");
		l1obj.put("cname", isMobile ? "kycmo" : "kycpc");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("obj", l1obj);
		Map<String, Object> result = fundAPIFeignService.getLinkOne(param, token);
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDataMap(data);
				String link = (String) dataMap.get("link");
				if (StringUtils.isNotBlank(link)) {
					String url = cathaysiteFundPath + link;
					return new SiteQuickReplay(4, "更改投資風險屬性", "", "", url, "");
				}
			}
		}
		return null;
	}
}