package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.FundRequest;

import java.util.Map;

/**
 * 新定期定額申請
 */
public interface RspFundService {
	/**
	 * For 新定期定額申請 申購基金清單
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRspFundList(FundRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 新定期定額申請 選擇扣款模式
	 * </pre>
	 * <ul>
	 * <li>1：定期定額</li>
	 * <li>2：定期不定額</li>
	 * </ul>
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult waitRspFundTradetype(FundRequest reqVO);

	/**
	 * For 新定期定額申請 輸入申購金額
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO waitRspFundMoney(FundRequest reqVO, Map<String, Object> param);

	/**
	 * For 新定期定額申請 驗證申購金額
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult validateRspFundMoney(FundRequest reqVO, Map<String, Object> param);

	/**
	 * For 新定期定額申請 選擇扣款帳號
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO waitRspFundAccount(FundRequest reqVO, Map<String, Object> param);

	/**
	 * <pre>
	 * For 新定期定額申請 選擇扣款方式
	 * </pre>
	 * <ul>
	 * <li>0：遇假日順延扣款</li>
	 * <li>1：遇假日不扣款</li>
	 * </ul>
	 *
	 * @param reqVO
	 * @return
	 * @throws Exception
	 */
	TPIGatewayResult waitRspFundStype(FundRequest reqVO);

	/**
	 * For 新定期定額申請 選擇扣款日期
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO waitRspFundSday(FundRequest reqVO, Map<String, Object> param);

	/**
	 * For 新定期定額申請 確認牌卡
	 * 
	 * @param vo
	 * @param reqVO
	 * @return
	 */
	TPIGatewayResult rsp_fund_detail(BotMessageVO vo, FundRequest reqVO);

	/**
	 * For 新定期定額申請 交易認證視窗
	 *
	 * @param reqVO
	 * @param errMsg
	 * @return
	 */
	BotMessageVO getBuyRspFunTxPwd(FundRequest reqVO, String errMsg);

	/**
	 * For 申購定期(不)定額
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 */
	TPIGatewayResult rspSetAdd(FundRequest reqVO, Map<String, Object> param);
}