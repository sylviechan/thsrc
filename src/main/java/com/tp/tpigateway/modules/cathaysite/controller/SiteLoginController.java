package com.tp.tpigateway.modules.cathaysite.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.annotation.NotShowAopLog;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.util.cathaysite.SiteApiUtils;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.service.CommonService;
import com.tp.tpigateway.modules.cathaysite.fegin.CustAPIFeignService;

@RestController
@RequestMapping("cathaysite/login")
public class SiteLoginController extends BaseController{
	
	@Autowired
	CustAPIFeignService custAPIFeignService;

	@Autowired
    CommonService commonService;

	@PostMapping("/loginView")
	public TPIGatewayResult loginView(@RequestBody BotRequest botRequest) {
		BotMessageVO botMessageVO = commonService.getLogin(botRequest);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@NotShowAopLog
	public TPIGatewayResult login(@RequestBody Map<String, String> loginRequest) {
		log.info("###########TPIGateWay登入驗證################");
		log.debug(loginRequest.get("uid"));
		log.debug(loginRequest.get("pwd"));
		log.info(loginRequest.get("sessionId"));
		log.info(loginRequest.get("ClientIP"));

		Map<String, String> map = new LinkedHashMap<String, String>();
		Map<String, Object> mapObj = new LinkedHashMap<String, Object>();
		map.put("uid", loginRequest.get("uid"));
		map.put("pwd", loginRequest.get("pwd"));
		map.put("SessionID", loginRequest.get("sessionId"));
//    	map.put("SessionID", "az23vr5xloy0fnl2dhpzmhuq");
		map.put("ClientIP", loginRequest.get("ClientIP"));
		log.info(map.toString());
		mapObj.put("obj", map);

		try {
			Map<String, Object> result = custAPIFeignService.login(mapObj);
//    	測試假資料
//    	String fakeObject = "{\"d\":\"{\\\"code\\\":\\\"S001\\\",\\\"msg\\\":\\\"登入成功\\\",\\\"DataValue\\\":{\\\"token\\\":\\\"IbByqS+AveFs33I/oXZlc2Js0sgDsF/fFOtoxdgqMgcF4T4EEXr4fP19srjdd5L6+5itS+2NZqCeCgtDw/N38QgyIWHe/w5Shzb2G8SamFw=\\\",\\\"logintime\\\":\\\"2019-09-05 09:32:36\\\",\\\"cstno\\\":\\\"YlnJpzxLQ/+BWa1pGhnw/g==\\\",\\\"uid\\\":null,\\\"pwd\\\":null,\\\"fund\\\":null,\\\"page_size\\\":0,\\\"page_index\\\":0,\\\"pagetype\\\":null,\\\"birth\\\":null,\\\"isSend\\\":false,\\\"SendTime\\\":null,\\\"source\\\":null,\\\"query\\\":null,\\\"useragent\\\":null,\\\"url\\\":null,\\\"urlreferrer\\\":null,\\\"ip\\\":null,\\\"UrlPara\\\":null,\\\"isCAPTCHA\\\":false,\\\"SendType\\\":null,\\\"userid\\\":null,\\\"cphone\\\":null,\\\"ctype\\\":null,\\\"SessionID\\\":null,\\\"ClientIP\\\":null,\\\"state\\\":null,\\\"CheckImageNumber\\\":null},\\\"log_time\\\":\\\"2019-09-05 09:32:36\\\",\\\"msg_type\\\":null}\"}";
//    	String fakeObject = "{\"d\":\"{\\\"code\\\":\\\"W001\\\",\\\"msg\\\":\\\"您的帳號登入異常，請洽客戶服務部(02)7713-3000詢問\\\",\\\"DataValue\\\":null,\\\"log_time\\\":\\\"2019-10-01 16:26:00\\\",\\\"msg_type\\\":null}\"}";
//    	fakeObject = "{\"d\": \"{\\\"code\\\":\\\"W003\\\",\\\"msg\\\":\\\"您的電子交易登入密碼已累計錯誤次數 5次\\\",\\\"DataValue\\\":{\\\"token\\\":null,\\\"logintime\\\":null,\\\"MsgPara\\\":{\\\"online_pc\\\":\\\"/online/password/resend.aspx\\\",\\\"online_mobile\\\":\\\"/online/mobile/forget-password/forget-password.aspx\\\"},\\\"error\\\":4,\\\"cstno\\\":null,\\\"uid\\\":null,\\\"pwd\\\":null,\\\"fund\\\":null,\\\"page_size\\\":0,\\\"page_index\\\":0,\\\"pagetype\\\":null,\\\"birth\\\":null,\\\"isSend\\\":false,\\\"SendTime\\\":null,\\\"source\\\":null,\\\"query\\\":null,\\\"useragent\\\":null,\\\"url\\\":null,\\\"urlreferrer\\\":null,\\\"ip\\\":null,\\\"isallot\\\":null,\\\"UrlPara\\\":null,\\\"isCAPTCHA\\\":false,\\\"CAPTCHA\\\":null,\\\"SendType\\\":null,\\\"userid\\\":null,\\\"cphone\\\":null,\\\"ctype\\\":null,\\\"SessionID\\\":null,\\\"ClientIP\\\":null,\\\"state\\\":null,\\\"CheckImageNumber\\\":null},\\\"log_time\\\":\\\"2019-10-04 16:03:00\\\",\\\"msg_type\\\":null}\"}";
//    	fakeObject = "{\"d\": \"{\\\"code\\\":\\\"W004\\\",\\\"msg\\\":\\\"您的電子交易登入密碼已累計錯誤次數 4次\\\",\\\"DataValue\\\":{\\\"token\\\":null,\\\"logintime\\\":null,\\\"MsgPara\\\":{\\\"online_pc\\\":\\\"/online/password/resend.aspx\\\",\\\"online_mobile\\\":\\\"/online/mobile/forget-password/forget-password.aspx\\\"},\\\"error\\\":4,\\\"cstno\\\":null,\\\"uid\\\":null,\\\"pwd\\\":null,\\\"fund\\\":null,\\\"page_size\\\":0,\\\"page_index\\\":0,\\\"pagetype\\\":null,\\\"birth\\\":null,\\\"isSend\\\":false,\\\"SendTime\\\":null,\\\"source\\\":null,\\\"query\\\":null,\\\"useragent\\\":null,\\\"url\\\":null,\\\"urlreferrer\\\":null,\\\"ip\\\":null,\\\"isallot\\\":null,\\\"UrlPara\\\":null,\\\"isCAPTCHA\\\":false,\\\"CAPTCHA\\\":null,\\\"SendType\\\":null,\\\"userid\\\":null,\\\"cphone\\\":null,\\\"ctype\\\":null,\\\"SessionID\\\":null,\\\"ClientIP\\\":null,\\\"state\\\":null,\\\"CheckImageNumber\\\":null},\\\"log_time\\\":\\\"2019-10-04 16:03:00\\\",\\\"msg_type\\\":null}\"}";
//    	ObjectMapper mapper = new ObjectMapper();
//    	log.info("");
//    	Map<String, Object> result = null;
//    	try {
//    		result = mapper.readValue(fakeObject, Map.class);
//    		log.info("result.get(d)   " + result.get("d"));
//    	} catch (IOException e) {
//    		// TODO Auto-generated catch block
//    		e.printStackTrace();
//    	}

			if (result != null) {
				return TPIGatewayResult.ok().setData(SiteApiUtils.getData(result));
			} else {
				log.error("result is null");
				return TPIGatewayResult.error("result is null");
			}
		} catch (feign.RetryableException e) {
			String msg = e.getClass() + ":" + e.getMessage();
			log.error(msg);
			e.printStackTrace();
			return TPIGatewayResult.error("連線失敗");
		} catch (Exception e) {
			String msg = e.getClass() + ":" + e.getMessage();
			log.error(msg);
			e.printStackTrace();
			return TPIGatewayResult.error(msg);
		}
	}
	
}
