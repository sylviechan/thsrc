package com.tp.tpigateway.modules.cathaysite.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysite.service.RspFundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("cathaysite/fund")
public class RspFundController extends BaseController {
	@Autowired
	RspFundService rspFundService;

	@RequestMapping(value = "/getRspFundList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getRspFundList(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("SendType", "4");

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = rspFundService.getRspFundList(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/waitRspFundTradetype", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitRspFundTradetype(@RequestBody FundRequest reqVO) {
		return rspFundService.waitRspFundTradetype(reqVO);
	}

	@RequestMapping(value = "/waitRspFundMoney", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitRspFundMoney(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = rspFundService.waitRspFundMoney(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/validateRspFundMoney", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult validateRspFundMoney(@RequestBody FundRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return rspFundService.validateRspFundMoney(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/waitRspFundAccount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitRspFundAccount(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = rspFundService.waitRspFundAccount(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/waitRspFundStype", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitRspFundStype(@RequestBody FundRequest reqVO) {
		return rspFundService.waitRspFundStype(reqVO);
	}

	@RequestMapping(value = "/waitRspFundSday", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult waitRspFundSday(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());
		requireParams.put("bact", reqVO.getSno() + "-" + reqVO.getAcnt());
		requireParams.put("tradetype", reqVO.getTradetype());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = rspFundService.waitRspFundSday(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/rspSetAdd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult rspSetAdd(@RequestBody FundRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("tradetype", reqVO.getTradetype());
			switch (reqVO.getTradetype()) {
			case "1":
				requireParams.put("samt", reqVO.getAmt());
				break;
			case "2":
				requireParams.put("highamt", reqVO.getHighamt());
				requireParams.put("midamt", reqVO.getMidamt());
				requireParams.put("lowamt", reqVO.getLowamt());
				break;
			}
			requireParams.put("sday", reqVO.getSday());
			requireParams.put("pwd", reqVO.getPwd());
			requireParams.put("bact", reqVO.getSno() + "-" + reqVO.getAcnt());
			requireParams.put("stype", reqVO.getStype());
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return rspFundService.rspSetAdd(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}
}