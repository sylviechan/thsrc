package com.tp.tpigateway.modules.cathaysite.fegin;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tp.common.annotation.TpSysLog;

@FeignClient(name = "ctiapi", url = "${cathaysite.fund.path}")
@TpSysLog(remark = "CtiAPIFeignService")
public interface CtiAPIFeignService {
	@RequestMapping(value = "online/api/cti.asmx/getBusinessDays", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, headers = {
			"sysKey=${cathaysite.sysKey}", "sysId=${cathaysite.sysId}" })
	Map<String, Object> getBusinessDays(Map<String, Object> obj);

	@RequestMapping(value = "online/api/cti.asmx/setAdd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, headers = {
			"sysKey=${cathaysite.sysKey}", "sysId=${cathaysite.sysId}" })
	Map<String, Object> ctiSetAdd(Map<String, Object> obj, @RequestHeader("token") String token);
}