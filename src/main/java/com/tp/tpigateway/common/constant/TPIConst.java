package com.tp.tpigateway.common.constant;

public class TPIConst {

    public static final String ROLE_ALPHA ="alpha";

    public static final String PARAMETER_ERROR ="缺少必要參數";

    public static final String BASE_EXECUTOR ="baseExecutor";

    //secondBot反問
    public static final String SECOND_BOT_ASK ="secondBotAsk";

    //完全聽不懂
    public static final String CAN_NOT_ANS ="canNotAnswer";

    //阿發猜你想什麼
    public static final String ASK_CARD ="askCard";

}
