package com.tp.tpigateway.common.mybatis.controller;

import com.tp.common.annotation.NotShowAopLog;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.common.mybatis.entity.SysLog;
import com.tp.tpigateway.common.mybatis.service.ISysLogService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;


@RestController
@RequestMapping("sysLog")
public class SysLogController extends BaseController {

    @Autowired
    private ISysLogService sysLogService;

    @ApiOperation(value = "save系統日誌")
    @PostMapping(value = "/saveSysLog", produces = "application/json; charset=UTF-8")
    @NotShowAopLog
    public TPIGatewayResult saveSysLog(@RequestBody SysLog sysLog) {

        LocalDateTime createTime = LocalDateTime.now();

        sysLog.setCreateTime(createTime);
        sysLog.setUpdateTime(createTime);

        sysLogService.save(sysLog);

        return TPIGatewayResult.ok();
    }

}
