package com.tp.tpigateway.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tp.tpigateway.common.mybatis.entity.FlowCallLog;
import com.tp.tpigateway.common.mybatis.mapper.FlowCallLogMapper;
import com.tp.tpigateway.common.mybatis.service.IFlowCallLogService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 服務實作
 * </p>
 *
 * @author Todd
 * @since 2019-09-24
 */
@Service
public class FlowCallLogServiceImpl extends ServiceImpl<FlowCallLogMapper, FlowCallLog> implements IFlowCallLogService {

    public boolean updateBySessionIdAndMsgId(FlowCallLog flowCallLog) {

        LambdaUpdateWrapper<FlowCallLog> lambdaUpdateWrapper = Wrappers.lambdaUpdate(flowCallLog)
                .eq(FlowCallLog::getSessionId, flowCallLog.getSessionId())
                .eq(FlowCallLog::getMsgId, flowCallLog.getMsgId());

        UpdateWrapper<FlowCallLog> updateWrapper = new UpdateWrapper<>();

        update(flowCallLog,
                updateWrapper
                        .eq("sessionId", flowCallLog.getSecondBotName())
                        .eq("msgId", flowCallLog.getMsgId())
        );

        return update(lambdaUpdateWrapper);
    }
    
    public List<FlowCallLog> selectFlowCallLogById(FlowCallLog flowCallLog) {
    	Map<String, Object> columnMap = new HashMap<>();
        columnMap.put("session_id", flowCallLog.getSessionId());            	
    	Collection<FlowCallLog> flowCallLogCol = listByMap(columnMap);   
    	
    	List<FlowCallLog> flosCallLogList;    	
    	if (flowCallLogCol instanceof List) {
    		flosCallLogList = (List)flowCallLogCol;
    	} else {
    		flosCallLogList = new ArrayList(flowCallLogCol);
    	}    		  
    	
    	return flosCallLogList;
    }
}
