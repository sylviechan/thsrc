package com.tp.tpigateway.common.mybatis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tp.tpigateway.common.constant.CacheConst;
import com.tp.tpigateway.common.mybatis.entity.BotTpiReplyText;
import com.tp.tpigateway.common.mybatis.mapper.BotTpiReplyTextMapper;
import com.tp.tpigateway.common.mybatis.service.IBotTpiReplyTextService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author matt
 * @since 2019-08-12
 */
@Service
public class BotTpiReplyTextServiceImpl extends ServiceImpl<BotTpiReplyTextMapper, BotTpiReplyText> implements IBotTpiReplyTextService {

    private static Logger log = LoggerFactory.getLogger(BotTpiReplyTextServiceImpl.class);

    @Autowired
    CacheManager cacheManager;

    @Override
    public String getReplyText(String roleId, String textId) {
        List<BotTpiReplyText> list;
        Map<String, String> replyTexts;

        if (cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.BOT_TPI_REPLAY_TEXT_KEY) == null) {
            list = this.list();

            if (CollectionUtils.isNotEmpty(list)) {
                replyTexts = list.stream().collect(Collectors.toMap(b -> b.getRoleId() + b.getTextId(), b -> b.getText()));
            } else {
                replyTexts = new HashMap<>();
            }
            cacheManager.getCache(CacheConst.TPI_CACHE).put(CacheConst.BOT_TPI_REPLAY_TEXT_KEY, replyTexts);

        } else {
            SimpleValueWrapper simpleValueWrapper = (SimpleValueWrapper) cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.SYS_CONFIG_KEY);
            replyTexts = (Map<String, String>) simpleValueWrapper.get();
        }

        String key = roleId + textId;
        String replyText = replyTexts.get(key);

        return replyText;
    }

}
