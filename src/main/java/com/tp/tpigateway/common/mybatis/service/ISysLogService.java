package com.tp.tpigateway.common.mybatis.service;

import com.tp.common.mybatis.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author matt
 * @since 2019-09-18
 */
public interface ISysLogService extends IService<SysLog> {

}
