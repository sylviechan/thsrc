package com.tp.tpigateway.common.mybatis.controller;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.common.annotation.NotShowAopLog;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.mybatis.entity.FlowCallLog;
import com.tp.tpigateway.common.mybatis.service.IFlowCallLogService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("flowCallLog")
public class FlowCallLogController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(FlowCallLogController.class);

    @Autowired
    private IFlowCallLogService flowCallLogService;

    @ApiOperation(value = "儲存Flow執行相關log")
    @PostMapping(value = "/saveLog", produces = "application/json; charset=UTF-8")
    @NotShowAopLog
    public TPIGatewayResult saveLog(@RequestBody FlowCallLog callLog) {

        try {
            logger.debug(callLog.toString());

            LocalDateTime createTime = LocalDateTime.now();
            callLog.setCreateTime(createTime);
            callLog.setUpdateTime(createTime);

            flowCallLogService.save(callLog);
        } catch (Exception e) {
            logger.error(e.getMessage());
            if (callLog != null)
                logger.error(callLog.toString());
        }

        return TPIGatewayResult.ok();
    }
    
    @ApiOperation(value = "搜尋Flow執行相關log")    
    @RequestMapping(value = "/selectLog", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @NotShowAopLog
    public String selectLog(@RequestBody(required = false) FlowCallLog callLog) {
        List<FlowCallLog> flowCallLogList = new ArrayList<FlowCallLog>();
        
        if(callLog != null) {
        	flowCallLogList = flowCallLogService.selectFlowCallLogById(callLog);
        }                

        ObjectMapper mapper = new ObjectMapper();
		String jsonInString = "";

		try {
			jsonInString = mapper.writeValueAsString(flowCallLogList);

		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonInString;
        
    	//return flowCallLogList;
    }

    /*
    @ApiOperation(value = "更新Flow執行相關log")
    @PostMapping(value = "/updateLog", produces = "application/json; charset=UTF-8")
    @NotShowAopLog
    public TPIGatewayResult updateLog(@RequestBody FlowCallLog callLog) {

        try {
            logger.debug(callLog.toString());

            LocalDateTime updateTime = LocalDateTime.now();
            callLog.setUpdateTime(updateTime);

            flowCallLogService.updateBySessionIdAndMsgId(callLog);
        } catch (Exception e) {
            logger.error(e.getMessage());
            if (callLog != null)
                logger.error(callLog.toString());
        }

        return TPIGatewayResult.ok();
    }
    */
}
