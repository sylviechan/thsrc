package com.tp.tpigateway.common.mybatis.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tp.tpigateway.common.mybatis.entity.FlowCallLog;

/**
 * <p>
 *  服務介面
 * </p>
 *
 * @author Todd
 * @since 2019-09-24
 */
public interface IFlowCallLogService extends IService<FlowCallLog> {

    public boolean updateBySessionIdAndMsgId(FlowCallLog flowCallLog);
    
    public List<FlowCallLog> selectFlowCallLogById(FlowCallLog flowCallLog);
}
