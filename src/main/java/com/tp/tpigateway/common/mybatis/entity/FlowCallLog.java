package com.tp.tpigateway.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.tp.common.mybatis.entity.BaseEntity;
import com.tp.tpigateway.common.support.LocalDateTimeJsonDeserializer;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author Todd
 * @since 2019-09-24
 */
public class FlowCallLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id; // log id

    private String custId; // 客戶編號

    private String sessionId; // 對話編號

    private String ip; // 客戶端ip

    /** 輸入類型 - 1:手動(對話框輸入、initMsg), 2:非手動(其他) */
    private String inputType; // 輸入類型

    private String msgType; // 對話類型(initSession、customerMsg...)

    private String msgId; // 訊息編號

    private String msg; // 訊息內容

    @TableField(value = "assign_intent")
    private String assignIntent;

    @TableField(value = "assign_second_bot_intent")
    private String assignSecondBotIntent;

    @TableField(value = "assign_parameter")
    private String assignParameter;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonDeserialize(using = LocalDateTimeJsonDeserializer.class)
    private LocalDateTime msgTime; // 訊息進入時間

    /* 總機NLU begin */
    private String firstNluIntent; // 總機NLU最高分的intent

    private String firstNluScore; // 總機NLU分數

    private Long firstNluCostTime; // 總機NLU執行花費時間(亳秒)
    /* 總機NLU end */

    /* 服務台NLU begin */
    private String firstNluSupportIntent; // 服務台NLU最高分的intent

    private String firstNluSupportScore; // 服務台NLU分數

    private Long firstNluSupportCostTime; // 服務台NLU執行花費時間(亳秒)
    /* 服務台NLU end */

    /* FAQ begin */
    private String faqAnswer; // FAQ最高分的題號

    private String faqScore; // FAQ分數

    private Long faqCostTime; // FAQ執行花費時間(亳秒)
    /* FAQ end */

    /* 操作(/部門)NLU begin */
    private String secondNluIntent; // 操作(/部門)NLU最高分的intent

    private String secondNluScore; // 操作(/部門)NLU分數

    /**
     * format: entity,key_user_phrase,text;entity,key_user_phrase,text…
     * ex. sys.數字,null,5;股票類型,股票,股票
     * ps.有其他參數需紀錄再往後以逗號分隔遞補
     */
    private String secondNluEntities; // 操作(/部門)NLU實際使用的entity

    private Long secondNluCostTime; // 操作(/部門)NLU執行花費時間(亳秒)
    /* 操作(/部門)NLU end */

    /* 其他1.NLU begin */
    @TableField(value = "second_nlu_add1_intent")
    private String secondNluAdd1Intent; // 其他1.NLU最高分的intent

    @TableField(value = "second_nlu_add1_score")
    private String secondNluAdd1Score; // 其他1.NLU分數

    /**
     * format: entity,key_user_phrase,text;entity,key_user_phrase,text…
     * ex. sys.數字,null,5;股票類型,股票,股票
     * ps.有其他參數需紀錄再往後以逗號分隔遞補
     */
    @TableField(value = "second_nlu_add1_entities")
    private String secondNluAdd1Entities; // 其他1.NLU實際使用的entity

    @TableField(value = "second_nlu_add1_cost_time")
    private Long secondNluAdd1CostTime; // 其他1.NLU執行花費時間(亳秒)
    /* 其他1.NLU end */

    /* 其他2.NLU begin */
    @TableField(value = "second_nlu_add2_intent")
    private String secondNluAdd2Intent; // 其他2.NLU最高分的intent

    @TableField(value = "second_nlu_add2_score")
    private String secondNluAdd2Score; // 其他2.NLU分數

    /**
     * format: entity,key_user_phrase,text;entity,key_user_phrase,text…
     * ex. sys.數字,null,5;股票類型,股票,股票
     * ps.有其他參數需紀錄再往後以逗號分隔遞補
     */
    @TableField(value = "second_nlu_add2_entities")
    private String secondNluAdd2Entities; // 其他2.NLU實際使用的entity

    @TableField(value = "second_nlu_add2_cost_time")
    private Long secondNluAdd2CostTime; // 其他2.NLU執行花費時間(亳秒)
    /* 其他2.NLU end */

    private String secondBotName; // second bot(ex.證券-定期定額)名稱

    private String cmdName; // 流程節點編號(call TPI前最末端節點/cmd)

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonDeserialize(using = LocalDateTimeJsonDeserializer.class)
    private LocalDateTime flowTime; // Flow(完整)執行完成時間

    private Long flowCostTime; // Flow(完整)執行花費時間(亳秒)

    private String flowHostName; // Flow執行server端名稱(ex. Host、ip，如果可以取到的話)

    private String flowFirstAppId; // Flow firstBot app id

    private String memo; // 備註

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime createTime; // 紀錄新增時間

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime updateTime; // 紀錄更新時間

    public Integer getId() {
        return id;
    }

    public FlowCallLog setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getCustId() {
        return custId;
    }

    public FlowCallLog setCustId(String custId) {
        this.custId = custId;
        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public FlowCallLog setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public String getIp() {
        return ip;
    }

    public FlowCallLog setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public String getInputType() {
        return inputType;
    }

    public FlowCallLog setInputType(String inputType) {
        this.inputType = inputType;
        return this;
    }

    public String getMsgType() {
        return msgType;
    }

    public FlowCallLog setMsgType(String msgType) {
        this.msgType = msgType;
        return this;
    }

    public String getMsgId() {
        return msgId;
    }

    public FlowCallLog setMsgId(String msgId) {
        this.msgId = msgId;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public FlowCallLog setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public LocalDateTime getMsgTime() {
        return msgTime;
    }

    public FlowCallLog setMsgTime(LocalDateTime msgTime) {
        this.msgTime = msgTime;
        return this;
    }

    public String getAssignIntent() {
        return assignIntent;
    }

    public FlowCallLog setAssignIntent(String assignIntent) {
        this.assignIntent = assignIntent;
        return this;
    }

    public String getAssignSecondBotIntent() {
        return assignSecondBotIntent;
    }

    public FlowCallLog setAssignSecondBotIntent(String assignSecondBotIntent) {
        this.assignSecondBotIntent = assignSecondBotIntent;
        return this;
    }

    public String getAssignParameter() {
        return assignParameter;
    }

    public FlowCallLog setAssignParameter(String assignParameter) {
        this.assignParameter = assignParameter;
        return this;
    }

    public String getFirstNluIntent() {
        return firstNluIntent;
    }

    public FlowCallLog setFirstNluIntent(String firstNluIntent) {
        this.firstNluIntent = firstNluIntent;
        return this;
    }

    public String getFirstNluScore() {
        return firstNluScore;
    }

    public FlowCallLog setFirstNluScore(String firstNluScore) {
        this.firstNluScore = firstNluScore;
        return this;
    }

    public Long getFirstNluCostTime() {
        return firstNluCostTime;
    }

    public FlowCallLog setFirstNluCostTime(Long firstNluCostTime) {
        this.firstNluCostTime = firstNluCostTime;
        return this;
    }

    public String getFirstNluSupportIntent() {
        return firstNluSupportIntent;
    }

    public FlowCallLog setFirstNluSupportIntent(String firstNluSupportIntent) {
        this.firstNluSupportIntent = firstNluSupportIntent;
        return this;
    }

    public String getFirstNluSupportScore() {
        return firstNluSupportScore;
    }

    public FlowCallLog setFirstNluSupportScore(String firstNluSupportScore) {
        this.firstNluSupportScore = firstNluSupportScore;
        return this;
    }

    public Long getFirstNluSupportCostTime() {
        return firstNluSupportCostTime;
    }

    public FlowCallLog setFirstNluSupportCostTime(Long firstNluSupportCostTime) {
        this.firstNluSupportCostTime = firstNluSupportCostTime;
        return this;
    }

    public String getFaqAnswer() {
        return faqAnswer;
    }

    public FlowCallLog setFaqAnswer(String faqAnswer) {
        this.faqAnswer = faqAnswer;
        return this;
    }

    public String getFaqScore() {
        return faqScore;
    }

    public FlowCallLog setFaqScore(String faqScore) {
        this.faqScore = faqScore;
        return this;
    }

    public Long getFaqCostTime() {
        return faqCostTime;
    }

    public FlowCallLog setFaqCostTime(Long faqCostTime) {
        this.faqCostTime = faqCostTime;
        return this;
    }

    public String getSecondNluIntent() {
        return secondNluIntent;
    }

    public FlowCallLog setSecondNluIntent(String secondNluIntent) {
        this.secondNluIntent = secondNluIntent;
        return this;
    }

    public String getSecondNluScore() {
        return secondNluScore;
    }

    public FlowCallLog setSecondNluScore(String secondNluScore) {
        this.secondNluScore = secondNluScore;
        return this;
    }

    public String getSecondNluEntities() {
        return secondNluEntities;
    }

    public FlowCallLog setSecondNluEntities(String secondNluEntities) {
        this.secondNluEntities = secondNluEntities;
        return this;
    }

    public Long getSecondNluCostTime() {
        return secondNluCostTime;
    }

    public FlowCallLog setSecondNluCostTime(Long secondNluCostTime) {
        this.secondNluCostTime = secondNluCostTime;
        return this;
    }

    public String getSecondNluAdd1Intent() {
        return secondNluAdd1Intent;
    }

    public FlowCallLog setSecondNluAdd1Intent(String secondNluAdd1Intent) {
        this.secondNluAdd1Intent = secondNluAdd1Intent;
        return this;
    }

    public String getSecondNluAdd1Score() {
        return secondNluAdd1Score;
    }

    public FlowCallLog setSecondNluAdd1Score(String secondNluAdd1Score) {
        this.secondNluAdd1Score = secondNluAdd1Score;
        return this;
    }

    public String getSecondNluAdd1Entities() {
        return secondNluAdd1Entities;
    }

    public FlowCallLog setSecondNluAdd1Entities(String secondNluAdd1Entities) {
        this.secondNluAdd1Entities = secondNluAdd1Entities;
        return this;
    }

    public Long getSecondNluAdd1CostTime() {
        return secondNluAdd1CostTime;
    }

    public FlowCallLog setSecondNluAdd1CostTime(Long secondNluAdd1CostTime) {
        this.secondNluAdd1CostTime = secondNluAdd1CostTime;
        return this;
    }

    public String getSecondNluAdd2Intent() {
        return secondNluAdd2Intent;
    }

    public FlowCallLog setSecondNluAdd2Intent(String secondNluAdd2Intent) {
        this.secondNluAdd2Intent = secondNluAdd2Intent;
        return this;
    }

    public String getSecondNluAdd2Score() {
        return secondNluAdd2Score;
    }

    public FlowCallLog setSecondNluAdd2Score(String secondNluAdd2Score) {
        this.secondNluAdd2Score = secondNluAdd2Score;
        return this;
    }

    public String getSecondNluAdd2Entities() {
        return secondNluAdd2Entities;
    }

    public FlowCallLog setSecondNluAdd2Entities(String secondNluAdd2Entities) {
        this.secondNluAdd2Entities = secondNluAdd2Entities;
        return this;
    }

    public Long getSecondNluAdd2CostTime() {
        return secondNluAdd2CostTime;
    }

    public FlowCallLog setSecondNluAdd2CostTime(Long secondNluAdd2CostTime) {
        this.secondNluAdd2CostTime = secondNluAdd2CostTime;
        return this;
    }

    public String getSecondBotName() {
        return secondBotName;
    }

    public FlowCallLog setSecondBotName(String secondBotName) {
        this.secondBotName = secondBotName;
        return this;
    }

    public String getCmdName() {
        return cmdName;
    }

    public FlowCallLog setCmdName(String cmdName) {
        this.cmdName = cmdName;
        return this;
    }

    public LocalDateTime getFlowTime() {
        return flowTime;
    }

    public FlowCallLog setFlowTime(LocalDateTime flowTime) {
        this.flowTime = flowTime;
        return this;
    }

    public Long getFlowCostTime() {
        return flowCostTime;
    }

    public FlowCallLog setFlowCostTime(Long flowCostTime) {
        this.flowCostTime = flowCostTime;
        return this;
    }

    public String getFlowHostName() {
        return flowHostName;
    }

    public FlowCallLog setFlowHostName(String flowHostName) {
        this.flowHostName = flowHostName;
        return this;
    }

    public String getFlowFirstAppId() {
        return flowFirstAppId;
    }

    public FlowCallLog setFlowFirstAppId(String flowFirstAppId) {
        this.flowFirstAppId = flowFirstAppId;
        return this;
    }

    public String getMemo() {
        return memo;
    }

    public FlowCallLog setMemo(String memo) {
        this.memo = memo;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public FlowCallLog setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public FlowCallLog setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "FlowCallLog{" +
                "id=" + id +
                ", custId=" + custId +
                ", sessionId=" + sessionId +
                ", ip=" + ip +
                ", inputType=" + inputType +
                ", msgType=" + msgType +
                ", msgId=" + msgId +
                ", msg=" + msg +
                ", msgTime=" + msgTime +
                ", firstNluIntent=" + firstNluIntent +
                ", firstNluScore=" + firstNluScore +
                ", firstNluCostTime=" + firstNluCostTime +
                ", firstNluSupportIntent=" + firstNluSupportIntent +
                ", firstNluSupportScore=" + firstNluSupportScore +
                ", firstNluSupportCostTime=" + firstNluSupportCostTime +
                ", faqAnswer=" + faqAnswer +
                ", faqScore=" + faqScore +
                ", faqCostTime=" + faqCostTime +
                ", secondNluIntent=" + secondNluIntent +
                ", secondNluScore=" + secondNluScore +
                ", secondNluEntities=" + secondNluEntities +
                ", secondNluCostTime=" + secondNluCostTime +
                ", secondNluAdd1Intent=" + secondNluAdd1Intent +
                ", secondNluAdd1Score=" + secondNluAdd1Score +
                ", secondNluAdd1Entities=" + secondNluAdd1Entities +
                ", secondNluAdd1CostTime=" + secondNluAdd1CostTime +
                ", secondNluAdd2Intent=" + secondNluAdd2Intent +
                ", secondNluAdd2Score=" + secondNluAdd2Score +
                ", secondNluAdd2Entities=" + secondNluAdd2Entities +
                ", secondNluAdd2CostTime=" + secondNluAdd2CostTime +
                ", secondBotName=" + secondBotName +
                ", cmdName=" + cmdName +
                ", flowTime=" + flowTime +
                ", flowCostTime=" + flowCostTime +
                ", flowHostName=" + flowHostName +
                ", flowFirstAppId=" + flowFirstAppId +
                ", memo=" + memo +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}
