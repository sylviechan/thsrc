package com.tp.tpigateway.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.tp.common.mybatis.entity.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author matt
 * @since 2019-08-12
 */
public class BotTpiReplyText extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    private String roleId;

    private String text;

    private String textId;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public BotTpiReplyText setId(Integer id) {
        this.id = id;
        return this;
    }
    public String getRoleId() {
        return roleId;
    }

    public BotTpiReplyText setRoleId(String roleId) {
        this.roleId = roleId;
        return this;
    }
    public String getText() {
        return text;
    }

    public BotTpiReplyText setText(String text) {
        this.text = text;
        return this;
    }
    public String getTextId() {
        return textId;
    }

    public BotTpiReplyText setTextId(String textId) {
        this.textId = textId;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public BotTpiReplyText setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public BotTpiReplyText setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("roleId", roleId)
                .append("text", text)
                .append("textId", textId)
                .append("createTime", createTime)
                .append("updateTime", updateTime)
                .toString();
    }
}
