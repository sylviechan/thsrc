package com.tp.tpigateway.common.mybatis.controller;

import com.tp.common.annotation.NotShowAopLog;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.mybatis.service.ISysConfigService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("sysConfig")
public class SysConfigController extends BaseController {

    @Autowired
    private ISysConfigService sysConfigService;

    @ApiOperation(value = "取得系統設定參數")
    @GetMapping(value = "/getAllSysConfig", produces = "application/json; charset=UTF-8")
    @NotShowAopLog
    public TPIGatewayResult getAllSysConfig() {
        Map<String, String> sysConfigMap = sysConfigService.getAllSysConfig();
        return TPIGatewayResult.ok().setData(sysConfigMap);
    }

}
