package com.tp.tpigateway.common.mybatis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tp.tpigateway.common.mybatis.entity.SysConfig;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author matt
 * @since 2019-08-19
 */
public interface ISysConfigService extends IService<SysConfig> {

    Map<String, String> getAllSysConfig();
}
