package com.tp.tpigateway.common.mybatis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tp.tpigateway.common.constant.CacheConst;
import com.tp.tpigateway.common.mybatis.entity.SysConfig;
import com.tp.tpigateway.common.mybatis.mapper.SysConfigMapper;
import com.tp.tpigateway.common.mybatis.service.ISysConfigService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author matt
 * @since 2019-08-19
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

    @Autowired
    CacheManager cacheManager;

    @Override
    public Map<String, String> getAllSysConfig() {
        List<SysConfig> list;
        Map<String, String> sysConfigMap;

        if (cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.SYS_CONFIG_KEY) == null) {
            list = this.list();

            if (CollectionUtils.isNotEmpty(list)) {
                sysConfigMap = list.stream().collect(Collectors.toMap(SysConfig::getKey1, SysConfig::getValue1));
            } else {
                sysConfigMap = new HashMap<>();
            }
            cacheManager.getCache(CacheConst.TPI_CACHE).put(CacheConst.SYS_CONFIG_KEY, sysConfigMap);

        } else {
            SimpleValueWrapper simpleValueWrapper = (SimpleValueWrapper) cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.SYS_CONFIG_KEY);
            sysConfigMap = (Map<String, String>) simpleValueWrapper.get();
        }


        return sysConfigMap;
    }
}
