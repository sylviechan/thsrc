package com.tp.tpigateway.common.mybatis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tp.tpigateway.common.constant.CacheConst;
import com.tp.tpigateway.common.mybatis.entity.AllAutocomplete;
import com.tp.tpigateway.common.mybatis.mapper.AllAutocompleteMapper;
import com.tp.tpigateway.common.mybatis.service.IAllAutocompleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author matt
 * @since 2019-08-08
 */
@Service
public class AllAutocompleteServiceImpl extends ServiceImpl<AllAutocompleteMapper, AllAutocomplete> implements IAllAutocompleteService {

    @Autowired
    private CacheManager cacheManager;

    @Override
    public List<AllAutocomplete> getAllAutoCompleteList() {
        List<AllAutocomplete> list;

        if (cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.ALL_AUTO_COMPLETE_LIST_KEY) == null) {
            list = this.list();
            cacheManager.getCache(CacheConst.TPI_CACHE).put(CacheConst.ALL_AUTO_COMPLETE_LIST_KEY, list);

        } else {
            SimpleValueWrapper simpleValueWrapper = (SimpleValueWrapper) cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.ALL_AUTO_COMPLETE_LIST_KEY);
            list = (List<AllAutocomplete>) simpleValueWrapper.get();
        }

        return list == null ? new ArrayList<>() : list;
    }
}
