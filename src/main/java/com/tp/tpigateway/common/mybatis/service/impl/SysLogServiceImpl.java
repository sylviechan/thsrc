package com.tp.tpigateway.common.mybatis.service.impl;

import com.tp.common.mybatis.entity.SysLog;
import com.tp.tpigateway.common.mybatis.mapper.SysLogMapper;
import com.tp.tpigateway.common.mybatis.service.ISysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author matt
 * @since 2019-09-18
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

}
