package com.tp.tpigateway.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tp.tpigateway.common.mybatis.entity.FlowCallLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Todd
 * @since 2019-09-24
 */
public interface FlowCallLogMapper extends BaseMapper<FlowCallLog> {

}
