package com.tp.tpigateway.common.mybatis.mapper;

import com.tp.common.mybatis.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author matt
 * @since 2019-09-18
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
