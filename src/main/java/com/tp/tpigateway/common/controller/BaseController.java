package com.tp.tpigateway.common.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.tp.tpigateway.common.util.PropUtils;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

abstract public class BaseController {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MessageSource messageSource;

    public String getMessage(String key, String... args) {
        return this.messageSource.getMessage(key, args, Locale.getDefault());
    }
    
    /**
	 * 輸入參數驗證
	 * 
	 * @param params
	 * @return true:pass, false:fail
	 */
	public boolean checkParams(Map<String, String> params) {
		
		boolean result = false;
		try {
			if(params != null && !params.isEmpty()) {

				StringBuilder paramSB = new StringBuilder();

				Set<String> keySet = params.keySet();
				for(String key : keySet) {
					if(StringUtils.isBlank(params.get(key))) {
						paramSB.append("[" + key + "]");
					}
				}

				if(paramSB.length() == 0) {
					result = true;
				} else {
					log.error(PropUtils.RESULT_DESCRIPTION_PARAMETER_ERROR + " - " + paramSB.toString());
				}
			} else {
				result = true;
			}
		} catch (Exception e) {
			log.error("error:" + e.getMessage());
			log.debug("", e);
		}

		return result;
	}
}