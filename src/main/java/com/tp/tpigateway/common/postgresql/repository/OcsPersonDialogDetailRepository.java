package com.tp.tpigateway.common.postgresql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tp.tpigateway.common.postgresql.model.OcsPersonDialogDetail;

// wayne
// 20200101
public interface OcsPersonDialogDetailRepository extends JpaRepository<OcsPersonDialogDetail, Integer> {

}
