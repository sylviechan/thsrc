package com.tp.tpigateway.common.postgresql.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ocs_persondialog")
public class OcsPersonDialog {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="seqno")
	private int seqno;
	@Column(name="session_id")
	private String sessionId;
	@Column(name="client_id")
	private String clientId;
	@Column(name="answer_id")
	private String answerId;
	@Column(name="incoming_type")
	private String incomingType;
	@Column(name="sensitive_words")
	private String sensitiveWords;
	@Column(name="boot_satisfaction")
	private String bootSatisfaction;
	@Column(name="person_satisfaction")
	private String personSatisfaction;
	@Column(name="question_category")
	private String questionCategory;
	@Column(name="start_time")
	private Timestamp startTime;
	@Column(name="incoming_time")
	private Timestamp incomingTime;
	@Column(name="end_time")
	private Timestamp endTime;
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getAnswerId() {
		return answerId;
	}
	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}
	public String getIncomingType() {
		return incomingType;
	}
	public void setIncomingType(String incomingType) {
		this.incomingType = incomingType;
	}
	public String getSensitiveWords() {
		return sensitiveWords;
	}
	public void setSensitiveWords(String sensitiveWords) {
		this.sensitiveWords = sensitiveWords;
	}
	public String getBootSatisfaction() {
		return bootSatisfaction;
	}
	public void setBootSatisfaction(String bootSatisfaction) {
		this.bootSatisfaction = bootSatisfaction;
	}
	public String getPersonSatisfaction() {
		return personSatisfaction;
	}
	public void setPersonSatisfaction(String personSatisfaction) {
		this.personSatisfaction = personSatisfaction;
	}
	public String getQuestionCategory() {
		return questionCategory;
	}
	public void setQuestionCategory(String questionCategory) {
		this.questionCategory = questionCategory;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getIncomingTime() {
		return incomingTime;
	}
	public void setIncomingTime(Timestamp incomingTime) {
		this.incomingTime = incomingTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
}
