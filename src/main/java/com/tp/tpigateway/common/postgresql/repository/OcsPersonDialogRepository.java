package com.tp.tpigateway.common.postgresql.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tp.tpigateway.common.postgresql.model.OcsPersonDialog;

//wayne
//20200101
public interface OcsPersonDialogRepository extends JpaRepository<OcsPersonDialog, Integer> {

}
