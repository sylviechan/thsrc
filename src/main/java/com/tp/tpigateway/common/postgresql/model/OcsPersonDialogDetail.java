package com.tp.tpigateway.common.postgresql.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ocs_persondialog")
public class OcsPersonDialogDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="seqno")
	private int seqno;
	@Column(name="session_id")
	private String sessionId;
	@Column(name="client_id")
	private String clientId;
	@Column(name="answer_id")
	private String answerId;
	@Column(name="start_time")
	private Timestamp startTime;
	@Column(name="client_msg_time")
	private Timestamp clientMsgTime;
	@Column(name="reply_time")
	private Timestamp replyTime;
	@Column(name="end_time")
	private Timestamp endTime;
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getAnswerId() {
		return answerId;
	}
	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getClientMsgTime() {
		return clientMsgTime;
	}
	public void setClientMsgTime(Timestamp clientMsgTime) {
		this.clientMsgTime = clientMsgTime;
	}
	public Timestamp getReplyTime() {
		return replyTime;
	}
	public void setReplyTime(Timestamp replyTime) {
		this.replyTime = replyTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
}
