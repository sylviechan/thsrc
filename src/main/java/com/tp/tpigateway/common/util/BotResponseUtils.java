package com.tp.tpigateway.common.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CLinkListItem;
import com.tp.common.model.BotMessageVO.ContentItem;
import com.tp.common.model.BotMessageVO.LinkListItem;
import com.tp.common.model.cathaysec.AccountData;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.mybatis.entity.AllAutocomplete;

@Component
public class BotResponseUtils {

	// wayne
	// 20191230
	private static final String THSR = "thsr";

	public enum Widget { // for type=12用
		doLogin, doBankCodeAutoComplete, doNotice, doGeolocation, doSurvey, doSelectSearchComp,
		// 證券
		doPasswordRelated, doSecLogin, doSecSelectAccount, doSecVoucher, doSecVoucher_check, doRegularPolicy, doRegular19Policy,
		doSecVoucher_buy, doEntrustRet, doClinchRtn, doRegular52Query, doRegularQAPolicy, doPopularShare,
		doRegularIFrame,
		// 投信		
		doTxPwd, doSiteCustomerServiceForm, doSiteFundList, doFundStopList, doRspFundList, doSiteFundList2, doSellFundList,doSellFundTrfundList,doDeductionDate,
		doIdentity, doCphoneForm, doCphoneCAPTCHA, doCphoneCAPTCHACheck, doSiteOrderGroupList, doSiteOrderDetailList, doRspvFundAmtForm,
		// 產險
		doInsList_Car, doInsList_Health, doInsList_Travel, doInsCard, doInsStatic, doInsRoadRescue, doInsCameraAlbum,doInsContentDesc, doInsList_TravelChange, doCreditCardView,
		doToAgentComponent, doInsDatePicker, doTopQuestion
	}

	public void setSurvey(BotMessageVO vo) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSurvey.name());
		vo.getContent().add(content.getContent());
	}

	public BotMessageVO setLogin(BotMessageVO vo) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doLogin.name());
		vo.getContent().add(content.getContent());
		return vo;
	}

	public BotMessageVO doTopQuestion(BotMessageVO vo) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doTopQuestion.name());
		vo.getContent().add(content.getContent());
		return vo;
	}
	
	public BotMessageVO setCreditCardView(BotMessageVO vo, String UplAppllteNo, String errorMsg) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doCreditCardView.name());
		content.getContent().put("uplAppllteNo", UplAppllteNo);
		content.getContent().put("errorMsg", errorMsg);
		vo.getContent().add(content.getContent());
		return vo;
	}
	
	public void setAgent(BotMessageVO vo, String text) {
		ContentItem content = vo.new ContentItem();
		content.setType(15);
		content.setText(text);
		content.setWidget(Widget.doToAgentComponent.name());
		vo.getContent().add(content.getContent());
	}

	public BotMessageVO initBotMessage(String channel, String role) {
		BotMessageVO vo = new BotMessageVO();
		vo.setChannel(channel);
		vo.setRole(role);
		vo.setContent(new ArrayList<>());
		return vo;
	}
	
	// wayne
	// 20191230
	public BotMessageVO initBotMessageForThsrCard(String channel) {
		BotMessageVO vo = new BotMessageVO();
		vo.setChannel(channel);
		vo.setRole(THSR);
		vo.setSource(THSR);
		vo.setContent(new ArrayList<>());
		return vo;
	}
	
	/**
	 * @author West
	 * @version 建立時間:Dec 30, 2019 23:34:37 PM 
	 * 說明:給日期時刻表等使用
	 */
	public BotMessageVO initBotMessage() {
		BotMessageVO vo = new BotMessageVO();
		vo.setRole(THSR);
		vo.setSource(THSR);
		vo.setContent(new ArrayList<>());
		return vo;
	}
	
	public void setDateView(BotMessageVO vo,Map<String, Object> viewDate) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doInsDatePicker.name());
		content.setDateView(viewDate);
		vo.getContent().add(content.getContent());
	}

	public void setTextResult(BotMessageVO vo, String text) {
		ContentItem contentH = this.getTextContentItem(vo, text);
		vo.getContent().add(contentH.getContent());
	}

	public void setNoteResult(BotMessageVO vo, String annoTitle, String annoText) {
		ContentItem contentH = this.getNoteContentItem(vo, annoTitle, annoText);
		vo.getContent().add(contentH.getContent());
	}

	public void setHNoteResult(BotMessageVO vo, String annoText) {
		ContentItem contentH = getHNoteContentItem(vo, annoText);
		vo.getContent().add(contentH.getContent());
	}

	public void setHNoteResultWithReplace(BotMessageVO vo, String annoText, Map<String, String> replaceParams) {
		ContentItem contentH = this.getHNoteContentItemWithReplace(vo, annoText, replaceParams);
		vo.getContent().add(contentH.getContent());
	}

	public ContentItem getHNoteContentItem(BotMessageVO vo, String annoText) {
		ContentItem contentH = vo.new ContentItem();
		contentH.setType(13);
		contentH.setAnnoText(annoText);
		return contentH;
	}

	public ContentItem getHNoteContentItemWithReplace(BotMessageVO vo, String annoText,
			Map<String, String> replaceParams) {
		return getHNoteContentItem(vo, replaceTextByParamMap(annoText, replaceParams));
	}

	public void setNoteResultWithReplace(BotMessageVO vo, String annoTitle, String annoText,
			Map<String, String> replaceParams) {
		ContentItem contentH = this.getNoteContentItemWithReplace(vo, annoTitle, annoText, replaceParams);
		vo.getContent().add(contentH.getContent());
	}

	public void setTextResultWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap) {
		ContentItem contentH = this.getTextContentItemWithReplace(vo, text, paramMap);
		vo.getContent().add(contentH.getContent());
	}

	public void setCardsResult(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
		ContentItem content = vo.new ContentItem();
		content.setType(2);
		content.setCType(ctype);
		content.setCards(cardList);
		vo.getContent().add(content.getContent());
	}
	
	/**
	 * @author west
	 * @version 建立時間:Dec 30, 2019 23:34:37 AM 類
	 * 說明:給日期使用
	 */
	public void setCardsResult(BotMessageVO vo, Integer action,String widget,String intent) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setAction(action);
		content.setWidget(widget);
		content.setIntent(intent);
		vo.getContent().add(content.getContent());
	}
	
	/**
	 * @author west
	 * @version 建立時間:Dec 31, 2019 11:10:37 AM 類
	 * 說明:給時刻表使用
	 */
	public void setCardsResult(BotMessageVO vo, Integer action,String widget,String opDate,String number,String direction,Integer nrCars,List<Map<String,Object>> station,List<Map<String,Object>> basicFare) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setAction(action);
		content.setWidget(widget);
		content.setOpDate(opDate);
		content.setNumber(number);
		content.setDirection(direction);
		content.setNrCars(nrCars);
		content.setStationList(station);
		content.setBasicFare(basicFare);
		vo.getContent().add(content.getContent());
	}
	
	/**
	 * @author west
	 * @version 建立時間:Dec 31, 2019 15:34:37 AM 類
	 * 說明:給日期使用
	 */
	public void setCardsResultWithPhone(BotMessageVO vo, Integer action,String widget,String phone) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setAction(action);
		content.setWidget(widget);
		content.setPhone(phone);
		vo.getContent().add(content.getContent());
	}
	
	public void setPicComponentResult(BotMessageVO vo, String imageUrl, Boolean isOutUrl) {
		ContentItem content = vo.new ContentItem();
		content.setType(5);
		content.setImageUrl(imageUrl);
		content.IsOutUrl(isOutUrl);
		vo.getContent().add(content.getContent());
	}
	
	public void setPicComponentResult3(BotMessageVO vo, String imageUrl, Boolean isOutUrl) {
		ContentItem content = vo.new ContentItem();
		content.setType(20);
		content.setImageUrl(imageUrl);
		content.IsOutUrl(isOutUrl);
//		content.setWidget("Card");
		vo.getContent().add(content.getContent());
	}

	public void setSecVoucherData_check(BotMessageVO vo) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSecVoucher_check.name());
		vo.getContent().add(content.getContent());
	}

	public void setSecVoucherData_buy(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSecVoucher_buy.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setRegularPolicy(BotMessageVO vo, List<Map<String, Object>> dataList, String title) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doRegularPolicy.name());
		content.setTitle(title);
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setRegular19Policy(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doRegular19Policy.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}
	
	public void setRegularIFrame(BotMessageVO vo, String url) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doRegularIFrame.name());
		content.setUrl(url);		
		vo.getContent().add(content.getContent());
	}

	public void setSiteOrderGroupList(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSiteOrderGroupList.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}
	
	public void setSiteOrderDetailList(BotMessageVO vo, String annoType, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setAnnoType(annoType);
		content.setWidget(Widget.doSiteOrderDetailList.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setEntrustRet(BotMessageVO vo, List<Map<String, Object>> dataList, String title) {
		ContentItem content = vo.new ContentItem();
		content.setTitle(title);
		content.setType(12);
		content.setWidget(Widget.doEntrustRet.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setClinchRtn(BotMessageVO vo, List<Map<String, Object>> dataList, String title) {
		ContentItem content = vo.new ContentItem();
		content.setTitle(title);
		content.setType(12);
		content.setWidget(Widget.doClinchRtn.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setPopularShare(BotMessageVO vo, List<Map<String, Object>> dataList, String title) {
		ContentItem content = vo.new ContentItem();
		content.setTitle(title);
		content.setType(12);
		content.setWidget(Widget.doPopularShare.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setRegular52Query(BotMessageVO vo, List<Map<String, Object>> dataList, String title) {
		ContentItem content = vo.new ContentItem();
		content.setTitle(title);
		content.setType(12);
		content.setWidget(Widget.doRegular52Query.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setInsTopQuestion(BotMessageVO vo, List<Map<String, Object>> data, String titleName) {
		ContentItem content = vo.new ContentItem();
		content.setText(titleName);
		content.setLType("select-tag04");
		content.setType(16);
		content.setLinkList(data);
		vo.getContent().add(content.getContent());
	}

	public void setCardsResult19(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
		ContentItem content = vo.new ContentItem();
		content.setType(19);
		content.setCType(ctype);
		content.setCards(cardList);
		vo.getContent().add(content.getContent());
	}
	
	public void setCardsResult30(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
		ContentItem content = vo.new ContentItem();
		content.setType(30);
		content.setCType(ctype);
		content.setCards(cardList);
		vo.getContent().add(content.getContent());
	}

	public void setCardsResult31(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
		ContentItem content = vo.new ContentItem();
		content.setType(31);
		content.setCType(ctype);
		content.setCards(cardList);
		vo.getContent().add(content.getContent());
	}

	public void setInsStatic(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doInsStatic.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setInsResData(BotMessageVO vo, Map<String, Object> dataMap) {
		vo.getContent().add(dataMap);
	}

	public void setInsRescue(BotMessageVO vo, String title, String text) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doInsRoadRescue.name());
		content.setTitle(title);
		content.setText(text);
		vo.getContent().add(content.getContent());
	}

	public void setSecLoginData(BotMessageVO vo) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSecLogin.name());
		vo.getContent().add(content.getContent());
	}

	public void doSecSelectAccount(BotMessageVO vo, List<AccountData> accountDatas) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSecSelectAccount.name());
		content.getContent().put("accountDatas", accountDatas);
		vo.getContent().add(content.getContent());
	}

	public void setSecVoucherData(BotMessageVO vo) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSecVoucher.name());
		vo.getContent().add(content.getContent());
	}

	public void setRegularQAPolicy(BotMessageVO vo, String id, String udid, String cType, String lType, String text,
			String image, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();		
		content.setAnnoTitle(id);
		content.setAnnoType(udid);
		content.setType(12);
		content.setCType(cType);
		content.setWidget(Widget.doRegularQAPolicy.name());
		content.setLType(lType);
		content.setText(text);
		content.setImage(image);
//		content.setAnnoText(annoText);
//		content.setLinkList(linkList);
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setDoSelectSearchComp(BotMessageVO vo, List<Map<String, Object>> dataList, String title,
			String modeType, String serviceKey,String slogan) {
		ContentItem content = vo.new ContentItem();
		content.setCType(modeType);
		content.setLVType(serviceKey);
		content.setTitle(title);
		content.setType(12);
		content.setWidget(Widget.doSelectSearchComp.name());
		content.setDataList(dataList);
		content.setLVTitle(slogan);
		vo.getContent().add(content.getContent());
	}

	public void setInsCameraAlbumComp(BotMessageVO vo, String modeType) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setCType(modeType);
		content.setWidget(Widget.doInsCameraAlbum.name());
		vo.getContent().add(content.getContent());
	}

	public void setInsList(BotMessageVO vo, List<Map<String, Object>> dataList, int type) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		switch (type) {
		case 0:
			content.setWidget(Widget.doInsList_Car.name());
			break;
		case 1:
			content.setWidget(Widget.doInsList_Health.name());
			break;
		case 2:
			content.setWidget(Widget.doInsList_Travel.name());
			break;
		case 3:
			content.setWidget(Widget.doInsList_TravelChange.name());
			break;
		}

		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setInsCard(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
		ContentItem content = vo.new ContentItem();
		content.setType(19);
		content.setCType(ctype);
		content.setWidget(Widget.doInsCard.name());
		content.setCards(cardList);
		vo.getContent().add(content.getContent());
	}

	public void setInsA06ContentDesc(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doInsContentDesc.name());

		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public void setListViewResult(BotMessageVO vo, String lvType, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		boolean isMultiple = false;
		content.setType(14);
		content.setLVType(lvType);
		if (StringUtils.equals("1", lvType)) {
			isMultiple = true;
			content.setLVTitle("點選欲終止的附約(可複選)");
			content.setText("我要終止<%RD_NAME_LIST%>");// 我要終止([特約種類])[特約種類名稱]附約、([特約種類])[特約種類名稱]附約
		} else if (StringUtils.equals("2", lvType)) {
			isMultiple = false;
			content.setLVTitle("點選欲縮小的附約");
			content.setText("我要縮小<%RD_NAME_LIST%>附約");// 我要縮小([特約種類])[特約種類名稱]附約
		} else if (StringUtils.equals("3", lvType)) {
			isMultiple = true;
			content.setLVTitle("你為被保險人所擁有的保障項目");
		} else if (StringUtils.equals("4", lvType)) {
			content.setLVType("3");
			isMultiple = true;
			content.setLVTitle("這張保單所擁有的保障項目");
		}
		content.setMultiple(isMultiple);
		content.setLVDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	public ContentItem getTextContentItem(BotMessageVO vo, String text) {
		ContentItem contentH = vo.new ContentItem();
		contentH.setType(1);
		contentH.setText(text);
		return contentH;
	}

	public ContentItem getNoteContentItem(BotMessageVO vo, String annoTitle, String annoText) {
		ContentItem contentH = vo.new ContentItem();
		contentH.setType(11);
		contentH.setAnnoTitle(annoTitle);
		contentH.setAnnoText(annoText);
		return contentH;
	}

	public ContentItem getNoteContentItemWithReplace(BotMessageVO vo, String annoTitle, String annoText,
			Map<String, String> paramMap) {
		return getNoteContentItem(vo, annoTitle, replaceTextByParamMap(annoText, paramMap));
	}

	public ContentItem getImageContentItem(BotMessageVO vo, String imageUrl) {
		ContentItem contentH = vo.new ContentItem();
		contentH.setType(5);
		contentH.setAction(6);
		contentH.setImageUrl(imageUrl);
		return contentH;
	}
	
	public ContentItem getImageContentItem31(BotMessageVO vo, String imageUrl) {
		ContentItem contentH = vo.new ContentItem();
		contentH.setType(31);
		contentH.setAction(6);
		contentH.setImageUrl(imageUrl);
		return contentH;
	}

	public void setImageResult(BotMessageVO vo, String imageUrl) {
		ContentItem contentH = this.getImageContentItem(vo, imageUrl);
		vo.getContent().add(contentH.getContent());
	}
	
	public void setImageResult31(BotMessageVO vo, String imageUrl) {
		ContentItem contentH = this.getImageContentItem31(vo, imageUrl);
		vo.getContent().add(contentH.getContent());
	}

	public void setGeolocationResult(BotMessageVO vo) {
		ContentItem contentH = vo.new ContentItem();
		contentH.setType(12);
		contentH.setWidget(Widget.doGeolocation.name());
		vo.getContent().add(contentH.getContent());
	}

	public void setPasswordRelated(BotMessageVO vo, List<Map<String, Object>> dataList, String pageShow) {
		ContentItem content = setPasswordRelatedContentItem(vo, dataList, pageShow);
		vo.getContent().add(content.getContent());
	}

	public void setPasswordRelated(BotMessageVO vo, List<Map<String, Object>> dataList, String pageShow, String oldPassword) {
		ContentItem content = setPasswordRelatedContentItem(vo, dataList, pageShow);
		content.setOldPassword(oldPassword);
		vo.getContent().add(content.getContent());
	}

	private ContentItem setPasswordRelatedContentItem(BotMessageVO vo, List<Map<String, Object>> dataList, String pageShow) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doPasswordRelated.name());
		content.setDataList(dataList);
		content.setLVType(pageShow);
		return content;
	}

	public void setDeductionDate(BotMessageVO vo, String title, List<Map<String, Object>> dataList,
			String selectedVal) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doDeductionDate.name());
		content.setDataList(dataList);
		content.setText(selectedVal);
		content.setTitle(title);
		vo.getContent().add(content.getContent());
	}

	public void setIdentityCheckForm(BotMessageVO vo, String title, String type) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doIdentity.name());
		content.getContent().put("identityType", type);
		content.setTitle(title);
		vo.getContent().add(content.getContent());
	}

	public void setQuickReplayResult(BotMessageVO vo, QuickReplay... quickReplay) {
		List<QuickReplay> links = Arrays.asList(quickReplay);

		int[] wordLen = new int[links.size()];

		List<Map<String, Object>> linkLists = new ArrayList<>();
		for (int i = 0; i < links.size(); i++) {
			QuickReplay link = links.get(i);
			LinkListItem linkItem = this.getLinkListItem(vo, link);
			linkLists.add(linkItem.getLinkList());

			wordLen[i] = getWordLen(link.getText());
		}

		String lType = "";
		int len = links.size();
		if (wordLen.length == 2)
			lType = (wordLen[0] <= 9 && wordLen[1] <= 9) ? BotMessageVO.LTYPE_01 : BotMessageVO.LTYPE_04;
		else if (len == 3)
			lType = (wordLen[0] <= 5 && wordLen[1] <= 5 && wordLen[2] <= 5) ? BotMessageVO.LTYPE_02
					: BotMessageVO.LTYPE_04;
		else if (len == 4)
			lType = (wordLen[0] <= 9 && wordLen[1] <= 9 && wordLen[2] <= 9 && wordLen[3] <= 9) ? BotMessageVO.LTYPE_03
					: BotMessageVO.LTYPE_04;
		else
			lType = BotMessageVO.LTYPE_04;

		this.setQuickReplayResult(vo, lType, quickReplay);
	}

	public void setQuickReplayResult(BotMessageVO vo, String linkType, QuickReplay... quickReplay) {
		ContentItem contentLinkListH = vo.new ContentItem();
		contentLinkListH.setType(4);

		List<QuickReplay> links = Arrays.asList(quickReplay);

		List<Map<String, Object>> linkLists = new ArrayList<>();
		for (int i = 0; i < links.size(); i++) {
			QuickReplay link = links.get(i);
			LinkListItem linkItem = this.getLinkListItem(vo, link);
			linkLists.add(linkItem.getLinkList());
		}

		contentLinkListH.setLType(linkType);
		contentLinkListH.setLinkList(linkLists);

		vo.getContent().add(contentLinkListH.getContent());
	}

	public void setQuickReplayResult(BotMessageVO vo, String linkType, List<QuickReplay> links) {
		ContentItem contentLinkListH = vo.new ContentItem();
		contentLinkListH.setType(4);

		List<Map<String, Object>> linkLists = new ArrayList<>();
		for (int i = 0; i < links.size(); i++) {
			QuickReplay link = links.get(i);
			LinkListItem linkItem = this.getLinkListItem(vo, link);
			linkLists.add(linkItem.getLinkList());
		}

		contentLinkListH.setLType(linkType);
		contentLinkListH.setLinkList(linkLists);

		vo.getContent().add(contentLinkListH.getContent());
	}

	protected int getWordLen(String str) {
		int chinese = 0;
		int english = 0;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if ((int) c < 256)
				english++;
			else
				chinese++;
		}
		int engLen = english / 2;

		return chinese * 1 + (english % 2 == 0 ? engLen : engLen + 1);
	}

	public String replaceTextByParamMap(String text, Map<String, String> paramMap) {

		if (StringUtils.isBlank(text) || !StringUtils.contains(text, "<%") || !StringUtils.contains(text, "%>"))
			return text;
		StringBuffer sbf = new StringBuffer();
		String[] col_ids = text.split("<%|%>");
		for (String col_id : col_ids) {
			if (col_id.length() > 0 && paramMap.get(col_id) != null) {
				sbf.append(MapUtils.getString(paramMap, col_id));
			} else {
				if (text.indexOf("<%" + col_id + "%>") != -1)
					sbf.append("<%" + col_id + "%>");
				else
					sbf.append(col_id);
			}
		}
		return sbf.toString();
	}

	public ContentItem getTextContentItemWithReplace(BotMessageVO vo, String text, Map<String, String> paramMap) {
		return getTextContentItem(vo, replaceTextByParamMap(text, paramMap));
	}

	public LinkListItem getLinkListItem(BotMessageVO vo, QuickReplay quickReplay) {
		LinkListItem linkList = vo.new LinkListItem();
		linkList.setLAction(quickReplay.getAction());
		linkList.setLText(quickReplay.getText());
		linkList.setLAlt(quickReplay.getAlt());
		linkList.setLReply(quickReplay.getReply());
		linkList.setLUrl(quickReplay.getUrl());
		linkList.setLCustomerAction(quickReplay.getCustomerAction());
		linkList.setLIntent(quickReplay.getIntent());
		linkList.setLSecondBotIntent(quickReplay.getSecondBotIntent());
		linkList.setLParameter(quickReplay.getParameter());
		return linkList;
	}

	public List<Map<String, Object>> getCLinkList(BotMessageVO vo, QuickReplay... quickReplay) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<>();

		List<QuickReplay> links = Arrays.asList(quickReplay);

		for (QuickReplay link : links)
			cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());

		return cLinkContentList;
	}
	
	// wayne 
	// 20191225
	// for ThsrCardService.card
	public List<Map<String, Object>> getCLinkList(BotMessageVO vo, List<QuickReplay> quickReplay) {
		List<Map<String, Object>> cLinkContentList = new ArrayList<>();

		for (QuickReplay link : quickReplay)
			if(!link.getText().equals("")) // 檢查是否有子項目
				cLinkContentList.add(this.getCLinkListItem(vo, link).getCLinkList());

		return cLinkContentList;
	}

	public CLinkListItem getCLinkListItem(BotMessageVO vo, QuickReplay quickReplay) {
		CLinkListItem cLinkContent = vo.new CLinkListItem();
		cLinkContent.setClAction(quickReplay.getAction());
		cLinkContent.setClText(quickReplay.getText());
		cLinkContent.setClAlt(quickReplay.getAlt());
		
		if(!StringUtils.isEmpty(quickReplay.getReply()))
			cLinkContent.setClReply(quickReplay.getReply());
		if(!StringUtils.isEmpty(quickReplay.getUrl()))
			cLinkContent.setClUrl(quickReplay.getUrl());
		if(!StringUtils.isEmpty(quickReplay.getIntent()))
		cLinkContent.setClIntent(quickReplay.getIntent());
		if(!StringUtils.isEmpty(quickReplay.getSecondBotIntent()))
		cLinkContent.setClSecondBotIntent(quickReplay.getSecondBotIntent());
		if(quickReplay.getParameter() != null && !quickReplay.getParameter().isEmpty())
			cLinkContent.setClParameter(quickReplay.getParameter());
		return cLinkContent;
	}

	public BotMessageVO getSimpleBotMessageVO(String channel, String role, String text) {
		BotMessageVO botMsgVO = initBotMessage(channel, role);
		setTextResult(botMsgVO, text);
		return botMsgVO;
	}

	public BotMessageVO getAutoCompleteBotMessageVO(List<AllAutocomplete> dataList) {
		BotMessageVO botMsgVO = initBotMessage(null, null);

		List<Map<String, Object>> linkList = new ArrayList<>();
		LinkListItem link;
		ContentItem content = botMsgVO.new ContentItem();

		content.setType(4);
		content.setWidget("autoComplete");
		content.setLType(BotMessageVO.LTYPE_AUTO_COMPLETE);

		for (AllAutocomplete allAutocomplete : dataList) {
			link = botMsgVO.new LinkListItem();
			link.setLAction(2);
			link.setLAlt(allAutocomplete.getHiddenString());
			link.setLText(allAutocomplete.getDisplayString());
			linkList.add(link.getLinkList());
		}
		content.setLinkList(linkList);

		List<Map<String, Object>> contentList = new ArrayList<>();
		contentList.add(content.getContent());
		botMsgVO.setContent(contentList);
		return botMsgVO;
	}

	/**
	 * <pre>
	 * 投信基金選單
	 * </pre>
	 * <ul>
	 * <li>查詢基金淨值</li>
	 * <li>查詢基金配息</li>
	 * </ul>
	 */
	public void setSiteFundList(BotMessageVO vo, String searchType, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSiteFundList.name());
		content.setDataList(dataList);
		content.getContent().put("searchType", searchType);
		vo.getContent().add(content.getContent());
	}

	/**
	 * <pre>
	 * 申購單筆基金
	 * </pre>
	 * 
	 * 申購基金選單
	 */
	public void setSiteFundList2(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSiteFundList2.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	/**
	 * <pre>
	 * 買回轉申購
	 * </pre>
	 * 
	 * 贖回基金選單
	 */
	public void setSellFundList(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSellFundList.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	/**
	 * <pre>
	 * 買回轉申購
	 * </pre>
	 * 
	 * 專申購基金選單
	 */
	public void setSellFundTrfundList(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doSellFundTrfundList.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	/**
	 * 停扣基金列表
	 */
	public void setSiteFundStopList(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doFundStopList.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}

	/**
	 * <pre>
	 * 新定期定額申請
	 * </pre>
	 * 
	 * 申購基金選單
	 */
	public void setSiteRspFundList(BotMessageVO vo, List<Map<String, Object>> dataList) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doRspFundList.name());
		content.setDataList(dataList);
		vo.getContent().add(content.getContent());
	}
	
	/**
	 * <pre>
	 * 新定期定額申請
	 * </pre>
	 * 
	 * 定期不定額投資金額 浮出視窗輸入三種金額
	 */
	public void setSiteRspvFundAmtForm(BotMessageVO vo, String rsplowAmt, String upperAmt, String currency_txt) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doRspvFundAmtForm.name());
		content.getContent().put("rsplowAmt", rsplowAmt);
		content.getContent().put("upperAmt", upperAmt);
		content.getContent().put("currency_txt", currency_txt);
		vo.getContent().add(content.getContent());
	}

	public void setFundCardsResult(BotMessageVO vo, String ctype, List<Map<String, Object>> cardList) {
		ContentItem content = vo.new ContentItem();
		content.setType(2);
		content.setCType(ctype);
		content.setCards(cardList);
		vo.getContent().add(content.getContent());
	}

	/**
	 * <pre>
		 * <u>投信</u>交易認證碼浮出視窗
	 * </pre>
	 * 
	 * @param vo
	 * @param sendBtnText     送出按鈕文字
	 * @param sendMsg         flow接收訊息
	 * @param intent          (sendData)
	 * @param secondBotIntent (sendData)
	 * @param parameter       (sendData)
	 * @param linkList        下方連結
	 * @param errMsg          若密碼錯誤顯示訊息
	 */
	public void doTxPwd(BotMessageVO vo, String sendBtnText, String sendMsg, String intent, String secondBotIntent,
			Map<String, Object> parameter, List<Map> linkList, String errMsg) {
		ContentItem content = vo.new ContentItem();
		content.setType(12);
		content.setWidget(Widget.doTxPwd.name());
		content.getContent().put("sendBtnText", sendBtnText);
		content.getContent().put("sendMsg", sendMsg);
		content.getContent().put("errMsg", errMsg);

		Map<String, Object> sendData = new HashMap<>();
		sendData.put("intent", intent);
		sendData.put("secondBotIntent", secondBotIntent);
		sendData.put("parameter", parameter);
		content.getContent().put("sendData", sendData);
		content.getContent().put("linkList", linkList);

		vo.getContent().add(content.getContent());
	}

	/**
	 * 取得ecxel內容資料
	 */
	public List<Map<String, Object>> getExcel(BotMessageVO vo, String filePath, String fileName) {
		final String EXCEL_XLS = "xls";
		final String EXCEL_XLSX = "xlsx";
		Workbook wb = null;
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

		// 抓excel資料並做成list
		try {
			FileInputStream fileIn = new FileInputStream(filePath + fileName);
			if (fileName.endsWith(EXCEL_XLS)) {// Excel 2003
				wb = new HSSFWorkbook(fileIn);
			} else if (fileName.endsWith(EXCEL_XLSX)) {// Excel 2007/2010
				wb = new XSSFWorkbook(fileIn);
			}
			// 取得第一個Sheet
			Sheet sheet = wb.getSheetAt(0);

			// getLastRowNum 如果sheet中一行數據都沒有則返回-1，只有第一行有數據則返回0，最後有數據的行是第n行則返回n-1；
			// getPhysicalNumberOfRows 獲取有紀錄的行數，即 : 最後有數據的行數是n行，前面有m行是空行沒數據，則返回n-m;
			int rowLength = sheet.getPhysicalNumberOfRows();
			Row rowZero = sheet.getRow(1);
			int colLength = rowZero.getPhysicalNumberOfCells();
			for (int i = 1; i < rowLength; i++) {
				Row row = sheet.getRow(i);
				LinkListItem linkList = vo.new LinkListItem();
				for (int j = 0; j < colLength; j++) {
					Cell cell = row.getCell(j);
					switch (j) {
					case 0:
						linkList.setLText(cell.toString());
						break;
					case 1:
						linkList.setLTopQuestion(cell.toString());
						break;
					case 2:
						linkList.setLAlt(cell.toString());
						break;
					}
				}
				linkList.setLAction(19);
				resultList.add(linkList.getLinkList());
			}

		} catch (Exception e) {

		}
		return resultList;
	}

	/**
	 * 取得ecxel內容資料 filePath:檔案路徑 fileName:檔名
	 */
	public List<Map<String, Object>> getExcel2(BotMessageVO vo, String filePath, String fileName) {
		return getExcel2(vo, filePath, fileName, null);
	}

	/**
	 * 取得ecxel內容資料 filePath:檔案路徑 fileName:檔名 keyStrings:key名稱
	 */
	public List<Map<String, Object>> getExcel2(BotMessageVO vo, String filePath, String fileName, String[] keyStrings) {
		return getExcel2(vo, filePath, fileName, 0, 0, keyStrings, null);
	}

	/**
	 * 取得ecxel內容資料 filePath:檔案路徑 fileName:檔名 rowInitial:起始列 colInitial:起始行
	 * keyStrings:key名稱 sheetName:Shee名稱
	 */
	public List<Map<String, Object>> getExcel2(BotMessageVO vo, String filePath, String fileName, int rowInitial,
			int colInitial, String[] keyStrings, String sheetName) {
		final String EXCEL_XLS = "xls";
		final String EXCEL_XLSX = "xlsx";
		String[] keys = null;
		Workbook wb = null;
		Sheet sheet = null;
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

		// 抓excel資料並做成list
		try {
			FileInputStream fileIn = new FileInputStream(filePath + fileName);
			if (fileName.endsWith(EXCEL_XLS)) {// Excel 2003
				wb = new HSSFWorkbook(fileIn);
			} else if (fileName.endsWith(EXCEL_XLSX)) {// Excel 2007/2010
				wb = new XSSFWorkbook(fileIn);
			}
			// 取得第一個Sheet
			if (StringUtils.isNotBlank(sheetName)) {
				sheet = wb.getSheet(sheetName);
			} else {
				sheet = wb.getSheetAt(0);
			}

			// getLastRowNum 如果sheet中一行數據都沒有則返回-1，只有第一行有數據則返回0，最後有數據的行是第n行則返回n-1；
			// getPhysicalNumberOfRows 獲取有紀錄的行數，即 : 最後有數據的行數是n行，前面有m行是空行沒數據，則返回n-m;

			int rowLength = sheet.getPhysicalNumberOfRows();
			Row rowZero = sheet.getRow(rowInitial);
			int colLength = rowZero.getPhysicalNumberOfCells();

			// 取得Map的Key
			if (keyStrings == null) {
				keys = new String[colLength - colInitial];
				Row row = sheet.getRow(rowInitial);
				for (int j = colInitial; j < colLength; j++) {
					Cell cell = row.getCell(j);
					keys[j - colInitial] = cell.toString();
				}
				rowLength += 1;
			} else {
				keys = keyStrings;
			}

			// 組資料
			for (int i = rowInitial + 1; i < rowLength; i++) {
				Row row = sheet.getRow(i);
				Map<String, Object> map = new HashMap<String, Object>();
				for (int j = colInitial; j < colLength; j++) {
					Cell cell = row.getCell(j);
					map.put(keys[j - colInitial], cell.toString());
				}
				resultList.add(map);
			}

		} catch (Exception e) {

		}
		return resultList;
	}
}
