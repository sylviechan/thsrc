//package com.tp.tpigateway.common.util;
//
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.common.unit.TimeValue;
//import org.elasticsearch.index.query.ConstantScoreQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.index.query.TermQueryBuilder;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
//@Service
//public class EsService {
//
//    private static final Logger log = LoggerFactory.getLogger(EsService.class);
//
//
//    @Autowired
//    RestHighLevelClient highLevelClient;
//
//    public boolean testEsRestClient() {
//        SearchRequest searchRequest = new SearchRequest("chatflow-report-sessions-363504b.92de8fc");
//        searchRequest.types("sessions");
//
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//
//        /*
//        sourceBuilder.query(QueryBuilders.constantScoreQuery(
//
//        ))*/
//
//        sourceBuilder.query(QueryBuilders.termQuery("id_str", "tp1111002"));
//        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
//        searchRequest.source(sourceBuilder);
//        try {
//            SearchResponse response = highLevelClient.search(searchRequest);
//            Arrays.stream(response.getHits().getHits())
//                    .forEach(i -> {
//                        System.out.println(i.getIndex());
//                        System.out.println(i.getSourceAsString());
//                        System.out.println(i.getType());
//
//                        Map<String, Object> sourceAsMap = i.getSourceAsMap();
//                        Object messages_nested_list = sourceAsMap.get("messages_nested_list");
//                        System.out.println("messages_nested_list = " + messages_nested_list);
//                    });
//
//            System.out.println(response.getHits().totalHits);
//            return true;
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    public List<Map<String, Object>> getChatLogBySessionId(String sessionId) throws IOException {
//        /*
//        "query" : {
//            "constant_score" : {
//                "filter" : {
//                    "term" : {
//                        "id_str" : sessionId
//                    }
//                }
//            }
//        }
//        */
//
//        List<Map<String, Object>> chatLogs = new ArrayList<>();
//
//        SearchRequest searchRequest = new SearchRequest("chatflow-report-sessions-363504b.92de8fc");
//        searchRequest.types("sessions");
//
//        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
//
//        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("id_str", "tp1111002");
//        ConstantScoreQueryBuilder constantScoreQueryBuilder = QueryBuilders.constantScoreQuery(termQueryBuilder);
//        sourceBuilder.query(constantScoreQueryBuilder);
//
////        sourceBuilder.query(QueryBuilders.constantScoreQuery(
////
////        ))
//
//        //sourceBuilder.sort(SortBuilders.fieldSort("registered_at_str").order(SortOrder.ASC));
//
//        //sourceBuilder.query(QueryBuilders.termQuery("id_str", sessionId));
//        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
//        searchRequest.source(sourceBuilder);
//
//        long searchStartTime = System.currentTimeMillis();
//
//        SearchResponse response = highLevelClient.search(searchRequest);
//
//        long searchEndTime = System.currentTimeMillis();
//        log.info("**************costTime = " +  (searchEndTime - searchStartTime) + "ms");
//        log.info(response.toString());
//
//        Arrays.stream(response.getHits().getHits()).forEach(i -> {
//            Map<String, Object> sourceAsMap = i.getSourceAsMap();
//            List<Map<String, Object>> messages_nested_list = (List<Map<String, Object>>) sourceAsMap.get("messages_nested_list");
//
//            if (messages_nested_list != null && messages_nested_list.size() > 0) {
//                for (Map<String, Object> messages_nested : messages_nested_list) {
//                    Map<String, Object> info_dict = (Map<String, Object>) messages_nested.get("info_dict");
//                    if (info_dict != null) {
//                        Map<String, Object> payload_dict = null;
//                        try {
//                            payload_dict = (Map<String, Object>) info_dict.get("payload_dict");
//                        } catch (Exception e) {
//                            // 轉不了的就當成垃圾資料
//                            //log.error(e.getMessage(), e);
//                        }
//                        if (payload_dict != null) {
//                            chatLogs.add(payload_dict);
//                        }
//                    }
//                }
//            }
//        });
//
//        return chatLogs;
//    }
//}