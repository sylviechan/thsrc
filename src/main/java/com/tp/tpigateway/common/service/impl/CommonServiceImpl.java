package com.tp.tpigateway.common.service.impl;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.tpigateway.common.service.CommonService;
import com.tp.tpigateway.common.util.BotResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("commonService")
public class CommonServiceImpl implements CommonService {

	@Autowired
    BotResponseUtils botResponseUtils;
	
	@Override
    public BotMessageVO getLogin(BotRequest botRequest) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(botRequest.getChannel(), botRequest.getRole());

		botResponseUtils.setLogin(botMessageVO);

		return botMessageVO;
    }

}
