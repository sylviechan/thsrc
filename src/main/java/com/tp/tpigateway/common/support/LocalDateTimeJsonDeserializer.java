package com.tp.tpigateway.common.support;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class LocalDateTimeJsonDeserializer extends JsonDeserializer<LocalDateTime> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext ctxt) {
        try {
            if (jsonParser != null && StringUtils.isNotEmpty(jsonParser.getText())) {
                Instant instant = Instant.ofEpochMilli(jsonParser.getLongValue());
                ZoneId zone = ZoneId.systemDefault();
                return LocalDateTime.ofInstant(instant, zone);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}