package com.tp.tpigateway.common.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 排程
 */
@Component
public class SpringSchedule {

	private static final Logger log = LoggerFactory.getLogger(SpringSchedule.class);
	
//	@Scheduled(cron = "0 0 1 * * ?")
	public void exportLog() {
	}

}
