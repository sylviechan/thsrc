package com.tp.tpigateway.common.model;

import java.util.List;

import com.tp.common.model.BotRequest;

public class TagRequest extends BotRequest {
	
	private List<String> input;

	public List<String> getInput() {
		return input;
	}

	public void setInput(List<String> input) {
		this.input = input;
	}
	
}
