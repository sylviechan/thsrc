package com.tp.tpigateway.common.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.tp.tpigateway.common.util.YamlPropertySourceFactory;

// wayne
// 20191231
@Configuration
@EnableJpaRepositories(
		   entityManagerFactoryRef = "ptoponlineEntityManagerFactory",
		   basePackages = { "com.tp.tpigateway.common.postgresql" },
		   transactionManagerRef = "ptoponlineTransactionManager"
		)
@PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:application-dev.yml")
public class PtoponlineDbConfig {
	
	@Autowired
    private Environment env;
	
	@Bean(name = "ptoponlineDatasource")
    @ConfigurationProperties(prefix = "ptoponline.datasource")
    public DataSource ptoponlineDataSource() {
        return DataSourceBuilder.create()
           .driverClassName(env.getProperty("ptoponline.datasource.driver-class-name"))
           .url(env.getProperty("ptoponline.datasource.jdbc-url"))
           .username(env.getProperty("ptoponline.datasource.username"))
           .password(env.getProperty("ptoponline.datasource.password"))
           .build();
        
    }
	
	private JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.POSTGRESQL);
        adapter.setShowSql(true);
        adapter.setGenerateDdl(false);
        adapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQL95Dialect");

        return adapter;
    }
	
	@Bean
    public LocalContainerEntityManagerFactoryBean ptoponlineEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(ptoponlineDataSource());
        emf.setPackagesToScan(new String[] { "com.tp.tpigateway.common.postgresql" });
        emf.setJpaVendorAdapter(this.jpaVendorAdapter());

        return emf;
    }
	
	@Bean(name = "ptoponlineTransactionManager")
    public PlatformTransactionManager ptoponlineTransactionManager() {
        JpaTransactionManager tm = new JpaTransactionManager();
        tm.setEntityManagerFactory(this.ptoponlineEntityManagerFactory().getObject());
        
        return tm;
    }
}
