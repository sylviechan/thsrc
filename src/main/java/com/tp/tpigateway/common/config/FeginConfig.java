package com.tp.tpigateway.common.config;

import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import okhttp3.ConnectionPool;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class FeginConfig {

    /**
     * Logger.Level 的具體級別如下：
     * NONE：不記錄任何信息
     * BASIC：僅記錄請求方法、URL以及響應狀態碼和執行時間
     * HEADERS：除了記錄 BASIC級別的信息外，還會記錄請求和響應的頭信息
     * FULL：記錄所有請求與響應的明細，包括頭信息、請求體、元數據
     *
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Configuration
    @ConditionalOnClass(Feign.class)
    @AutoConfigureBefore(FeignAutoConfiguration.class)
    protected static class FeignOkHttpConfig {

        @Value("${connect.timeout}")
        private long connectTimeout;

        @Value("${read.timeout}")
        private long readTimeout;

        @Value("${write.timeout}")
        private long writeTimeout;

        @Bean
        public okhttp3.OkHttpClient okHttpClient() {
            return new okhttp3.OkHttpClient.Builder()
                    //設置連接超時
                    .connectTimeout(getDefaultTimeout(connectTimeout), TimeUnit.SECONDS)
                    //設置讀超時
                    .readTimeout(getDefaultTimeout(readTimeout), TimeUnit.SECONDS)
                    //設置寫超時
                    .writeTimeout(getDefaultTimeout(writeTimeout), TimeUnit.SECONDS)
                    //是否自動重連
                    .retryOnConnectionFailure(true)
                    .connectionPool(new ConnectionPool())
                    //構建OkHttpClient對象
                    .build();
        }

        private long getDefaultTimeout(long timeout) {
            if (timeout < 1) {
                return 30;
            }

            return  timeout;
        }
    }

    /*
        全局默認不重試
     */
//    @Bean
//    public Retryer feignRetryer() {
//        return Retryer.NEVER_RETRY;
//    }

    /*
        要重試可自己指定
        ex: @FeignClient(name = "${service.test}", configuration = FeignRetryableConfig.class ,fallback = TestService.class)

        你的远程调用接口方法的必须是幂等的(比如GET方法认为是幂等的，调用多少次结果都一样，而POST方法有可能有重复提交问题)，不然还是不会重试的,
        因为其他HttpMethod被认为是非幂等的，不能重复执行，因此不能被重试
     */
//    public class FeignRetryableConfig {
//        @Bean
//        public Retryer feignRetryer() {
//            //Retryer retryer = new Retryer.Default(100, 1000, 4);
//            return new Retryer.Default();
//        }
//    }

    @Configuration
    protected static class FeignRequestInterceptor implements RequestInterceptor {

        private org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

        @Override
        public void apply(RequestTemplate template) {
            //template.header("Authorization","lixue:123456");
        }

        /*
        @Autowired
        private ObjectMapper objectMapper;

        @Override
        public void apply(RequestTemplate template) {
            //template.header("Authorization","lixue:123456");

            // feign 不支持 GET 方法傳 POJO, json body轉query
            if (template.method().equals("GET") && template.body() != null) {
                try {
                    JsonNode jsonNode = objectMapper.readTree(template.body());
                    template.body(null);

                    Map<String, Collection<String>> queries = new HashMap<>();
                    buildQuery(jsonNode, "", queries);
                    template.queries(queries);
                } catch (IOException e) {
                    //提示:根據實踐項目情況處理此處異常，這裡不做擴展。
                    e.printStackTrace();
                }
            }
        }

        private void buildQuery(JsonNode jsonNode, String path, Map<String, Collection<String>> queries) {
            if (!jsonNode.isContainerNode()) {   // 葉子節點
                if (jsonNode.isNull()) {
                    return;
                }
                Collection<String> values = queries.get(path);
                if (null == values) {
                    values = new ArrayList<>();
                    queries.put(path, values);
                }
                values.add(jsonNode.asText());
                return;
            }
            if (jsonNode.isArray()) {   // 數組節點
                Iterator<JsonNode> it = jsonNode.elements();
                while (it.hasNext()) {
                    buildQuery(it.next(), path, queries);
                }
            } else {
                Iterator<Map.Entry<String, JsonNode>> it = jsonNode.fields();
                while (it.hasNext()) {
                    Map.Entry<String, JsonNode> entry = it.next();
                    if (StringUtils.hasText(path)) {
                        buildQuery(entry.getValue(), path + "." + entry.getKey(), queries);
                    } else {  // 根節點
                        buildQuery(entry.getValue(), entry.getKey(), queries);
                    }
                }
            }
        }
        */
    }

}