//package com.tp.tpigateway.common.config;
//
//import com.mongodb.Mongo;
//import com.mongodb.MongoClient;
//import com.mongodb.MongoClientOptions;
//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.ObjectProvider;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.autoconfigure.mongo.MongoProperties;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import java.io.UnsupportedEncodingException;
//import java.net.UnknownHostException;
//
//@Configuration
//@ConditionalOnClass({Mongo.class, MongoTemplate.class})
//@EnableConfigurationProperties({MongoProperties.class})
//public class MongoConfig {
//
//    private final MongoProperties properties;
//
//    private final MongoClientOptions options;
//
//    private final Environment environment;
//
//    private MongoClient mongo;
//
//    public MongoConfig(MongoProperties properties,
//                                  ObjectProvider<MongoClientOptions> options, Environment environment) {
//        this.properties = properties;
//        this.options = options.getIfAvailable();
//        this.environment = environment;
//    }
//
//    @Bean
//    public MongoClient mongo() throws UnknownHostException, UnsupportedEncodingException {
//		String uri = environment.getProperty("spring.data.mongodb.uri");
//		if (!StringUtils.contains(uri, "mongodb")) {
//			uri = new String((new Base64()).decode(uri), "UTF-8");
//		}
//		this.properties.setUri(uri);
//		this.mongo = this.properties.createMongoClient(this.options, this.environment);
//		return this.mongo;
//    }
//
//}
