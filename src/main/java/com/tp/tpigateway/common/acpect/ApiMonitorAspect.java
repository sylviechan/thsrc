package com.tp.tpigateway.common.acpect;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.tp.common.util.DataUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.feginError.OtherAPIFeignErrorService;

@Aspect
@Component
public class ApiMonitorAspect {

	@Autowired
	OtherAPIFeignErrorService otherAPIFeignErrorService;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	// 決定監控哪一個部分
	@Pointcut("execution(* com.tp.tpigateway.modules.cathaysec.fegin..*(..)) ")
	public void commonControllerPointCut() {
	}

	@Around("commonControllerPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {

		Map req = null;
		Map resp = null;

		Object[] args = point.getArgs();// API傳入
		if (args != null) {
			for (Object object : args) {
				if (object != null) {
					if (StringUtils.indexOf(object.getClass().getName(), "Map") > -1) {
						req = (Map) object;
					} else if (StringUtils.indexOf(object.getClass().getName(), "String") > -1) {
						String jsonSource = (String) object;
						//req = new ObjectMapper().readValue(jsonSource, HashMap.class);
						req =  JSON.parseObject(jsonSource,HashMap.class);
					}
				}
			}
		}

		Object result = point.proceed();// API傳出
		if (result != null) {
			if (StringUtils.indexOf(result.getClass().getName(), "Map") > -1) {
				resp = (Map) result;
			} else if (StringUtils.indexOf(result.getClass().getName(), "String") > -1) {
				String jsonSource = (String) result;
				//resp = new ObjectMapper().readValue(jsonSource, HashMap.class);
				resp =  JSON.parseObject(jsonSource,HashMap.class);
			}
		}
		
		//logger.info("^^^^^^^^^^^^^^^^^"+req);
		//logger.info("^^^^^^^^^^^^^^^^^"+resp);
		//logger.info("^^^^^^^^^^^^^^^^^"+PropUtils.getIsOpenApiMonitor());
		if (resp != null && resp.containsKey("ResultCode")) {
			if (!StringUtils.equals(MapUtils.getString(resp, "ResultCode"), "0000")) {
				try {
					if (PropUtils.getIsOpenApiMonitor()) {
						otherAPIFeignErrorService.sendeErrorLogRB29(DataUtils.abnormalOrder(req, resp));
					} else {
						logger.debug("##########攔截功能未啟動");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return result;
	}

}