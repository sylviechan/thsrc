package com.tp.tpigateway.common.acpect;

import com.alibaba.fastjson.JSON;
import com.tp.common.annotation.NotShowAopLog;
import com.tp.common.util.HttpContextUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
public class LogAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("within(com.tp.tpigateway.common.controller..*) || " +
            "within(com.tp.tpigateway.common.mybatis.controller..*) || " +
            "within(com.tp.tpigateway.modules.system.controller..*) || " +
            "within(com.tp.tpigateway.modules.cathaysec.controller..*) || " +
            "within(com.tp.tpigateway.modules.cathaysite.controller..*) || " +
            "within(com.tp.tpigateway.modules.cathayins.controller..*)")
    public void commonControllerPointCut() {
    }

    @Around("commonControllerPointCut()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {

        long startTimeMillis = System.currentTimeMillis(); // 記錄方法開始執行的時間
        boolean prettyFormat = true;

        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();

        // 獲取輸入參數
        Map<?, ?> inputParamMap = request.getParameterMap();
        // 獲取請求地址
        String requestPath = request.getRequestURI();
        //String args =A;
        Object[] args = pjp.getArgs();
        String argsStr;

        try {
            argsStr = JSON.toJSONString(args, prettyFormat);
        } catch (Exception e) {
            argsStr = Arrays.toString(args);
        }


        // 執行完方法的返回值：調用proceed()方法，就會觸發切入點方法執行
        Map<String, Object> outputParamMap = new HashMap<>();

        Object result;
        try {
            result = pjp.proceed();// result的值就是被攔截方法的返回值
            outputParamMap.put("result", result);
        } finally {
            long endTimeMillis = System.currentTimeMillis(); // 記錄方法執行完成的時間
            long costTime = endTimeMillis - startTimeMillis;

            try {
                MethodSignature signature = (MethodSignature) pjp.getSignature();
                Method method = signature.getMethod();
                NotShowAopLog notShowInfoLog = method.getAnnotation(NotShowAopLog.class);
                if(notShowInfoLog != null){
                    logger.debug("\n" +
                            "**********************************************************************************" + "\n" +
                            "signature : " + pjp.getSignature().toString() + "\n" +
                            "url：" + requestPath + "\n" +
                            //"result：" + JSON.toJSONString(outputParamMap, prettyFormat) + "\n" +
                            "costTime : " + costTime + "\n" +
                            "**********************************************************************************" + "\n"
                    );
                } else {
                    logger.debug("\n" +
                            "**********************************************************************************" + "\n" +
                            //"startTime：" + startTime + "\n" +
                            "signature : " + pjp.getSignature().toString() + "\n" +
                            "url：" + requestPath + "\n" +
                            "args：" +  argsStr + "\n" +
                            "param：" + JSON.toJSONString(inputParamMap, prettyFormat) + "\n" +
                            "result：" + JSON.toJSONString(outputParamMap, prettyFormat) + "\n" +
                            //"endTime : " + endTime + "\n" +
                            "costTime : " + costTime + "\n" +
                            "**********************************************************************************" + "\n"
                    );
                }
            } catch (Exception e) {
                //不處理
            }

        }

        return result;
    }

}