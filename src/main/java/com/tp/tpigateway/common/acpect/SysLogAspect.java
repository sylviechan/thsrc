package com.tp.tpigateway.common.acpect;

import com.alibaba.fastjson.JSON;
import com.tp.common.annotation.TpSysLog;
import com.tp.common.util.HttpContextUtils;
import com.tp.common.mybatis.entity.SysLog;
import com.tp.tpigateway.common.mybatis.service.ISysLogService;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
public class SysLogAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ISysLogService sysLogService;

    @Pointcut("@annotation(com.tp.common.annotation.TpSysLog) || @within(com.tp.common.annotation.TpSysLog)")
    public void logPointCut() {
    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();

        Object result = point.proceed();

        long time = System.currentTimeMillis() - beginTime;

        try {
            //保存日志
            saveSysLog(point, time, result);
        } catch (Exception e) {
            logger.error("saveSysLog error: " + e.getMessage());
        }
        return result;
    }

    private void saveSysLog(ProceedingJoinPoint joinPoint, long time, Object result) {
        String sessionId = HttpContextUtils.getSessionId();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        TpSysLog tpSysLog = method.getAnnotation(TpSysLog.class);

        if (tpSysLog == null) {
            Class<?> declaringClass = method.getDeclaringClass();
            tpSysLog = declaringClass.getAnnotation(TpSysLog.class);
        }

        if (tpSysLog != null) {
            SysLog sysLog = new SysLog();

            //请求的方法名
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = signature.getName();

            if (StringUtils.isBlank(sessionId))
                sessionId = "no sessionId";

            if (tpSysLog.showParams()) {
                String params;
                Object[] args = joinPoint.getArgs();
                try {
                    params = JSON.toJSONString(args);
                } catch (Exception e) {
                    params = Arrays.toString(args);
                }

                if (StringUtils.isNotEmpty(params) && params.length() > 1000) {
                    params = params.substring(0, 1000);
                }

                sysLog.setParams(params);

            }

            if (tpSysLog.showResult()) {
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("result", result);

                String resultStr = JSON.toJSONString(resultMap);
                if (StringUtils.isNotEmpty(resultStr) && resultStr.length() > 2000) {
                    resultStr = resultStr.substring(0, 2000);
                }

                sysLog.setResult(resultStr);
            }

            sysLog.setSessionId(sessionId);
            sysLog.setCostTime(time);
            sysLog.setMethod(className + "." + methodName + "()");
            sysLog.setRemark(tpSysLog.remark());
            sysLog.setCreateTime(LocalDateTime.now());
            sysLog.setUpdateTime(LocalDateTime.now());

            sysLogService.save(sysLog);
        }
    }
}