package com.tp.tpigateway.common.mongo.repository;

import com.tp.tpigateway.common.mongo.entity.Nodemessages;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface NodemessagesRepository extends MongoRepository<Nodemessages, String> {

    public List<Nodemessages> findBySessionOrderByRegisteredDateTimeAsc(ObjectId session);

    public List<Nodemessages> findBySessionAndRegisteredDateTimeGreaterThanEqualOrderByRegisteredDateTimeAsc(ObjectId session, Date startDateTime);

    //@Query("{'id': {$in : ?0}}")
    //public List<Nodemessages> findByIds(List<String> _ids);

}
