package com.tp.tpigateway.common.mongo.repository;


import com.tp.tpigateway.common.mongo.entity.Messagesessions;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface MessagesessionsRepository extends MongoRepository<Messagesessions, String> {

    public List<Messagesessions> findBySessionIdAndAppIdOrderByStartDateAsc(String sessiondId, String appId);

    public List<Messagesessions> findByStartDateBetweenOrderByStartDateAsc(Date startDateTime, Date endDateTime);

    public List<Messagesessions> findBySessionIdAndStartDateBetweenOrderByStartDateAsc(String sessionId, Date startDateTime, Date endDateTime);

}
